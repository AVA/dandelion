#!/bin/bash

COQBIN="coq-prover.coqc"

LBR=$'\n'
TIMES=""

for f in ./*.v;
do
    FNAME="${f/.\//}"
    FNAME="${FNAME/.tptp/}"
    RES1="$( { /usr/bin/time -f "%e" -o "/tmp/time_$FNAME.txt" "$COQBIN" -quiet -w "-all" "$f"; } )"
    TIME1="$(tail -n1 /tmp/"time_$FNAME.txt")"

    RES2="$( { /usr/bin/time -f "%e" -o "/tmp/time_$FNAME.txt" "$COQBIN" -quiet -w "-all" "$f"; } )"
    TIME2="$(tail -n1 /tmp/"time_$FNAME.txt")"

    RES3="$( { /usr/bin/time -f "%e" -o "/tmp/time_$FNAME.txt" "$COQBIN" -quiet -w "-all" "$f"; } )"
    TIME3="$(tail -n1 /tmp/"time_$FNAME.txt")"

    if [ "$(echo "$RES1" | ack "success" | wc -l)" -eq 0 ];
    then
        echo "Failure for $f$: $RES1"
        TIMES="$TIMES$FNAME, Failure, $TIME1, $TIME2, $TIME3"
    else
        TIMES="$TIMES$FNAME, success, $TIME1, $TIME2, $TIME3"
    fi

    TIMES="$TIMES$LBR"

    echo "$TIMES" > log_coq.csv
done
