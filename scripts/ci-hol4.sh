#!/bin/bash

git clone https://gitlab.mpi-sws.org/AVA/FloVer FloVer
cd FloVer/hol4/
Holmake
cd ..
export FLOVERDIR="$(pwd)"
cd ..

git clone https://github.com/CakeML/cakeml cakeml
cd cakeml
#Should work with master now
#git checkout d8dc70502a73effe17411f0d7eeaa80c1d9fab98
export CMLDIR="$(pwd)"
cd ..

Holmake -j1

cd binary
Holmake sturmChecker

