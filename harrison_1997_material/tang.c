/*
 * Finds counterexamples to Tang's claim that abs(R1 + R2) < 0.010831
 * in the range we're interested in.
 *
 * A straightforward error analysis gives an upper bound of 0.010843945
 * In practice we do much better, but not as well as Tang says.
 */

#include <math.h>

#define Int_32 ((float) 32)
#define Int_2e9 ((float) (1u<<9))

static float THRESHOLD_1;
static float Inv_L;
static float L1;
static float L2;
static float x;

float tang(float x)
{ float n, n1, n2, r1, r2;
  n = rint(x * Inv_L);
  n2 = fmod(n,Int_32);
  n1 = n - n2;
  if (fabs(n) >= Int_2e9)
    r1 = (x - n1 * L1) - n2 * L1;
  else
    r1 = x - n * L1;
  r2 = -n * L2;
  return r1 + r2;
}

int main(void)
{ int i;
  unsigned ix;
  unsigned best;
  float rval;
  float max = 0;
  *((unsigned *) &Inv_L) = 0x4238aa3bu;
  *((unsigned *) &L1) = 0x3cb17200u;
  *((unsigned *) &L2) = 0x333fbe8eu;
  *((unsigned *) &THRESHOLD_1) = 0x435c6bba;
  x = 2.047;
  printf("Searching up from %10.10f\n",x);
  for (; x < THRESHOLD_1 - 0.5;)
   { /* printf("Now at %10.10f; worst so far %10.10f\n",
           (double) x,(double) max); */
     for (i = 0; i < 100000; ++i)
      { rval = tang(x);
        ix = *((unsigned *) &x);
        if (fabs(rval) > max && x < THRESHOLD_1)
         { best = ix;
           max = fabs(rval);
           printf("x = %10.10f (bits %8x) gives R1 + R2 = %4.10f\n",
                  (double) x,*((unsigned *) &x),(double) rval);
         }
        *((unsigned *) &x) = ix + 1;
      }
   }
  return 0;
}

/******************

Tail of output from past the 0.010831 point:

x = 45.7585449219 (bits 423708c0) gives R1 + R2 = 0.0108310049
x = 46.6683006287 (bits 423aac57) gives R1 + R2 = 0.0108310375
x = 49.8957672119 (bits 42479544) gives R1 + R2 = 0.0108310608
x = 50.8055229187 (bits 424b38db) gives R1 + R2 = 0.0108310934
x = 54.0329895020 (bits 425821c8) gives R1 + R2 = 0.0108311167
x = 54.9427452087 (bits 425bc55f) gives R1 + R2 = 0.0108311493
x = 58.1702117920 (bits 4268ae4c) gives R1 + R2 = 0.0108311735
x = 59.0799674988 (bits 426c51e3) gives R1 + R2 = 0.0108312052
x = 62.3074340820 (bits 42793ad0) gives R1 + R2 = 0.0108312294
x = 63.2171897888 (bits 427cde67) gives R1 + R2 = 0.0108312611
x = 66.4446563721 (bits 4284e3aa) gives R1 + R2 = 0.0108312853
x = 70.5818786621 (bits 428d29ec) gives R1 + R2 = 0.0108313411
x = 74.7191009521 (bits 4295702e) gives R1 + R2 = 0.0108313970
x = 78.8563232422 (bits 429db670) gives R1 + R2 = 0.0108314538
x = 82.9935455322 (bits 42a5fcb2) gives R1 + R2 = 0.0108315097
x = 87.1307678223 (bits 42ae42f4) gives R1 + R2 = 0.0108315656
x = 91.2679901123 (bits 42b68936) gives R1 + R2 = 0.0108316215
x = 95.4052124023 (bits 42becf78) gives R1 + R2 = 0.0108316774
x = 99.5424346924 (bits 42c715ba) gives R1 + R2 = 0.0108317342
x = 103.6796569824 (bits 42cf5bfc) gives R1 + R2 = 0.0108317900
x = 107.8168792725 (bits 42d7a23e) gives R1 + R2 = 0.0108318459
x = 111.9541015625 (bits 42dfe880) gives R1 + R2 = 0.0108319018
x = 116.0913238525 (bits 42e82ec2) gives R1 + R2 = 0.0108319577
x = 120.2285461426 (bits 42f07504) gives R1 + R2 = 0.0108320145
x = 124.3657684326 (bits 42f8bb46) gives R1 + R2 = 0.0108320704
x = 128.5029907227 (bits 430080c4) gives R1 + R2 = 0.0108321263
x = 132.6402130127 (bits 4304a3e5) gives R1 + R2 = 0.0108321821
x = 136.7774353027 (bits 4308c706) gives R1 + R2 = 0.0108322389
x = 140.9146575928 (bits 430cea27) gives R1 + R2 = 0.0108322948
x = 145.0518798828 (bits 43110d48) gives R1 + R2 = 0.0108323507
x = 149.1891021729 (bits 43153069) gives R1 + R2 = 0.0108324066
x = 153.3263244629 (bits 4319538a) gives R1 + R2 = 0.0108324625
x = 157.4635467529 (bits 431d76ab) gives R1 + R2 = 0.0108325193
x = 161.6007690430 (bits 432199cc) gives R1 + R2 = 0.0108325751
x = 165.7379913330 (bits 4325bced) gives R1 + R2 = 0.0108326310
x = 169.8752136230 (bits 4329e00e) gives R1 + R2 = 0.0108326869
x = 174.0124359131 (bits 432e032f) gives R1 + R2 = 0.0108327428
x = 178.1496582031 (bits 43322650) gives R1 + R2 = 0.0108327996
x = 182.2868804932 (bits 43364971) gives R1 + R2 = 0.0108328555
x = 186.4241027832 (bits 433a6c92) gives R1 + R2 = 0.0108329114
x = 190.5613250732 (bits 433e8fb3) gives R1 + R2 = 0.0108329672
x = 194.6985473633 (bits 4342b2d4) gives R1 + R2 = 0.0108330231
x = 203.4711914062 (bits 434b78a0) gives R1 + R2 = 0.0108330622
x = 207.6084136963 (bits 434f9bc1) gives R1 + R2 = 0.0108331190
x = 211.7456359863 (bits 4353bee2) gives R1 + R2 = 0.0108331749
x = 215.8828582764 (bits 4357e203) gives R1 + R2 = 0.0108332308
x = 220.0200805664 (bits 435c0524) gives R1 + R2 = 0.0108332867

*******************/
