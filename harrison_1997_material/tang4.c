#include <math.h>

#define Int_32 ((float) 32)
#define Int_2e9 ((float) (1u<<9))

static float THRESHOLD_1;
static float Inv_L;
static float L1;
static float L2;
static float A1;
static float A2;

static float trail[32];
static float lead[32];

float my_fmod(float m,float n)
{ float p = fmod(m,n);
  return p < 0 ? n + fmod(m,n) : fmod(m,n);
}

float tang(float x)
{ float n, n1, n2, r1, r2, r, p, q, s, e1, e, xinvl, n1l1,n2l1,subr;
  int m, j;
  xinvl = x * Inv_L;
  n = rint(xinvl);
  n2 = my_fmod(n,Int_32);
  n1 = n - n2;
  if (fabs(n) >= Int_2e9)
    n1l1 = n1 * L1, n2l1 = n2 * L1, subr = x - n1l1, r1 = subr - n2l1;
  else
    r1 = x - n * L1;
  r2 = -n * L2;
  m = (int) (n1 / 32);
  j = (int) n2;
  r = r1 + r2;
  q = r * (r * (A1 + r * A2));
  p = r1 + (r2 + q);
  s = lead[j] + trail[j];
  e1 = lead[j] + (trail[j] + s * p);
  e = e1 * pow2(m);
  return e;
}

int main(void)
{ float e;
  int i;
  *((unsigned *) &Inv_L) = 0x4238aa3bu;
  *((unsigned *) &L1) = 0x3cb17200u;
  *((unsigned *) &L2) = 0x333fbe8eu;
  *((unsigned *) &THRESHOLD_1) = 0x435c6bba;
  *((unsigned *) &A1) = 0x3f000044;
  *((unsigned *) &A2) = 0x3e2aaaec;
  *((unsigned *) &lead[0] ) = 0x3f800000;
  *((unsigned *) &lead[1] ) = 0x3f82cd80;
  *((unsigned *) &lead[2] ) = 0x3f85aac0;
  *((unsigned *) &lead[3] ) = 0x3f889800;
  *((unsigned *) &lead[4] ) = 0x3f8b95c0;
  *((unsigned *) &lead[5] ) = 0x3f8ea400;
  *((unsigned *) &lead[6] ) = 0x3f91c3c0;
  *((unsigned *) &lead[7] ) = 0x3f94f4c0;
  *((unsigned *) &lead[8] ) = 0x3f9837c0;
  *((unsigned *) &lead[9] ) = 0x3f9b8d00;
  *((unsigned *) &lead[10] ) = 0x3f9ef500;
  *((unsigned *) &lead[11] ) = 0x3fa27040;
  *((unsigned *) &lead[12] ) = 0x3fa5fec0;
  *((unsigned *) &lead[13] ) = 0x3fa9a140;
  *((unsigned *) &lead[14] ) = 0x3fad5800;
  *((unsigned *) &lead[15] ) = 0x3fb123c0;
  *((unsigned *) &lead[16] ) = 0x3fb504c0;
  *((unsigned *) &lead[17] ) = 0x3fb8fb80;
  *((unsigned *) &lead[18] ) = 0x3fbd0880;
  *((unsigned *) &lead[19] ) = 0x3fc12c40;
  *((unsigned *) &lead[20] ) = 0x3fc56700;
  *((unsigned *) &lead[21] ) = 0x3fc9b980;
  *((unsigned *) &lead[22] ) = 0x3fce2480;
  *((unsigned *) &lead[23] ) = 0x3fd2a800;
  *((unsigned *) &lead[24] ) = 0x3fd744c0;
  *((unsigned *) &lead[25] ) = 0x3fdbfb80;
  *((unsigned *) &lead[26] ) = 0x3fe0ccc0;
  *((unsigned *) &lead[27] ) = 0x3fe5b900;
  *((unsigned *) &lead[28] ) = 0x3feac0c0;
  *((unsigned *) &lead[29] ) = 0x3fefe480;
  *((unsigned *) &lead[30] ) = 0x3ff52540;
  *((unsigned *) &lead[31] ) = 0x3ffa8380;
  *((unsigned *) &trail[0] ) = 0x00000000;
  *((unsigned *) &trail[1] ) = 0x35531585;
  *((unsigned *) &trail[2] ) = 0x34d9f312;
  *((unsigned *) &trail[3] ) = 0x35e8092e;
  *((unsigned *) &trail[4] ) = 0x3471f546;
  *((unsigned *) &trail[5] ) = 0x36e62d17;
  *((unsigned *) &trail[6] ) = 0x361b9d59;
  *((unsigned *) &trail[7] ) = 0x36bea3fc;
  *((unsigned *) &trail[8] ) = 0x36c14637;
  *((unsigned *) &trail[9] ) = 0x36e6e755;
  *((unsigned *) &trail[10] ) = 0x36c98247;
  *((unsigned *) &trail[11] ) = 0x34c0c312;
  *((unsigned *) &trail[12] ) = 0x36354d8b;
  *((unsigned *) &trail[13] ) = 0x3655a754;
  *((unsigned *) &trail[14] ) = 0x36fba90b;
  *((unsigned *) &trail[15] ) = 0x36d6074b;
  *((unsigned *) &trail[16] ) = 0x36cccfe7;
  *((unsigned *) &trail[17] ) = 0x36bd1d8c;
  *((unsigned *) &trail[18] ) = 0x368e7d60;
  *((unsigned *) &trail[19] ) = 0x35cca667;
  *((unsigned *) &trail[20] ) = 0x36a84554;
  *((unsigned *) &trail[21] ) = 0x36f619b9;
  *((unsigned *) &trail[22] ) = 0x35c151f8;
  *((unsigned *) &trail[23] ) = 0x366c8f89;
  *((unsigned *) &trail[24] ) = 0x36f32b5a;
  *((unsigned *) &trail[25] ) = 0x36de5f6c;
  *((unsigned *) &trail[26] ) = 0x36776155;
  *((unsigned *) &trail[27] ) = 0x355cef90;
  *((unsigned *) &trail[28] ) = 0x355cfba5;
  *((unsigned *) &trail[29] ) = 0x36e66f73;
  *((unsigned *) &trail[30] ) = 0x36f45492;
  *((unsigned *) &trail[31] ) = 0x36cb6dc9;

  tang(3.0);
  return 0;
}
