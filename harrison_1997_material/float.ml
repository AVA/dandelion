(* ========================================================================= *)
(* Useful properties of floating point numbers and machine integers.         *)
(* ========================================================================= *)

prioritize_num();;

let REAL_POW2_SUB = prove
 (`!a b. a <= b ==> (&2 pow (b - a) = &2 pow b / &2 pow a)`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC REAL_POW_SUB THEN
  ASM_REWRITE_TAC[REAL_OF_NUM_EQ; ARITH]);;

(* ------------------------------------------------------------------------- *)
(* We also need machine integers; we'll make them 32 bit, 2s complement.     *)
(* ------------------------------------------------------------------------- *)

let INT_TYBIJ =
  let th = prove
   (`?n. n < 2 EXP 32`,
    EXISTS_TAC `0` THEN REWRITE_TAC[EXP_LT_0] THEN REWRITE_TAC[ARITH]) in
  new_type_definition "INT" ("INT","INUM") th;;

(* ------------------------------------------------------------------------- *)
(* Fake type bijections into the reals.                                      *)
(* ------------------------------------------------------------------------- *)

let IVAL = new_definition
  `IVAL a = if INUM a < 2 EXP 31 then &(INUM a)
            else &(INUM a) - &(2 EXP 32)`;;

let ICOERCE = new_definition
  `ICOERCE x = @a. IVAL a = x`;;

let INT_DIFFERENCE_LEMMA = prove
 (`!a b. ?k. abs(&a - &b) = &k`,
  REPEAT GEN_TAC THEN
  DISJ_CASES_THEN MP_TAC (SPECL [`a:num`; `b:num`] LE_CASES) THEN
  REWRITE_TAC[LE_EXISTS] THEN DISCH_THEN(CHOOSE_THEN SUBST1_TAC) THEN
  REWRITE_TAC[GSYM REAL_OF_NUM_ADD; REAL_ADD_SUB] THEN
  REWRITE_TAC[REAL_ARITH `x:real - (x + y) = --y`] THEN
  REWRITE_TAC[REAL_ABS_NEG; REAL_ABS_NUM] THEN
  MESON_TAC[]);;

let INT_IVAL_LEMMA = prove
 (`!i. ?n. abs(IVAL i) = &n`,
  GEN_TAC THEN REWRITE_TAC[IVAL] THEN COND_CASES_TAC THEN
  REWRITE_TAC[INT_DIFFERENCE_LEMMA; REAL_ABS_NUM; REAL_OF_NUM_EQ] THEN
  MESON_TAC[]);;

let INT_real_tybij = prove
 (`(!a. ICOERCE(IVAL a) = a) /\
   (!r. (?n. abs r = &n) /\ --(&2 pow 31) <= r /\ r < &2 pow 31 <=>
        (IVAL(ICOERCE r) = r))`,
  SUBGOAL_THEN `!i. --(&2 pow 31) <= IVAL(i) /\ IVAL(i) < &2 pow 31`
  ASSUME_TAC THENL
   [GEN_TAC THEN REWRITE_TAC[IVAL] THEN
    COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
    REWRITE_TAC[REAL_LE_LNEG; REAL_OF_NUM_POW; REAL_OF_NUM_ADD; REAL_POS] THEN
    ASM_REWRITE_TAC[REAL_OF_NUM_LT] THEN
    MP_TAC (SPEC `INUM i` (CONJUNCT2 INT_TYBIJ)) THEN
    POP_ASSUM MP_TAC THEN REWRITE_TAC[GSYM REAL_OF_NUM_LT] THEN
    REWRITE_TAC[num_CONV `32`; EXP; GSYM REAL_OF_NUM_MUL] THEN
    REWRITE_TAC[REAL_ARITH `&0 <= a + (b - c) <=> c <= a + b`] THEN
    REWRITE_TAC[REAL_LT_SUB_RADD; REAL_NOT_LT] THEN
    REWRITE_TAC[CONJUNCT1 INT_TYBIJ] THEN
    MP_TAC(SPECL [`&2`; `31`] REAL_POW_LT) THEN
    REWRITE_TAC[REAL_ARITH `&0 < &2`; REAL_OF_NUM_POW] THEN
    REAL_ARITH_TAC; ALL_TAC] THEN
  SUBGOAL_THEN `!i j. (IVAL(i) = IVAL(j)) <=> (i = j)` ASSUME_TAC THENL
   [REPEAT GEN_TAC THEN EQ_TAC THENL [ALL_TAC; MESON_TAC[]] THEN
    REWRITE_TAC[IVAL] THEN REPEAT COND_CASES_TAC THEN
    REWRITE_TAC[REAL_ARITH `(x:real - z = y - z) <=> (x = y)`] THEN
    ASM_REWRITE_TAC[REAL_OF_NUM_EQ] THENL
     [MESON_TAC[INT_TYBIJ]; ALL_TAC; ALL_TAC; MESON_TAC[INT_TYBIJ]] THEN
    REWRITE_TAC[REAL_EQ_SUB_RADD; REAL_EQ_SUB_LADD] THEN
    REWRITE_TAC[REAL_OF_NUM_ADD; REAL_OF_NUM_EQ] THENL
     [DISCH_THEN(ASSUME_TAC o SYM) THEN
      MP_TAC (SPEC `INUM j` (CONJUNCT2 INT_TYBIJ)) THEN
      ASM_REWRITE_TAC[CONJUNCT1 INT_TYBIJ] THEN ARITH_TAC;
      DISCH_TAC THEN MP_TAC (SPEC `INUM i` (CONJUNCT2 INT_TYBIJ)) THEN
      ASM_REWRITE_TAC[CONJUNCT1 INT_TYBIJ] THEN ARITH_TAC]; ALL_TAC] THEN
  SUBGOAL_THEN `!x. (?n. abs(x) = &n) /\ --(&2 pow 31) <= x /\ x < &2 pow 31
                    ==> ?i. x = IVAL(i)`
  ASSUME_TAC THENL
   [GEN_TAC THEN DISCH_THEN(CONJUNCTS_THEN2 MP_TAC STRIP_ASSUME_TAC) THEN
    DISCH_THEN(X_CHOOSE_THEN `n:num` MP_TAC) THEN
    DISCH_THEN(MP_TAC o MATCH_MP (REAL_ARITH
      `(abs(x) = &n) ==> (x = &n) \/ (x = --(&n)) /\ ~(x = &0)`)) THEN
    DISCH_THEN(DISJ_CASES_THEN2 SUBST_ALL_TAC MP_TAC) THEN
    REWRITE_TAC[IVAL] THENL
     [EXISTS_TAC `INT n` THEN
      SUBGOAL_THEN `INUM(INT n) = n` SUBST_ALL_TAC THENL
       [REWRITE_TAC[GSYM INT_TYBIJ] THEN UNDISCH_TAC `&n < &2 pow 31` THEN
        REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_POW; ARITH] THEN
        REWRITE_TAC[REAL_OF_NUM_LT] THEN ARITH_TAC;
        ASM_REWRITE_TAC[GSYM REAL_OF_NUM_LT; GSYM REAL_OF_NUM_POW]];
      DISCH_THEN(CONJUNCTS_THEN2 MP_TAC ASSUME_TAC) THEN
      DISCH_THEN SUBST_ALL_TAC THEN EXISTS_TAC `INT(2 EXP 32 - n)` THEN
      UNDISCH_TAC `~(-- (&n) = &0)` THEN
      REWRITE_TAC[REAL_NEG_EQ; REAL_NEG_0; REAL_OF_NUM_EQ] THEN DISCH_TAC THEN
      SUBGOAL_THEN `INUM(INT(2 EXP 32 - n)) = 2 EXP 32 - n`
      SUBST_ALL_TAC THENL
       [REWRITE_TAC[GSYM INT_TYBIJ] THEN
        MATCH_MP_TAC(ARITH_RULE `~(a = 0) /\ ~(b = 0) ==> a - b < a`) THEN
        ASM_REWRITE_TAC[EXP_EQ_0; ARITH];
        SUBGOAL_THEN `~(2 EXP 32 - n < 2 EXP 31)` ASSUME_TAC THENL
         [REWRITE_TAC[NOT_LT] THEN UNDISCH_TAC `--(&2 pow 31) <= --(&n)` THEN
          REWRITE_TAC[REAL_LE_NEG2] THEN
          REWRITE_TAC[num_CONV `32`; EXP; REAL_OF_NUM_POW; REAL_OF_NUM_LE] THEN
          ARITH_TAC;
          ASM_REWRITE_TAC[] THEN
          UNDISCH_TAC `--(&2 pow 31) <= --(&n)` THEN
          REWRITE_TAC[REAL_LE_NEG2; REAL_OF_NUM_POW; REAL_OF_NUM_LE] THEN
          DISCH_TAC THEN SUBGOAL_THEN `n <= 2 EXP 32` MP_TAC THENL
           [MATCH_MP_TAC LE_TRANS THEN EXISTS_TAC `2 EXP 31` THEN
            ASM_REWRITE_TAC[] THEN REWRITE_TAC[ARITH];
            REWRITE_TAC[LE_EXISTS] THEN STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
            REWRITE_TAC[ADD_SUB2; GSYM REAL_OF_NUM_ADD] THEN
            REAL_ARITH_TAC]]]]; ALL_TAC] THEN
  ASM_REWRITE_TAC[ICOERCE] THEN REWRITE_TAC[GSYM ICOERCE] THEN
  GEN_TAC THEN EQ_TAC THENL [ALL_TAC;
    DISCH_THEN(SUBST1_TAC o SYM) THEN ASM_REWRITE_TAC[INT_IVAL_LEMMA]] THEN
  STRIP_TAC THEN REWRITE_TAC[ICOERCE] THEN CONV_TAC SELECT_CONV THEN
  CONV_TAC(ONCE_DEPTH_CONV SYM_CONV) THEN FIRST_ASSUM MATCH_MP_TAC THEN
  ASM_REWRITE_TAC[REAL_OF_NUM_EQ; GSYM EXISTS_REFL]);;

(* ------------------------------------------------------------------------- *)
(* Operations on machine integers.                                           *)
(* ------------------------------------------------------------------------- *)

let prioritize_Int() =
  overload_interface ("+",`INT_ADD:INT->INT->INT`);
  overload_interface ("*",`INT_MUL:INT->INT->INT`);
  overload_interface ("-",`INT_SUB:INT->INT->INT`);
  overload_interface ("/",`INT_DIV:INT->INT->INT`);
  overload_interface ("%",`Int_rem:INT->INT->INT`);
  overload_interface ("<",`INT_LT:INT->INT->bool`);
  overload_interface ("<=",`INT_LE:INT->INT->bool`);
  overload_interface (">",`INT_GT:INT->INT->bool`);
  overload_interface (">=",`INT_GE:INT->INT->bool`);
  overload_interface ("==",`INT_EQ:INT->INT->bool`);
  overload_interface ("abs",`INT_ABS:INT->INT`);
  overload_interface ("--",`INT_NEG:INT->INT`);;

prioritize_Int();;

let INT_ADD = new_definition
  `a + b = ICOERCE(IVAL a + IVAL b)`;;

let INT_SUB = new_definition
  `a - b = ICOERCE(IVAL a - IVAL b)`;;

let INT_MUL = new_definition
  `a * b = ICOERCE(IVAL a * IVAL b)`;;

let INT_DIVMOD = new_definition
  `INT_DIVMOD a b =
      @dm. &0 <= IVAL(SND dm) /\
           IVAL(SND dm) < abs(IVAL b) /\
           (IVAL a = IVAL(FST dm) * IVAL(b) + IVAL(SND dm))`;;

let INT_DIV = new_definition
  `a / b = FST(INT_DIVMOD a b)`;;

let INT_MOD = new_definition
  `a % b = SND(INT_DIVMOD a b)`;;

let INT_NEG = new_definition
  `--a = ICOERCE(--(IVAL a))`;;

let INT_LT = new_definition
  `a < b <=> IVAL a < IVAL b`;;

let INT_LE = new_definition
  `a <= b <=> IVAL a <= IVAL b`;;

let INT_GT = new_definition
  `a > b <=> IVAL a > IVAL b`;;

let INT_GE = new_definition
  `a >= b <=> IVAL a >= IVAL b`;;

let INT_EQ = new_definition
  `a == b <=> (IVAL a = IVAL b)`;;

let INT_ABS = new_definition
  `abs(a) = if a >= INT 0 then a else --a`;;

(* ------------------------------------------------------------------------- *)
(* Scaling by 2^N.                                                           *)
(* ------------------------------------------------------------------------- *)

let fscalb = new_definition
  `fscalb(X) m (a,N)  =
        if is_nan(X) a then some_nan(X)
        else if is_infinity(X) a then a
        else zerosign(X) (sign(a))
               (round(X) m (exp(IVAL N * ln(&2)) * valof(X) a))`;;

let SCALB = new_definition
  `SCALB(a,N) = float(fscalb(float_format) To_nearest (defloat a,N))`;;

(* ------------------------------------------------------------------------- *)
(* Interconversions between the types.                                       *)
(* ------------------------------------------------------------------------- *)

let TOINT = new_definition
  `TOINT a = @i. IVAL(i) = VAL(a)`;;

let TOFLOAT = new_definition
  `TOFLOAT i = @a. ISFINITE(a) /\ (VAL(a) = IVAL(i))`;;

let INTRND = new_definition
  `INTRND(a) = TOINT (ROUNDFLOAT a)`;;

(* ------------------------------------------------------------------------- *)
(* Proof of basic integer properties.                                        *)
(* ------------------------------------------------------------------------- *)

let INT_DIVMOD_LEMMA = prove
 (`!a b. (?m. abs(a) = &m) /\ (?n. abs(b) = &n) /\ ~(b = &0)
         ==> ?q r. (?p. abs(q) = &p) /\ (?s. abs(r) = &s) /\
                   &0 <= r /\ r < abs(b) /\
                   abs(q) <= abs(a) /\
                   (a = q * b + r)`,
  REPEAT GEN_TAC THEN DISCH_THEN(CONJUNCTS_THEN2 MP_TAC ASSUME_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `m:num` MP_TAC) THEN
  DISCH_THEN(MP_TAC o MATCH_MP (REAL_ARITH
    `(abs(x:real) = y) ==> (x = y) \/ (x = --y)`)) THEN
  POP_ASSUM(CONJUNCTS_THEN2 MP_TAC ASSUME_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `n:num` ASSUME_TAC) THEN
  DISCH_THEN(DISJ_CASES_THEN SUBST_ALL_TAC) THENL
   [FIRST_ASSUM(DISJ_CASES_TAC o MATCH_MP (REAL_ARITH
      `(abs(x:real) = y) ==> (x = y) \/ (x = --y)`)) THENL
     [MAP_EVERY EXISTS_TAC [`&(m DIV n)`; `&(m MOD n)`];
      MAP_EVERY EXISTS_TAC [`--(&(m DIV n))`; `&(m MOD n)`]] THEN
    (ASM_REWRITE_TAC[REAL_POS] THEN REWRITE_TAC
      [REAL_ABS_NEG; REAL_ABS_NUM; REAL_OF_NUM_LE; REAL_OF_NUM_LT] THEN
     REWRITE_TAC[REAL_MUL_RNEG; REAL_MUL_LNEG; REAL_NEG_NEG] THEN
     REWRITE_TAC[REAL_OF_NUM_ADD; REAL_OF_NUM_MUL; REAL_OF_NUM_EQ] THEN
     SUBGOAL_THEN `~(n = 0)` (fun th -> MESON_TAC[th; DIVISION; DIV_LE]) THEN
     UNDISCH_TAC `~(b = &0)` THEN ASM_REWRITE_TAC[REAL_OF_NUM_EQ] THEN
     REWRITE_TAC[REAL_ARITH `(--x = &0) <=> (x = &0)`; REAL_OF_NUM_EQ]);
    ASM_CASES_TAC `m MOD n = 0` THENL
     [FIRST_ASSUM(DISJ_CASES_TAC o MATCH_MP (REAL_ARITH
        `(abs(x:real) = y) ==> (x = y) \/ (x = --y)`)) THENL
       [MAP_EVERY EXISTS_TAC [`--(&(m DIV n))`; `&(m MOD n)`];
        MAP_EVERY EXISTS_TAC [`&(m DIV n)`; `&(m MOD n)`]] THEN
      ASM_REWRITE_TAC[REAL_POS] THEN REWRITE_TAC
       [REAL_ABS_NEG; REAL_ABS_NUM; REAL_OF_NUM_LE; REAL_OF_NUM_LT] THEN
      REWRITE_TAC[EXISTS_REFL; GSYM EXISTS_REFL; REAL_OF_NUM_EQ] THEN
      REWRITE_TAC[REAL_MUL_RNEG; REAL_MUL_LNEG; REAL_NEG_NEG] THEN
      REWRITE_TAC[REAL_OF_NUM_ADD; REAL_OF_NUM_MUL; REAL_OF_NUM_EQ] THEN
      REWRITE_TAC[REAL_ADD_RID; REAL_EQ_NEG2; REAL_OF_NUM_EQ] THEN
      (SUBGOAL_THEN `~(n = 0)` ASSUME_TAC THENL
        [UNDISCH_TAC `~(b = &0)` THEN ASM_REWRITE_TAC[REAL_OF_NUM_EQ] THEN
         REWRITE_TAC[REAL_ARITH `(--x = &0) <=> (x = &0)`; REAL_OF_NUM_EQ];
         FIRST_ASSUM(ASSUME_TAC o MATCH_MP
           (ARITH_RULE `~(p = 0) ==> 0 < p`)) THEN
         ASM_REWRITE_TAC[] THEN CONJ_TAC THENL
          [ASM_MESON_TAC[DIV_LE];
           FIRST_ASSUM(MP_TAC o SPEC `m:num` o MATCH_MP DIVISION) THEN
           ASM_REWRITE_TAC[ADD_CLAUSES]]]);
      FIRST_ASSUM(DISJ_CASES_TAC o MATCH_MP (REAL_ARITH
        `(abs(x:real) = y) ==> (x = y) \/ (x = --y)`)) THENL
       [MAP_EVERY EXISTS_TAC [`--(&(m DIV n + 1))`; `&n - &(m MOD n)`];
        MAP_EVERY EXISTS_TAC [`&(m DIV n + 1)`; `&n - &(m MOD n)`]] THEN
      ASM_REWRITE_TAC[REAL_POS] THEN
      REWRITE_TAC[REAL_ARITH `&0 <= a - b <=> b <= a`] THEN
      REWRITE_TAC[REAL_ARITH `a - b < a <=> &0 < b`] THEN
      REWRITE_TAC[REAL_ABS_NEG; REAL_ABS_NUM; INT_DIFFERENCE_LEMMA] THEN
      REWRITE_TAC[REAL_ARITH `(--x = y:real) <=> (x = --y)`] THEN
      REWRITE_TAC[REAL_MUL_LNEG; REAL_MUL_RNEG;
        REAL_NEG_NEG; REAL_NEG_ADD; REAL_NEG_SUB] THEN
      REWRITE_TAC[REAL_OF_NUM_LE; REAL_OF_NUM_LT] THEN
      REWRITE_TAC[GSYM REAL_OF_NUM_ADD; REAL_ADD_RDISTRIB] THEN
      REWRITE_TAC[REAL_MUL_LID] THEN
      REWRITE_TAC[REAL_ARITH `(a + b) + c - b = a + c:real`] THEN
      REWRITE_TAC[REAL_OF_NUM_MUL; REAL_OF_NUM_ADD; REAL_OF_NUM_EQ] THEN
      REWRITE_TAC[EXISTS_REFL; GSYM EXISTS_REFL] THEN
      (SUBGOAL_THEN `~(n = 0)` ASSUME_TAC THENL
        [UNDISCH_TAC `~(b = &0)` THEN ASM_REWRITE_TAC[REAL_OF_NUM_EQ] THEN
         REWRITE_TAC[REAL_ARITH `(--x = &0) <=> (x = &0)`; REAL_OF_NUM_EQ];
         REPEAT CONJ_TAC THENL
          [MATCH_MP_TAC LT_IMP_LE THEN ASM_MESON_TAC[DIVISION];
           MATCH_MP_TAC(ARITH_RULE `~(n = 0) ==> 0 < n`) THEN ASM_REWRITE_TAC[];
           FIRST_ASSUM(MP_TAC o MATCH_MP DIVISION) THEN
           DISCH_THEN(MP_TAC o CONJUNCT1 o SPEC `m:num`) THEN
           DISCH_THEN(fun th -> GEN_REWRITE_TAC RAND_CONV [th]) THEN
           MATCH_MP_TAC LE_TRANS THEN EXISTS_TAC `m DIV n * n + 1` THEN
           REWRITE_TAC[LE_ADD_LCANCEL; LE_ADD_RCANCEL] THEN CONJ_TAC THENL
            [MATCH_MP_TAC LE_TRANS THEN EXISTS_TAC `m DIV n * 1` THEN
             REWRITE_TAC[LE_MULT_LCANCEL] THEN
             REWRITE_TAC[MULT_CLAUSES; LE_REFL] THEN
             DISJ2_TAC THEN UNDISCH_TAC `~(n = 0)` THEN ARITH_TAC;
             UNDISCH_TAC `~(m MOD n = 0)` THEN ARITH_TAC];
           ASM_MESON_TAC[DIVISION]]])]]);;

let INT_DIVMOD_EXISTS = prove
 (`!a b. ~(IVAL(b) = &0) /\ --(&2 pow 31) < IVAL(a)
         ==> ?q r. &0 <= IVAL(r) /\ IVAL(r) < abs(IVAL b) /\
                   (IVAL a = IVAL q * IVAL b + IVAL r)`,
  let lemma = prove
   (`!i. (?n. abs(IVAL i) = &n) /\
         --(&2 pow 31) <= IVAL(i) /\
         IVAL(i) < &2 pow 31`,
    REWRITE_TAC[INT_real_tybij]) in
  REPEAT STRIP_TAC THEN
  MP_TAC(SPECL [`IVAL a`; `IVAL b`] INT_DIVMOD_LEMMA) THEN
  ASM_REWRITE_TAC[lemma] THEN
  DISCH_THEN(X_CHOOSE_THEN `q:real` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `r:real` MP_TAC) THEN
  DISCH_THEN(REPEAT_TCL CONJUNCTS_THEN ASSUME_TAC) THEN
  MAP_EVERY EXISTS_TAC [`ICOERCE q`; `ICOERCE r`] THEN
  SUBGOAL_THEN `(IVAL(ICOERCE q) = q) /\
                (IVAL(ICOERCE r) = r)`
  (fun th -> ASM_REWRITE_TAC[th]) THEN
  REWRITE_TAC[GSYM INT_real_tybij] THEN
  ASM_REWRITE_TAC[] THEN REPEAT CONJ_TAC THENL
   [MATCH_MP_TAC(REAL_ARITH `abs(q:real) <= z ==> --z <= q`) THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `abs(IVAL a)` THEN
    CONJ_TAC THENL [FIRST_ASSUM ACCEPT_TAC; ALL_TAC] THEN
    MATCH_MP_TAC (REAL_ARITH `--z <= a /\ a < z ==> abs(a:real) <= z`) THEN
    REWRITE_TAC[lemma];
    MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `abs(q:real)` THEN
    REWRITE_TAC[REAL_ABS_LE] THEN
    MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `abs(IVAL a)` THEN
    CONJ_TAC THENL [FIRST_ASSUM ACCEPT_TAC; ALL_TAC] THEN
    MATCH_MP_TAC (REAL_ARITH `--z < a /\ a < z ==> abs(a:real) < z`) THEN
    REWRITE_TAC[lemma] THEN FIRST_ASSUM ACCEPT_TAC;
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&0` THEN
    ASM_REWRITE_TAC[] THEN CONV_TAC REAL_RAT_REDUCE_CONV;
    MATCH_MP_TAC REAL_LTE_TRANS THEN EXISTS_TAC `abs(IVAL b)` THEN
    ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC (REAL_ARITH `--z <= a /\ a < z ==> abs(a:real) <= z`) THEN
    REWRITE_TAC[lemma]]);;

let INT_DIVMOD = prove
 (`!a b. ~(IVAL(b) = &0) /\ --(&2 pow 31) < IVAL(a)
         ==> &0 <= IVAL(a % b) /\ IVAL(a % b) < abs(IVAL b) /\
             (IVAL a = IVAL(a / b) * IVAL b + IVAL(a % b))`,
  REPEAT GEN_TAC THEN DISCH_TAC THEN
  REWRITE_TAC[INT_DIV; INT_MOD; INT_DIVMOD] THEN CONV_TAC SELECT_CONV THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP INT_DIVMOD_EXISTS) THEN
  DISCH_THEN(X_CHOOSE_THEN `q:INT` (X_CHOOSE_TAC `r:INT`)) THEN
  EXISTS_TAC `(q:INT,r:INT)` THEN ASM_REWRITE_TAC[]);;

let IVAL_INT_LEMMA = prove
 (`!n. n < 2 EXP 31 ==> (IVAL(INT n) = &n)`,
  GEN_TAC THEN DISCH_TAC THEN REWRITE_TAC[IVAL] THEN
  SUBGOAL_THEN `INUM(INT n) = n` (fun th -> ASM_REWRITE_TAC[th]) THEN
  REWRITE_TAC[GSYM INT_TYBIJ] THEN POP_ASSUM MP_TAC THEN
  REWRITE_TAC[ARITH] THEN ARITH_TAC);;

let INT_NEG = prove
 (`!a. ~(IVAL(a) = --(&2 pow 31)) ==> (IVAL(--a) = --(IVAL(a)))`,
  REPEAT STRIP_TAC THEN REWRITE_TAC[INT_NEG] THEN
  REWRITE_TAC[GSYM (CONJUNCT2 INT_real_tybij)] THEN
  ONCE_REWRITE_TAC[GSYM REAL_LE_NEG2; GSYM REAL_LT_NEG2] THEN
  REWRITE_TAC[REAL_NEG_NEG; REAL_ABS_NEG] THEN
  MP_TAC(SPEC `IVAL a` (CONJUNCT2 INT_real_tybij)) THEN
  REWRITE_TAC[CONJUNCT1 INT_real_tybij] THEN
  DISCH_TAC THEN ASM_REWRITE_TAC[] THEN CONJ_TAC THENL
   [MATCH_MP_TAC REAL_LT_IMP_LE THEN ASM_REWRITE_TAC[];
    ASM_REWRITE_TAC[REAL_LT_LE]]);;

let INT_ABS = prove
 (`!a. ~(IVAL(a) = --(&2 pow 31)) ==> (IVAL(abs a) = abs(IVAL(a)))`,
  REPEAT STRIP_TAC THEN REWRITE_TAC[INT_ABS; INT_GE] THEN
  SUBGOAL_THEN `IVAL(INT 0) = &0` SUBST1_TAC THENL
   [MATCH_MP_TAC IVAL_INT_LEMMA THEN REWRITE_TAC[ARITH];
    REWRITE_TAC[real_ge] THEN COND_CASES_TAC THEN
    ASM_REWRITE_TAC[real_abs] THEN
    MATCH_MP_TAC INT_NEG THEN ASM_REWRITE_TAC[]]);;

(* ------------------------------------------------------------------------- *)
(* A few rules for valuations, classifications etc.                          *)
(* ------------------------------------------------------------------------- *)

let VAL_FLOAT_CONV =
  let pth = prove
   (`is_valid(8,23) a ==> (VAL(float a) = valof(8,23) a)`,
    DISCH_TAC THEN REWRITE_TAC[VAL; float_tybij] THEN
    ASM_MESON_TAC[float_format; float_tybij]) in
  let match_pth = PART_MATCH (lhs o rand) pth in
  let simp_rule = PURE_REWRITE_RULE[is_valid; valof; fracwidth; expwidth] in
  let cond_rule = GEN_REWRITE_RULE RAND_CONV [COND_CLAUSES] in
  let bias_conv =
    GEN_REWRITE_CONV I [bias] THENC
    GEN_REWRITE_CONV (LAND_CONV o RAND_CONV o LAND_CONV) [expwidth] THENC
    NUM_REDUCE_CONV in
  fun tm ->
    let th1 = match_pth tm in
    let th2 = simp_rule th1 in
    let th3 = MP th2 (EQT_ELIM(NUM_REDUCE_CONV(lhand(concl th2)))) in
    let th4 = CONV_RULE (RAND_CONV (RATOR_CONV (LAND_CONV NUM_EQ_CONV))) th3 in
    let th5 = cond_rule th4 in
    let th6 =
      CONV_RULE (RAND_CONV(RAND_CONV(LAND_CONV(RAND_CONV
       (RAND_CONV bias_conv))))) th5 in
    CONV_RULE(RAND_CONV REAL_RAT_REDUCE_CONV) th6;;

(* ------------------------------------------------------------------------- *)
(* Useful lemmas.                                                            *)
(* ------------------------------------------------------------------------- *)

let SIGN = prove
 (`!a. sign(a) = FST a`,
  GEN_TAC THEN SUBST1_TAC(SYM(REWRITE_CONV[PAIR]
    `FST(a):num,FST(SND(a)):num,SND(SND(a)):num`)) THEN
  PURE_REWRITE_TAC[sign] THEN REWRITE_TAC[]);;

let EXPONENT = prove
 (`!a. exponent(a) = FST(SND a)`,
  GEN_TAC THEN SUBST1_TAC(SYM(REWRITE_CONV[PAIR]
    `FST(a):num,FST(SND(a)):num,SND(SND(a)):num`)) THEN
  PURE_REWRITE_TAC[exponent] THEN REWRITE_TAC[]);;

let FRACTION = prove
 (`!a. fraction(a) = SND(SND a)`,
  GEN_TAC THEN SUBST1_TAC(SYM(REWRITE_CONV[PAIR]
    `FST(a):num,FST(SND(a)):num,SND(SND(a)):num`)) THEN
  PURE_REWRITE_TAC[fraction] THEN REWRITE_TAC[]);;

let IS_VALID = prove
 (`!X a. is_valid(X) a <=>
         sign a < 2 /\
         exponent a < 2 EXP (expwidth X) /\
         fraction a < 2 EXP (fracwidth X)`,
  REPEAT GEN_TAC THEN
  SUBST1_TAC(SYM(REWRITE_CONV[PAIR]
  `FST(a):num,FST(SND(a)):num,SND(SND(a)):num`)) THEN
  PURE_REWRITE_TAC[is_valid; sign; exponent; fraction] THEN
  REWRITE_TAC[]);;

let VALOF = prove
 (`!X a. valof X a =
           if exponent(a) = 0
           then --(&1) pow sign(a) * &2 / &2 pow bias(X) *
                &(fraction(a)) / &2 pow fracwidth(X)
           else --(&1) pow sign(a) *
                &2 pow exponent(a) / &2 pow bias(X) *
                (&1 + &(fraction(a)) / &2 pow fracwidth(X))`,
  REPEAT GEN_TAC THEN
  SUBST1_TAC(SYM(REWRITE_CONV[PAIR]
  `FST(a):num,FST(SND(a)):num,SND(SND(a)):num`)) THEN
  PURE_REWRITE_TAC[valof; sign; exponent; fraction] THEN
  REWRITE_TAC[]);;

let IS_VALID_DEFLOAT = prove
 (`!a. is_valid(float_format) (defloat a)`,
  REWRITE_TAC[float_tybij]);;

let IS_FINITE_ALT = prove
 (`!a. is_finite(float_format) a <=>
         is_valid(float_format) a /\ exponent(a) < 255`,
  REPEAT GEN_TAC THEN
  REWRITE_TAC[is_finite; is_normal; is_denormal; is_zero; IS_VALID] THEN
  REWRITE_TAC[float_format; emax; expwidth; fracwidth] THEN
  REWRITE_TAC[ARITH] THEN
  REWRITE_TAC[GSYM CONJ_ASSOC] THEN AP_TERM_TAC THEN
  ASM_CASES_TAC `exponent a = 0` THEN ASM_REWRITE_TAC[ARITH] THENL
   [CONV_TAC TAUT; POP_ASSUM MP_TAC THEN ARITH_TAC]);;

let IS_FINITE_EXPLICIT = prove
 (`!a. is_finite(float_format) a <=> sign(a) < 2 /\ exponent(a) < 255 /\
                                     fraction(a) < 8388608`,
  GEN_TAC THEN REWRITE_TAC[IS_FINITE_ALT; IS_VALID] THEN
  REWRITE_TAC[expwidth; fracwidth; float_format; ARITH] THEN
  ARITH_TAC);;

let FLOAT_CASES = prove
 (`!a. ISNAN(a) \/ INFINITY(a) \/ ISNORMAL(a) \/ ISDENORMAL(a) \/ ISZERO(a)`,
  GEN_TAC THEN
  REWRITE_TAC[ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
  REWRITE_TAC[is_nan; is_infinity; is_normal; is_denormal; is_zero] THEN
  MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN REWRITE_TAC[IS_VALID] THEN
  REWRITE_TAC[emax] THEN
  SUBGOAL_THEN `!n p. n < 2 EXP p <=> n < (2 EXP p - 1) \/ (n = 2 EXP p - 1)`
  (fun th -> REWRITE_TAC[th]) THENL
   [REWRITE_TAC[GSYM LE_LT] THEN
    REWRITE_TAC[GSYM LT_SUC_LE] THEN
    REPEAT GEN_TAC THEN AP_TERM_TAC THEN
    REWRITE_TAC[num_CONV `1`; SUB] THEN
    MP_TAC(SPECL [`2`; `p:num`] EXP_EQ_0) THEN
    REWRITE_TAC[ARITH] THEN
    SPEC_TAC(`2 EXP p`,`k:num`) THEN ARITH_TAC;
    STRIP_TAC THEN ASM_REWRITE_TAC[] THENL
     [ASM_CASES_TAC `fraction (defloat a) = 0` THEN ASM_REWRITE_TAC[] THEN
      DISJ2_TAC THEN ARITH_TAC;
      DISJ2_TAC THEN DISJ2_TAC THEN
      ASM_CASES_TAC `2 EXP fracwidth(float_format) - 1 = 0` THEN
      ASM_REWRITE_TAC[] THEN ARITH_TAC;
      ASM_CASES_TAC `fraction (defloat a) = 0` THEN ASM_REWRITE_TAC[];
      ASM_CASES_TAC `2 EXP fracwidth(float_format) - 1 = 0` THEN
      ASM_REWRITE_TAC[]]]);;

let FLOAT_CASES_FINITE = prove
 (`!a. ISNAN(a) \/ INFINITY(a) \/ ISFINITE(a)`,
  MESON_TAC[FLOAT_CASES; ISFINITE]);;

let FLOAT_DISTINCT = prove
 (`!a. ~(ISNAN(a) /\ INFINITY(a)) /\
       ~(ISNAN(a) /\ ISNORMAL(a)) /\
       ~(ISNAN(a) /\ ISDENORMAL(a)) /\
       ~(ISNAN(a) /\ ISZERO(a)) /\
       ~(INFINITY(a) /\ ISNORMAL(a)) /\
       ~(INFINITY(a) /\ ISDENORMAL(a)) /\
       ~(INFINITY(a) /\ ISZERO(a)) /\
       ~(ISNORMAL(a) /\ ISDENORMAL(a)) /\
       ~(ISNORMAL(a) /\ ISZERO(a)) /\
       ~(ISDENORMAL(a) /\ ISZERO(a))`,
  GEN_TAC THEN
  REWRITE_TAC[ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
  REWRITE_TAC[float_format] THEN
  REWRITE_TAC[is_nan; is_infinity; is_normal; is_denormal; is_zero] THEN
  REWRITE_TAC[emax; expwidth] THEN
  REWRITE_TAC[ARITH] THEN
  REPEAT CONJ_TAC THEN ARITH_TAC);;

let FLOAT_DISTINCT_FINITE = prove
 (`!a. ~(ISNAN(a) /\ INFINITY(a)) /\
       ~(ISNAN(a) /\ ISFINITE(a)) /\
       ~(INFINITY(a) /\ ISFINITE(a))`,
  MESON_TAC[FLOAT_DISTINCT; ISFINITE]);;

let FLOAT_INFINITIES_SIGNED = prove
 (`(sign (defloat PLUS_INFINITY) = 0) /\
   (sign (defloat MINUS_INFINITY) = 1)`,
  REWRITE_TAC[PLUS_INFINITY; MINUS_INFINITY] THEN
  SUBGOAL_THEN
   `(defloat(float(plus_infinity float_format)) =
     plus_infinity float_format) /\
    (defloat(float(minus_infinity float_format)) =
     minus_infinity float_format)`
  (fun th -> REWRITE_TAC[th]) THENL
   [REWRITE_TAC[GSYM float_tybij] THEN
    REWRITE_TAC[is_valid; plus_infinity; minus_infinity; float_format] THEN
    REWRITE_TAC[emax; fracwidth; expwidth] THEN REWRITE_TAC[ARITH];
    REWRITE_TAC[sign; plus_infinity; minus_infinity]]);;

let INFINITY_IS_INFINITY = prove
 (`INFINITY(PLUS_INFINITY) /\ INFINITY(MINUS_INFINITY)`,
  REWRITE_TAC[INFINITY; PLUS_INFINITY; MINUS_INFINITY] THEN
  SUBGOAL_THEN
   `(defloat(float(plus_infinity float_format)) =
     plus_infinity float_format) /\
    (defloat(float(minus_infinity float_format)) =
     minus_infinity float_format)`
  (fun th -> REWRITE_TAC[th]) THENL
   [REWRITE_TAC[GSYM float_tybij] THEN
    REWRITE_TAC[is_valid; plus_infinity; minus_infinity; float_format] THEN
    REWRITE_TAC[emax; fracwidth; expwidth] THEN REWRITE_TAC[ARITH];
    REWRITE_TAC[is_infinity; plus_infinity; minus_infinity] THEN
    REWRITE_TAC[exponent; fraction]]);;

let ZERO_IS_ZERO = prove
 (`ISZERO(PLUS_ZERO) /\ ISZERO(MINUS_ZERO)`,
  REWRITE_TAC[ISZERO; PLUS_ZERO; MINUS_ZERO] THEN
  SUBGOAL_THEN
   `(defloat(float(plus_zero float_format)) =
     plus_zero float_format) /\
    (defloat(float(minus_zero float_format)) =
     minus_zero float_format)`
  (fun th -> REWRITE_TAC[th]) THENL
   [REWRITE_TAC[GSYM float_tybij] THEN
    REWRITE_TAC[is_valid; plus_zero; minus_zero; float_format] THEN
    REWRITE_TAC[emax; fracwidth; expwidth] THEN REWRITE_TAC[ARITH];
    REWRITE_TAC[is_zero; plus_zero; minus_zero] THEN
    REWRITE_TAC[exponent; fraction]]);;

let INFINITY_NOT_NAN = prove
 (`~(ISNAN(PLUS_INFINITY)) /\
   ~(ISNAN(MINUS_INFINITY))`,
  MESON_TAC[INFINITY_IS_INFINITY; FLOAT_DISTINCT_FINITE]);;

let ZERO_NOT_NAN = prove
 (`~(ISNAN(PLUS_ZERO)) /\
   ~(ISNAN(MINUS_ZERO))`,
  MESON_TAC[ZERO_IS_ZERO; FLOAT_DISTINCT]);;

let FLOAT_INFINITIES = prove
 (`!a. INFINITY(a) <=> (a == PLUS_INFINITY \/ a == MINUS_INFINITY)`,
  GEN_TAC THEN
  REPEAT_TCL DISJ_CASES_THEN ASSUME_TAC
  (SPEC `a:float` FLOAT_CASES_FINITE) THENL
   [SUBGOAL_THEN `~(INFINITY(a))` MP_TAC THENL
     [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE]; ALL_TAC] THEN
    UNDISCH_TAC `ISNAN(a)` THEN
    REWRITE_TAC[ISNAN; INFINITY] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[fcompare; feq; eq_f] THEN
    REWRITE_TAC[prove_constructors_distinct ccode_RECURSION];
    ASM_REWRITE_TAC[] THEN
    SUBGOAL_THEN `~(ISNAN(a))` MP_TAC THENL
     [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE]; ALL_TAC] THEN
    UNDISCH_TAC `INFINITY(a)` THEN
    REWRITE_TAC[ISNAN; INFINITY] THEN
    REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[fcompare; feq; eq_f] THEN
    REWRITE_TAC[REWRITE_RULE[ISNAN] INFINITY_NOT_NAN] THEN
    REWRITE_TAC[REWRITE_RULE[INFINITY] INFINITY_IS_INFINITY] THEN
    REWRITE_TAC[FLOAT_INFINITIES_SIGNED; ARITH] THEN
    REPEAT (COND_CASES_TAC THEN REWRITE_TAC[]) THEN
    MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
    REWRITE_TAC[IS_VALID] THEN DISCH_THEN(MP_TAC o CONJUNCT1) THEN
    REWRITE_TAC[num_CONV `2`; num_CONV `1`; LT_SUC_LE; LE] THEN
    ASM_REWRITE_TAC[SYM(num_CONV `1`)];
    SUBGOAL_THEN `~(INFINITY(a)) /\ ~(ISNAN(a))` MP_TAC THENL
     [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE]; ALL_TAC] THEN
    REWRITE_TAC[INFINITY; ISNAN] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[fcompare; eq_f; feq] THEN
    REWRITE_TAC[REWRITE_RULE[ISNAN] INFINITY_NOT_NAN] THEN
    REWRITE_TAC[REWRITE_RULE[INFINITY] INFINITY_IS_INFINITY] THEN
    REWRITE_TAC[FLOAT_INFINITIES_SIGNED; ARITH] THEN
    REWRITE_TAC[prove_constructors_distinct ccode_RECURSION]]);;

let FLOAT_INFINITES_DISTINCT = prove
 (`!a. ~(a == PLUS_INFINITY /\ a == MINUS_INFINITY)`,
  let lemma = prove
   (`((if b then x else y) = z) <=> if b then x = z else y = z`,
    BOOL_CASES_TAC `b:bool` THEN REWRITE_TAC[]) in
  GEN_TAC THEN REWRITE_TAC[PLUS_INFINITY; MINUS_INFINITY] THEN
  REWRITE_TAC[feq; eq_f; fcompare] THEN REWRITE_TAC[lemma] THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  REWRITE_TAC[REWRITE_RULE[PLUS_INFINITY; MINUS_INFINITY]
              FLOAT_INFINITIES_SIGNED; ARITH] THEN
  REWRITE_TAC[REWRITE_RULE[ISNAN; PLUS_INFINITY; MINUS_INFINITY]
              INFINITY_NOT_NAN] THEN
  REWRITE_TAC[REWRITE_RULE[INFINITY; PLUS_INFINITY; MINUS_INFINITY]
              INFINITY_IS_INFINITY] THEN
  REPEAT(COND_CASES_TAC THEN ASM_REWRITE_TAC[ARITH]));;

(* ------------------------------------------------------------------------- *)
(* Lifting of the nonexceptional comparison operations.                      *)
(* ------------------------------------------------------------------------- *)

let FLOAT_LIFT_TAC =
  REPEAT STRIP_TAC THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a)) /\
                ~(ISNAN(b)) /\ ~(INFINITY(b))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISFINITE; ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC];;

let FLOAT_LT = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) ==> (a < b <=> VAL(a) < VAL(b))`,
  FLOAT_LIFT_TAC THEN
  ASM_REWRITE_TAC[lt_f; flt; fcompare; VAL] THEN
  REPEAT COND_CASES_TAC THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION]);;

let FLOAT_GT = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) ==> (a > b <=> VAL(a) > VAL(b))`,
  FLOAT_LIFT_TAC THEN
  ASM_REWRITE_TAC[gt_f; fgt; fcompare; VAL] THEN
  REPEAT COND_CASES_TAC THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  ASM_REWRITE_TAC[real_gt; REAL_LT_REFL] THEN
  ASM_MESON_TAC[REAL_LT_ANTISYM; REAL_LT_TOTAL]);;

let FLOAT_LE = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) ==> (a <= b <=> VAL(a) <= VAL(b))`,
  FLOAT_LIFT_TAC THEN
  ASM_REWRITE_TAC[le_f; fle; fcompare; VAL] THEN
  REPEAT COND_CASES_TAC THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  REWRITE_TAC[GSYM REAL_NOT_LT] THEN
  ASM_MESON_TAC[REAL_LT_ANTISYM; REAL_LT_TOTAL]);;

let FLOAT_GE = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) ==> (a >= b <=> VAL(a) >= VAL(b))`,
  FLOAT_LIFT_TAC THEN
  ASM_REWRITE_TAC[ge_f; fge; fcompare; VAL] THEN
  REPEAT COND_CASES_TAC THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  REWRITE_TAC[real_ge] THEN
  REWRITE_TAC[GSYM REAL_NOT_LT] THEN
  ASM_MESON_TAC[REAL_LT_ANTISYM; REAL_LT_TOTAL]);;

let FLOAT_EQ = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) ==> (a == b <=> (VAL(a) = VAL(b)))`,
  FLOAT_LIFT_TAC THEN
  ASM_REWRITE_TAC[eq_f; feq; fcompare; VAL] THEN
  REPEAT COND_CASES_TAC THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  ASM_MESON_TAC[REAL_LT_REFL]);;

let FLOAT_EQ_REFL = prove
 (`!a. (a == a) <=> ~(ISNAN(a))`,
  GEN_TAC THEN REWRITE_TAC[eq_f; feq; fcompare; ISNAN] THEN
  REPEAT(COND_CASES_TAC THEN
         ASM_REWRITE_TAC[prove_constructors_distinct ccode_RECURSION]) THEN
  ASM_MESON_TAC[REAL_LT_REFL]);;

(* ------------------------------------------------------------------------- *)
(* Various lemmas.                                                           *)
(* ------------------------------------------------------------------------- *)

let IS_VALID_SPECIAL = prove
 (`!X. is_valid(X) (minus_infinity(X)) /\
       is_valid(X) (plus_infinity(X)) /\
       is_valid(X) (topfloat(X)) /\
       is_valid(X) (bottomfloat(X)) /\
       is_valid(X) (plus_zero(X)) /\
       is_valid(X) (minus_zero(X))`,
  GEN_TAC THEN REWRITE_TAC[is_valid; minus_infinity; plus_infinity;
    plus_zero; minus_zero; topfloat; bottomfloat; emax] THEN
  REWRITE_TAC[ARITH; EXP_LT_0] THEN
  SUBGOAL_THEN `!n. 0 < 2 EXP n` ASSUME_TAC THENL
   [REWRITE_TAC[GSYM NOT_LE; LE; EXP_EQ_0; ARITH]; ALL_TAC] THEN
  SUBGOAL_THEN `!n. 2 EXP n - 1 < 2 EXP n` ASSUME_TAC THENL
   [X_GEN_TAC `n:num` THEN POP_ASSUM(MP_TAC o SPEC `n:num`) THEN ARITH_TAC;
    ASM_REWRITE_TAC[ARITH] THEN MATCH_MP_TAC LET_TRANS THEN
    EXISTS_TAC `2 EXP (expwidth(X)) - 1` THEN
    ASM_REWRITE_TAC[] THEN ARITH_TAC]);;

let IS_CLOSEST_EXISTS = prove
 (`!v x (s:A->bool). FINITE(s) ==> ~(s = EMPTY) ==> ?a. is_closest v s x a`,
  GEN_TAC THEN GEN_TAC THEN
  MATCH_MP_TAC FINITE_INDUCT THEN REWRITE_TAC[NOT_INSERT_EMPTY] THEN
  X_GEN_TAC `a:A` THEN X_GEN_TAC `s:A->bool` THEN
  ASM_CASES_TAC `s:A->bool = EMPTY` THEN ASM_REWRITE_TAC[] THENL
   [EXISTS_TAC `a:A` THEN REWRITE_TAC[is_closest] THEN
    REWRITE_TAC[IN_INSERT; NOT_IN_EMPTY] THEN
    REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[REAL_LE_REFL];
    DISCH_THEN(X_CHOOSE_TAC `b:A`) THEN
    ASM_CASES_TAC `abs ((v:A->real) a - x) <= abs (v b - x)` THENL
     [EXISTS_TAC `a:A` THEN UNDISCH_TAC `is_closest v s x (b:A)` THEN
      REWRITE_TAC[is_closest] THEN REWRITE_TAC[IN_INSERT] THEN
      REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[REAL_LE_REFL] THEN
      MATCH_MP_TAC REAL_LE_TRANS THEN
      EXISTS_TAC `abs((v:A->real) b - x)` THEN
      ASM_REWRITE_TAC[] THEN FIRST_ASSUM MATCH_MP_TAC THEN
      ASM_REWRITE_TAC[];
      EXISTS_TAC `b:A` THEN UNDISCH_TAC `is_closest v s x (b:A)` THEN
      REWRITE_TAC[is_closest] THEN REWRITE_TAC[IN_INSERT] THEN
      REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[REAL_LE_REFL] THENL
       [ASM_MESON_TAC[REAL_LE_TOTAL];
        FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[]]]]);;

let CLOSEST_IS_EVERYTHING = prove
 (`!(v:A->real) p x s.
        FINITE(s) ==> ~(s = EMPTY) ==>
            is_closest v s x (closest v p s x) /\
            ((?b. is_closest v s x b /\ p b) ==> p (closest v p s x))`,
  REPEAT GEN_TAC THEN REPEAT DISCH_TAC THEN
  REWRITE_TAC[closest] THEN CONV_TAC SELECT_CONV THEN
  ASM_CASES_TAC `?a. is_closest v s x (a:A) /\ p a` THENL
   [POP_ASSUM(X_CHOOSE_TAC `a:A`) THEN EXISTS_TAC `a:A`;
    ASM_REWRITE_TAC[] THEN
    FIRST_ASSUM(MATCH_MP_TAC o MATCH_MP IS_CLOSEST_EXISTS)] THEN
  ASM_REWRITE_TAC[]);;

let CLOSEST_IN_SET = prove
 (`!(v:A->real) p x s. FINITE(s) ==> ~(s = EMPTY) ==> (closest v p s x) IN s`,
  REPEAT GEN_TAC THEN REPEAT DISCH_TAC THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP CLOSEST_IS_EVERYTHING) THEN
  ASM_MESON_TAC[is_closest]);;

let CLOSEST_IS_CLOSEST = prove
 (`!(v:A->real) p x s. FINITE(s) ==> ~(s = EMPTY) ==>
      is_closest v s x (closest v p s x)`,
  REPEAT GEN_TAC THEN REPEAT DISCH_TAC THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP CLOSEST_IS_EVERYTHING) THEN
  ASM_MESON_TAC[is_closest]);;

let FINITE_R3 = prove
 (`!m n p. FINITE
           {a:num#num#num | FST(a) < m /\ FST(SND a) < n /\ SND(SND a) < p}`,
  let lemma1 = prove
   (`({x | FST x < 0 /\ P x} = EMPTY) /\
     ({x | FST x < SUC n /\ P x} =
      {x | (FST x = n) /\ P x} UNION {x | FST x < n /\ P x})`,
    PURE_REWRITE_TAC[EXTENSION; IN_UNION] THEN
    REWRITE_TAC[IN_ELIM_THM; LT; NOT_IN_EMPTY] THEN
    MESON_TAC[]) in
  let lemma2 = prove
   (`({x | P x /\ f x < 0 /\ Q x} = EMPTY) /\
     ({x | P x /\ f x < SUC n /\ Q x} =
      {x | P x /\ (f x = n) /\ Q x} UNION {x | P x /\ f x < n /\ Q x})`,
    PURE_REWRITE_TAC[EXTENSION; IN_UNION] THEN
    REWRITE_TAC[IN_ELIM_THM; LT; NOT_IN_EMPTY] THEN
    MESON_TAC[]) in
  let lemma3 = prove
   (`({x | P x /\ Q x /\ f x < 0} = EMPTY) /\
     ({x | P x /\ Q x /\ f x < SUC n} =
      {x | P x /\ Q x /\ (f x = n)} UNION {x | P x /\ Q x /\ f x < n})`,
    PURE_REWRITE_TAC[EXTENSION; IN_UNION] THEN
    REWRITE_TAC[IN_ELIM_THM; LT; NOT_IN_EMPTY] THEN
    MESON_TAC[]) in
  let lemma4 = prove
   (`{a:num#num#num | (FST(a) = m) /\ (FST(SND a) = n) /\ (SND(SND a) = p)} =
     (m,n,p) INSERT {}`,
    REWRITE_TAC[EXTENSION; IN_INSERT; NOT_IN_EMPTY] THEN
    X_GEN_TAC `a:num#num#num` THEN
    REWRITE_TAC[IN_ELIM_THM] THEN
    SUBST1_TAC(SYM(REWRITE_CONV[PAIR]
      `FST(a):num,FST(SND(a)):num,SND(SND(a)):num`)) THEN
    PURE_REWRITE_TAC[FST; SND] THEN
    REWRITE_TAC[PAIR_EQ]) in
  INDUCT_TAC THEN ASM_REWRITE_TAC[lemma1; FINITE_RULES] THEN
  REPEAT GEN_TAC THEN MATCH_MP_TAC FINITE_UNION_IMP THEN
  ASM_REWRITE_TAC[] THEN SPEC_TAC(`p:num`,`p:num`) THEN
  SPEC_TAC(`n:num`,`n:num`) THEN INDUCT_TAC THEN
  ASM_REWRITE_TAC[lemma2; FINITE_RULES] THEN
  REPEAT GEN_TAC THEN MATCH_MP_TAC FINITE_UNION_IMP THEN
  ASM_REWRITE_TAC[] THEN SPEC_TAC(`p:num`,`p:num`) THEN
  INDUCT_TAC THEN ASM_REWRITE_TAC[lemma3; FINITE_RULES] THEN
  REPEAT GEN_TAC THEN MATCH_MP_TAC FINITE_UNION_IMP THEN
  ASM_REWRITE_TAC[lemma4; FINITE_INSERT; FINITE_RULES]);;

let IS_VALID_FINITE = prove
 (`FINITE {a | is_valid(X) a}`,
  REWRITE_TAC[IS_VALID; SIGN; EXPONENT; FRACTION; FINITE_R3]);;

let IS_FINITE_FINITE = prove
 (`!X. FINITE {a | is_finite(X) a}`,
  GEN_TAC THEN MATCH_MP_TAC FINITE_SUBSET THEN
  EXISTS_TAC `{a | is_valid(X) a}` THEN
  REWRITE_TAC[IS_VALID_FINITE] THEN
  REWRITE_TAC[SUBSET; IN_ELIM_THM] THEN
  REWRITE_TAC[is_finite] THEN MESON_TAC[]);;

let IS_VALID_NONEMPTY = prove
 (`~({a | is_valid(X) a} = EMPTY)`,
  REWRITE_TAC[EXTENSION; NOT_FORALL_THM] THEN
  EXISTS_TAC `0,0,0` THEN REWRITE_TAC[NOT_IN_EMPTY] THEN
  REWRITE_TAC[IN_ELIM_THM] THEN
  REWRITE_TAC[is_valid; is_finite; is_zero; exponent; fraction] THEN
  REWRITE_TAC[GSYM NOT_LE; LE; EXP_EQ_0; ARITH]);;

let IS_FINITE_NONEMPTY = prove
 (`~({a | is_finite(X) a} = EMPTY)`,
  REWRITE_TAC[EXTENSION; NOT_FORALL_THM] THEN
  EXISTS_TAC `0,0,0` THEN REWRITE_TAC[NOT_IN_EMPTY] THEN
  REWRITE_TAC[IN_ELIM_THM] THEN
  REWRITE_TAC[is_valid; is_finite; is_zero; exponent; fraction] THEN
  REWRITE_TAC[GSYM NOT_LE; LE; EXP_EQ_0; ARITH]);;

let IS_FINITE_CLOSEST = prove
 (`!X P v p x. is_finite(X) (closest v p {a | is_finite(X) a} x)`,
  REPEAT GEN_TAC THEN
  SUBGOAL_THEN `(closest v p {a | is_finite(X) a} x) IN {a | is_finite(X) a}`
  MP_TAC THENL
   [MATCH_MP_TAC
     (REWRITE_RULE[IMP_IMP] CLOSEST_IN_SET) THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC FINITE_SUBSET THEN
      EXISTS_TAC `{a | is_valid(X) a}` THEN
      REWRITE_TAC[IS_VALID_FINITE] THEN
      REWRITE_TAC[SUBSET; IN_ELIM_THM; is_finite] THEN MESON_TAC[];
      REWRITE_TAC[EXTENSION; NOT_FORALL_THM] THEN
      EXISTS_TAC `0,0,0` THEN REWRITE_TAC[NOT_IN_EMPTY] THEN
      REWRITE_TAC[IN_ELIM_THM] THEN
      REWRITE_TAC[is_valid; is_finite; is_zero; exponent; fraction] THEN
      REWRITE_TAC[GSYM NOT_LE; LE; EXP_EQ_0; ARITH]];
    REWRITE_TAC[IN_ELIM_THM] THEN
    REWRITE_TAC[is_finite] THEN STRIP_TAC THEN ASM_REWRITE_TAC[]]);;

let IS_VALID_CLOSEST = prove
 (`!X P v p x. is_valid(X) (closest v p {a | is_finite(X) a} x)`,
  REWRITE_TAC[GEN_REWRITE_RULE I [is_finite] (SPEC_ALL IS_FINITE_CLOSEST)]);;

let IS_VALID_ROUND = prove
 (`!X x. is_valid(X) (round(X) To_nearest x)`,
  REPEAT GEN_TAC THEN REWRITE_TAC[is_valid; round] THEN
  REPEAT COND_CASES_TAC THEN
  ASM_REWRITE_TAC[IS_VALID_SPECIAL; IS_VALID_CLOSEST]);;

let DEFLOAT_FLOAT_ROUND = prove
 (`!X x. defloat(float(round(float_format) To_nearest x)) =
         round(float_format) To_nearest x`,
  REWRITE_TAC[GSYM float_tybij; IS_VALID_ROUND]);;

let DEFLOAT_FLOAT_ZEROSIGN_ROUND = prove
 (`!x b. defloat(float(zerosign(float_format) b
           (round(float_format) To_nearest x))) =
         zerosign(float_format) b (round(float_format) To_nearest x)`,
  REPEAT GEN_TAC THEN REWRITE_TAC[GSYM float_tybij] THEN
  REWRITE_TAC[zerosign] THEN
  REPEAT COND_CASES_TAC THEN REWRITE_TAC[IS_VALID_SPECIAL] THEN
  REWRITE_TAC[IS_VALID_ROUND]);;

let VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND = prove
 (`!x b. valof(float_format)
            (defloat(float(zerosign(float_format) b
                (round(float_format) To_nearest x)))) =
         valof(float_format) (round(float_format) To_nearest x)`,
  REPEAT GEN_TAC THEN REWRITE_TAC[DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
  REWRITE_TAC[zerosign] THEN
  REPEAT COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[minus_zero; plus_zero] THEN
  GEN_REWRITE_TAC LAND_CONV [valof] THEN
  REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_MUL_RZERO] THEN
  UNDISCH_TAC `is_zero float_format (round float_format To_nearest x)` THEN
  SPEC_TAC (`round float_format To_nearest x`,`a:num#num#num`) THEN
  GEN_TAC THEN
  SUBST1_TAC(SYM(REWRITE_CONV[PAIR]
  `FST(a):num,FST(SND(a)):num,SND(SND(a)):num`)) THEN
  PURE_REWRITE_TAC[is_zero; exponent; fraction; valof] THEN
  STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_MUL_RZERO]);;

let ISFINITE_THM = prove
 (`!a. ISFINITE(a) = is_finite(float_format) (defloat a)`,
  REWRITE_TAC[ISFINITE; is_finite; ISNORMAL; ISDENORMAL; ISZERO] THEN
  GEN_TAC THEN REWRITE_TAC[float_tybij]);;

let VAL_FINITE = prove
 (`!a. ISFINITE(a) ==> abs(VAL a) <= largest(float_format)`,
  GEN_TAC THEN REWRITE_TAC[ISFINITE_THM; VAL] THEN
  REWRITE_TAC[IS_FINITE_EXPLICIT; VALOF] THEN
  REWRITE_TAC[GSYM REAL_OF_NUM_LT] THEN STRIP_TAC THEN
  REWRITE_TAC[float_format; fracwidth; bias; emax; expwidth; largest] THEN
  REWRITE_TAC[ARITH] THEN COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[REAL_ABS_POW; REAL_ABS_MUL; REAL_ABS_DIV] THEN
  REWRITE_TAC[REAL_ABS_NEG; REAL_POW_ONE; REAL_ABS_NUM] THEN
  REWRITE_TAC[REAL_MUL_LID; real_div; GSYM REAL_MUL_ASSOC] THEN
  ONCE_REWRITE_TAC[AC REAL_MUL_AC `a:real * b * c = b * a * c`] THEN
  MATCH_MP_TAC REAL_LE_LMUL THEN
  (REWRITE_TAC[REAL_LE_INV_EQ] THEN CONJ_TAC THENL
    [MATCH_MP_TAC REAL_POW_LE THEN REWRITE_TAC[REAL_POS]; ALL_TAC]) THEN
  REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
  ONCE_REWRITE_TAC[AC REAL_MUL_AC `a:real * b * c = (c * a) * b`] THEN
  CONV_TAC REAL_RAT_REDUCE_CONV THENL
   [MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `(&1 / &4194304) * &8388608` THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_LMUL THEN CONJ_TAC THENL
       [CONV_TAC REAL_RAT_REDUCE_CONV;
        MATCH_MP_TAC REAL_LT_IMP_LE THEN ASM_REWRITE_TAC[]];
      CONV_TAC REAL_RAT_REDUCE_CONV];
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&2 pow 254 *
                abs (&1 + &(fraction (defloat a)) * &1 / &8388608)` THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_RMUL THEN REWRITE_TAC[REAL_ABS_POS] THEN
      MATCH_MP_TAC REAL_POW_MONO THEN
      CONJ_TAC THENL
       [REAL_ARITH_TAC;
        REWRITE_TAC[GSYM LT_SUC_LE] THEN REWRITE_TAC[ARITH] THEN
        ASM_REWRITE_TAC[GSYM REAL_OF_NUM_LT]];
      MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN EXISTS_TAC `inv(&2 pow 255)` THEN
      REWRITE_TAC[REAL_MUL_ASSOC] THEN CONV_TAC REAL_RAT_REDUCE_CONV THEN
      REWRITE_TAC[REAL_MUL_LID; real_div] THEN
      SUBGOAL_THEN `!a b c. abs(&a + &b * inv(&c)) = &a + &b * inv(&c)`
      (fun th -> REWRITE_TAC[th]) THENL
       [REWRITE_TAC[REAL_ABS_REFL] THEN REPEAT GEN_TAC THEN
        MATCH_MP_TAC REAL_LE_ADD THEN REWRITE_TAC[REAL_POS] THEN
        MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_POS] THEN
        MATCH_MP_TAC REAL_LE_INV THEN REWRITE_TAC[REAL_POS];
        MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN EXISTS_TAC `&2` THEN
        REWRITE_TAC[REAL_MUL_ASSOC] THEN
        CONV_TAC REAL_RAT_REDUCE_CONV THEN
        REWRITE_TAC[REAL_MUL_LID] THEN
        REWRITE_TAC[REAL_ARITH `a + b:real <= c <=> b <= c - a`] THEN
        CONV_TAC REAL_RAT_REDUCE_CONV THEN
        REWRITE_TAC[real_div; REAL_MUL_ASSOC; REAL_MUL_RID] THEN
        MATCH_MP_TAC REAL_LE_RMUL THEN
        REWRITE_TAC[REAL_LE_INV_EQ; REAL_POS] THEN
        REWRITE_TAC[REAL_OF_NUM_LE] THEN REWRITE_TAC[GSYM LT_SUC_LE] THEN
        REWRITE_TAC[ARITH] THEN ASM_REWRITE_TAC[GSYM REAL_OF_NUM_LT]]]]);;

let ISFINITE_LEMMA = prove
 (`!s e f. s < 2 /\ e < 255 /\ f < 2 EXP 23
           ==> ISFINITE(float(s,e,f)) /\ is_valid(float_format)(s,e,f)`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN REWRITE_TAC[ISFINITE_THM] THEN
  SUBGOAL_THEN `is_valid(float_format) (s,e,f)` ASSUME_TAC THENL
   [ASM_REWRITE_TAC[is_valid; float_format; expwidth; fracwidth] THEN
    MATCH_MP_TAC LT_TRANS THEN EXISTS_TAC `255` THEN
    ASM_REWRITE_TAC[] THEN REWRITE_TAC[ARITH];
    ASM_REWRITE_TAC[] THEN
    FIRST_ASSUM(SUBST1_TAC o GEN_REWRITE_RULE I [float_tybij]) THEN
    ASM_REWRITE_TAC[is_finite; is_normal; is_denormal; is_zero] THEN
    REWRITE_TAC[fraction; exponent; emax; float_format] THEN
    REWRITE_TAC[expwidth; fracwidth] THEN
    UNDISCH_TAC `e < 255` THEN REWRITE_TAC[ARITH] THEN ARITH_TAC]);;

(* ------------------------------------------------------------------------- *)
(* Conversion for finiteness.                                                *)
(* ------------------------------------------------------------------------- *)

let ISFINITE_FLOAT_CONV tm =
  let ith = PART_MATCH (lhand o rand) ISFINITE_LEMMA tm in
  CONJUNCT1(REWRITE_RULE[ARITH] ith);;

(* ------------------------------------------------------------------------- *)
(* Explicit numeric value for threshold, to save repeated recalculation.     *)
(* ------------------------------------------------------------------------- *)

let FLOAT_THRESHOLD_EXPLICIT = prove
 (`threshold(float_format) = &340282356779733661637539395458142568448`,
  REWRITE_TAC[threshold; float_format; emax; bias; fracwidth; expwidth] THEN
  REWRITE_TAC[ARITH] THEN CONV_TAC REAL_RAT_REDUCE_CONV);;

let VAL_THRESHOLD = prove
 (`!a. ISFINITE(a) ==> abs(VAL a) < threshold float_format`,
  GEN_TAC THEN DISCH_TAC THEN
  MATCH_MP_TAC REAL_LET_TRANS THEN
  EXISTS_TAC `largest(float_format)` THEN CONJ_TAC THENL
   [MATCH_MP_TAC VAL_FINITE THEN ASM_REWRITE_TAC[];
    REWRITE_TAC[FLOAT_THRESHOLD_EXPLICIT] THEN
    REWRITE_TAC[largest; float_format; emax; bias; fracwidth; expwidth] THEN
    REWRITE_TAC[ARITH; real_div] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV]);;

(* ------------------------------------------------------------------------- *)
(* Lifting up of rounding (to nearest).                                      *)
(* ------------------------------------------------------------------------- *)

let error = new_definition
  `error(x) = VAL(float(round(float_format) To_nearest x)) - x`;;

let BOUND_AT_WORST_LEMMA = prove
 (`!a x. abs(x) < threshold(float_format) /\ is_finite(float_format) a
         ==>  abs(valof(float_format) (round(float_format) To_nearest x) - x)
              <= abs(valof(float_format) a - x)`,
  REPEAT GEN_TAC THEN
  REWRITE_TAC[REAL_ARITH `abs(x:real) < y <=> ~(x <= --y) /\ ~(x >= y)`] THEN
  STRIP_TAC THEN ASM_REWRITE_TAC[round] THEN
  REWRITE_TAC[round] THEN
  MP_TAC(MATCH_MP CLOSEST_IS_CLOSEST
    (SPEC `float_format` IS_FINITE_FINITE)) THEN
  REWRITE_TAC[IS_FINITE_NONEMPTY] THEN
  DISCH_THEN(MP_TAC o SPEC `valof(float_format)`) THEN
  DISCH_THEN(MP_TAC o SPEC `\a. EVEN (fraction a)`) THEN
  DISCH_THEN(MP_TAC o SPEC `x:real`) THEN
  REWRITE_TAC[is_closest] THEN
  DISCH_THEN(MATCH_MP_TAC o SPEC `a:num#num#num` o CONJUNCT2) THEN
  ASM_REWRITE_TAC[IN_ELIM_THM]);;

let ERROR_AT_WORST_LEMMA = prove
 (`!a x. abs(x) < threshold(float_format) /\ ISFINITE(a)
         ==> abs(error(x)) <= abs(VAL(a) - x)`,
  REWRITE_TAC[ISFINITE_THM; VAL;
    error; BOUND_AT_WORST_LEMMA; VAL; DEFLOAT_FLOAT_ROUND]);;

let ERROR_IS_ZERO = prove
 (`!a x. ISFINITE(a) /\ (VAL(a) = x) ==> (error(x) = &0)`,
  REPEAT GEN_TAC THEN DISCH_THEN(STRIP_ASSUME_TAC o GSYM) THEN
  MP_TAC(SPECL [`a:float`; `x:real`] ERROR_AT_WORST_LEMMA) THEN
  ASM_REWRITE_TAC[REAL_SUB_REFL; REAL_ABS_0] THEN
  REWRITE_TAC[REAL_ARITH `abs(x) <= &0 <=> (x = &0)`] THEN
  DISCH_THEN MATCH_MP_TAC THEN
  MATCH_MP_TAC REAL_LET_TRANS THEN
  EXISTS_TAC `largest(float_format)` THEN CONJ_TAC THENL
   [MATCH_MP_TAC VAL_FINITE THEN ASM_REWRITE_TAC[];
    REWRITE_TAC[largest; threshold; float_format] THEN
    REWRITE_TAC[emax; expwidth; fracwidth; bias] THEN
    REWRITE_TAC[ARITH] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV]);;

let ERROR_BOUND_LEMMA1 = prove
 (`!x. &0 <= x /\ x < &1
       ==> ?n. n < 2 EXP 23 /\
               &n / &2 pow 23 <= x /\
               x < &(SUC n) / &2 pow 23`,
  GEN_TAC THEN STRIP_TAC THEN
  MP_TAC(SPEC `\n. &n / &2 pow 23 <= x` num_MAX) THEN
  REWRITE_TAC[] THEN
  W(C SUBGOAL_THEN MP_TAC o lhs o lhand o snd) THENL
   [CONJ_TAC THENL
     [EXISTS_TAC `0` THEN ASM_REWRITE_TAC[REAL_MUL_LZERO; real_div];
      EXISTS_TAC `2 EXP 23` THEN X_GEN_TAC `n:num` THEN
      DISCH_TAC THEN
      REWRITE_TAC[GSYM REAL_OF_NUM_LE; GSYM REAL_OF_NUM_POW] THEN
      MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
      EXISTS_TAC `inv(&2 pow 23)` THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      MATCH_MP_TAC REAL_LT_IMP_LE THEN
      MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `x:real` THEN
      ASM_REWRITE_TAC[] THEN
      UNDISCH_TAC `&n / &2 pow 23 <= x` THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      REWRITE_TAC[real_div; REAL_MUL_AC; REAL_MUL_RID; REAL_MUL_LID]];
    DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
    DISCH_THEN(X_CHOOSE_THEN `n:num` MP_TAC) THEN
    DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
    DISCH_THEN(MP_TAC o SPEC `SUC n`) THEN
    REWRITE_TAC[GSYM NOT_LT; LT] THEN
    REWRITE_TAC[REAL_NOT_LE] THEN DISCH_TAC THEN
    EXISTS_TAC `n:num` THEN ASM_REWRITE_TAC[] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_LT; GSYM REAL_OF_NUM_POW] THEN
    MATCH_MP_TAC REAL_LT_LCANCEL_IMP THEN
    EXISTS_TAC `inv(&2 pow 23)` THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    MATCH_MP_TAC REAL_LET_TRANS THEN
    EXISTS_TAC `x:real` THEN ASM_REWRITE_TAC[] THEN
    UNDISCH_TAC `&n / &2 pow 23 <= x` THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    REWRITE_TAC[real_div; REAL_MUL_AC; REAL_MUL_RID; REAL_MUL_LID]]);;

let ERROR_BOUND_LEMMA2 = prove
 (`!x. &0 <= x /\ x < &1
       ==> ?n. n <= 2 EXP 23 /\ abs(x - &n / &2 pow 23) <= inv(&2 pow 24)`,
  let lemma = REAL_ARITH
    `!a b x d. a <= x /\ x < b
               ==> b <= a + &2 * d
                   ==> abs(x - a) <= d \/ abs(x - b) <= d` in
  GEN_TAC THEN DISCH_TAC THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP ERROR_BOUND_LEMMA1) THEN
  DISCH_THEN(X_CHOOSE_THEN `n:num` MP_TAC) THEN
  DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
  DISCH_THEN(MP_TAC o MATCH_MP lemma) THEN
  DISCH_THEN(MP_TAC o SPEC `inv(&2 pow 24)`) THEN
  W(C SUBGOAL_THEN MP_TAC o lhand o lhand o snd) THENL
   [REWRITE_TAC[GSYM REAL_OF_NUM_SUC; real_div; REAL_ADD_RDISTRIB] THEN
    REWRITE_TAC[REAL_LE_LADD] THEN CONV_TAC REAL_RAT_REDUCE_CONV;
    DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
    DISCH_THEN DISJ_CASES_TAC THENL
     [EXISTS_TAC `n:num`; EXISTS_TAC `SUC n`] THEN
    ASM_REWRITE_TAC[LE_SUC_LT] THEN
    MATCH_MP_TAC LT_IMP_LE THEN ASM_REWRITE_TAC[]]);;

let ERROR_BOUND_LEMMA3 = prove
 (`!x. &1 <= x /\ x < &2
       ==> ?n. n <= 2 EXP 23 /\
               abs((&1 + &n / &2 pow 23) - x) <= inv(&2 pow 24)`,
  GEN_TAC THEN DISCH_TAC THEN
  SUBGOAL_THEN `&0 <= x - &1 /\ x - &1 < &1` MP_TAC THENL
   [POP_ASSUM MP_TAC THEN REAL_ARITH_TAC;
    DISCH_THEN(MP_TAC o MATCH_MP ERROR_BOUND_LEMMA2)] THEN
  DISCH_THEN(MP_TAC o ONCE_REWRITE_RULE[GSYM REAL_ABS_NEG]) THEN
  REWRITE_TAC[REAL_NEG_SUB; REAL_ARITH `a - (b - c) = (c + a:real) - b`]);;

let ERROR_BOUND_LEMMA4 = prove
 (`!x. &1 <= x /\ x < &2
       ==> ?e f. abs(VAL(float(0,e,f)) - x) <= inv(&2 pow 24) /\
                 f < 2 EXP 23 /\
                 ((e = bias(float_format)) \/
                  (e = SUC(bias(float_format))) /\ (f = 0))`,
  GEN_TAC THEN DISCH_TAC THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP ERROR_BOUND_LEMMA3) THEN
  DISCH_THEN(X_CHOOSE_THEN `n:num` STRIP_ASSUME_TAC) THEN
  UNDISCH_TAC `n <= 2 EXP 23` THEN REWRITE_TAC[LE_LT] THEN
  DISCH_THEN DISJ_CASES_TAC THENL
   [EXISTS_TAC `bias(float_format)` THEN EXISTS_TAC `n:num` THEN
    ASM_REWRITE_TAC[VAL] THEN
    SUBGOAL_THEN `defloat (float (0,(bias float_format,n))) =
                  0,bias(float_format),n`
    SUBST1_TAC THENL
     [REWRITE_TAC[GSYM float_tybij] THEN
      REWRITE_TAC[is_valid; float_format] THEN
      ASM_REWRITE_TAC[bias; expwidth; fracwidth] THEN
      REWRITE_TAC[ARITH];
      REWRITE_TAC[valof; bias; expwidth; fracwidth; float_format] THEN
      REWRITE_TAC[ARITH] THEN REWRITE_TAC[real_pow; REAL_MUL_LID] THEN
      REWRITE_TAC[REAL_MUL_ASSOC; real_div] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN REWRITE_TAC[REAL_MUL_LID] THEN
      UNDISCH_TAC `abs ((&1 + &n / &2 pow 23) - x) <= inv (&2 pow 24)` THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN REWRITE_TAC[real_div; REAL_MUL_LID]];
    EXISTS_TAC `SUC(bias(float_format))` THEN EXISTS_TAC `0` THEN
    REWRITE_TAC[VAL; ARITH] THEN
    SUBGOAL_THEN `defloat (float (0,(SUC (bias float_format),0))) =
                  0,SUC(bias(float_format)),0`
    SUBST1_TAC THENL
     [REWRITE_TAC[GSYM float_tybij] THEN
      REWRITE_TAC[is_valid; float_format] THEN
      ASM_REWRITE_TAC[bias; expwidth; fracwidth] THEN
      REWRITE_TAC[ARITH];
      REWRITE_TAC[valof; bias; expwidth; fracwidth; float_format] THEN
      REWRITE_TAC[ARITH] THEN
      REWRITE_TAC[real_pow; REAL_MUL_LID; real_div; REAL_MUL_LZERO] THEN
      REWRITE_TAC[REAL_ADD_RID; REAL_MUL_RID] THEN
      UNDISCH_TAC `abs((&1 + &n / &2 pow 23) - x) <= inv(&2 pow 24)` THEN
      ASM_REWRITE_TAC[GSYM REAL_OF_NUM_POW] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV]]);;

let ERROR_BOUND_LEMMA5 = prove
 (`!x. &1 <= abs(x) /\ abs(x) < &2
       ==> ?s e f. abs(VAL(float(s,e,f)) - x) <= inv(&2 pow 24) /\
                   s < 2 /\
                   f < 2 EXP 23 /\
                   ((e = bias(float_format)) \/
                    (e = SUC(bias(float_format))) /\ (f = 0))`,
  GEN_TAC THEN DISCH_TAC THEN
  SUBGOAL_THEN `&1 <= x /\ x < &2 \/
                &1 <= --x /\ --x < &2`
  MP_TAC THENL
   [POP_ASSUM MP_TAC THEN REAL_ARITH_TAC;
    DISCH_THEN(DISJ_CASES_THEN (MP_TAC o MATCH_MP ERROR_BOUND_LEMMA4))] THEN
  DISCH_THEN(X_CHOOSE_THEN `e:num` (X_CHOOSE_THEN `f:num`
       (CONJUNCTS_THEN ASSUME_TAC))) THENL
   [EXISTS_TAC `0`; EXISTS_TAC `1`] THEN
  EXISTS_TAC `e:num` THEN EXISTS_TAC `f:num`  THEN
  ASM_REWRITE_TAC[ARITH] THEN
  ONCE_REWRITE_TAC[GSYM REAL_ABS_NEG] THEN
  UNDISCH_TAC `abs(VAL(float (0,(e,f))) - -- x) <= inv (&2 pow 24)` THEN
  MATCH_MP_TAC EQ_IMP THEN
  AP_THM_TAC THEN AP_TERM_TAC THEN
  REWRITE_TAC[REAL_ARITH `--(x - y) = --(x:real) - --y`] THEN
  AP_TERM_TAC THEN AP_THM_TAC THEN AP_TERM_TAC THEN
  REWRITE_TAC[VAL] THEN
  SUBGOAL_THEN `(defloat(float(0,e,f)) = 0,e,f) /\
                (defloat(float(1,e,f)) = 1,e,f)`
  (CONJUNCTS_THEN SUBST1_TAC) THENL
   [REWRITE_TAC[GSYM float_tybij] THEN
    REWRITE_TAC[is_valid; float_format] THEN
    ASM_REWRITE_TAC[bias; expwidth; fracwidth] THEN
    FIRST_ASSUM(DISJ_CASES_TAC o CONJUNCT2) THEN
    ASM_REWRITE_TAC[bias; expwidth; fracwidth; float_format] THEN
    REWRITE_TAC[ARITH];
    REWRITE_TAC[valof] THEN COND_CASES_TAC THEN
    ASM_REWRITE_TAC[REAL_POW_1; real_pow] THEN
    REWRITE_TAC[REAL_MUL_LNEG; REAL_NEG_NEG]]);;

let ERROR_BOUND_LEMMA6 = prove
 (`!x. &0 <= x /\ x < inv(&2 pow 126)
       ==> ?n. n <= 2 EXP 23 /\
               abs(x - &2 / &2 pow 127 * &n / &2 pow 23) <= inv(&2 pow 150)`,
  REPEAT STRIP_TAC THEN
  MP_TAC(SPEC `&2 pow 126 * x` ERROR_BOUND_LEMMA2) THEN
  W(C SUBGOAL_THEN MP_TAC o lhand o lhand o snd) THENL
   [CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
      EXISTS_TAC `inv(&2 pow 126)` THEN CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_INV THEN MATCH_MP_TAC REAL_POW_LT THEN
        REAL_ARITH_TAC; REWRITE_TAC[REAL_MUL_RZERO]] THEN
      SUBGOAL_THEN `inv(&2 pow 126) * &2 pow 126 = &1`
      (fun th -> REWRITE_TAC[REAL_MUL_ASSOC; th; REAL_MUL_LID]) THENL
       [MATCH_MP_TAC REAL_MUL_LINV THEN MATCH_MP_TAC REAL_LT_IMP_NZ THEN
        MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
        ASM_REWRITE_TAC[]];
      MATCH_MP_TAC REAL_LT_LCANCEL_IMP THEN
      EXISTS_TAC `inv(&2 pow 126)` THEN CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_INV THEN MATCH_MP_TAC REAL_POW_LT THEN
        REAL_ARITH_TAC; REWRITE_TAC[REAL_MUL_RZERO]] THEN
      SUBGOAL_THEN `inv(&2 pow 126) * &2 pow 126 = &1`
      (fun th -> ASM_REWRITE_TAC[REAL_MUL_ASSOC; th; REAL_MUL_LID;
                   REAL_MUL_RID]) THEN
      MATCH_MP_TAC REAL_MUL_LINV THEN MATCH_MP_TAC REAL_LT_IMP_NZ THEN
      MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC];
    DISCH_THEN(fun th -> REWRITE_TAC[th])] THEN
  DISCH_THEN(X_CHOOSE_THEN `n:num` STRIP_ASSUME_TAC) THEN
  EXISTS_TAC `n:num` THEN ASM_REWRITE_TAC[] THEN
  MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN EXISTS_TAC `&2 pow 126` THEN
  CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
  SUBGOAL_THEN `!x. &2 pow 126 * abs(x) = abs(&2 pow 126 * x)`
  (fun th -> REWRITE_TAC[th]) THENL
   [REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NUM]; ALL_TAC] THEN
  UNDISCH_TAC `abs(&2 pow 126 * x - &n / &2 pow 23) <= inv(&2 pow 24)` THEN
  REWRITE_TAC[REAL_MUL_ASSOC; real_div; REAL_SUB_LDISTRIB] THEN
  CONV_TAC REAL_RAT_REDUCE_CONV THEN
  REWRITE_TAC[REAL_MUL_LID; REAL_MUL_RID; real_div; REAL_MUL_AC]);;

let ERROR_BOUND_LEMMA7 = prove
 (`!x. &0 <= x /\ x < inv(&2 pow 126)
       ==> ?e f. abs(VAL(float(0,e,f)) - x) <= inv(&2 pow 150) /\
                 f < 2 EXP 23 /\
                 ((e = 0) \/ (e = 1) /\ (f = 0))`,
  GEN_TAC THEN DISCH_TAC THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP ERROR_BOUND_LEMMA6) THEN
  DISCH_THEN(X_CHOOSE_THEN `n:num` MP_TAC) THEN
  DISCH_THEN(CONJUNCTS_THEN2 MP_TAC ASSUME_TAC) THEN
  DISCH_THEN(DISJ_CASES_TAC o REWRITE_RULE[LE_LT]) THENL
   [EXISTS_TAC `0` THEN EXISTS_TAC `n:num` THEN
    ASM_REWRITE_TAC[VAL] THEN
    SUBGOAL_THEN `defloat(float(0,0,n)) = 0,0,n` SUBST1_TAC THENL
     [REWRITE_TAC[GSYM float_tybij] THEN
      REWRITE_TAC[is_valid; float_format; expwidth; fracwidth] THEN
      ASM_REWRITE_TAC[] THEN REWRITE_TAC[ARITH];
      REWRITE_TAC[valof; float_format; bias; fracwidth; expwidth] THEN
      UNDISCH_TAC `abs(x - &2 / &2 pow 127 * &n / &2 pow 23)
                   <= inv (&2 pow 150)` THEN
      REWRITE_TAC[ARITH; real_pow; REAL_MUL_LID; REAL_ABS_SUB]];
    EXISTS_TAC `1` THEN EXISTS_TAC `0` THEN
    ASM_REWRITE_TAC[VAL; EXP_LT_0; ARITH] THEN
    SUBGOAL_THEN `defloat(float(0,1,0)) = 0,1,0` SUBST1_TAC THENL
     [REWRITE_TAC[GSYM float_tybij] THEN
      REWRITE_TAC[is_valid; float_format; expwidth; fracwidth] THEN
      ASM_REWRITE_TAC[] THEN REWRITE_TAC[ARITH];
      REWRITE_TAC[valof; float_format; bias; fracwidth; expwidth] THEN
      REWRITE_TAC[ARITH; real_pow; REAL_MUL_LID] THEN
      REWRITE_TAC[REAL_POW_1; real_div; REAL_MUL_LZERO] THEN
      REWRITE_TAC[REAL_ADD_RID; REAL_MUL_RID] THEN
      UNDISCH_TAC `abs(x - &2 / &2 pow 127 * &n / &2 pow 23)
                   <= inv (&2 pow 150)` THEN
      ASM_REWRITE_TAC[GSYM REAL_OF_NUM_POW] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      REWRITE_TAC[REAL_ABS_SUB]]]);;

let ERROR_BOUND_LEMMA8 = prove
 (`!x. abs(x) < inv(&2 pow 126)
       ==> ?s e f. abs(VAL(float(s,e,f)) - x) <= inv(&2 pow 150) /\
                   s < 2 /\
                   f < 2 EXP 23 /\
                   ((e = 0) \/ (e = 1) /\ (f = 0))`,
  GEN_TAC THEN DISCH_TAC THEN
  SUBGOAL_THEN `&0 <= x /\ x < inv(&2 pow 126) \/
                &0 <= --x /\ --x < inv(&2 pow 126)`
  MP_TAC THENL
   [POP_ASSUM MP_TAC THEN REAL_ARITH_TAC;
    DISCH_THEN(DISJ_CASES_THEN (MP_TAC o MATCH_MP ERROR_BOUND_LEMMA7))] THEN
  DISCH_THEN(X_CHOOSE_THEN `e:num` (X_CHOOSE_THEN `f:num`
       (CONJUNCTS_THEN ASSUME_TAC))) THENL
   [EXISTS_TAC `0`; EXISTS_TAC `1`] THEN
  EXISTS_TAC `e:num` THEN EXISTS_TAC `f:num`  THEN
  ASM_REWRITE_TAC[ARITH] THEN
  ONCE_REWRITE_TAC[GSYM REAL_ABS_NEG] THEN
  UNDISCH_TAC `abs(VAL(float (0,(e,f))) - -- x) <= inv (&2 pow 150)` THEN
  MATCH_MP_TAC EQ_IMP THEN
  AP_THM_TAC THEN AP_TERM_TAC THEN
  REWRITE_TAC[REAL_ARITH `--(x - y) = --(x:real) - --y`] THEN
  AP_TERM_TAC THEN AP_THM_TAC THEN AP_TERM_TAC THEN
  REWRITE_TAC[VAL] THEN
  SUBGOAL_THEN `(defloat(float(0,e,f)) = 0,e,f) /\
                (defloat(float(1,e,f)) = 1,e,f)`
  (CONJUNCTS_THEN SUBST1_TAC) THENL
   [REWRITE_TAC[GSYM float_tybij] THEN
    REWRITE_TAC[is_valid; float_format] THEN
    ASM_REWRITE_TAC[bias; expwidth; fracwidth] THEN
    FIRST_ASSUM(DISJ_CASES_TAC o CONJUNCT2) THEN
    ASM_REWRITE_TAC[bias; expwidth; fracwidth; float_format] THEN
    REWRITE_TAC[ARITH];
    REWRITE_TAC[valof] THEN COND_CASES_TAC THEN
    ASM_REWRITE_TAC[REAL_POW_1; real_pow] THEN
    REWRITE_TAC[REAL_MUL_LNEG; REAL_NEG_NEG]]);;

let VALOF_SCALE_UP = prove
 (`!s e k f. ~(e = 0) ==> (valof(float_format) (s,e + k,f) =
                           &2 pow k * valof(float_format) (s,e,f))`,
  REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[valof; ADD_EQ_0] THEN
  REWRITE_TAC[REAL_POW_ADD; REAL_MUL_AC; real_div]);;

let VALOF_SCALE_DOWN = prove
 (`!s e k f. k < e ==> (valof(float_format) (s,e - k,f) =
                        inv(&2 pow k) * valof(float_format) (s,e,f))`,
  REPEAT GEN_TAC THEN DISCH_TAC THEN
  SUBGOAL_THEN `e:num = (e - k) + k` MP_TAC THENL
   [POP_ASSUM MP_TAC THEN ARITH_TAC;
    DISCH_THEN(fun th -> GEN_REWRITE_TAC
      (RAND_CONV o ONCE_DEPTH_CONV) [th])] THEN
  SUBGOAL_THEN `~(e - k = 0)` MP_TAC THENL
   [POP_ASSUM MP_TAC THEN ARITH_TAC;
    DISCH_THEN(fun th -> REWRITE_TAC[MATCH_MP VALOF_SCALE_UP th]) THEN
    SUBGOAL_THEN `inv(&2 pow k) * &2 pow k = &1`
      (fun th -> REWRITE_TAC[REAL_MUL_ASSOC; th; REAL_MUL_LID]) THEN
    MATCH_MP_TAC REAL_MUL_LINV THEN
    REWRITE_TAC[REAL_POW_EQ_0; REAL_OF_NUM_EQ; ARITH]]);;

let ERROR_BOUND_BIG = prove
 (`!k x. &2 pow k <= abs(x) /\ abs(x) < &2 pow (SUC k) /\
         abs(x) < threshold(float_format)
         ==> abs(error(x)) <= &2 pow k / &2 pow 24`,
  let lemma = REAL_ARITH `abs(a - b) <= c:real ==> abs(a) <= abs(b) + c` in
  REPEAT STRIP_TAC THEN
  SUBGOAL_THEN `?a. ISFINITE(a) /\ abs(VAL(a) - x) <= &2 pow k / &2 pow 24`
  CHOOSE_TAC THENL
   [ALL_TAC;
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `abs(VAL(a) - x)` THEN
    ASM_REWRITE_TAC[] THEN MATCH_MP_TAC ERROR_AT_WORST_LEMMA THEN
    ASM_REWRITE_TAC[]] THEN
  MP_TAC(SPEC `x / &2 pow k` ERROR_BOUND_LEMMA5) THEN
  W(C SUBGOAL_THEN MP_TAC o lhand o lhand o snd) THENL
   [REWRITE_TAC[REAL_ABS_DIV; REAL_ABS_POW; REAL_ABS_NUM] THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN
      EXISTS_TAC `&2 pow k` THEN CONJ_TAC THENL
       [MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
        REWRITE_TAC[REAL_MUL_LID; real_div; GSYM REAL_MUL_ASSOC] THEN
        UNDISCH_TAC `&2 pow k <= abs(x)` THEN
        MATCH_MP_TAC EQ_IMP THEN AP_TERM_TAC THEN
        GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THEN AP_TERM_TAC THEN
        CONV_TAC SYM_CONV THEN MATCH_MP_TAC REAL_MUL_LINV THEN
        REWRITE_TAC[REAL_POW_EQ_0; REAL_OF_NUM_EQ; ARITH]];
      MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN
      EXISTS_TAC `&2 pow k` THEN CONJ_TAC THENL
       [MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
        REWRITE_TAC[REAL_MUL_LID; real_div; GSYM REAL_MUL_ASSOC] THEN
        UNDISCH_TAC `abs(x) < &2 pow (SUC k)` THEN
        REWRITE_TAC[real_pow] THEN
        MATCH_MP_TAC EQ_IMP THEN
        AP_THM_TAC THEN AP_TERM_TAC THEN
        GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THEN AP_TERM_TAC THEN
        CONV_TAC SYM_CONV THEN MATCH_MP_TAC REAL_MUL_LINV THEN
        REWRITE_TAC[REAL_POW_EQ_0; REAL_OF_NUM_EQ; ARITH]]];
    DISCH_THEN(fun th -> REWRITE_TAC[th])] THEN
  DISCH_THEN(X_CHOOSE_THEN `s:num` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `e:num` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `f:num` MP_TAC) THEN STRIP_TAC THENL
   [EXISTS_TAC `float(s,e + k,f)` THEN
    SUBGOAL_THEN `ISFINITE(float(s,e + k,f)) /\
                  is_valid(float_format)(s,e + k,f)` STRIP_ASSUME_TAC THENL
     [MATCH_MP_TAC ISFINITE_LEMMA THEN ASM_REWRITE_TAC[] THEN
      REWRITE_TAC[bias; float_format; fracwidth; expwidth] THEN
      REWRITE_TAC[ARITH] THEN
      SUBGOAL_THEN `k < 128` (fun th -> MP_TAC th THEN ARITH_TAC) THEN
      REWRITE_TAC[GSYM NOT_LE] THEN DISCH_TAC THEN
      MP_TAC(SPECL [`128`; `k:num`; `&2`] REAL_POW_MONO) THEN
      ASM_REWRITE_TAC[REAL_RAT_REDUCE_CONV `&1 <= &2`] THEN
      REWRITE_TAC[REAL_NOT_LE] THEN
      MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `abs(x):real` THEN
      ASM_REWRITE_TAC[] THEN MATCH_MP_TAC REAL_LTE_TRANS THEN
      EXISTS_TAC `threshold(float_format)` THEN ASM_REWRITE_TAC[] THEN
      REWRITE_TAC[threshold; float_format; bias;
        expwidth; fracwidth; emax] THEN
      REWRITE_TAC[ARITH] THEN CONV_TAC REAL_RAT_REDUCE_CONV;
      CONJ_TAC THENL [ASM_REWRITE_TAC[]; ALL_TAC] THEN
      REWRITE_TAC[VAL] THEN
      FIRST_ASSUM(SUBST1_TAC o GEN_REWRITE_RULE I [float_tybij]) THEN
      SUBGOAL_THEN `~(e = 0)`
      (fun th -> REWRITE_TAC[MATCH_MP VALOF_SCALE_UP th]) THENL
       [ASM_REWRITE_TAC[bias; float_format; expwidth] THEN
        REWRITE_TAC[ARITH]; ALL_TAC] THEN
      MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
      EXISTS_TAC `inv(&2 pow k)` THEN CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_INV THEN
        MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
        SUBGOAL_THEN `!x. inv(&2 pow k) * abs(x) = abs(inv(&2 pow k) * x)`
        (fun th -> REWRITE_TAC[th]) THENL
         [REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_INV;
            REAL_ABS_POW; REAL_ABS_NUM]; ALL_TAC] THEN
        REWRITE_TAC[REAL_SUB_LDISTRIB; REAL_MUL_ASSOC; real_div] THEN
        SUBGOAL_THEN `inv(&2 pow k) * (&2 pow k) = &1`
        (fun th -> REWRITE_TAC[th]) THENL
         [MATCH_MP_TAC REAL_MUL_LINV THEN
          MATCH_MP_TAC REAL_LT_IMP_NZ THEN
          MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC; ALL_TAC] THEN
        REWRITE_TAC[REAL_MUL_LID] THEN
        UNDISCH_TAC `abs (VAL (float (s,(e,f))) - x / &2 pow k)
                     <= inv (&2 pow 24)` THEN
        REWRITE_TAC[VAL] THEN
        SUBGOAL_THEN `defloat (float (s,e,f)) = s,e,f` SUBST1_TAC THENL
         [REWRITE_TAC[GSYM float_tybij] THEN
          REWRITE_TAC[is_valid; expwidth; fracwidth; float_format] THEN
          ASM_REWRITE_TAC[] THEN
          REWRITE_TAC[bias; expwidth; float_format] THEN
          REWRITE_TAC[ARITH];
          REWRITE_TAC[real_div; REAL_MUL_AC]]]];
    ASM_CASES_TAC `e + k < 255` THENL
     [EXISTS_TAC `float(s,e + k,f)` THEN
      SUBGOAL_THEN `ISFINITE(float(s,e + k,f)) /\
                    is_valid(float_format)(s,e + k,f)` STRIP_ASSUME_TAC THENL
       [MATCH_MP_TAC ISFINITE_LEMMA THEN ASM_REWRITE_TAC[];
        CONJ_TAC THENL [ASM_REWRITE_TAC[]; ALL_TAC] THEN
        REWRITE_TAC[VAL] THEN
        FIRST_ASSUM(SUBST1_TAC o GEN_REWRITE_RULE I [float_tybij]) THEN
        SUBGOAL_THEN `~(e = 0)`
        (fun th -> REWRITE_TAC[MATCH_MP VALOF_SCALE_UP th]) THENL
         [ASM_REWRITE_TAC[bias; float_format; expwidth] THEN
          REWRITE_TAC[ARITH]; ALL_TAC] THEN
        MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
        EXISTS_TAC `inv(&2 pow k)` THEN CONJ_TAC THENL
         [MATCH_MP_TAC REAL_LT_INV THEN
          MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
          SUBGOAL_THEN `!x. inv(&2 pow k) * abs(x) = abs(inv(&2 pow k) * x)`
          (fun th -> REWRITE_TAC[th]) THENL
           [REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_INV;
              REAL_ABS_POW; REAL_ABS_NUM]; ALL_TAC] THEN
          REWRITE_TAC[REAL_SUB_LDISTRIB; REAL_MUL_ASSOC; real_div] THEN
          SUBGOAL_THEN `inv(&2 pow k) * (&2 pow k) = &1`
          (fun th -> REWRITE_TAC[th]) THENL
           [MATCH_MP_TAC REAL_MUL_LINV THEN
            MATCH_MP_TAC REAL_LT_IMP_NZ THEN
            MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC; ALL_TAC] THEN
          REWRITE_TAC[REAL_MUL_LID] THEN
          UNDISCH_TAC `abs (VAL (float (s,(e,f))) - x / &2 pow k)
                       <= inv (&2 pow 24)` THEN
          REWRITE_TAC[VAL] THEN
          SUBGOAL_THEN `defloat (float (s,e,f)) = s,e,f` SUBST1_TAC THENL
           [REWRITE_TAC[GSYM float_tybij] THEN
            REWRITE_TAC[is_valid; expwidth; fracwidth; float_format] THEN
            ASM_REWRITE_TAC[] THEN
            REWRITE_TAC[bias; expwidth; float_format] THEN
            REWRITE_TAC[ARITH];
            REWRITE_TAC[real_div; REAL_MUL_AC]]]];
      SUBGOAL_THEN `&2 pow k < threshold(float_format)` MP_TAC THENL
       [MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `abs(x:real)` THEN
        ASM_REWRITE_TAC[]; ALL_TAC] THEN
      REWRITE_TAC[threshold; float_format; emax; bias] THEN
      REWRITE_TAC[expwidth; fracwidth] THEN REWRITE_TAC[ARITH] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      DISCH_TAC THEN SUBGOAL_THEN `&2 pow k < &2 pow 128` ASSUME_TAC THENL
       [MATCH_MP_TAC REAL_LTE_TRANS THEN
        EXISTS_TAC `&340282356779733661637539395458142568448` THEN
        ASM_REWRITE_TAC[] THEN CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
      MP_TAC(SPECL [`128`; `k:num`; `&2`] REAL_POW_MONO) THEN
      ASM_REWRITE_TAC[GSYM REAL_NOT_LT] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN REWRITE_TAC[NOT_LE] THEN
      DISCH_TAC THEN SUBGOAL_THEN `k = 127` ASSUME_TAC THENL
       [UNDISCH_TAC `k < 128` THEN UNDISCH_TAC `~(e + k < 255)` THEN
        ASM_REWRITE_TAC[bias; float_format; expwidth] THEN
        REWRITE_TAC[ARITH] THEN ARITH_TAC; ALL_TAC] THEN
      UNDISCH_TAC `abs(x) < threshold(float_format)` THEN
      CONV_TAC CONTRAPOS_CONV THEN DISCH_THEN(K ALL_TAC) THEN
      REWRITE_TAC[REAL_NOT_LT; threshold; float_format] THEN
      REWRITE_TAC[emax; bias; fracwidth; expwidth] THEN
      REWRITE_TAC[ARITH] THEN UNDISCH_TAC
        `abs(VAL(float(s,e,f)) - x / &2 pow k) <= inv (&2 pow 24)` THEN
      ASM_REWRITE_TAC[] THEN REWRITE_TAC[bias; expwidth; float_format] THEN
      REWRITE_TAC[ARITH] THEN DISCH_THEN(MP_TAC o MATCH_MP lemma) THEN
      SUBGOAL_THEN `ISFINITE(float(s,128,0)) /\
                    is_valid(float_format)(s,128,0)`
      STRIP_ASSUME_TAC THENL
       [MATCH_MP_TAC ISFINITE_LEMMA THEN ASM_REWRITE_TAC[] THEN
        REWRITE_TAC[ARITH]; ALL_TAC] THEN
      REWRITE_TAC[VAL] THEN
      FIRST_ASSUM(SUBST1_TAC o GEN_REWRITE_RULE I [float_tybij]) THEN
      REWRITE_TAC[float_format; valof] THEN
      REWRITE_TAC[ARITH] THEN
      REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NEG] THEN
      REWRITE_TAC[REAL_ABS_NUM; REAL_POW_ONE; REAL_MUL_LID] THEN
      REWRITE_TAC[bias; fracwidth; expwidth] THEN
      REWRITE_TAC[ARITH] THEN
      REWRITE_TAC[real_div; REAL_MUL_LZERO] THEN
      REWRITE_TAC[REAL_ADD_RID; REAL_ABS_NUM; REAL_MUL_RID] THEN
      REWRITE_TAC[GSYM REAL_LE_SUB_RADD] THEN
      DISCH_THEN(MP_TAC o CONV_RULE REAL_RAT_REDUCE_CONV) THEN
      REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_DIV; REAL_ABS_NUM] THEN
      DISCH_TAC THEN MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN
      EXISTS_TAC `inv(&2 pow 127)` THEN CONV_TAC REAL_RAT_REDUCE_CONV THEN
      ASM_REWRITE_TAC[]]]);;

let ERROR_BOUND_SMALL = prove
 (`!k x. inv(&2 pow (SUC k)) <= abs(x) /\
         abs(x) < inv(&2 pow k) /\
         k < 126
         ==> abs(error(x)) <= inv(&2 pow (SUC k) * &2 pow 24)`,
  REPEAT STRIP_TAC THEN SUBGOAL_THEN
    `?a. ISFINITE(a) /\ abs(VAL(a) - x) <= inv(&2 pow (SUC k) * &2 pow 24)`
  CHOOSE_TAC THENL
   [ALL_TAC;
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `abs(VAL(a) - x)` THEN
    ASM_REWRITE_TAC[] THEN MATCH_MP_TAC ERROR_AT_WORST_LEMMA THEN
    ASM_REWRITE_TAC[] THEN MATCH_MP_TAC REAL_LT_TRANS THEN
    EXISTS_TAC `inv(&2 pow k)` THEN ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `inv(&1)` THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_INV2 THEN REWRITE_TAC[REAL_LT_01] THEN
      MATCH_MP_TAC REAL_POW_LE_1 THEN REAL_ARITH_TAC;
      REWRITE_TAC[threshold; float_format; bias; emax] THEN
      REWRITE_TAC[expwidth; fracwidth] THEN
      REWRITE_TAC[ARITH] THEN CONV_TAC REAL_RAT_REDUCE_CONV]] THEN
  MP_TAC(SPEC `x * &2 pow (SUC k)` ERROR_BOUND_LEMMA5) THEN
  W(C SUBGOAL_THEN MP_TAC o lhand o lhand o snd) THENL
   [REWRITE_TAC[REAL_ABS_DIV; REAL_ABS_POW; REAL_ABS_NUM] THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN
      EXISTS_TAC `inv(&2 pow (SUC k))` THEN CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_INV THEN MATCH_MP_TAC REAL_POW_LT THEN
        REAL_ARITH_TAC;
        REWRITE_TAC[REAL_MUL_LID; real_div; GSYM REAL_MUL_ASSOC] THEN
        MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `abs(x:real)` THEN
        ASM_REWRITE_TAC[] THEN MATCH_MP_TAC REAL_EQ_IMP_LE THEN
        REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NUM] THEN
        REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
        GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THEN
        AP_TERM_TAC THEN CONV_TAC SYM_CONV THEN
        MATCH_MP_TAC REAL_MUL_RINV THEN
        REWRITE_TAC[REAL_POW_EQ_0; REAL_OF_NUM_EQ; ARITH]];
      MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN
      EXISTS_TAC `inv(&2 pow (SUC k))` THEN CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_INV THEN MATCH_MP_TAC REAL_POW_LT THEN
        REAL_ARITH_TAC;
        REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NUM] THEN
        REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
        SUBGOAL_THEN `&2 pow SUC k * inv(&2 pow SUC k) = &1` SUBST1_TAC THENL
         [MATCH_MP_TAC REAL_MUL_RINV THEN
          REWRITE_TAC[REAL_POW_EQ_0; REAL_OF_NUM_EQ; ARITH];
          REWRITE_TAC[REAL_MUL_RID; real_pow; REAL_INV_MUL]] THEN
        REWRITE_TAC[REAL_MUL_ASSOC; REAL_RAT_REDUCE_CONV `&2 * inv(&2)`] THEN
        ASM_REWRITE_TAC[REAL_MUL_LID]]];
    DISCH_THEN(fun th -> REWRITE_TAC[th])] THEN
  DISCH_THEN(X_CHOOSE_THEN `s:num` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `e:num` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `f:num` MP_TAC) THEN
  DISCH_THEN(REPEAT_TCL CONJUNCTS_THEN ASSUME_TAC) THEN
  EXISTS_TAC `float(s,e - SUC k,f)` THEN
  SUBGOAL_THEN `ISFINITE(float(s,e - SUC k,f)) /\
                is_valid(float_format)(s,e - SUC k,f)` STRIP_ASSUME_TAC THENL
   [MATCH_MP_TAC ISFINITE_LEMMA THEN ASM_REWRITE_TAC[] THEN
    FIRST_ASSUM(DISJ_CASES_THEN STRIP_ASSUME_TAC) THEN
    ASM_REWRITE_TAC[bias; float_format; expwidth] THEN
    REWRITE_TAC[ARITH] THEN ARITH_TAC; ALL_TAC] THEN
  ASM_REWRITE_TAC[] THEN REWRITE_TAC[VAL] THEN
  FIRST_ASSUM(SUBST1_TAC o GEN_REWRITE_RULE I [float_tybij]) THEN
  SUBGOAL_THEN `SUC k < e`
  (fun th -> REWRITE_TAC[MATCH_MP VALOF_SCALE_DOWN th]) THENL
   [FIRST_ASSUM(DISJ_CASES_THEN STRIP_ASSUME_TAC) THEN
    ASM_REWRITE_TAC[bias; float_format; expwidth] THEN
    REWRITE_TAC[ARITH] THEN UNDISCH_TAC `k < 126` THEN ARITH_TAC;
    ALL_TAC] THEN
  MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
  EXISTS_TAC `&2 pow (SUC k)` THEN CONJ_TAC THENL
   [MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC; ALL_TAC] THEN
  SUBGOAL_THEN `!x. &2 pow (SUC k) * abs(x) = abs(&2 pow (SUC k) * x)`
  (fun th -> REWRITE_TAC[th]) THENL
   [REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_INV;
      REAL_ABS_POW; REAL_ABS_NUM]; ALL_TAC] THEN
  REWRITE_TAC[REAL_SUB_LDISTRIB; REAL_MUL_ASSOC; REAL_INV_MUL] THEN
  SUBGOAL_THEN `&2 pow (SUC k) * inv(&2 pow (SUC k)) = &1` SUBST1_TAC THENL
   [MATCH_MP_TAC REAL_MUL_RINV THEN
    MATCH_MP_TAC REAL_LT_IMP_NZ THEN
    MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC; ALL_TAC] THEN
  REWRITE_TAC[REAL_MUL_LID] THEN
  UNDISCH_TAC `abs(VAL(float(s,e,f)) - x * &2 pow SUC k)
               <= inv (&2 pow 24)` THEN
  REWRITE_TAC[VAL] THEN
  SUBGOAL_THEN `defloat(float(s,e,f)) = s,e,f`
    (fun th -> REWRITE_TAC[th; REAL_MUL_AC]) THEN
  REWRITE_TAC[GSYM float_tybij] THEN
  REWRITE_TAC[is_valid; emax; bias; expwidth; fracwidth; float_format] THEN
  ASM_REWRITE_TAC[] THEN
  FIRST_ASSUM(DISJ_CASES_THEN STRIP_ASSUME_TAC) THEN
  ASM_REWRITE_TAC[bias; expwidth; float_format] THEN
  REWRITE_TAC[ARITH] THEN CONV_TAC REAL_RAT_REDUCE_CONV);;

let ERROR_BOUND_TINY = prove
 (`!x. abs(x) < inv(&2 pow 126)
       ==> abs(error(x)) <= inv(&2 pow 150)`,
  REPEAT STRIP_TAC THEN SUBGOAL_THEN
    `?a. ISFINITE(a) /\ abs(VAL(a) - x) <= inv(&2 pow 150)`
  CHOOSE_TAC THENL
   [ALL_TAC;
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `abs(VAL(a) - x)` THEN
    ASM_REWRITE_TAC[] THEN MATCH_MP_TAC ERROR_AT_WORST_LEMMA THEN
    ASM_REWRITE_TAC[] THEN MATCH_MP_TAC REAL_LT_TRANS THEN
    EXISTS_TAC `inv(&2 pow 126)` THEN ASM_REWRITE_TAC[] THEN
    REWRITE_TAC[threshold; float_format; bias; emax] THEN
    REWRITE_TAC[expwidth; fracwidth] THEN
    REWRITE_TAC[ARITH] THEN CONV_TAC REAL_RAT_REDUCE_CONV] THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP ERROR_BOUND_LEMMA8) THEN
  DISCH_THEN(X_CHOOSE_THEN `s:num` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `e:num` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `f:num` MP_TAC) THEN
  DISCH_THEN(REPEAT_TCL CONJUNCTS_THEN ASSUME_TAC) THEN
  EXISTS_TAC `float(s,e,f)` THEN
  SUBGOAL_THEN `ISFINITE(float(s,e,f)) /\
                is_valid(float_format)(s,e,f)` STRIP_ASSUME_TAC THENL
   [MATCH_MP_TAC ISFINITE_LEMMA THEN ASM_REWRITE_TAC[] THEN
    FIRST_ASSUM DISJ_CASES_TAC THEN ASM_REWRITE_TAC[ARITH];
    ASM_REWRITE_TAC[VAL] THEN
    FIRST_ASSUM(SUBST1_TAC o GEN_REWRITE_RULE I [float_tybij])]);;

(* ------------------------------------------------------------------------- *)
(* Stronger versions not requiring exact location of the interval.           *)
(* ------------------------------------------------------------------------- *)

let ERROR_BOUND_NORM_STRONG = prove
 (`!x j. abs(x) < threshold(float_format) /\
         abs(x) < (&2 pow (SUC j) / &2 pow 126)
         ==> abs(error(x)) <= &2 pow j / &2 pow 150`,
  GEN_TAC THEN INDUCT_TAC THENL
   [REWRITE_TAC[ARITH] THEN REWRITE_TAC[real_pow; REAL_POW_1] THEN
    REWRITE_TAC[real_div; REAL_MUL_LID] THEN DISCH_TAC THEN
    ASM_CASES_TAC `abs(x) < inv(&2 pow 126)` THENL
     [MATCH_MP_TAC ERROR_BOUND_TINY THEN ASM_REWRITE_TAC[];
      MP_TAC(SPECL [`125`; `x:real`] ERROR_BOUND_SMALL) THEN
      REWRITE_TAC[GSYM REAL_POW_ADD] THEN REWRITE_TAC[ARITH] THEN
      DISCH_THEN MATCH_MP_TAC THEN
      ASM_REWRITE_TAC[GSYM REAL_NOT_LT] THEN
      MATCH_MP_TAC REAL_LTE_TRANS THEN EXISTS_TAC `&2 * inv(&2 pow 126)` THEN
      ASM_REWRITE_TAC[] THEN CONV_TAC REAL_RAT_REDUCE_CONV];
    STRIP_TAC THEN ASM_CASES_TAC `abs x < &2 pow SUC j / &2 pow 126` THENL
     [MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&2 pow j / &2 pow 150` THEN
      CONJ_TAC THENL
       [FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[];
        REWRITE_TAC[real_div] THEN MATCH_MP_TAC REAL_LE_RMUL THEN
        CONV_TAC REAL_RAT_REDUCE_CONV THEN
        MATCH_MP_TAC REAL_POW_MONO THEN REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
        ARITH_TAC];
      RULE_ASSUM_TAC(REWRITE_RULE[REAL_NOT_LT]) THEN
      ASM_CASES_TAC `j <= 124` THENL
       [MP_TAC(SPECL [`124 - j`; `x:real`] ERROR_BOUND_SMALL) THEN
        REWRITE_TAC[GSYM REAL_POW_ADD] THEN REWRITE_TAC[ARITH] THEN
        SUBGOAL_THEN `inv (&2 pow (SUC (124 - j) + 24)) =
                      &2 pow SUC j / &2 pow 150`
        SUBST1_TAC THENL
         [SUBGOAL_THEN `SUC(124 - j) + 24 = 150 - SUC j` SUBST1_TAC THENL
           [UNDISCH_TAC `j <= 124` THEN ARITH_TAC; ALL_TAC] THEN
          SUBGOAL_THEN `150 = (150 - SUC j) + SUC j` MP_TAC THENL
           [UNDISCH_TAC `j <= 124` THEN ARITH_TAC; ALL_TAC] THEN
          DISCH_THEN(fun th ->
            GEN_REWRITE_TAC (RAND_CONV o ONCE_DEPTH_CONV) [th]) THEN
          REWRITE_TAC[REAL_POW_ADD] THEN REWRITE_TAC[real_div] THEN
          ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
          REWRITE_TAC[GSYM REAL_MUL_ASSOC; REAL_INV_MUL] THEN
          GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THEN AP_TERM_TAC THEN
          CONV_TAC SYM_CONV THEN MATCH_MP_TAC REAL_MUL_LINV THEN
          REWRITE_TAC[REAL_POW_EQ_0; REAL_OF_NUM_EQ; ARITH]; ALL_TAC] THEN
        DISCH_THEN MATCH_MP_TAC THEN REPEAT CONJ_TAC THENL
         [MATCH_MP_TAC REAL_LE_TRANS THEN
          EXISTS_TAC `&2 pow SUC j / &2 pow 126` THEN
          ASM_REWRITE_TAC[] THEN
          SUBGOAL_THEN `126 = (124 - j) + SUC(SUC j)` SUBST1_TAC THENL
           [UNDISCH_TAC `j <= 124` THEN ARITH_TAC; ALL_TAC] THEN
          REWRITE_TAC[REAL_POW_ADD; real_pow; REAL_INV_MUL; real_div] THEN
          ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
          MATCH_MP_TAC REAL_EQ_IMP_LE THEN
          REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN AP_TERM_TAC THEN
          SUBGOAL_THEN `(inv(&2) * &2) * (inv(&2 pow j) * &2 pow j) = &1`
          MP_TAC THENL
           [CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN
            REWRITE_TAC[REAL_MUL_LID] THEN
            MATCH_MP_TAC REAL_MUL_LINV THEN
            REWRITE_TAC[REAL_POW_EQ_0; REAL_OF_NUM_EQ; ARITH];
            DISCH_THEN(ASSUME_TAC o REWRITE_RULE[REAL_MUL_AC]) THEN
            GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THEN AP_TERM_TAC THEN
            ASM_REWRITE_TAC[REAL_MUL_AC]];
          MATCH_MP_TAC REAL_LTE_TRANS THEN
          EXISTS_TAC `&2 pow SUC(SUC j) / &2 pow 126` THEN
          ASM_REWRITE_TAC[] THEN
          SUBGOAL_THEN `126 = (124 - j) + SUC(SUC j)` SUBST1_TAC THENL
           [UNDISCH_TAC `j <= 124` THEN ARITH_TAC; ALL_TAC] THEN
          REWRITE_TAC[real_div; REAL_INV_MUL; REAL_POW_ADD] THEN
          MATCH_MP_TAC REAL_EQ_IMP_LE THEN
          ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
          GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_RID] THEN
          REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN AP_TERM_TAC THEN
          MATCH_MP_TAC REAL_MUL_LINV THEN
          REWRITE_TAC[REAL_POW_EQ_0; REAL_OF_NUM_EQ; ARITH];
          ARITH_TAC];
        MP_TAC(SPECL [`SUC j - 126`; `x:real`] ERROR_BOUND_BIG) THEN
        ASM_REWRITE_TAC[] THEN
        SUBGOAL_THEN
          `&2 pow (SUC j - 126) / &2 pow 24 = &2 pow SUC j / &2 pow 150`
        SUBST1_TAC THENL
         [SUBGOAL_THEN `?d. j = 125 + d` (CHOOSE_THEN SUBST1_TAC) THENL
           [EXISTS_TAC `j - 125` THEN UNDISCH_TAC `~(j <= 124)` THEN
            ARITH_TAC; ALL_TAC] THEN
          SUBGOAL_THEN `SUC(125 + d) - 126 = d` SUBST1_TAC THENL
           [ARITH_TAC; ALL_TAC] THEN
          SUBGOAL_THEN `SUC(125 + d) = 126 + d` SUBST1_TAC THENL
           [ARITH_TAC; ALL_TAC] THEN
          ONCE_REWRITE_TAC[ADD_SYM] THEN
          REWRITE_TAC[REAL_POW_ADD; real_div] THEN
          REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN AP_TERM_TAC THEN
          CONV_TAC REAL_RAT_REDUCE_CONV;
          DISCH_THEN MATCH_MP_TAC] THEN
        CONJ_TAC THENL
         [MATCH_MP_TAC REAL_LE_TRANS THEN
          EXISTS_TAC `&2 pow SUC j / &2 pow 126` THEN
          ASM_REWRITE_TAC[] THEN
          SUBGOAL_THEN `?d. j = 125 + d` (CHOOSE_THEN SUBST1_TAC) THENL
           [EXISTS_TAC `j - 125` THEN UNDISCH_TAC `~(j <= 124)` THEN
            ARITH_TAC; ALL_TAC] THEN
          SUBGOAL_THEN `SUC(125 + d) - 126 = d` SUBST1_TAC THENL
           [ARITH_TAC; ALL_TAC] THEN
          SUBGOAL_THEN `SUC(125 + d) = 126 + d` SUBST1_TAC THENL
           [ARITH_TAC; ALL_TAC] THEN
          ONCE_REWRITE_TAC[ADD_SYM] THEN
          REWRITE_TAC[REAL_POW_ADD; real_div] THEN
          REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
          MATCH_MP_TAC REAL_EQ_IMP_LE THEN
          GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THEN
          AP_TERM_TAC THEN CONV_TAC REAL_RAT_REDUCE_CONV;
          MATCH_MP_TAC REAL_LTE_TRANS THEN
          EXISTS_TAC `&2 pow SUC(SUC j) / &2 pow 126` THEN
          ASM_REWRITE_TAC[] THEN
          SUBGOAL_THEN `?d. j = 125 + d` (CHOOSE_THEN SUBST1_TAC) THENL
           [EXISTS_TAC `j - 125` THEN UNDISCH_TAC `~(j <= 124)` THEN
            ARITH_TAC; ALL_TAC] THEN
          SUBGOAL_THEN `SUC(SUC (125 + d) - 126) = SUC d` SUBST1_TAC THENL
           [ARITH_TAC; ALL_TAC] THEN
          SUBGOAL_THEN `SUC(SUC (125 + d)) = SUC d + 126` SUBST1_TAC THENL
           [ARITH_TAC; ALL_TAC] THEN
          REWRITE_TAC[REAL_POW_ADD; real_div] THEN
          REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
          MATCH_MP_TAC REAL_EQ_IMP_LE THEN
          GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_RID] THEN
          AP_TERM_TAC THEN CONV_TAC REAL_RAT_REDUCE_CONV]]]]);;

(* ------------------------------------------------------------------------- *)
(* We also want to ensure that the result is actually finite!                *)
(* ------------------------------------------------------------------------- *)

let DEFLOAT_FLOAT_ZEROSIGN_ROUND_FINITE = prove
 (`!b x. abs(x) < threshold(float_format)
         ==> is_finite(float_format)
               (defloat(float(zerosign(float_format) b
                  (round float_format To_nearest x))))`,
  REPEAT GEN_TAC THEN
  REWRITE_TAC[REAL_ARITH `abs(x:real) < y <=> ~(x <= --y) /\ ~(x >= y)`] THEN
  STRIP_TAC THEN ASM_REWRITE_TAC[round] THEN
  SUBGOAL_THEN `is_finite(float_format)
                 (zerosign float_format b
                    (closest (valof float_format) (\a. EVEN (fraction a))
                             {a | is_finite float_format a} x))`
  ASSUME_TAC THENL
   [REWRITE_TAC[zerosign] THEN
    REPEAT (COND_CASES_TAC THEN REWRITE_TAC[]) THENL
     [REWRITE_TAC[IS_FINITE_EXPLICIT; plus_zero] THEN
      REWRITE_TAC[float_format; sign; exponent; fraction] THEN
      REWRITE_TAC[ARITH];
      REWRITE_TAC[IS_FINITE_EXPLICIT; minus_zero] THEN
      REWRITE_TAC[float_format; sign; exponent; fraction] THEN
      REWRITE_TAC[ARITH];
      MATCH_ACCEPT_TAC IS_FINITE_CLOSEST];
    SUBGOAL_THEN `!X x. is_finite(X) x ==> is_valid(X) x` MP_TAC THENL
     [REWRITE_TAC[is_finite] THEN MESON_TAC[];
      DISCH_THEN(fun th -> FIRST_ASSUM(ASSUME_TAC o MATCH_MP th))] THEN
    FIRST_ASSUM(fun th ->
      REWRITE_TAC[GEN_REWRITE_RULE I [float_tybij] th]) THEN
    ASM_REWRITE_TAC[]]);;

(* ------------------------------------------------------------------------- *)
(* Lifting of arithmetic operations.                                         *)
(* ------------------------------------------------------------------------- *)

let FLOAT_ADD = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         abs(VAL(a) + VAL(b)) < threshold(float_format)
         ==> ISFINITE(a + b) /\
             (VAL(a + b) = (VAL(a) + VAL(b)) + error(VAL(a) + VAL(b)))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a)) /\
                ~(ISNAN(b)) /\ ~(INFINITY(b))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISFINITE_THM; ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC] THEN
  UNDISCH_TAC `abs(VAL(a) + VAL(b)) < threshold(float_format)` THEN
  ASM_REWRITE_TAC[add_f; fadd; VAL] THEN
  REWRITE_TAC[VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
  REWRITE_TAC[error; VAL; DEFLOAT_FLOAT_ROUND] THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  DISCH_TAC THEN CONJ_TAC THENL
   [MATCH_MP_TAC DEFLOAT_FLOAT_ZEROSIGN_ROUND_FINITE THEN ASM_REWRITE_TAC[];
    REAL_ARITH_TAC]);;

let FLOAT_SUB = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         abs(VAL(a) - VAL(b)) < threshold(float_format)
         ==> ISFINITE(a - b) /\
             (VAL(a - b) = (VAL(a) - VAL(b)) + error(VAL(a) - VAL(b)))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a)) /\
                ~(ISNAN(b)) /\ ~(INFINITY(b))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISFINITE_THM; ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC] THEN
  UNDISCH_TAC `abs(VAL(a) - VAL(b)) < threshold(float_format)` THEN
  ASM_REWRITE_TAC[sub_f; fsub; VAL] THEN
  REWRITE_TAC[VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
  REWRITE_TAC[error; VAL; DEFLOAT_FLOAT_ROUND] THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  DISCH_TAC THEN CONJ_TAC THENL
   [MATCH_MP_TAC DEFLOAT_FLOAT_ZEROSIGN_ROUND_FINITE THEN ASM_REWRITE_TAC[];
    REAL_ARITH_TAC]);;

let FLOAT_MUL = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         abs(VAL(a) * VAL(b)) < threshold(float_format)
         ==> ISFINITE(a * b) /\
             (VAL(a * b) = (VAL(a) * VAL(b)) + error(VAL(a) * VAL(b)))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a)) /\
                ~(ISNAN(b)) /\ ~(INFINITY(b))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISFINITE_THM; ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC] THEN
  UNDISCH_TAC `abs(VAL(a) * VAL(b)) < threshold(float_format)` THEN
  ASM_REWRITE_TAC[mul_f; fmul; VAL] THEN
  REWRITE_TAC[VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
  REWRITE_TAC[error; VAL; DEFLOAT_FLOAT_ROUND] THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  DISCH_TAC THEN CONJ_TAC THENL
   [MATCH_MP_TAC DEFLOAT_FLOAT_ZEROSIGN_ROUND_FINITE THEN ASM_REWRITE_TAC[];
    REAL_ARITH_TAC]);;

let FLOAT_DIV = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\ ~(ISZERO(b)) /\
         abs(VAL(a) / VAL(b)) < threshold(float_format)
         ==> ISFINITE(a / b) /\
             (VAL(a / b) = (VAL(a) / VAL(b)) + error(VAL(a) / VAL(b)))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a)) /\
                ~(ISNAN(b)) /\ ~(INFINITY(b))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISFINITE_THM; ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC] THEN
  UNDISCH_TAC `abs(VAL(a) / VAL(b)) < threshold(float_format)` THEN
  ASM_REWRITE_TAC[div_f; fdiv; VAL] THEN
  UNDISCH_TAC `~(ISZERO(b))` THEN REWRITE_TAC[ISZERO] THEN
  DISCH_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
  REWRITE_TAC[error; VAL; DEFLOAT_FLOAT_ROUND] THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  DISCH_TAC THEN CONJ_TAC THENL
   [MATCH_MP_TAC DEFLOAT_FLOAT_ZEROSIGN_ROUND_FINITE THEN ASM_REWRITE_TAC[];
    REAL_ARITH_TAC]);;

let FLOAT_REM = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\ ~(ISZERO(b)) /\
         abs(VAL(a) irem VAL(b)) < threshold(float_format)
         ==> ISFINITE(a % b) /\
             (VAL(a % b) = (VAL(a) irem VAL(b)) + error(VAL(a) irem VAL(b)))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a)) /\
                ~(ISNAN(b)) /\ ~(INFINITY(b))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISFINITE_THM; ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC] THEN
  UNDISCH_TAC `abs(VAL(a) irem VAL(b)) < threshold(float_format)` THEN
  ASM_REWRITE_TAC[rem_f; frem; VAL] THEN
  UNDISCH_TAC `~(ISZERO(b))` THEN REWRITE_TAC[ISZERO] THEN
  DISCH_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
  REWRITE_TAC[error; VAL; DEFLOAT_FLOAT_ROUND] THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  DISCH_TAC THEN CONJ_TAC THENL
   [MATCH_MP_TAC DEFLOAT_FLOAT_ZEROSIGN_ROUND_FINITE THEN ASM_REWRITE_TAC[];
    REAL_ARITH_TAC]);;

let FLOAT_SQRT = prove
 (`!a. ISFINITE(a) /\ VAL(a) >= &0 /\
       abs(sqrt(VAL(a))) < threshold(float_format)
       ==> ISFINITE(sqrt(a)) /\
           (VAL(sqrt(a)) = sqrt(VAL(a)) + error(sqrt(VAL(a))))`,
  GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC] THEN
  ASM_REWRITE_TAC[sqrt_f; fsqrt] THEN
  ASM_CASES_TAC `is_zero(float_format) (defloat a)` THEN
  ASM_REWRITE_TAC[] THENL
   [SUBGOAL_THEN `(defloat a = 0,0,0) \/ (defloat a = 1,0,0)` MP_TAC THENL
     [UNDISCH_TAC `is_zero(float_format) (defloat a)` THEN
      REWRITE_TAC[is_zero] THEN MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
      SPEC_TAC(`defloat a`,`z:num#num#num`) THEN
      GEN_TAC THEN SUBST1_TAC(SYM(REWRITE_CONV[PAIR]
        `FST(z):num,FST(SND(z)):num,SND(SND(z)):num`)) THEN
      PURE_REWRITE_TAC[exponent; is_valid; fraction; FST; SND; PAIR_EQ] THEN
      DISCH_THEN(MP_TAC o CONJUNCT1) THEN REWRITE_TAC[] THEN
      REWRITE_TAC[num_CONV `2`; num_CONV `1`; LT] THEN
      REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[];
      REWRITE_TAC[VAL]] THEN
    SUBGOAL_THEN `is_valid(float_format) (0,0,0) /\
                  is_valid(float_format)(1,0,0)`
    STRIP_ASSUME_TAC THENL
     [REWRITE_TAC[is_valid; float_format; expwidth; fracwidth] THEN
      REWRITE_TAC[EXP_LT_0] THEN REWRITE_TAC[ARITH]; ALL_TAC] THEN
    REWRITE_TAC[ISFINITE_THM] THEN
    DISCH_THEN(DISJ_CASES_THEN SUBST_ALL_TAC) THEN
    EVERY_ASSUM(fun th -> try SUBST1_TAC(GEN_REWRITE_RULE I [float_tybij] th)
                          with Failure _ -> ALL_TAC) THEN
    REWRITE_TAC[IS_FINITE_EXPLICIT; valof; float_format] THEN
    REWRITE_TAC[sign; exponent; fraction; bias; fracwidth; expwidth] THEN
    REWRITE_TAC[ARITH] THEN
    REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_MUL_RZERO] THEN
    REWRITE_TAC[SQRT_0; REAL_ADD_LID] THEN CONV_TAC SYM_CONV THEN
    MATCH_MP_TAC ERROR_IS_ZERO THEN
    EXISTS_TAC `float(0,0,0)` THEN
    REWRITE_TAC[ISFINITE_THM; IS_FINITE_EXPLICIT; VAL] THEN
    EVERY_ASSUM(fun th -> try SUBST1_TAC(GEN_REWRITE_RULE I [float_tybij] th)
                          with Failure _ -> ALL_TAC) THEN
    REWRITE_TAC[sign; exponent; fraction; EXP_LT_0] THEN
    REWRITE_TAC[valof; float_format] THEN
    REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_MUL_RZERO] THEN
    REWRITE_TAC[ARITH]; ALL_TAC] THEN
  ASM_CASES_TAC `sign(defloat a) = 1` THEN ASM_REWRITE_TAC[] THENL
   [UNDISCH_TAC `VAL(a) >= &0` THEN CONV_TAC CONTRAPOS_CONV THEN
    DISCH_THEN(K ALL_TAC) THEN REWRITE_TAC[real_ge; REAL_NOT_LE] THEN
    REWRITE_TAC[VAL] THEN UNDISCH_TAC `~is_zero float_format (defloat a)` THEN
    REWRITE_TAC[VALOF; is_zero; float_format] THEN
    COND_CASES_TAC THEN ASM_REWRITE_TAC[] THENL
     [DISCH_TAC THEN ASM_REWRITE_TAC[fracwidth; bias; expwidth] THEN
      REWRITE_TAC[ARITH] THEN REWRITE_TAC[REAL_POW_1] THEN
      REWRITE_TAC[REAL_MUL_LNEG; real_div] THEN
      REWRITE_TAC[REAL_ARITH `--x < &0 <=> &0 < x`; REAL_MUL_LID] THEN
      REPEAT(MATCH_MP_TAC REAL_LT_MUL THEN CONJ_TAC) THEN
      TRY(MATCH_MP_TAC REAL_LT_INV) THEN CONV_TAC REAL_RAT_REDUCE_CONV THEN
      UNDISCH_TAC `~(fraction(defloat a) = 0)` THEN
      REWRITE_TAC[REAL_OF_NUM_LT] THEN ARITH_TAC;
      ASM_REWRITE_TAC[fracwidth; bias; expwidth] THEN
      REWRITE_TAC[ARITH] THEN REWRITE_TAC[REAL_POW_1] THEN
      REWRITE_TAC[REAL_MUL_LNEG; real_div] THEN
      REWRITE_TAC[REAL_ARITH `--x < &0 <=> &0 < x`; REAL_MUL_LID] THEN
      REPEAT(MATCH_MP_TAC REAL_LT_MUL THEN CONJ_TAC) THEN
      TRY(MATCH_MP_TAC REAL_LT_INV) THEN CONV_TAC REAL_RAT_REDUCE_CONV THEN
      REWRITE_TAC[REAL_OF_NUM_LT; REAL_OF_NUM_POW; EXP_LT_0] THEN
      REWRITE_TAC[ARITH] THEN
      MATCH_MP_TAC REAL_LTE_ADD THEN CONJ_TAC THENL
       [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
      MATCH_MP_TAC REAL_LE_MUL THEN CONJ_TAC THENL
       [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
      REWRITE_TAC[REAL_POS]];
    ALL_TAC] THEN
  CONJ_TAC THENL
   [REWRITE_TAC[ISFINITE_THM] THEN
    MATCH_MP_TAC DEFLOAT_FLOAT_ZEROSIGN_ROUND_FINITE THEN
    ASM_REWRITE_TAC[GSYM VAL];
    REWRITE_TAC[VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
    REWRITE_TAC[error; VAL; DEFLOAT_FLOAT_ROUND] THEN
    REWRITE_TAC[VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
    REAL_ARITH_TAC]);;

let FLOAT_NEG = prove
 (`!a. ISFINITE(a) ==> ISFINITE(--a) /\ (VAL(--a) = --(VAL(a)))`,
  let lemma = prove
   (`n < 2 ==> (--(&1) pow (1 - n) = --(--(&1) pow n))`,
    REWRITE_TAC[num_CONV `2`; num_CONV `1`; LT] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[real_pow; SUB] THEN
    REWRITE_TAC[ARITH; real_pow] THEN REAL_ARITH_TAC) in
  GEN_TAC THEN DISCH_TAC THEN REWRITE_TAC[neg_f; fneg] THEN
  SUBGOAL_THEN `ISFINITE (float (1 - sign (defloat a),
                         (exponent (defloat a),fraction (defloat a)))) /\
                is_valid(float_format) (1 - sign (defloat a),
                         (exponent (defloat a),fraction (defloat a)))`
  STRIP_ASSUME_TAC THENL
   [MATCH_MP_TAC ISFINITE_LEMMA THEN UNDISCH_TAC `ISFINITE a` THEN
    REWRITE_TAC[IS_FINITE_EXPLICIT; ISFINITE_THM] THEN DISCH_TAC THEN
    ASM_REWRITE_TAC[ARITH] THEN ARITH_TAC;
    ASM_REWRITE_TAC[VAL] THEN
    FIRST_ASSUM(SUBST1_TAC o GEN_REWRITE_RULE I [float_tybij]) THEN
    REWRITE_TAC[VALOF; exponent] THEN COND_CASES_TAC THEN
    ASM_REWRITE_TAC[] THEN REWRITE_TAC[fraction; sign] THEN
    REWRITE_TAC[REAL_MUL_ASSOC; real_div; GSYM REAL_MUL_LNEG] THEN
    REPEAT(AP_THM_TAC THEN AP_TERM_TAC) THEN
    MATCH_MP_TAC lemma THEN
    REWRITE_TAC[REWRITE_RULE[IS_VALID] (SPEC `a:float` IS_VALID_DEFLOAT)]]);;

let FLOAT_ABS = prove
 (`!a. ISFINITE(a) ==> ISFINITE(abs(a)) /\ (VAL(abs(a)) = abs(VAL(a)))`,
  GEN_TAC THEN DISCH_TAC THEN REWRITE_TAC[real_abs; abs_f] THEN
  SUBGOAL_THEN `ISFINITE(a) /\ ISFINITE(PLUS_ZERO)` MP_TAC THENL
   [ASM_REWRITE_TAC[] THEN REWRITE_TAC[PLUS_ZERO; plus_zero] THEN
    CONV_TAC ISFINITE_FLOAT_CONV;
    DISCH_THEN(ASSUME_TAC o MATCH_MP FLOAT_GE)] THEN
  ASM_REWRITE_TAC[] THEN
  SUBGOAL_THEN `VAL PLUS_ZERO = &0` SUBST1_TAC THENL
   [REWRITE_TAC[PLUS_ZERO; plus_zero] THEN
    CONV_TAC(LAND_CONV VAL_FLOAT_CONV) THEN REFL_TAC;
    REWRITE_TAC[real_ge]] THEN
  COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
  ASM_MESON_TAC[FLOAT_NEG]);;

(* ------------------------------------------------------------------------- *)
(* A special case of perfect rounding: few enough significant digits.        *)
(* ------------------------------------------------------------------------- *)

let ERROR_ISZERO_SIGDIGITS_LEMMA1 = prove
 (`!p x e k. inv(&2 pow 126) <= x /\ x < threshold(float_format) /\
             (x = (&2 pow e / &2 pow 150) * &k) /\
             p <= 24 /\ k < 2 EXP p
             ==> ?a. ISFINITE a /\ (VAL a = x)`,
  INDUCT_TAC THENL
   [REPEAT GEN_TAC THEN REWRITE_TAC[ARITH] THEN
    REWRITE_TAC[num_CONV `1`; LT] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[REAL_MUL_RZERO] THEN
    EXISTS_TAC `PLUS_ZERO` THEN REWRITE_TAC[PLUS_ZERO; plus_zero] THEN
    CONV_TAC(LAND_CONV(EQT_INTRO o ISFINITE_FLOAT_CONV)) THEN
    CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN REWRITE_TAC[];
    REPEAT STRIP_TAC THEN ASM_CASES_TAC `k < 2 EXP p` THENL
     [FIRST_ASSUM MATCH_MP_TAC THEN
      MAP_EVERY EXISTS_TAC [`e:num`; `k:num`] THEN
      REPEAT CONJ_TAC THEN TRY(FIRST_ASSUM ACCEPT_TAC) THEN
      UNDISCH_TAC `SUC p <= 24` THEN ARITH_TAC; ALL_TAC] THEN
    EXISTS_TAC `float(0,(e + p) - 23,k * 2 EXP (23 - p) - 2 EXP 23)` THEN
    SUBGOAL_THEN
      `is_finite(float_format) (0,(e + p) - 23,k * 2 EXP (23 - p) - 2 EXP 23)`
    MP_TAC THENL
     [REWRITE_TAC[IS_FINITE_EXPLICIT; exponent; sign; fraction] THEN
      REWRITE_TAC[ARITH] THEN CONJ_TAC THENL
       [MATCH_MP_TAC(ARITH_RULE `e + p < 278 ==> (e + p) - 23 < 255`) THEN
        UNDISCH_TAC `x < threshold float_format` THEN
        ASM_REWRITE_TAC[] THEN
        CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[REAL_NOT_LT; NOT_LT] THEN
        DISCH_TAC THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
        MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN
        EXISTS_TAC `&2 pow 150` THEN
        CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
        REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
        CONV_TAC(RAND_CONV REAL_RAT_REDUCE_CONV) THEN
        REWRITE_TAC[REAL_MUL_RID] THEN
        MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN
        EXISTS_TAC `&2 pow p` THEN
        REWRITE_TAC[GSYM REAL_MUL_ASSOC; GSYM REAL_POW_ADD] THEN
        CONJ_TAC THENL [MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
          ALL_TAC] THEN
        MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&k * &2 pow 278` THEN
        CONJ_TAC THENL
         [MATCH_MP_TAC REAL_LE_TRANS THEN
          EXISTS_TAC `&(2 EXP p) * &2 pow 278` THEN CONJ_TAC THENL
           [GEN_REWRITE_TAC RAND_CONV [REAL_MUL_SYM] THEN
            REWRITE_TAC[REAL_POW_ADD; GSYM REAL_OF_NUM_POW] THEN
            REWRITE_TAC[REAL_MUL_ASSOC] THEN MATCH_MP_TAC REAL_LE_RMUL THEN
            REWRITE_TAC[FLOAT_THRESHOLD_EXPLICIT] THEN
            CONV_TAC REAL_RAT_REDUCE_CONV THEN
            MATCH_MP_TAC REAL_POW_LE THEN REAL_ARITH_TAC;
            MATCH_MP_TAC REAL_LE_RMUL THEN
            ASM_REWRITE_TAC[REAL_OF_NUM_LE; GSYM NOT_LT] THEN
            MATCH_MP_TAC REAL_POW_LE THEN REAL_ARITH_TAC];
          MATCH_MP_TAC REAL_LE_LMUL THEN REWRITE_TAC[REAL_POS] THEN
          MATCH_MP_TAC REAL_POW_MONO THEN ASM_REWRITE_TAC[REAL_OF_NUM_LE] THEN
          REWRITE_TAC[ARITH]];
        MATCH_MP_TAC(ARITH_RULE `a < 2 * b ==> a - b < b`) THEN
        MATCH_MP_TAC LTE_TRANS THEN EXISTS_TAC `2 EXP 24` THEN CONJ_TAC THENL
         [ALL_TAC; REWRITE_TAC[ARITH]] THEN
        UNDISCH_TAC `k < 2 EXP (SUC p)` THEN
        UNDISCH_TAC `SUC p <= 24` THEN
        REWRITE_TAC[num_CONV `24`; LE_SUC] THEN
        REWRITE_TAC[LE_EXISTS] THEN
        DISCH_THEN(CHOOSE_THEN SUBST1_TAC) THEN
        REWRITE_TAC[ADD_SUB2; EXP; EXP_ADD] THEN
        DISCH_TAC THEN REWRITE_TAC[MULT_ASSOC; LT_MULT_RCANCEL] THEN
        ASM_REWRITE_TAC[EXP_EQ_0; ARITH]]; ALL_TAC] THEN
    DISCH_THEN(fun th -> ASSUME_TAC th THEN MP_TAC th) THEN
    REWRITE_TAC[is_finite; float_tybij] THEN
    REWRITE_TAC[VAL; ISFINITE_THM] THEN DISCH_THEN(SUBST1_TAC o CONJUNCT1) THEN
    ASM_REWRITE_TAC[valof; SUB_EQ_0] THEN

    SUBGOAL_THEN `23 < e + p` ASSUME_TAC THENL
     [UNDISCH_TAC `inv (&2 pow 126) <= x` THEN ASM_REWRITE_TAC[] THEN
      CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[NOT_LT; REAL_NOT_LE] THEN
      DISCH_TAC THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
      MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN
      EXISTS_TAC `&2 pow 150` THEN
      CONV_TAC(RAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
      REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      REWRITE_TAC[REAL_MUL_RID] THEN MATCH_MP_TAC REAL_LTE_TRANS THEN
      EXISTS_TAC `&(2 EXP (SUC p)) * &2 pow e` THEN CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_RMUL THEN
        ASM_REWRITE_TAC[REAL_OF_NUM_LT] THEN
        MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
        REWRITE_TAC[real_pow; GSYM REAL_OF_NUM_POW] THEN
        MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&2 * &2 pow 23` THEN
        CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
        REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
        MATCH_MP_TAC REAL_LE_LMUL THEN
        REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
        REWRITE_TAC[GSYM REAL_POW_ADD] THEN
        MATCH_MP_TAC REAL_POW_MONO THEN
        ONCE_REWRITE_TAC[ADD_SYM] THEN
        ASM_REWRITE_TAC[] THEN REAL_ARITH_TAC];
      ASM_REWRITE_TAC[GSYM NOT_LT]] THEN
    REWRITE_TAC[real_pow; REAL_MUL_LID; bias; float_format] THEN
    REWRITE_TAC[expwidth; fracwidth] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_ADD; GSYM REAL_OF_NUM_MUL] THEN
    SUBGOAL_THEN `&2 pow ((e + p) - 23) = &2 pow (e + p) / &2 pow 23`
    SUBST1_TAC THENL
     [MATCH_MP_TAC REAL_POW2_SUB THEN MATCH_MP_TAC LT_IMP_LE THEN
      ASM_REWRITE_TAC[]; ALL_TAC] THEN
    SUBGOAL_THEN `&(k * 2 EXP (23 - p) - 2 EXP 23) =
                  &k * &2 pow 23 / &2 pow p - &2 pow 23`
    SUBST1_TAC THENL
     [UNDISCH_TAC `SUC p <= 24` THEN
      REWRITE_TAC[num_CONV `24`; LE_SUC] THEN
      DISCH_THEN(fun th -> ASSUME_TAC th THEN MP_TAC th) THEN
      DISCH_THEN(fun th -> REWRITE_TAC[GSYM(MATCH_MP REAL_POW2_SUB th)]) THEN
      REWRITE_TAC[REAL_OF_NUM_ADD; REAL_OF_NUM_MUL; REAL_OF_NUM_POW] THEN
      MATCH_MP_TAC(GSYM REAL_OF_NUM_SUB) THEN
      UNDISCH_TAC `~(k < 2 EXP p)` THEN REWRITE_TAC[NOT_LT] THEN
      UNDISCH_TAC `p <= 23` THEN
      DISCH_THEN(STRIP_ASSUME_TAC o REWRITE_RULE[LE_EXISTS]) THEN
      ASM_REWRITE_TAC[ADD_SUB2; EXP_ADD; LE_MULT_RCANCEL] THEN
      REWRITE_TAC[EXP_EQ_0; ARITH];
      REWRITE_TAC[ARITH] THEN
      REWRITE_TAC[real_div; REAL_SUB_RDISTRIB] THEN
      REWRITE_TAC[REAL_SUB_LDISTRIB; REAL_ADD_LDISTRIB] THEN
      REWRITE_TAC[REAL_MUL_RID; GSYM REAL_MUL_ASSOC] THEN
      MATCH_MP_TAC(REAL_ARITH
       `(a = c) /\ (b = d:real) ==> (a + (b - c) = d)`) THEN
      CONJ_TAC THENL
       [AP_TERM_TAC THEN CONV_TAC REAL_RAT_REDUCE_CONV;
        REWRITE_TAC[REAL_POW_ADD; GSYM REAL_MUL_ASSOC] THEN
        AP_TERM_TAC THEN
        GEN_REWRITE_TAC LAND_CONV [AC REAL_MUL_AC
          `a * b * c * d * e * f * g= ((a * f) * c * b * e * g) * d:real`] THEN
        AP_THM_TAC THEN AP_TERM_TAC THEN
        GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_LID] THEN
        BINOP_TAC THENL
         [MATCH_MP_TAC REAL_MUL_RINV THEN REWRITE_TAC[REAL_POW_EQ_0] THEN
          REWRITE_TAC[REAL_OF_NUM_EQ; ARITH];
          CONV_TAC REAL_RAT_REDUCE_CONV]]]]);;

let ERROR_ISZERO_SIGDIGITS_LEMMA2 = prove
 (`!p x e k. inv(&2 pow 126) <= abs(x) /\ abs(x) < threshold(float_format) /\
             (abs(x) = (&2 pow e / &2 pow 150) * &k) /\
             p <= 24 /\ k < 2 EXP p
             ==> ?a. ISFINITE a /\ (VAL a = x)`,
  REPEAT GEN_TAC THEN
  DISCH_THEN(MP_TAC o MATCH_MP ERROR_ISZERO_SIGDIGITS_LEMMA1) THEN
  DISCH_THEN(X_CHOOSE_THEN `a:float` MP_TAC) THEN ASM_CASES_TAC `&0 <= x` THEN
  ASM_REWRITE_TAC[real_abs] THEN STRIP_TAC THENL
   [EXISTS_TAC `a:float` THEN ASM_REWRITE_TAC[];
    EXISTS_TAC `--(a:float)` THEN ASM_MESON_TAC[FLOAT_NEG; REAL_NEG_NEG]]);;

let ERROR_ISZERO_SIGDIGITS_LEMMA3 = prove
 (`!p x e k. inv(&2 pow 126) <= abs(x) /\ abs(x) < threshold(float_format) /\
             (abs(x) = (&2 pow e / &2 pow 150) * &k) /\
             k < 2 EXP 24
             ==> ?a. ISFINITE a /\ (VAL a = x)`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC ERROR_ISZERO_SIGDIGITS_LEMMA2 THEN
  MAP_EVERY EXISTS_TAC [`24`; `e:num`; `k:num`] THEN
  ASM_REWRITE_TAC[LE_REFL]);;

let ERROR_ISZERO_SIGDIGITS_LEMMA4 = prove
 (`!x e k. abs(x) < threshold(float_format) /\
           (abs(x) = (&2 pow e / &2 pow 149) * &k) /\
           k < 2 EXP 24
           ==> ?a. ISFINITE a /\ (VAL a = x)`,
  REPEAT STRIP_TAC THEN
  ASM_CASES_TAC `inv(&2 pow 126) <= abs(x)` THENL
   [MATCH_MP_TAC ERROR_ISZERO_SIGDIGITS_LEMMA3 THEN
    EXISTS_TAC `SUC e` THEN EXISTS_TAC `k:num` THEN
    ASM_REWRITE_TAC[] THEN AP_THM_TAC THEN AP_TERM_TAC THEN
    REWRITE_TAC[real_pow] THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
    AP_TERM_TAC THEN CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
  EXISTS_TAC `float((if x < &0 then 1 else 0),0,2 EXP e * k)` THEN
  MP_TAC(SPECL [`if x < &0 then 1 else 0`; `0`] ISFINITE_LEMMA) THEN
  DISCH_THEN(MP_TAC o SPEC `2 EXP e * k`) THEN
  W(C SUBGOAL_THEN MP_TAC o lhand o lhand o snd) THENL
   [REWRITE_TAC[ARITH] THEN
    SUBGOAL_THEN `(if x < &0 then 1 else 0) < 2` ASSUME_TAC THENL
     [COND_CASES_TAC THEN REWRITE_TAC[ARITH]; ASM_REWRITE_TAC[]] THEN
    UNDISCH_TAC `~(inv (&2 pow 126) <= abs x)` THEN
    CONV_TAC CONTRAPOS_CONV THEN ASM_REWRITE_TAC[NOT_LT] THEN
    DISCH_TAC THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 149` THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC; real_div] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    UNDISCH_TAC `8388608 <= 2 EXP e * k` THEN
    REWRITE_TAC[REAL_OF_NUM_MUL; REAL_OF_NUM_POW] THEN
    REWRITE_TAC[MULT_CLAUSES; REAL_OF_NUM_LE] THEN
    REWRITE_TAC[MULT_AC; REAL_LE_REFL];
    DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
    DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
    ASM_REWRITE_TAC[float_tybij; VAL] THEN
    DISCH_THEN SUBST1_TAC THEN REWRITE_TAC[valof] THEN
    REWRITE_TAC[float_format; bias; expwidth; fracwidth] THEN
    REWRITE_TAC[ARITH]] THEN
  COND_CASES_TAC THEN REWRITE_TAC[real_pow] THENL
   [UNDISCH_TAC `abs x = &2 pow e / &2 pow 149 * &k` THEN
    ASM_REWRITE_TAC[GSYM REAL_NOT_LT; real_abs] THEN
    REWRITE_TAC[REAL_ARITH `(--x = y:real) <=> (x = --y)`] THEN
    DISCH_THEN SUBST1_TAC THEN
    REWRITE_TAC[real_div; REAL_MUL_ASSOC] THEN
    ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    REWRITE_TAC[REAL_POW_1; REAL_MUL_RNEG; REAL_MUL_LNEG] THEN
    AP_TERM_TAC THEN REWRITE_TAC[REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    REWRITE_TAC[real_div; REAL_MUL_LID] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_MUL; GSYM REAL_OF_NUM_POW] THEN
    REWRITE_TAC[REAL_MUL_AC];
    UNDISCH_TAC `abs x = &2 pow e / &2 pow 149 * &k` THEN
    ASM_REWRITE_TAC[GSYM REAL_NOT_LT; real_abs] THEN
    DISCH_THEN SUBST1_TAC THEN REWRITE_TAC[REAL_MUL_LID] THEN
    REWRITE_TAC[real_div; REAL_MUL_ASSOC] THEN
    ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    REWRITE_TAC[REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    REWRITE_TAC[real_div; REAL_MUL_LID] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_MUL; GSYM REAL_OF_NUM_POW] THEN
    REWRITE_TAC[REAL_MUL_AC]]);;

let EXISTS_EXACT_SIGDIGITS = prove
 (`!x e k. (abs(x) = (&2 pow e / &2 pow 149) * &k) /\
           k < 2 EXP 24 /\ e < 254
           ==> ?a. ISFINITE(a) /\ (VAL(a) = x)`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC ERROR_ISZERO_SIGDIGITS_LEMMA4 THEN
  MAP_EVERY EXISTS_TAC [`e:num`; `k:num`] THEN ASM_REWRITE_TAC[] THEN
  ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN
  EXISTS_TAC `&2 pow 149` THEN
  REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC; FLOAT_THRESHOLD_EXPLICIT] THEN
  CONV_TAC REAL_RAT_REDUCE_CONV THEN REWRITE_TAC[REAL_MUL_RID] THEN
  MATCH_MP_TAC REAL_LET_TRANS THEN
  EXISTS_TAC `(&2 pow 24 - &1) * &2 pow 253` THEN
  CONJ_TAC THENL
   [MATCH_MP_TAC REAL_LE_MUL2 THEN
    REWRITE_TAC[REAL_POS; REAL_OF_NUM_POW] THEN
    CONJ_TAC THEN REWRITE_TAC[REAL_LE_SUB_LADD] THEN
    REWRITE_TAC[REAL_OF_NUM_ADD; GSYM ADD1; REAL_OF_NUM_LE] THEN
    ASM_REWRITE_TAC[LE_SUC_LT] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_LE; GSYM REAL_OF_NUM_POW] THEN
    MATCH_MP_TAC REAL_POW_MONO THEN
    REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
    UNDISCH_TAC `e < 254` THEN ARITH_TAC;
    REWRITE_TAC[REAL_SUB_LDISTRIB; REAL_SUB_RDISTRIB] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV]);;

let ERROR_ISZERO_SIGDIGITS = prove
 (`!x e k. (abs(x) = (&2 pow e / &2 pow 149) * &k) /\
           k < 2 EXP 24 /\ e < 254
           ==> (error(x) = &0)`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC ERROR_IS_ZERO THEN
  MATCH_MP_TAC EXISTS_EXACT_SIGDIGITS THEN EXISTS_TAC `e:num` THEN
  EXISTS_TAC `k:num` THEN ASM_REWRITE_TAC[]);;

(* ------------------------------------------------------------------------- *)
(* Another special case: "a - b" where b / 2 <= a <= 2 * b                   *)
(* ------------------------------------------------------------------------- *)

let ERROR_IS_ZERO_SUB = prove
 (`!a b c. ISFINITE(a) /\ ISFINITE(b) /\
           ISFINITE(c) /\ (VAL(c) = VAL(a) - VAL(b))
           ==> ISFINITE(a - b) /\ (VAL(a - b) = VAL(a) - VAL(b))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  MP_TAC(SPEC `c:float` VAL_THRESHOLD) THEN
  ASM_REWRITE_TAC[] THEN DISCH_TAC THEN
  SUBGOAL_THEN `ISFINITE(a - b)` ASSUME_TAC THENL
   [ASM_MESON_TAC[FLOAT_SUB]; ASM_REWRITE_TAC[]] THEN
  MP_TAC(SPECL [`a:float`; `b:float`] FLOAT_SUB) THEN
  ASM_REWRITE_TAC[] THEN DISCH_THEN SUBST1_TAC THEN
  SUBGOAL_THEN `error(VAL(a) - VAL(b)) = &0`
   (fun th -> REWRITE_TAC[th; REAL_ADD_RID]) THEN
  MATCH_MP_TAC ERROR_IS_ZERO THEN
  EXISTS_TAC `c:float` THEN ASM_REWRITE_TAC[]);;

let FLOAT_SUB_CANCEL_LEMMA0 = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         (sign(defloat a) = sign(defloat b)) /\
         (exponent(defloat a) = exponent(defloat b)) /\
         fraction(defloat b) <= fraction(defloat a)
         ==> ISFINITE(a - b) /\ (VAL(a - b) = VAL(a) - VAL(b))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN MATCH_MP_TAC ERROR_IS_ZERO_SUB THEN
  SUBST1_TAC(REWRITE_CONV[VAL; VALOF] `VAL(a)`) THEN
  SUBST1_TAC(REWRITE_CONV[VAL; VALOF] `VAL(b)`) THEN
  ASM_CASES_TAC `exponent(defloat b) = 0` THEN ASM_REWRITE_TAC[] THENL
   [REWRITE_TAC[GSYM REAL_SUB_LDISTRIB] THEN
    REWRITE_TAC[real_div; GSYM REAL_SUB_RDISTRIB] THEN
    REWRITE_TAC[GSYM real_div] THEN
    EXISTS_TAC `float(sign (defloat b),0,
                      (fraction (defloat a)) - (fraction (defloat b)))` THEN
    MP_TAC(SPECL [`sign (defloat b)`; `0`] ISFINITE_LEMMA) THEN
    DISCH_THEN(MP_TAC o SPEC `fraction(defloat a) - fraction(defloat b)`) THEN
    W(C SUBGOAL_THEN MP_TAC o lhand o lhand o snd) THENL
     [REWRITE_TAC[ARITH] THEN MP_TAC(ASSUME `ISFINITE(b)`) THEN
      REWRITE_TAC[ISFINITE_THM; IS_FINITE_EXPLICIT] THEN
      STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
      MP_TAC(ASSUME `ISFINITE(a)`) THEN
      REWRITE_TAC[ISFINITE_THM; IS_FINITE_EXPLICIT] THEN STRIP_TAC THEN
      MATCH_MP_TAC(ARITH_RULE `a < c /\ b < c ==> a - b < c:num`) THEN
      ASM_REWRITE_TAC[];
      STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
      DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
      ASM_REWRITE_TAC[VAL] THEN REWRITE_TAC[float_tybij] THEN
      DISCH_THEN SUBST1_TAC THEN REWRITE_TAC[valof] THEN
      REPEAT(AP_TERM_TAC ORELSE AP_THM_TAC) THEN
      MATCH_MP_TAC(GSYM REAL_OF_NUM_SUB) THEN
      ASM_REWRITE_TAC[]];
    REWRITE_TAC[REAL_ADD_LDISTRIB] THEN
    REWRITE_TAC[REAL_ARITH `(a + b) - (a + c) = b - c:real`] THEN
    REWRITE_TAC[GSYM REAL_SUB_LDISTRIB] THEN
    REWRITE_TAC[real_div; GSYM REAL_SUB_RDISTRIB] THEN
    REWRITE_TAC[GSYM real_div] THEN
    MATCH_MP_TAC EXISTS_EXACT_SIGDIGITS THEN
    REWRITE_TAC[fracwidth; expwidth; bias; float_format] THEN
    EXISTS_TAC `PRE(exponent(defloat b))` THEN
    EXISTS_TAC `fraction(defloat a) - fraction(defloat b)` THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_NEG; REAL_ABS_POW; REAL_ABS_NUM] THEN
    REWRITE_TAC[REAL_POW_ONE; REAL_MUL_LID] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_MUL] THEN
    FIRST_ASSUM(fun th -> REWRITE_TAC[GSYM(MATCH_MP REAL_OF_NUM_SUB th)]) THEN
    REPEAT CONJ_TAC THENL
     [UNDISCH_TAC `~(exponent (defloat b) = 0)` THEN
      SPEC_TAC(`exponent (defloat b)`,`p:num`) THEN
      INDUCT_TAC THEN REWRITE_TAC[NOT_SUC; PRE] THEN
      REWRITE_TAC[real_div; REAL_ABS_MUL] THEN
      SUBGOAL_THEN `abs(&(fraction(defloat a)) - &(fraction(defloat b))) =
                    &(fraction(defloat a)) - &(fraction(defloat b))`
      SUBST1_TAC THENL
       [REWRITE_TAC[REAL_ABS_REFL; REAL_LE_SUB_LADD] THEN
        ASM_REWRITE_TAC[REAL_ADD_LID; REAL_OF_NUM_LE]; ALL_TAC] THEN
      REWRITE_TAC[ARITH] THEN
      REWRITE_TAC[REAL_ABS_MUL; real_pow; REAL_ABS_INV] THEN
      REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NUM] THEN
      REWRITE_TAC[AC REAL_MUL_AC
        `((a * b) * c) * d * e = b * (a * c * e) * d:real`] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      REWRITE_TAC[REAL_MUL_AC];
      MATCH_MP_TAC LET_TRANS THEN EXISTS_TAC `fraction(defloat a)` THEN
      REWRITE_TAC[ARITH_RULE `a:num - b <= a`] THEN
      MATCH_MP_TAC LT_TRANS THEN EXISTS_TAC `2 EXP 23` THEN CONJ_TAC THENL
       [MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
        REWRITE_TAC[IS_VALID; fracwidth; float_format] THEN
        STRIP_TAC THEN ASM_REWRITE_TAC[];
        REWRITE_TAC[ARITH]];
      MATCH_MP_TAC(ARITH_RULE `~(b = 0) /\ a < SUC b ==> PRE a < b`) THEN
      REWRITE_TAC[ARITH] THEN
      UNDISCH_TAC `ISFINITE(b)` THEN
      REWRITE_TAC[ISFINITE_THM; IS_FINITE_EXPLICIT] THEN
      STRIP_TAC THEN ASM_REWRITE_TAC[]]]);;

let FLOAT_SUB_CANCEL_LEMMA1 = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         (sign(defloat a) = sign(defloat b)) /\
         (exponent(defloat a) = exponent(defloat b))
         ==> ISFINITE(a - b) /\ (VAL(a - b) = VAL(a) - VAL(b))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  DISJ_CASES_TAC(SPECL [`fraction(defloat b)`; `fraction(defloat a)`]
  LE_CASES) THENL
   [MATCH_MP_TAC FLOAT_SUB_CANCEL_LEMMA0 THEN ASM_REWRITE_TAC[];
    MATCH_MP_TAC ERROR_IS_ZERO_SUB THEN
    EXISTS_TAC `--(b - a:float)` THEN ASM_REWRITE_TAC[] THEN
    SUBGOAL_THEN `ISFINITE(b - a) /\ (VAL(b - a) = VAL(b) - VAL(a))`
    MP_TAC THENL
     [MATCH_MP_TAC FLOAT_SUB_CANCEL_LEMMA0 THEN ASM_REWRITE_TAC[];
      MESON_TAC[FLOAT_NEG; REAL_NEG_SUB]]]);;

let FLOAT_SUB_CANCEL_LEMMA2a = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\ abs(VAL(b)) <= abs(VAL(a))
         ==> exponent(defloat b) <= exponent(defloat a)`,
  REPEAT STRIP_TAC THEN UNDISCH_TAC `abs(VAL(b)) <= abs(VAL(a))` THEN
  CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[NOT_LE; REAL_NOT_LE] THEN
  DISCH_TAC THEN REWRITE_TAC[VAL; VALOF] THEN
  SUBGOAL_THEN `~(exponent(defloat b) = 0)` ASSUME_TAC THENL
   [UNDISCH_TAC `exponent (defloat a) < exponent (defloat b)` THEN ARITH_TAC;
    ASM_REWRITE_TAC[]] THEN
  COND_CASES_TAC THEN REWRITE_TAC[REAL_ABS_MUL; real_div] THEN
  REWRITE_TAC[REAL_ABS_POW; REAL_ABS_INV; REAL_ABS_NUM; REAL_ABS_NEG] THEN
  REWRITE_TAC[REAL_POW_ONE; REAL_MUL_LID] THEN
  REWRITE_TAC[bias; expwidth; fracwidth; float_format] THEN
  REWRITE_TAC[ARITH] THENL
   [ONCE_REWRITE_TAC
      [AC REAL_MUL_AC `(a:real * b) * c = b * a * c`] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    MATCH_MP_TAC REAL_LT_LMUL THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 23` THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    SUBGOAL_THEN `abs(&1 + &(fraction (defloat b)) * inv(&2 pow 23)) =
                  &1 + &(fraction (defloat b)) * inv(&2 pow 23)`
    SUBST1_TAC THENL
     [REWRITE_TAC[REAL_ABS_REFL] THEN MATCH_MP_TAC REAL_LE_ADD THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
      MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_POS] THEN
      MATCH_MP_TAC REAL_LE_INV THEN MATCH_MP_TAC REAL_POW_LE THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH];
      REWRITE_TAC[REAL_ADD_LDISTRIB; REAL_ADD_RDISTRIB]] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN REWRITE_TAC[REAL_MUL_RID] THEN
    MATCH_MP_TAC(REAL_ARITH `a < b /\ &0 <= c ==> a < b + c`) THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LTE_TRANS THEN EXISTS_TAC `&2 * &2 pow 23` THEN
      CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_LMUL THEN REWRITE_TAC[REAL_OF_NUM_LT; ARITH] THEN
        MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
        REWRITE_TAC[IS_VALID; fracwidth; float_format] THEN
        STRIP_TAC THEN ASM_REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_LT];
        MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&2 * &8388608` THEN
        CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
        MATCH_MP_TAC REAL_LE_RMUL THEN REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
        MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&2 pow 1` THEN
        CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
        MATCH_MP_TAC REAL_POW_MONO THEN REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
        UNDISCH_TAC `~(exponent (defloat b) = 0)` THEN ARITH_TAC];
      MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_OF_NUM_POW; REAL_POS]];
    ONCE_REWRITE_TAC
      [AC REAL_MUL_AC `(a:real * b) * c = b * a * c`] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    MATCH_MP_TAC REAL_LT_LMUL THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    SUBGOAL_THEN `!b. abs(&1 + &(fraction (defloat b)) * inv(&2 pow 23)) =
                       &1 + &(fraction (defloat b)) * inv(&2 pow 23)`
    (fun th -> REWRITE_TAC[th]) THENL
     [GEN_TAC THEN REWRITE_TAC[REAL_ABS_REFL] THEN
      MATCH_MP_TAC REAL_LE_ADD THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
      MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_POS] THEN
      MATCH_MP_TAC REAL_LE_INV THEN MATCH_MP_TAC REAL_POW_LE THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH];
      MATCH_MP_TAC REAL_LTE_TRANS THEN
      EXISTS_TAC `&2 pow (SUC(exponent(defloat a))) *
                  (&1 + &(fraction (defloat b)) * inv (&2 pow 23))` THEN
      CONJ_TAC THENL
       [REWRITE_TAC[real_pow] THEN
        GEN_REWRITE_TAC (RAND_CONV o LAND_CONV) [REAL_MUL_SYM] THEN
        REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
        MATCH_MP_TAC REAL_LT_LMUL THEN CONJ_TAC THENL
         [MATCH_MP_TAC REAL_POW_LT THEN
          REWRITE_TAC[REAL_OF_NUM_LT; ARITH]; ALL_TAC] THEN
        MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN
        EXISTS_TAC `&2 pow 23` THEN
        REWRITE_TAC[REAL_ADD_RDISTRIB; GSYM REAL_MUL_ASSOC] THEN
        CONV_TAC REAL_RAT_REDUCE_CONV THEN
        REWRITE_TAC[REAL_MUL_RID; REAL_ADD_LDISTRIB] THEN
        MATCH_MP_TAC REAL_LTE_TRANS THEN EXISTS_TAC `&2 * &8388608` THEN
        CONJ_TAC THENL
         [REWRITE_TAC[REAL_MUL_2; REAL_LT_LADD; REAL_OF_NUM_LT] THEN
          MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
          REWRITE_TAC[IS_VALID; fracwidth; float_format; ARITH] THEN
          STRIP_TAC THEN ASM_REWRITE_TAC[];
          REWRITE_TAC[REAL_LE_ADDR] THEN
          REWRITE_TAC[REAL_OF_NUM_MUL; REAL_POS]];
        MATCH_MP_TAC REAL_LE_RMUL THEN CONJ_TAC THENL
         [MATCH_MP_TAC REAL_POW_MONO THEN
          ASM_REWRITE_TAC[LE_SUC_LT] THEN
          REWRITE_TAC[REAL_OF_NUM_LE; ARITH];
          MATCH_MP_TAC REAL_LE_ADD THEN REWRITE_TAC[REAL_LE_01] THEN
          MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_POS] THEN
          MATCH_MP_TAC REAL_LE_INV THEN
          REWRITE_TAC[REAL_OF_NUM_POW; REAL_POS]]]]]);;

let FLOAT_SUB_CANCEL_LEMMA2 = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         abs(VAL(b)) <= abs(VAL(a)) /\ abs(VAL(a)) <= &2 * abs(VAL(b))
         ==> (exponent(defloat a) = exponent(defloat b)) \/
             (exponent(defloat a) = SUC(exponent(defloat b)))`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC
    (ARITH_RULE `n <= m /\ m <= SUC n ==> (m = n) \/ (m = SUC n)`) THEN
  CONJ_TAC THENL
   [MATCH_MP_TAC FLOAT_SUB_CANCEL_LEMMA2a THEN ASM_REWRITE_TAC[];
    UNDISCH_TAC `abs (VAL a) <= &2 * abs (VAL b)` THEN
    CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[NOT_LE; REAL_NOT_LE]] THEN
  DISCH_TAC THEN REWRITE_TAC[VAL; VALOF] THEN
  SUBGOAL_THEN `~(exponent(defloat a) = 0)` ASSUME_TAC THENL
   [UNDISCH_TAC `SUC(exponent (defloat b)) < exponent (defloat a)` THEN
    ARITH_TAC; ASM_REWRITE_TAC[]] THEN
  COND_CASES_TAC THEN REWRITE_TAC[REAL_ABS_MUL; real_div] THEN
  REWRITE_TAC[REAL_ABS_POW; REAL_ABS_INV; REAL_ABS_NUM; REAL_ABS_NEG] THEN
  REWRITE_TAC[REAL_POW_ONE; REAL_MUL_LID] THEN
  REWRITE_TAC[bias; expwidth; fracwidth; float_format] THEN
  REWRITE_TAC[ARITH] THENL
   [GEN_REWRITE_TAC LAND_CONV
      [AC REAL_MUL_AC `&2 * (a * b) * c * d = b * c * (&2 * a) * d`] THEN
    GEN_REWRITE_TAC RAND_CONV
      [AC REAL_MUL_AC `(a:real * b) * c = b * a * c`] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    MATCH_MP_TAC REAL_LT_LMUL THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 23` THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    SUBGOAL_THEN `abs(&1 + &(fraction (defloat a)) * inv(&2 pow 23)) =
                  &1 + &(fraction (defloat a)) * inv(&2 pow 23)`
    SUBST1_TAC THENL
     [REWRITE_TAC[REAL_ABS_REFL] THEN MATCH_MP_TAC REAL_LE_ADD THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
      MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_POS] THEN
      MATCH_MP_TAC REAL_LE_INV THEN MATCH_MP_TAC REAL_POW_LE THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH];
      REWRITE_TAC[REAL_ADD_LDISTRIB; REAL_ADD_RDISTRIB]] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN REWRITE_TAC[REAL_MUL_RID] THEN
    MATCH_MP_TAC(REAL_ARITH `a < b /\ &0 <= c ==> a < b + c`) THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LTE_TRANS THEN EXISTS_TAC `&2 pow 23 * &4` THEN
      CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_RMUL THEN REWRITE_TAC[REAL_OF_NUM_LT; ARITH] THEN
        MP_TAC(SPEC `b:float` IS_VALID_DEFLOAT) THEN
        REWRITE_TAC[IS_VALID; fracwidth; float_format] THEN
        STRIP_TAC THEN ASM_REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_LT];
        MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&4 * &8388608` THEN
        CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
        MATCH_MP_TAC REAL_LE_RMUL THEN REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
        MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&2 pow 2` THEN
        CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
        MATCH_MP_TAC REAL_POW_MONO THEN REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
        UNDISCH_TAC `SUC (exponent (defloat b)) < exponent (defloat a)` THEN
        ARITH_TAC];
      MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_OF_NUM_POW; REAL_POS]];
    GEN_REWRITE_TAC LAND_CONV [REAL_MUL_SYM] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    ONCE_REWRITE_TAC
      [AC REAL_MUL_AC `a:real * b * c = b * a * c`] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    MATCH_MP_TAC REAL_LT_LMUL THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    SUBGOAL_THEN `!b. abs(&1 + &(fraction (defloat b)) * inv(&2 pow 23)) =
                       &1 + &(fraction (defloat b)) * inv(&2 pow 23)`
    (fun th -> REWRITE_TAC[th]) THENL
     [GEN_TAC THEN REWRITE_TAC[REAL_ABS_REFL] THEN
      MATCH_MP_TAC REAL_LE_ADD THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
      MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_POS] THEN
      MATCH_MP_TAC REAL_LE_INV THEN MATCH_MP_TAC REAL_POW_LE THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH];
      MATCH_MP_TAC REAL_LTE_TRANS THEN
      EXISTS_TAC `&2 pow (exponent(defloat b) + 2) *
                  (&1 + &(fraction (defloat a)) * inv (&2 pow 23))` THEN
      CONJ_TAC THENL
       [REWRITE_TAC[REAL_POW_ADD] THEN
        REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
        MATCH_MP_TAC REAL_LT_LMUL THEN CONJ_TAC THENL
         [MATCH_MP_TAC REAL_POW_LT THEN
          REWRITE_TAC[REAL_OF_NUM_LT; ARITH]; ALL_TAC] THEN
        MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN
        EXISTS_TAC `&2 pow 23` THEN
        REWRITE_TAC[REAL_ADD_RDISTRIB; GSYM REAL_MUL_ASSOC] THEN
        CONV_TAC REAL_RAT_REDUCE_CONV THEN
        REWRITE_TAC[REAL_MUL_RID; REAL_ADD_LDISTRIB] THEN
        MATCH_MP_TAC REAL_LTE_TRANS THEN EXISTS_TAC `&4 * &8388608` THEN
        CONJ_TAC THENL
         [ONCE_REWRITE_TAC[REAL_ADD_SYM] THEN
          REWRITE_TAC[GSYM REAL_LT_SUB_LADD] THEN
          MATCH_MP_TAC REAL_LTE_TRANS THEN
          EXISTS_TAC `&2 pow 23 * &2` THEN
          CONV_TAC(RAND_CONV REAL_RAT_REDUCE_CONV) THEN
          REWRITE_TAC[] THEN MATCH_MP_TAC REAL_LT_RMUL THEN
          CONV_TAC(RAND_CONV REAL_RAT_REDUCE_CONV) THEN
          MP_TAC(SPEC `b:float` IS_VALID_DEFLOAT) THEN
          REWRITE_TAC[IS_VALID; fracwidth; float_format] THEN
          STRIP_TAC THEN ASM_REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_LT];
          REWRITE_TAC[REAL_LE_ADDR] THEN
          REWRITE_TAC[REAL_OF_NUM_MUL; REAL_POS]];
        MATCH_MP_TAC REAL_LE_RMUL THEN CONJ_TAC THENL
         [MATCH_MP_TAC REAL_POW_MONO THEN
          REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
          UNDISCH_TAC `SUC(exponent(defloat b)) < exponent(defloat a)` THEN
          ARITH_TAC;
          MATCH_MP_TAC REAL_LE_ADD THEN REWRITE_TAC[REAL_LE_01] THEN
          MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_POS] THEN
          MATCH_MP_TAC REAL_LE_INV THEN
          REWRITE_TAC[REAL_OF_NUM_POW; REAL_POS]]]]]);;

let FLOAT_SIGN_CASES = prove
 (`!a. (sign(defloat a) = 0) \/ (sign(defloat a) = 1)`,
  GEN_TAC THEN MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
  REWRITE_TAC[IS_VALID] THEN DISCH_THEN(MP_TAC o CONJUNCT1) THEN
  ARITH_TAC);;

let FLOAT_SIGN = prove
 (`!a. ISFINITE(a)
       ==> (&0 < VAL(a) ==> (sign(defloat a) = 0)) /\
           (VAL(a) < &0 ==> (sign(defloat a) = 1))`,
  GEN_TAC THEN DISCH_TAC THEN
  DISJ_CASES_TAC(SPEC `a:float` FLOAT_SIGN_CASES) THEN
  ASM_REWRITE_TAC[ARITH; REAL_NOT_LT] THEN
  ASM_REWRITE_TAC[VALOF; VAL] THEN
  REWRITE_TAC[real_pow; REAL_POW_1; REAL_MUL_LNEG; REAL_MUL_LID] THEN
  REPEAT COND_CASES_TAC THEN ASM_REWRITE_TAC[real_div] THEN
  TRY(MATCH_MP_TAC(REAL_ARITH `&0 <= x ==> --x <= &0`)) THEN
  REPEAT(CONJ_TAC ORELSE
         (MATCH_MP_TAC REAL_LE_MUL THEN CONJ_TAC) ORELSE
         (MATCH_MP_TAC REAL_LE_ADD THEN CONJ_TAC) ORELSE
         MATCH_MP_TAC REAL_POW_LE ORELSE
         MATCH_MP_TAC REAL_LE_INV) THEN
  REWRITE_TAC[REAL_POS]);;

let FLOAT_SIGN_01 = prove
 (`!a b. (sign(defloat a) = 1) /\ (sign(defloat b) = 0)
         ==> VAL(a) <= VAL(b)`,
  REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[VAL; VALOF] THEN
  REWRITE_TAC[real_pow; REAL_POW_1; REAL_MUL_LNEG; REAL_MUL_LID] THEN
  REWRITE_TAC[real_div] THEN
  REPEAT COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
  MATCH_MP_TAC(REAL_ARITH `&0 <= x /\ &0 <= y ==> --x <= y`) THEN
  REPEAT(CONJ_TAC ORELSE
         (MATCH_MP_TAC REAL_LE_MUL THEN CONJ_TAC) ORELSE
         (MATCH_MP_TAC REAL_LE_ADD THEN CONJ_TAC) ORELSE
         MATCH_MP_TAC REAL_POW_LE ORELSE
         MATCH_MP_TAC REAL_LE_INV) THEN
  REWRITE_TAC[REAL_POS]);;

let FLOAT_SUB_CANCEL_LEMMA = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         VAL(b) <= VAL(a) /\ VAL(a) <= &2 * VAL(b)
         ==> ISFINITE(a - b) /\ (VAL(a - b) = VAL(a) - VAL(b))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  ASM_CASES_TAC `sign(defloat a) = sign(defloat b)` THENL
   [ALL_TAC;
    UNDISCH_TAC `~(sign (defloat a) = sign (defloat b))` THEN
    DISJ_CASES_TAC(SPEC `a:float` FLOAT_SIGN_CASES) THEN
    DISJ_CASES_TAC(SPEC `b:float` FLOAT_SIGN_CASES) THEN
    ASM_REWRITE_TAC[ARITH] THEN
    (MP_TAC(SPEC `a:float` FLOAT_SIGN) THEN
     MP_TAC(SPEC `b:float` FLOAT_SIGN) THEN
     ASM_REWRITE_TAC[ARITH; REAL_NOT_LT] THEN
     REPEAT DISCH_TAC THEN
     MATCH_MP_TAC ERROR_IS_ZERO_SUB THEN
     SUBGOAL_THEN `VAL(a) = VAL(b)` SUBST_ALL_TAC THENL
      [POP_ASSUM_LIST(MP_TAC o end_itlist CONJ) THEN REAL_ARITH_TAC;
       ASM_REWRITE_TAC[REAL_SUB_REFL] THEN
       EXISTS_TAC `PLUS_ZERO` THEN
       MP_TAC(SPECL [`0`; `0`; `0`] ISFINITE_LEMMA) THEN
       REWRITE_TAC[ARITH; PLUS_ZERO; plus_zero] THEN
       DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
       ASM_REWRITE_TAC[float_tybij; VAL] THEN
       DISCH_THEN SUBST1_TAC THEN
       REWRITE_TAC[valof; REAL_MUL_RZERO; real_div; REAL_MUL_LZERO]])] THEN
  SUBGOAL_THEN `abs(VAL(b)) <= abs(VAL(a)) /\
                abs(VAL(a)) <= &2 * abs(VAL(b))`
  ASSUME_TAC THENL
   [UNDISCH_TAC `VAL a <= &2 * VAL b` THEN
    UNDISCH_TAC `VAL b <= VAL a` THEN REAL_ARITH_TAC; ALL_TAC] THEN
  MP_TAC(SPECL [`a:float`; `b:float`] FLOAT_SUB_CANCEL_LEMMA2) THEN
  ASM_REWRITE_TAC[] THEN DISCH_THEN DISJ_CASES_TAC THENL
   [MATCH_MP_TAC FLOAT_SUB_CANCEL_LEMMA1 THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
  MATCH_MP_TAC ERROR_IS_ZERO_SUB THEN ASM_REWRITE_TAC[] THEN
  MATCH_MP_TAC EXISTS_EXACT_SIGDIGITS THEN
  ASM_CASES_TAC `exponent(defloat b) = 0` THENL
   [FIRST_ASSUM(MP_TAC o CONJUNCT1) THEN
    REWRITE_TAC[VAL; VALOF] THEN ASM_REWRITE_TAC[NOT_SUC] THEN
    REWRITE_TAC[bias; expwidth; fracwidth; float_format] THEN
    REWRITE_TAC[ARITH; REAL_POW_1] THEN
    REWRITE_TAC[real_div; GSYM REAL_SUB_RDISTRIB] THEN
    REWRITE_TAC[GSYM REAL_SUB_LDISTRIB] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_NUM; REAL_ABS_INV] THEN
    REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NUM; REAL_POW_ONE; REAL_ABS_NEG] THEN
    REWRITE_TAC[REAL_MUL_LID] THEN
    DISCH_THEN(MP_TAC o CONJ (EQT_ELIM(REAL_RAT_REDUCE_CONV
     `&0 < &2 * inv(&2 pow 127)`))) THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    ONCE_REWRITE_TAC[REAL_MUL_ASSOC] THEN
    DISCH_THEN(MP_TAC o MATCH_MP REAL_LE_LCANCEL_IMP) THEN
    REWRITE_TAC[REAL_MUL_ASSOC] THEN
    SUBST1_TAC(SYM
     (REAL_RAT_REDUCE_CONV `&2 pow 23 * inv(&2 pow 23)`)) THEN
    REWRITE_TAC[GSYM REAL_ADD_RDISTRIB] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC; REAL_ABS_MUL] THEN
    REWRITE_TAC[REAL_ABS_INV; REAL_ABS_POW; REAL_ABS_NUM] THEN
    DISCH_THEN(MP_TAC o CONJ (EQT_ELIM(REAL_RAT_REDUCE_CONV
     `&0 < inv(&2 pow 23)`))) THEN
    REWRITE_TAC[REAL_MUL_ASSOC] THEN
    DISCH_THEN(MP_TAC o MATCH_MP REAL_LE_RCANCEL_IMP) THEN
    REWRITE_TAC[GSYM REAL_SUB_RDISTRIB] THEN
    REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_ADD; REAL_ABS_NUM] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_POW; GSYM REAL_OF_NUM_ADD] THEN
    REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_ADD; REAL_OF_NUM_LE] THEN
    STRIP_TAC THEN EXISTS_TAC `0` THEN
    EXISTS_TAC `(2 EXP 23 + fraction(defloat a)) - fraction(defloat b)` THEN
    FIRST_ASSUM(fun th -> REWRITE_TAC[MATCH_MP REAL_OF_NUM_SUB th]) THEN
    REWRITE_TAC[REAL_ABS_NUM; REAL_ABS_MUL] THEN
    REWRITE_TAC[REAL_ABS_INV; REAL_ABS_NUM] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_POW; GSYM REAL_OF_NUM_MUL] THEN
    REWRITE_TAC[real_pow] THEN REPEAT CONJ_TAC THENL
     [REWRITE_TAC[REAL_MUL_ASSOC] THEN
      GEN_REWRITE_TAC LAND_CONV [REAL_MUL_SYM] THEN
      REWRITE_TAC[REAL_MUL_ASSOC] THEN AP_THM_TAC THEN AP_TERM_TAC THEN
      CONV_TAC REAL_RAT_REDUCE_CONV;
      MATCH_MP_TAC(ARITH_RULE `0 < c /\ a < b + c ==> a - b < c`) THEN
      CONJ_TAC THENL [REWRITE_TAC[ARITH]; ALL_TAC] THEN
      MATCH_MP_TAC LTE_TRANS THEN EXISTS_TAC `2 EXP 23 + 2 EXP 23` THEN
      CONJ_TAC THENL
       [REWRITE_TAC[LT_ADD_LCANCEL] THEN
        MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
        REWRITE_TAC[IS_VALID; fracwidth; float_format] THEN
        STRIP_TAC THEN ASM_REWRITE_TAC[];
        MATCH_MP_TAC LE_TRANS THEN EXISTS_TAC `16777216` THEN
        REWRITE_TAC[ARITH] THEN ARITH_TAC];
      REWRITE_TAC[ARITH]];

    FIRST_ASSUM(MP_TAC o CONJUNCT1) THEN
    REWRITE_TAC[VAL; VALOF] THEN ASM_REWRITE_TAC[NOT_SUC] THEN
    REWRITE_TAC[bias; expwidth; fracwidth; float_format] THEN
    REWRITE_TAC[ARITH; REAL_POW_1] THEN
    REWRITE_TAC[real_div; GSYM REAL_SUB_RDISTRIB] THEN
    REWRITE_TAC[GSYM REAL_SUB_LDISTRIB] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_NUM; REAL_ABS_INV] THEN
    REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NUM; REAL_POW_ONE; REAL_ABS_NEG] THEN
    REWRITE_TAC[REAL_MUL_LID] THEN
    REWRITE_TAC[real_pow] THEN
    SUBST1_TAC(SYM
     (REAL_RAT_REDUCE_CONV `&2 pow 23 * inv(&2 pow 23)`)) THEN
    REWRITE_TAC[GSYM REAL_ADD_RDISTRIB] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC; REAL_ABS_MUL] THEN
    REWRITE_TAC[REAL_ABS_INV; REAL_ABS_POW; REAL_ABS_NUM] THEN
    DISCH_THEN(MP_TAC o CONJ (EQT_ELIM(REAL_RAT_REDUCE_CONV
     `&0 < inv(&2 pow 23)`))) THEN
    REWRITE_TAC[REAL_MUL_ASSOC] THEN
    DISCH_THEN(MP_TAC o MATCH_MP REAL_LE_RCANCEL_IMP) THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    GEN_REWRITE_TAC (LAND_CONV o RAND_CONV)
     [AC REAL_MUL_AC `a:real * b * c * d = b * c * a * d`] THEN
    SUBGOAL_THEN `&0 < &2 pow (exponent(defloat b)) * inv(&2 pow 127)`
    MP_TAC THENL
     [MATCH_MP_TAC REAL_LT_MUL THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
      REWRITE_TAC[IMP_IMP]] THEN
    ONCE_REWRITE_TAC[REAL_MUL_ASSOC] THEN
    DISCH_THEN(MP_TAC o MATCH_MP REAL_LE_LCANCEL_IMP) THEN
    REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_ADD; REAL_ABS_NUM] THEN
    REWRITE_TAC[REAL_OF_NUM_MUL; REAL_OF_NUM_LE] THEN STRIP_TAC THEN
    REWRITE_TAC[REAL_MUL_ASSOC; GSYM REAL_SUB_RDISTRIB] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_MUL; GSYM REAL_OF_NUM_POW] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    REWRITE_TAC[AC REAL_MUL_AC `&2 * a * b * c = a * b * &2 * c`] THEN
    REWRITE_TAC[GSYM REAL_SUB_LDISTRIB] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW] THEN
    REWRITE_TAC[REAL_ABS_INV; REAL_ABS_NUM] THEN
    REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NUM] THEN
    REWRITE_TAC[REAL_OF_NUM_MUL] THEN
    EXISTS_TAC `PRE(exponent(defloat b))` THEN
    EXISTS_TAC `2 * (2 EXP 23 + fraction (defloat a)) -
                (2 EXP 23 + fraction (defloat b))` THEN
    REPEAT CONJ_TAC THENL
     [GEN_REWRITE_TAC LAND_CONV [REAL_MUL_SYM] THEN
      REWRITE_TAC[REAL_MUL_ASSOC] THEN BINOP_TAC THENL
       [UNDISCH_TAC `~(exponent (defloat b) = 0)` THEN
        SPEC_TAC(`exponent (defloat b)`,`p:num`) THEN
        INDUCT_TAC THEN REWRITE_TAC[NOT_SUC; PRE] THEN
        REWRITE_TAC[real_pow] THEN
        ONCE_REWRITE_TAC
         [AC REAL_MUL_AC `(a:real * b * c) * d = c * (a * b * d)`] THEN
        AP_TERM_TAC THEN CONV_TAC REAL_RAT_REDUCE_CONV;
        FIRST_ASSUM(fun th -> REWRITE_TAC[MATCH_MP REAL_OF_NUM_SUB th]) THEN
        REWRITE_TAC[REAL_ABS_REFL] THEN
        FIRST_ASSUM(fun th -> REWRITE_TAC
         [GSYM(MATCH_MP REAL_OF_NUM_SUB th)]) THEN
        REWRITE_TAC[REAL_SUB_LE] THEN
        ASM_REWRITE_TAC[REAL_OF_NUM_LE]];
      MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
      REWRITE_TAC[IS_VALID; fracwidth; float_format] THEN
      DISCH_THEN(MP_TAC o last o CONJUNCTS) THEN
      REWRITE_TAC[ARITH] THEN ALL_TAC THEN ALL_TAC;
      MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
      ASM_REWRITE_TAC[IS_VALID; expwidth; float_format] THEN
      DISCH_THEN(MP_TAC o CONJUNCT1 o CONJUNCT2) THEN
      REWRITE_TAC[ARITH] THEN ARITH_TAC] THEN
    SUBGOAL_THEN `fraction(defloat a) <= fraction(defloat b)` MP_TAC THENL
     [ALL_TAC; REWRITE_TAC[ARITH] THEN ARITH_TAC] THEN
    FIRST_ASSUM(MP_TAC o CONJUNCT2) THEN
    REWRITE_TAC[VAL; VALOF] THEN ASM_REWRITE_TAC[NOT_SUC] THEN
    REWRITE_TAC[bias; expwidth; fracwidth; float_format] THEN
    REWRITE_TAC[ARITH; REAL_POW_1] THEN
    REWRITE_TAC[real_div; GSYM REAL_SUB_RDISTRIB] THEN
    REWRITE_TAC[GSYM REAL_SUB_LDISTRIB] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_NUM; REAL_ABS_INV] THEN
    REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NUM; REAL_POW_ONE; REAL_ABS_NEG] THEN
    REWRITE_TAC[REAL_MUL_LID] THEN
    REWRITE_TAC[real_pow] THEN
    SUBST1_TAC(SYM
     (REAL_RAT_REDUCE_CONV `&2 pow 23 * inv(&2 pow 23)`)) THEN
    REWRITE_TAC[GSYM REAL_ADD_RDISTRIB] THEN
    DISCH_THEN(MP_TAC o CONJ (EQT_ELIM(REAL_RAT_REDUCE_CONV `&0 < &2`))) THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    DISCH_THEN(MP_TAC o MATCH_MP REAL_LE_LCANCEL_IMP) THEN
    REWRITE_TAC[GSYM REAL_ADD_RDISTRIB] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC; REAL_ABS_MUL] THEN
    REWRITE_TAC[REAL_ABS_INV; REAL_ABS_POW; REAL_ABS_NUM] THEN
    DISCH_THEN(MP_TAC o CONJ (EQT_ELIM(REAL_RAT_REDUCE_CONV
     `&0 < inv(&2 pow 23)`))) THEN
    REWRITE_TAC[REAL_MUL_ASSOC] THEN
    DISCH_THEN(MP_TAC o MATCH_MP REAL_LE_RCANCEL_IMP) THEN
    SUBGOAL_THEN `&0 < &2 pow (exponent(defloat b)) * inv(&2 pow 127)`
    MP_TAC THENL
     [MATCH_MP_TAC REAL_LT_MUL THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
      REWRITE_TAC[IMP_IMP]] THEN
    DISCH_THEN(MP_TAC o MATCH_MP REAL_LE_LCANCEL_IMP) THEN
    REWRITE_TAC[REAL_OF_NUM_ADD; REAL_ABS_NUM; REAL_OF_NUM_POW] THEN
    REWRITE_TAC[REAL_OF_NUM_LE; LE_ADD_LCANCEL]]);;

let FLOAT_SUB_CANCEL = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         VAL(b) / &2 <= VAL(a) /\ VAL(a) <= &2 * VAL(b)
         ==> ISFINITE(a - b) /\ (VAL(a - b) = VAL(a) - VAL(b))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  DISJ_CASES_TAC(SPECL [`VAL(a)`; `VAL(b)`] REAL_LE_TOTAL) THENL
   [ALL_TAC; MATCH_MP_TAC FLOAT_SUB_CANCEL_LEMMA THEN ASM_REWRITE_TAC[]] THEN
  SUBGOAL_THEN `abs(VAL(a) - VAL(b)) < threshold(float_format)`
  ASSUME_TAC THENL
   [MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `abs(VAL(b))` THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC(REAL_ARITH
        `a <= b /\ b <= &2 * a ==> abs(a - b) <= abs(b)`) THEN
      ASM_REWRITE_TAC[] THEN MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
      EXISTS_TAC `inv(&2)` THEN REWRITE_TAC[REAL_MUL_ASSOC] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      REWRITE_TAC[REAL_MUL_LID] THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
      REWRITE_TAC[real_div; REAL_MUL_LID] THEN
      ASM_REWRITE_TAC[GSYM real_div];
      MATCH_MP_TAC VAL_THRESHOLD THEN ASM_REWRITE_TAC[]]; ALL_TAC] THEN
  MP_TAC(SPECL [`a:float`; `b:float`] FLOAT_SUB) THEN
  ASM_REWRITE_TAC[] THEN
  DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC SUBST1_TAC) THEN
  ASM_REWRITE_TAC[] THEN
  SUBGOAL_THEN `error(VAL(a) - VAL(b)) = &0`
  (fun th -> REWRITE_TAC[th; REAL_ADD_RID]) THEN
  MATCH_MP_TAC ERROR_IS_ZERO THEN EXISTS_TAC `--(b:float - a)` THEN
  SUBGOAL_THEN `ISFINITE(b - a) /\ (VAL(b - a) = VAL(b) - VAL(a))` MP_TAC THENL
   [MATCH_MP_TAC FLOAT_SUB_CANCEL_LEMMA THEN
    ASM_REWRITE_TAC[] THEN MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
    EXISTS_TAC `inv(&2)` THEN REWRITE_TAC[REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    REWRITE_TAC[REAL_MUL_LID] THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    REWRITE_TAC[real_div; REAL_MUL_LID] THEN
    ASM_REWRITE_TAC[GSYM real_div];
    STRIP_TAC THEN GEN_REWRITE_TAC (RAND_CONV o RAND_CONV)
      [GSYM REAL_NEG_SUB] THEN
    ASM_MESON_TAC[FLOAT_NEG]]);;

let FLOAT_SUB_CANCEL_ABS = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         &2 * abs(VAL(a) - VAL(b)) <= abs(VAL(a))
         ==> ISFINITE(a - b) /\ (VAL(a - b) = VAL(a) - VAL(b))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `VAL(b) <= &2 * VAL(a) /\
                VAL(a) <= &2 * VAL(b) \/
                --(VAL(b)) <= --(&2 * VAL(a)) /\
                --(VAL(a)) <= --(&2 * VAL(b)) \/
               (VAL(a) = &0) \/ (VAL(b) = &0)`
  (REPEAT_TCL DISJ_CASES_THEN ASSUME_TAC) THENL
   [UNDISCH_TAC `&2 * abs(VAL(a) - VAL(b)) <= abs(VAL(a))` THEN REAL_ARITH_TAC;
    MATCH_MP_TAC FLOAT_SUB_CANCEL THEN ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN EXISTS_TAC `&2` THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    REWRITE_TAC[REAL_MUL_RID] THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    ASM_REWRITE_TAC[];
    MATCH_MP_TAC ERROR_IS_ZERO_SUB THEN EXISTS_TAC `--b - --(a:float)` THEN
    ASM_REWRITE_TAC[] THEN
    ONCE_REWRITE_TAC[REAL_ARITH `a:real - b = --b - --a`] THEN
    SUBGOAL_THEN `ISFINITE(--a) /\ (VAL(--a) = --(VAL a))`
    (STRIP_ASSUME_TAC o GSYM) THENL
     [MATCH_MP_TAC FLOAT_NEG THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
    SUBGOAL_THEN `ISFINITE(--b) /\ (VAL(--b) = --(VAL b))`
    (STRIP_ASSUME_TAC o GSYM) THENL
     [MATCH_MP_TAC FLOAT_NEG THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
    ASM_REWRITE_TAC[] THEN MATCH_MP_TAC FLOAT_SUB_CANCEL THEN
    ASM_REWRITE_TAC[] THEN
    UNDISCH_TAC `-- (VAL b) = VAL (-- b)` THEN
    DISCH_THEN(SUBST1_TAC o SYM) THEN
    UNDISCH_TAC `-- (VAL a) = VAL (-- a)` THEN
    DISCH_THEN(SUBST1_TAC o SYM) THEN
    ASM_REWRITE_TAC[REAL_MUL_RNEG] THEN
    MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN EXISTS_TAC `&2` THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    REWRITE_TAC[REAL_MUL_RID] THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    ASM_REWRITE_TAC[REAL_MUL_RNEG];
    MATCH_MP_TAC ERROR_IS_ZERO_SUB THEN
    ASM_REWRITE_TAC[REAL_SUB_LZERO] THEN ASM_MESON_TAC[FLOAT_NEG];
    MATCH_MP_TAC ERROR_IS_ZERO_SUB THEN
    ASM_REWRITE_TAC[REAL_SUB_RZERO] THEN ASM_MESON_TAC[]]);;

let FLOAT_SUB_CANCEL_ABS2 = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         &2 * abs(VAL(a) - VAL(b)) <= abs(VAL(b))
         ==> ISFINITE(a - b) /\ (VAL(a - b) = VAL(a) - VAL(b))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  MATCH_MP_TAC ERROR_IS_ZERO_SUB THEN
  EXISTS_TAC `--(b - a:float)` THEN
  SUBGOAL_THEN `ISFINITE(b - a) /\ (VAL(b - a) = VAL(b) - VAL(a))` MP_TAC THENL
   [MATCH_MP_TAC FLOAT_SUB_CANCEL_ABS THEN
    ONCE_REWRITE_TAC[REAL_ABS_SUB] THEN ASM_REWRITE_TAC[];
    ASM_MESON_TAC[FLOAT_NEG; REAL_NEG_SUB]]);;

(* ------------------------------------------------------------------------- *)
(* Monotonicity of rounding and some consequences.                           *)
(* ------------------------------------------------------------------------- *)

let IS_CLOSEST_MONO = prove
 (`!(v:A->real) s x y a b. x < y /\ is_closest v s x a /\ is_closest v s y b
                 ==> v(a) <= v(b)`,
  REWRITE_TAC[is_closest] THEN REPEAT STRIP_TAC THEN
  SUBGOAL_THEN `abs ((v:A->real) b - y) <= abs (v a - y) /\
                abs (v a - x) <= abs (v b - x)`
  MP_TAC THENL
   [CONJ_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[];
    UNDISCH_TAC `x:real < y` THEN REAL_ARITH_TAC]);;

let CLOSEST_MONO = prove
 (`!(v:A->real) p s x y.
        FINITE(s) /\ ~(s = EMPTY) /\ x <= y
        ==> v(closest v p s x) <= v(closest v p s y)`,
  MESON_TAC[REAL_LE_LT; CLOSEST_IS_CLOSEST; IS_CLOSEST_MONO]);;

let ROUND_MONO = prove
 (`!x y. x <= y /\
         abs(x) < threshold(float_format) /\
         abs(y) < threshold(float_format)
         ==> VAL(float(round(float_format) To_nearest x))
             <= VAL(float(round(float_format) To_nearest y))`,
  REWRITE_TAC[REAL_ARITH `abs(x:real) < b <=> ~(x <= --b) /\ ~(x >= b)`] THEN
  REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[round] THEN REWRITE_TAC[VAL] THEN
  REWRITE_TAC[REWRITE_RULE[float_tybij]
    (SPEC `float_format` IS_VALID_CLOSEST)] THEN
  MATCH_MP_TAC CLOSEST_MONO THEN ASM_REWRITE_TAC[IS_FINITE_FINITE] THEN
  REWRITE_TAC[EXTENSION] THEN DISCH_THEN(MP_TAC o SPEC `0,0,0`) THEN
  REWRITE_TAC[IN_ELIM_THM; NOT_IN_EMPTY] THEN
  REWRITE_TAC[is_finite; float_format] THEN
  REWRITE_TAC[is_zero; is_valid; fraction; exponent] THEN
  REWRITE_TAC[EXP_LT_0] THEN REWRITE_TAC[ARITH]);;

let FLOAT_ADD_MONOROUND_LEMMA = prove
 (`!a b. ISFINITE(a) /\ ISFINITE(b) /\
         abs(VAL(a) + VAL(b)) < threshold(float_format) /\
         VAL(a + b) < VAL(a) ==> VAL(a) + VAL(b) < VAL(a)`,
  REPEAT GEN_TAC THEN
  DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
  DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
  DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a)) /\
                ~(ISNAN(b)) /\ ~(INFINITY(b))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISFINITE_THM; ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC] THEN
  UNDISCH_TAC `abs(VAL(a) + VAL(b)) < threshold(float_format)` THEN
  ASM_REWRITE_TAC[add_f; fadd; VAL] THEN
  REWRITE_TAC[VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
  REWRITE_TAC[error; VAL; DEFLOAT_FLOAT_ROUND] THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  DISCH_TAC THEN CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[REAL_NOT_LT] THEN
  DISCH_TAC THEN MATCH_MP_TAC REAL_LE_TRANS THEN
  EXISTS_TAC `valof float_format
     (round float_format To_nearest (valof float_format (defloat a)))` THEN
  CONJ_TAC THENL
   [MATCH_MP_TAC(REAL_ARITH `(b - a = &0) ==> a <= b`) THEN
    MP_TAC error THEN REWRITE_TAC[VAL; DEFLOAT_FLOAT_ROUND] THEN
    DISCH_THEN(fun th -> REWRITE_TAC[GSYM th]) THEN
    MATCH_MP_TAC ERROR_IS_ZERO THEN
    EXISTS_TAC `a:float` THEN ASM_REWRITE_TAC[VAL];
    MATCH_MP_TAC(REWRITE_RULE[VAL; DEFLOAT_FLOAT_ROUND] ROUND_MONO) THEN
    ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC(REWRITE_RULE[VAL] VAL_THRESHOLD) THEN
    ASM_REWRITE_TAC[]]);;

(* ------------------------------------------------------------------------- *)
(* More lemmas.                                                              *)
(* ------------------------------------------------------------------------- *)

let ISNORMAL_VALUE = prove
 (`!a. ISNORMAL(a) <=> ISFINITE(a) /\ inv(&2 pow 126) <= abs(VAL(a))`,
  GEN_TAC THEN REWRITE_TAC[ISFINITE] THEN
  ASM_CASES_TAC `ISNORMAL(a)` THEN ASM_REWRITE_TAC[] THENL
   [UNDISCH_TAC `ISNORMAL a` THEN REWRITE_TAC[ISNORMAL; is_normal] THEN
    REWRITE_TAC[VAL; VALOF] THEN
    COND_CASES_TAC THEN ASM_REWRITE_TAC[LT_REFL] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NEG] THEN
    REWRITE_TAC[REAL_ABS_NUM; REAL_MUL_LID; REAL_POW_ONE] THEN
    REWRITE_TAC[bias; fracwidth; expwidth; float_format; emax] THEN
    REWRITE_TAC[ARITH] THEN STRIP_TAC THEN
    REWRITE_TAC[REAL_ABS_DIV; REAL_ABS_POW; REAL_ABS_NUM] THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `(&2 pow exponent (defloat a) / &2 pow 127) * &1` THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&2 pow 1 / &2 pow 127` THEN
      CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
      REWRITE_TAC[REAL_MUL_RID] THEN REWRITE_TAC[real_div] THEN
      MATCH_MP_TAC REAL_LE_RMUL THEN
      CONV_TAC(RAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
      MATCH_MP_TAC REAL_POW_MONO THEN REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
      ASM_REWRITE_TAC[GSYM NOT_LT; num_CONV `1`; LT];
      MATCH_MP_TAC REAL_LE_LMUL THEN CONJ_TAC THENL
       [REWRITE_TAC[real_div] THEN MATCH_MP_TAC REAL_LE_MUL THEN
        CONJ_TAC THEN TRY(MATCH_MP_TAC REAL_LE_INV) THEN
        MATCH_MP_TAC REAL_POW_LE THEN REAL_ARITH_TAC;
        MATCH_MP_TAC REAL_LE_TRANS THEN
        EXISTS_TAC `&1 + &(fraction (defloat a)) / &2 pow 23` THEN
        REWRITE_TAC[REAL_ABS_LE] THEN
        REWRITE_TAC[REAL_LE_ADDR] THEN
        REWRITE_TAC[real_div] THEN MATCH_MP_TAC REAL_LE_MUL THEN
        REWRITE_TAC[REAL_POS] THEN CONV_TAC REAL_RAT_REDUCE_CONV]];
    ASM_CASES_TAC `ISZERO a` THEN ASM_REWRITE_TAC[] THENL
     [UNDISCH_TAC `ISZERO a` THEN REWRITE_TAC[ISZERO; is_zero] THEN
      STRIP_TAC THEN ASM_REWRITE_TAC[VAL; VALOF] THEN
      REWRITE_TAC[real_div; REAL_MUL_RZERO; REAL_MUL_LZERO] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    ASM_CASES_TAC `ISDENORMAL a` THEN ASM_REWRITE_TAC[] THEN
    UNDISCH_TAC `ISDENORMAL a` THEN REWRITE_TAC[ISDENORMAL; is_denormal] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[VAL; VALOF] THEN
    REWRITE_TAC[bias; float_format; expwidth; fracwidth] THEN
    REWRITE_TAC[ARITH; REAL_NOT_LE] THEN
    REWRITE_TAC[real_div; REAL_ABS_MUL; REAL_ABS_POW] THEN
    REWRITE_TAC[REAL_ABS_NEG; REAL_ABS_NUM; REAL_MUL_LID; REAL_POW_ONE] THEN
    MATCH_MP_TAC REAL_LTE_TRANS THEN
    EXISTS_TAC `(&2 * abs (inv (&2 pow 127))) * &2 pow 23 *
                abs (inv (&2 pow 23))` THEN
    CONV_TAC(RAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LT_LMUL THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LT_RMUL THEN
    CONV_TAC(RAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    REWRITE_TAC[REAL_OF_NUM_LT; REAL_OF_NUM_POW] THEN
    MP_TAC(REWRITE_RULE[IS_VALID] (SPEC `a:float` IS_VALID_DEFLOAT)) THEN
    REWRITE_TAC[bias; fracwidth; expwidth; float_format; emax] THEN
    DISCH_THEN(fun th -> REWRITE_TAC[th])]);;

let ISZERO_VALUE = prove
 (`!a. ISZERO(a) <=> ISFINITE(a) /\ (VAL(a) = &0)`,
  GEN_TAC THEN REWRITE_TAC[ISFINITE] THEN
  ASM_CASES_TAC `ISZERO(a)` THEN ASM_REWRITE_TAC[] THENL
   [POP_ASSUM MP_TAC THEN REWRITE_TAC[VAL; ISZERO; is_zero] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[VALOF] THEN
    REWRITE_TAC[real_div; REAL_MUL_RZERO; REAL_MUL_LZERO];
    ASM_CASES_TAC `ISNORMAL(a)` THEN ASM_REWRITE_TAC[] THENL
     [UNDISCH_TAC `ISNORMAL(a)` THEN REWRITE_TAC[ISNORMAL_VALUE] THEN
      CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[] THEN
      DISCH_THEN SUBST1_TAC THEN REWRITE_TAC[DE_MORGAN_THM] THEN
      DISJ2_TAC THEN CONV_TAC REAL_RAT_REDUCE_CONV;
      ASM_CASES_TAC `ISDENORMAL(a)` THEN ASM_REWRITE_TAC[] THEN
      UNDISCH_TAC `ISDENORMAL a` THEN REWRITE_TAC[ISDENORMAL; is_denormal] THEN
      STRIP_TAC THEN ASM_REWRITE_TAC[VAL; VALOF] THEN
      REWRITE_TAC[REAL_ENTIRE; DE_MORGAN_THM; real_div] THEN
      REWRITE_TAC[REAL_INV_EQ_0; REAL_POW_EQ_0] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      ASM_REWRITE_TAC[REAL_OF_NUM_EQ]]]);;

let ISDENORMAL_VALUE = prove
 (`!a. ISDENORMAL(a) <=> ISFINITE(a) /\ abs(VAL(a)) < inv(&2 pow 126) /\
                         ~(VAL(a) = &0)`,
  let lemma = prove
   (`ISDENORMAL(a) <=> ISFINITE(a) /\ ~(ISNORMAL(a)) /\ ~(ISZERO(a))`,
    MESON_TAC[ISFINITE; FLOAT_CASES; FLOAT_DISTINCT]) in
  GEN_TAC THEN REWRITE_TAC[lemma] THEN
  REWRITE_TAC[ISZERO_VALUE; ISNORMAL_VALUE; GSYM REAL_NOT_LE] THEN
  CONV_TAC TAUT);;

let ISNORMAL_BININTERVAL_VALUE = prove
 (`!a. ISNORMAL(a)
       ==> &2 pow (exponent(defloat a)) / &2 pow 127 <= abs(VAL(a)) /\
           abs(VAL(a)) < &2 pow (SUC(exponent(defloat a))) / &2 pow 127`,
  GEN_TAC THEN REWRITE_TAC[ISNORMAL; is_normal] THEN REPEAT STRIP_TAC THENL
   [MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 127` THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    REWRITE_TAC[VAL; VALOF] THEN
    ASM_REWRITE_TAC[ARITH_RULE `(x = 0) <=> ~(0 < x)`] THEN
    REWRITE_TAC[bias; emax; fracwidth; expwidth; float_format] THEN
    REWRITE_TAC[ARITH] THEN REWRITE_TAC[REAL_ABS_MUL] THEN
    REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NEG; REAL_ABS_NUM;
      REAL_POW_ONE; REAL_MUL_LID] THEN
    GEN_REWRITE_TAC (RAND_CONV o LAND_CONV) [REAL_MUL_SYM] THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC; REAL_ABS_MUL] THEN
    REWRITE_TAC[REAL_ABS_INV; REAL_ABS_POW; REAL_ABS_NUM] THEN
    SUBGOAL_THEN `inv(&2 pow 127) * &2 pow 127 = &1` SUBST1_TAC THENL
     [MATCH_MP_TAC REAL_MUL_LINV THEN
      REWRITE_TAC[REAL_POW_EQ_0] THEN REWRITE_TAC[ARITH] THEN
      REWRITE_TAC[REAL_OF_NUM_EQ; ARITH]; ALL_TAC] THEN
    REWRITE_TAC[REAL_MUL_RID; REAL_MUL_ASSOC] THEN
    GEN_REWRITE_TAC RAND_CONV [REAL_MUL_SYM] THEN
    GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THEN
    MATCH_MP_TAC REAL_LE_LMUL THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_POW_LE THEN REAL_ARITH_TAC;
      MATCH_MP_TAC REAL_LE_TRANS THEN
      EXISTS_TAC `&1 + &(fraction (defloat a)) * inv (&2 pow 23)` THEN
      REWRITE_TAC[REAL_ABS_LE] THEN REWRITE_TAC[REAL_LE_ADDR] THEN
      MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_POS] THEN
      MATCH_MP_TAC REAL_LE_INV THEN MATCH_MP_TAC REAL_POW_LE THEN
      REWRITE_TAC[REAL_POS]];
    MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 127` THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    REWRITE_TAC[VAL; VALOF] THEN
    ASM_REWRITE_TAC[ARITH_RULE `(x = 0) <=> ~(0 < x)`] THEN
    REWRITE_TAC[bias; emax; fracwidth; expwidth; float_format] THEN
    REWRITE_TAC[ARITH] THEN REWRITE_TAC[REAL_ABS_MUL] THEN
    REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NEG; REAL_ABS_NUM;
      REAL_POW_ONE; REAL_MUL_LID] THEN
    GEN_REWRITE_TAC (LAND_CONV o LAND_CONV) [REAL_MUL_SYM] THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC; REAL_ABS_MUL] THEN
    REWRITE_TAC[REAL_ABS_INV; REAL_ABS_POW; REAL_ABS_NUM] THEN
    SUBGOAL_THEN `inv(&2 pow 127) * &2 pow 127 = &1` SUBST1_TAC THENL
     [MATCH_MP_TAC REAL_MUL_LINV THEN
      REWRITE_TAC[REAL_POW_EQ_0] THEN REWRITE_TAC[ARITH] THEN
      REWRITE_TAC[REAL_OF_NUM_EQ; ARITH]; ALL_TAC] THEN
    REWRITE_TAC[REAL_MUL_RID; REAL_MUL_ASSOC] THEN
    REWRITE_TAC[real_pow] THEN MATCH_MP_TAC REAL_LT_RMUL THEN
    CONJ_TAC THENL
     [ALL_TAC;
      MATCH_MP_TAC REAL_POW_LT THEN CONV_TAC REAL_RAT_REDUCE_CONV] THEN
    MATCH_MP_TAC REAL_LET_TRANS THEN
    EXISTS_TAC `&1 + &(fraction (defloat a)) * inv (&2 pow 23)` THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC REAL_EQ_IMP_LE THEN REWRITE_TAC[REAL_ABS_REFL] THEN
      MATCH_MP_TAC REAL_LE_ADD THEN CONJ_TAC THEN
      TRY(MATCH_MP_TAC REAL_LE_MUL) THEN
      REWRITE_TAC[REAL_POS; REAL_LE_INV_EQ] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    GEN_REWRITE_TAC RAND_CONV [REAL_ARITH `&2 = &1 + &1`] THEN
    REWRITE_TAC[REAL_LT_LADD] THEN
    MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 23` THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN
    REWRITE_TAC[REAL_MUL_LID; GSYM REAL_MUL_ASSOC] THEN
    SUBST1_TAC(REAL_RAT_REDUCE_CONV `inv(&2 pow 23) * &2 pow 23`) THEN
    REWRITE_TAC[REAL_MUL_RID; REAL_OF_NUM_POW; REAL_OF_NUM_LT] THEN
    MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
    REWRITE_TAC[IS_VALID] THEN
    REWRITE_TAC[fracwidth; float_format] THEN MESON_TAC[]]);;

let ISNORMAL_BININTERVAL_VALUE_STRONG = prove
 (`!a. ISFINITE(a)
       ==> !p. 0 < p /\
               &2 pow p / &2 pow 127 <= abs(VAL(a)) /\
               abs(VAL(a)) < &2 pow (SUC p) / &2 pow 127 <=>
               ISNORMAL(a) /\ (exponent(defloat a) = p)`,
  REPEAT(STRIP_TAC ORELSE EQ_TAC) THENL
   [ASM_REWRITE_TAC[ISNORMAL_VALUE] THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&2 pow p / &2 pow 127` THEN
    ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN
    EXISTS_TAC `&2 pow 127` THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&2 pow 1` THEN CONJ_TAC THENL
     [REWRITE_TAC[REAL_LE_REFL; REAL_POW_1];
      REWRITE_TAC[REAL_MUL_RID]] THEN
    MATCH_MP_TAC REAL_POW_MONO THEN
    REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
    ASM_REWRITE_TAC[num_CONV `1`; LE_SUC_LT];
    ALL_TAC;
    UNDISCH_TAC `ISNORMAL(a)` THEN
    REWRITE_TAC[ISNORMAL; is_normal] THEN
    ASM_MESON_TAC[];
    ASM_MESON_TAC[ISNORMAL_BININTERVAL_VALUE];
    ASM_MESON_TAC[ISNORMAL_BININTERVAL_VALUE]] THEN
  SUBGOAL_THEN `ISNORMAL(a)` ASSUME_TAC THENL
   [REWRITE_TAC[ISNORMAL_VALUE] THEN ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&2 pow p / &2 pow 127` THEN
    ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN
    EXISTS_TAC `&2 pow 127` THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&2 pow 1` THEN CONJ_TAC THENL
     [REWRITE_TAC[REAL_LE_REFL; REAL_POW_1];
      REWRITE_TAC[REAL_MUL_RID]] THEN
    MATCH_MP_TAC REAL_POW_MONO THEN
    REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
    ASM_REWRITE_TAC[num_CONV `1`; LE_SUC_LT]; ALL_TAC] THEN
  SUBGOAL_THEN `&2 pow (exponent(defloat a)) / &2 pow 127 <= abs(VAL(a)) /\
                abs(VAL(a)) < &2 pow (SUC(exponent(defloat a))) / &2 pow 127`
  STRIP_ASSUME_TAC THENL
   [ASM_MESON_TAC[ISNORMAL_BININTERVAL_VALUE]; ALL_TAC] THEN
  REWRITE_TAC[GSYM LE_ANTISYM] THEN
  REWRITE_TAC[GSYM LT_SUC_LE] THEN
  REWRITE_TAC[GSYM NOT_LE] THEN REPEAT STRIP_TAC THENL
   [SUBGOAL_THEN `&2 pow (SUC p) <= &2 pow exponent(defloat a)` MP_TAC THENL
     [MATCH_MP_TAC REAL_POW_MONO THEN ASM_REWRITE_TAC[] THEN REAL_ARITH_TAC;
      REWRITE_TAC[REAL_NOT_LE]] THEN
    MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `inv(&2 pow 127)` THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    REWRITE_TAC[GSYM real_div] THEN MATCH_MP_TAC REAL_LET_TRANS THEN
    EXISTS_TAC `abs(VAL a)` THEN ASM_REWRITE_TAC[];
    SUBGOAL_THEN `&2 pow (SUC (exponent(defloat a))) <= &2 pow p` MP_TAC THENL
     [MATCH_MP_TAC REAL_POW_MONO THEN ASM_REWRITE_TAC[] THEN REAL_ARITH_TAC;
      REWRITE_TAC[REAL_NOT_LE]] THEN
    MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `inv(&2 pow 127)` THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN REWRITE_TAC[] THEN
    REWRITE_TAC[GSYM real_div] THEN MATCH_MP_TAC REAL_LET_TRANS THEN
    EXISTS_TAC `abs(VAL a)` THEN ASM_REWRITE_TAC[]]);;

(* ------------------------------------------------------------------------- *)
(* Properties of type conversions and integer rounding.                      *)
(* ------------------------------------------------------------------------- *)

let FLOAT_BIG_ISINT = prove
 (`!a. ISFINITE(a) /\ exponent(defloat a) >= 150
       ==> is_integral(float_format) (defloat a)`,
  GEN_TAC THEN REWRITE_TAC[ISFINITE_THM; is_integral] THEN STRIP_TAC THEN
  ASM_REWRITE_TAC[] THEN REWRITE_TAC[VALOF] THEN
  SUBGOAL_THEN `?d. exponent (defloat a) = 150 + d` MP_TAC THENL
   [EXISTS_TAC `exponent (defloat a) - 150` THEN
    UNDISCH_TAC `exponent (defloat a) >= 150` THEN
    ARITH_TAC; DISCH_THEN(CHOOSE_THEN SUBST1_TAC)] THEN
  REWRITE_TAC[ADD_EQ_0; ARITH] THEN
  REWRITE_TAC[float_format; fracwidth; expwidth; bias; emax] THEN
  REWRITE_TAC[ARITH] THEN
  ONCE_REWRITE_TAC[REAL_ABS_MUL] THEN
  REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NEG; REAL_ABS_NUM] THEN
  REWRITE_TAC[REAL_POW_ONE; REAL_MUL_LID] THEN
  REWRITE_TAC[real_div; REAL_POW_ADD; GSYM REAL_MUL_ASSOC] THEN
  REWRITE_TAC[REAL_ADD_LDISTRIB; REAL_MUL_RID] THEN
  ONCE_REWRITE_TAC
   [AC REAL_MUL_AC `a:real * b * c * d * e = (a * c * e) * b * d`] THEN
  CONV_TAC REAL_RAT_REDUCE_CONV THEN
  ONCE_REWRITE_TAC[AC REAL_MUL_AC `a:real * b * c = (a * c) * b`] THEN
  CONV_TAC REAL_RAT_REDUCE_CONV THEN
  REWRITE_TAC[REAL_OF_NUM_MUL; REAL_OF_NUM_ADD; REAL_OF_NUM_POW] THEN
  REWRITE_TAC[REAL_ABS_NUM] THEN MESON_TAC[]);;

let INT_SMALL_ISFLOAT_LEMMA1 = prove
 (`!p x. 2 EXP p <= x /\ x < 2 EXP (SUC p) /\ p <= 23
         ==> is_finite(float_format)
                (0,127 + p,(x - 2 EXP p) * 2 EXP (23 - p)) /\
             (VAL(float(0,127 + p,(x - 2 EXP p) * 2 EXP (23 - p))) = &x)`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  UNDISCH_TAC `2 EXP p <= x` THEN
  DISCH_THEN(MP_TAC o REWRITE_RULE[LE_EXISTS]) THEN
  DISCH_THEN(X_CHOOSE_THEN `d:num` SUBST_ALL_TAC) THEN
  REWRITE_TAC[ADD_SUB2] THEN
  SUBGOAL_THEN `is_finite(float_format) (0,127 + p,d * 2 EXP (23 - p))`
  ASSUME_TAC THENL
   [REWRITE_TAC[IS_FINITE_EXPLICIT; sign; exponent; fraction] THEN
    REWRITE_TAC[ARITH] THEN CONJ_TAC THENL
     [UNDISCH_TAC `p <= 23` THEN ARITH_TAC; ALL_TAC] THEN
    MATCH_MP_TAC LTE_TRANS THEN
    EXISTS_TAC `2 EXP p * 2 EXP (23 - p)` THEN CONJ_TAC THENL
     [REWRITE_TAC[LT_MULT_RCANCEL; EXP_EQ_0; ARITH] THEN
      UNDISCH_TAC `2 EXP p + d < 2 EXP SUC p` THEN
      REWRITE_TAC[EXP] THEN ARITH_TAC;
      SUBST1_TAC(SYM(REWRITE_CONV[ARITH] `2 EXP 23`)) THEN
      FIRST_ASSUM(MP_TAC o GEN_REWRITE_RULE I [LE_EXISTS]) THEN
      DISCH_THEN(CHOOSE_THEN SUBST1_TAC) THEN
      REWRITE_TAC[ADD_SUB2; EXP_ADD; EXP; MULT_AC; LE_REFL]]; ALL_TAC] THEN
  SUBGOAL_THEN `is_valid(float_format) (0,127 + p,d * 2 EXP (23 - p))`
  MP_TAC THENL [ASM_MESON_TAC[is_finite]; ALL_TAC] THEN
  ASM_REWRITE_TAC[VAL; float_tybij] THEN DISCH_THEN SUBST1_TAC THEN
  ASM_REWRITE_TAC[VALOF; float_format; sign; exponent; fraction] THEN
  REWRITE_TAC[real_pow; REAL_MUL_LID; bias; fracwidth; expwidth] THEN
  REWRITE_TAC[ADD_EQ_0; ARITH] THEN
   FIRST_ASSUM(MP_TAC o GEN_REWRITE_RULE I [LE_EXISTS]) THEN
    DISCH_THEN(CHOOSE_THEN SUBST1_TAC) THEN
  REWRITE_TAC[ADD_SUB2; REAL_POW_ADD] THEN
  REWRITE_TAC[GSYM REAL_OF_NUM_POW; GSYM REAL_OF_NUM_ADD; GSYM
    REAL_OF_NUM_MUL] THEN REWRITE_TAC[real_div] THEN
  ONCE_REWRITE_TAC[AC REAL_MUL_AC
    `((a * b) * c) * d:real = (a * c) * b * d`] THEN
  CONV_TAC(LAND_CONV(LAND_CONV REAL_RAT_REDUCE_CONV)) THEN
  REWRITE_TAC[REAL_MUL_LID; REAL_ADD_LDISTRIB; REAL_MUL_RID] THEN
  AP_TERM_TAC THEN REWRITE_TAC[REAL_INV_MUL] THEN
  ONCE_REWRITE_TAC[AC REAL_MUL_AC
    `a:real * (b * c) * d * e = (d * a) * (e * c) * b`] THEN
  GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_LID] THEN
  REWRITE_TAC[REAL_MUL_ASSOC] THEN AP_THM_TAC THEN AP_TERM_TAC THEN
  GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_LID] THEN
  ONCE_REWRITE_TAC[AC REAL_MUL_AC
    `((a:real * b) * c) * d = (a * b) * (c * d)`] THEN
  BINOP_TAC THEN MATCH_MP_TAC REAL_MUL_LINV THEN
  REWRITE_TAC[REAL_POW_EQ_0; REAL_OF_NUM_EQ; ARITH]);;

let INT_SMALL_ISFLOAT_LEMMA2 = prove
 (`!p x. x < 2 EXP p /\ p <= 24
         ==> ?e f. is_finite(float_format) (0,e,f) /\
                   (VAL(float(0,e,f)) = &x)`,
  INDUCT_TAC THENL
   [GEN_TAC THEN REWRITE_TAC[EXP] THEN REWRITE_TAC[num_CONV `1`; LT] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
    EXISTS_TAC `0` THEN EXISTS_TAC `0` THEN
    REWRITE_TAC[IS_FINITE_EXPLICIT; sign; exponent; fraction; ARITH] THEN
    CONV_TAC (LAND_CONV VAL_FLOAT_CONV) THEN REFL_TAC;
    GEN_TAC THEN STRIP_TAC THEN ASM_CASES_TAC `x < 2 EXP p` THENL
     [FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[] THEN
      UNDISCH_TAC `SUC p <= 24` THEN ARITH_TAC;
      RULE_ASSUM_TAC(REWRITE_RULE[NOT_LT]) THEN
      EXISTS_TAC `127 + p` THEN
      EXISTS_TAC `(x - 2 EXP p) * 2 EXP (23 - p)` THEN
      MATCH_MP_TAC INT_SMALL_ISFLOAT_LEMMA1 THEN
      ASM_REWRITE_TAC[] THEN
      UNDISCH_TAC `SUC p <= 24` THEN ARITH_TAC]]);;

let INT_SMALL_ISFLOAT_LEMMA3 = prove
 (`!x. x <= 2 EXP 24
       ==> ?e f. is_finite(float_format) (0,e,f) /\
                 (VAL(float(0,e,f)) = &x)`,
  GEN_TAC THEN REWRITE_TAC[LE_LT] THEN STRIP_TAC THENL
   [MATCH_MP_TAC INT_SMALL_ISFLOAT_LEMMA2 THEN
    EXISTS_TAC `24` THEN ASM_REWRITE_TAC[LE_REFL];
    EXISTS_TAC `151` THEN EXISTS_TAC `0` THEN
    REWRITE_TAC[IS_FINITE_EXPLICIT; sign; exponent; fraction; ARITH] THEN
    CONV_TAC(LAND_CONV VAL_FLOAT_CONV) THEN ASM_REWRITE_TAC[ARITH]]);;

let INT_SMALL_ISFLOAT = prove
 (`!x. abs(x) <= &2 pow 24 /\ (?n. abs(x) = &n)
       ==> ?a. ISFINITE(a) /\ (VAL a = x)`,
  GEN_TAC THEN DISCH_THEN(CONJUNCTS_THEN MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `n:num` MP_TAC) THEN
  REWRITE_TAC[real_abs] THEN COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[REAL_ARITH `(--x = y:real) <=> (x = --y)`] THEN
  DISCH_THEN SUBST_ALL_TAC THEN REWRITE_TAC[REAL_OF_NUM_POW] THEN
  REWRITE_TAC[REAL_NEG_NEG; REAL_ABS_NEG; REAL_ABS_NUM; REAL_OF_NUM_LE] THEN
  DISCH_THEN(MP_TAC o MATCH_MP INT_SMALL_ISFLOAT_LEMMA3) THEN
  REWRITE_TAC[VAL] THEN STRIP_TAC THENL
   [EXISTS_TAC `float(0,e,f)`; EXISTS_TAC `float(1,e,f)`] THEN
  REWRITE_TAC[ISFINITE_THM; VAL] THENL
   [REWRITE_TAC[ISFINITE_THM] THEN ASM_MESON_TAC[is_finite; float_tybij];
    ALL_TAC] THEN
  UNDISCH_TAC `valof(float_format) (defloat (float (0,(e,f)))) = &n` THEN
  SUBGOAL_THEN `is_finite(float_format)(0,e,f) /\
                is_finite(float_format)(1,e,f)`
  MP_TAC THENL
   [UNDISCH_TAC `is_finite(float_format) (0,e,f)` THEN
    REWRITE_TAC[IS_FINITE_EXPLICIT] THEN
    REWRITE_TAC[sign; exponent; fraction] THEN
    REWRITE_TAC[ARITH];
    DISCH_THEN(CONJUNCTS_THEN
      (fun th -> ASSUME_TAC th THEN
                 ASSUME_TAC(CONJUNCT1(REWRITE_RULE[is_finite] th)))) THEN
    RULE_ASSUM_TAC(REWRITE_RULE[float_tybij]) THEN
    ASM_REWRITE_TAC[] THEN DISCH_THEN(SUBST1_TAC o SYM) THEN
    REWRITE_TAC[VALOF; exponent; fraction; sign] THEN
    COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
    REWRITE_TAC[REAL_MUL_LNEG; real_pow; REAL_POW_1;
      REAL_MUL_LID; REAL_NEG_NEG]]);;

let FLOAT_ROUNDFLOAT_LEMMA1 = prove
 (`!a. ISFINITE(a)
       ==> is_closest (valof float_format)
                 {a | is_integral float_format a}
                 (valof float_format (defloat a))
                 (closest (valof float_format)
                        (\a. ?n. EVEN n /\ (abs (valof float_format a) = &n))
                        {a | is_integral float_format a}
                        (valof float_format (defloat a)))`,
  GEN_TAC THEN DISCH_TAC THEN
  MATCH_MP_TAC(REWRITE_RULE[IMP_IMP]
       CLOSEST_IS_CLOSEST) THEN
  CONJ_TAC THENL
   [MATCH_MP_TAC FINITE_SUBSET THEN
    EXISTS_TAC `{a | is_finite(float_format) a}` THEN
    REWRITE_TAC[IS_FINITE_FINITE] THEN
    REWRITE_TAC[SUBSET; IN_ELIM_THM] THEN
    REWRITE_TAC[is_integral] THEN MESON_TAC[];
    REWRITE_TAC[EXTENSION] THEN
    DISCH_THEN(MP_TAC o SPEC `0,0,0`) THEN
    REWRITE_TAC[IN_ELIM_THM; NOT_IN_EMPTY] THEN
    REWRITE_TAC[is_integral; float_format] THEN
    REWRITE_TAC[is_finite; is_valid; is_zero] THEN
    REWRITE_TAC[exponent; fraction; expwidth; fracwidth] THEN
    REWRITE_TAC[ARITH] THEN
    EXISTS_TAC `0` THEN REWRITE_TAC[valof] THEN
    REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_MUL_RZERO] THEN
    REWRITE_TAC[REAL_ABS_0]]);;

let FLOAT_ROUNDFLOAT_LEMMA2 = prove
 (`!a. ISFINITE(a)
       ==> (defloat(float
               (zerosign float_format (sign (defloat a))
                   (closest (valof float_format)
                       (\a. ?n. EVEN n /\ (abs (valof float_format a) = &n))
                       {a | is_integral float_format a}
                       (valof float_format (defloat a))))) =
            zerosign float_format (sign (defloat a))
               (closest (valof float_format)
                   (\a. ?n. EVEN n /\ (abs (valof float_format a) = &n))
                   {a | is_integral float_format a}
                   (valof float_format (defloat a))))`,
  let lemma = prove
   (`is_integral X a ==> is_valid X a`,
    MESON_TAC[is_integral; is_finite]) in
  GEN_TAC THEN DISCH_TAC THEN REWRITE_TAC[GSYM float_tybij] THEN
  REWRITE_TAC[zerosign] THEN
  REPEAT COND_CASES_TAC THEN REWRITE_TAC[IS_VALID_SPECIAL] THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP FLOAT_ROUNDFLOAT_LEMMA1) THEN
  REWRITE_TAC[is_closest] THEN DISCH_THEN(MP_TAC o CONJUNCT1) THEN
  REWRITE_TAC[IN_ELIM_THM] THEN MATCH_ACCEPT_TAC lemma);;

let FLOAT_ROUNDFLOAT_LEMMA3 = prove
 (`!b a. valof(float_format) (zerosign(float_format) b a) =
         valof(float_format) a`,
  REPEAT GEN_TAC THEN REWRITE_TAC[zerosign] THEN
  REPEAT COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
  RULE_ASSUM_TAC(REWRITE_RULE[is_zero]) THEN
  ASM_REWRITE_TAC[VALOF] THEN
  REWRITE_TAC[plus_zero; minus_zero; exponent; sign; fraction] THEN
  REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_MUL_RZERO]);;

let FLOAT_ROUNDFLOAT_LEMMA4 = prove
 (`!a. ISFINITE(a)
       ==> ?b. is_integral(float_format) b /\
               abs(valof(float_format) b -
                   valof(float_format) (defloat a)) <= inv(&2)`,
  GEN_TAC THEN DISCH_TAC THEN
  ASM_CASES_TAC `exponent(defloat a) >= 150` THENL
   [EXISTS_TAC `defloat a` THEN REWRITE_TAC[REAL_SUB_REFL] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    MATCH_MP_TAC FLOAT_BIG_ISINT THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
  SUBGOAL_THEN `!x. &0 <= x ==> ?n. &n <= x /\ x <= &(SUC n)` ASSUME_TAC THENL
   [MP_TAC(MATCH_MP REAL_ARCH_LEAST REAL_LT_01) THEN
    REWRITE_TAC[REAL_MUL_RID] THEN MESON_TAC[REAL_LT_IMP_LE]; ALL_TAC] THEN
  SUBGOAL_THEN `!x. &0 <= x ==> ?n. abs(x - &n) <= inv(&2)` ASSUME_TAC THENL
   [GEN_TAC THEN DISCH_THEN(fun th -> FIRST_ASSUM(MP_TAC o C MATCH_MP th)) THEN
    DISCH_THEN(X_CHOOSE_THEN `n:num` STRIP_ASSUME_TAC) THEN
    MP_TAC(REAL_ARITH `!a b x d. a <= x /\ x <= b /\ (b = &2 * d + a)
                                 ==> abs(x - a) <= d \/ abs(x - b) <= d`) THEN
    DISCH_THEN(MP_TAC o SPECL [`&n`; `&(SUC n)`; `x:real`; `inv(&2)`]) THEN
    ASM_REWRITE_TAC[] THEN CONV_TAC REAL_RAT_REDUCE_CONV THEN
    GEN_REWRITE_TAC (LAND_CONV o LAND_CONV o ONCE_DEPTH_CONV)
     [GSYM REAL_OF_NUM_SUC] THEN
    REWRITE_TAC[REAL_ADD_AC] THEN MESON_TAC[]; ALL_TAC] THEN
  SUBGOAL_THEN `!x. &0 <= x /\ x <= &2 pow 24
                    ==> ?b. is_integral float_format b /\
                            abs (valof float_format b - x) <= inv(&2)`
  ASSUME_TAC THENL
   [GEN_TAC THEN DISCH_TAC THEN
    UNDISCH_TAC `!x. &0 <= x ==> (?n. abs (x - &n) <= inv (&2))` THEN
    DISCH_THEN(MP_TAC o SPEC `x:real`) THEN ASM_REWRITE_TAC[] THEN
    DISCH_THEN(X_CHOOSE_THEN `n:num` ASSUME_TAC) THEN
    MP_TAC(SPEC `&n` INT_SMALL_ISFLOAT) THEN
    REWRITE_TAC[REAL_ABS_NUM; REAL_OF_NUM_EQ; GSYM EXISTS_REFL] THEN
    SUBGOAL_THEN `&n <= &2 pow 24` (fun th -> REWRITE_TAC[th]) THENL
     [REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_LE] THEN
      REWRITE_TAC[GSYM LT_SUC_LE] THEN
      REWRITE_TAC[GSYM REAL_OF_NUM_SUC; GSYM REAL_OF_NUM_LT] THEN
      REWRITE_TAC[GSYM REAL_OF_NUM_POW] THEN
      MATCH_MP_TAC
        (REAL_ARITH `!x:real. x <= b /\ abs(x - y) < d ==> y < b + d`) THEN
      EXISTS_TAC `x:real` THEN ASM_REWRITE_TAC[] THEN
      MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `inv(&2)` THEN
      ASM_REWRITE_TAC[] THEN CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    DISCH_THEN(X_CHOOSE_THEN `c:float` MP_TAC) THEN
    REWRITE_TAC[ISFINITE_THM; VAL] THEN STRIP_TAC THEN
    EXISTS_TAC `defloat c` THEN ASM_REWRITE_TAC[] THEN
    ONCE_REWRITE_TAC[REAL_ABS_SUB] THEN ASM_REWRITE_TAC[] THEN
    ASM_REWRITE_TAC[is_integral] THEN
    REWRITE_TAC[REAL_ABS_NUM; REAL_OF_NUM_EQ; GSYM EXISTS_REFL]; ALL_TAC] THEN
  SUBGOAL_THEN
   `!x. abs(x) <= &2 pow 24
           ==> (?b. is_integral float_format b /\
                    abs (valof float_format b - x) <= inv(&2))`
  MP_TAC THENL
   [GEN_TAC THEN ASM_CASES_TAC `&0 <= x` THEN ASM_REWRITE_TAC[real_abs] THEN
    REWRITE_TAC[GSYM real_abs] THEN DISCH_TAC THENL
     [FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
    SUBGOAL_THEN `&0 <= --x /\ --x <= &2 pow 24` MP_TAC THENL
     [ASM_REWRITE_TAC[] THEN UNDISCH_TAC `~(&0 <= x)` THEN
      REAL_ARITH_TAC; ALL_TAC] THEN
    DISCH_THEN(fun th -> FIRST_ASSUM(MP_TAC o C MATCH_MP th)) THEN
    DISCH_THEN(X_CHOOSE_THEN `b:num#num#num` MP_TAC) THEN
    DISCH_THEN(fun th -> EXISTS_TAC `1 - sign(b),exponent(b),fraction(b)` THEN
                         MP_TAC th) THEN
    REWRITE_TAC[is_integral; IS_FINITE_EXPLICIT] THEN
    REWRITE_TAC[sign; exponent; fraction] THEN
    DISCH_THEN(REPEAT_TCL CONJUNCTS_THEN ASSUME_TAC) THEN
    ASM_REWRITE_TAC[ARITH_RULE `1 - x < 2`] THEN
    SUBGOAL_THEN `valof(float_format) (1 - sign b,(exponent b,fraction b)) =
                  --(valof(float_format) b)`
    SUBST1_TAC THENL
     [ALL_TAC; ASM_REWRITE_TAC[REAL_ABS_NEG] THEN
      ASM_REWRITE_TAC[REAL_ARITH `abs(--a - b:real) = abs(a - --b)`]] THEN
    UNDISCH_TAC `sign(b) < 2` THEN REWRITE_TAC[num_CONV `2`] THEN
    REWRITE_TAC[num_CONV `1`; LT] THEN REWRITE_TAC[SYM(num_CONV `1`)] THEN
    REWRITE_TAC[VALOF; sign; exponent; fraction] THEN
    DISCH_THEN(DISJ_CASES_THEN SUBST1_TAC) THEN
    COND_CASES_TAC THEN ASM_REWRITE_TAC[ARITH; REAL_POW_1; real_pow] THEN
    REWRITE_TAC[REAL_MUL_LNEG; REAL_NEG_NEG]; ALL_TAC] THEN
  DISCH_THEN MATCH_MP_TAC THEN REWRITE_TAC[VALOF] THEN
  COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NEG] THEN
  REWRITE_TAC[REAL_ABS_NUM; REAL_POW_ONE; REAL_MUL_LID] THEN
  REWRITE_TAC[bias; float_format; expwidth; fracwidth] THEN
  REWRITE_TAC[ARITH] THENL
   [REWRITE_TAC[real_div; REAL_ABS_MUL] THEN
    ONCE_REWRITE_TAC[REAL_ARITH `(a:real * b) * c * d = (a * b * d) * c`] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN REWRITE_TAC[real_div; REAL_MUL_LID] THEN
    ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN REWRITE_TAC[GSYM real_div] THEN
    MATCH_MP_TAC REAL_LE_LDIV THEN CONV_TAC REAL_RAT_REDUCE_CONV THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&2 pow 23` THEN CONJ_TAC THENL
     [MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
      REWRITE_TAC[IS_VALID; float_format; expwidth; fracwidth] THEN
      STRIP_TAC THEN REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_LE] THEN
      MATCH_MP_TAC LT_IMP_LE THEN ASM_REWRITE_TAC[];
      CONV_TAC REAL_RAT_REDUCE_CONV];
    REWRITE_TAC[real_div; REAL_ABS_MUL] THEN
    ONCE_REWRITE_TAC[REAL_ARITH `(a:real * b) * c = (a * c) * b`] THEN
    REWRITE_TAC[REAL_MUL_ASSOC; REAL_ABS_INV] THEN
    REWRITE_TAC[GSYM real_div] THEN MATCH_MP_TAC REAL_LE_LDIV THEN
    CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC
     `abs(&2 pow 149) * abs (&1 + &(fraction (defloat a)) / &2 pow 23)` THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_RMUL THEN REWRITE_TAC[REAL_ABS_POS] THEN
      REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NUM] THEN
      MATCH_MP_TAC REAL_POW_MONO THEN REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
      UNDISCH_TAC `~(exponent (defloat a) >= 150)` THEN ARITH_TAC;
      ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
      GEN_REWRITE_TAC (LAND_CONV o RAND_CONV) [GSYM REAL_INV_INV] THEN
      REWRITE_TAC[GSYM real_div] THEN MATCH_MP_TAC REAL_LE_LDIV THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      SUBGOAL_THEN `abs(&1 + &(fraction (defloat a)) / &8388608) =
                    &1 + &(fraction (defloat a)) / &8388608`
      SUBST1_TAC THENL
       [REWRITE_TAC[REAL_ABS_REFL] THEN
        MATCH_MP_TAC REAL_LE_ADD THEN REWRITE_TAC[REAL_POS] THEN
        REWRITE_TAC[real_div] THEN MATCH_MP_TAC REAL_LE_MUL THEN
        REWRITE_TAC[REAL_POS] THEN MATCH_MP_TAC REAL_LE_INV THEN
        REWRITE_TAC[REAL_POS];
        ONCE_REWRITE_TAC[REAL_ADD_SYM] THEN
        REWRITE_TAC[GSYM REAL_LE_SUB_LADD] THEN
        MATCH_MP_TAC REAL_LE_LDIV THEN
        CONV_TAC REAL_RAT_REDUCE_CONV THEN
        MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&2 pow 23` THEN
        CONJ_TAC THENL
         [MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
          REWRITE_TAC[IS_VALID; float_format; expwidth; fracwidth] THEN
          STRIP_TAC THEN REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_LE] THEN
          MATCH_MP_TAC LT_IMP_LE THEN ASM_REWRITE_TAC[];
          CONV_TAC REAL_RAT_REDUCE_CONV]]]]);;

let FLOAT_ROUNDFLOAT = prove
 (`!a. ISFINITE(a) ==> ISINTEGRAL(ROUNDFLOAT a) /\
                       abs(VAL(ROUNDFLOAT a) - VAL(a)) <= inv(&2)`,
  GEN_TAC THEN DISCH_TAC THEN REWRITE_TAC[ROUNDFLOAT] THEN
  REWRITE_TAC[fintrnd] THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE]; ALL_TAC] THEN
  REWRITE_TAC[ISNAN; INFINITY] THEN STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[ISINTEGRAL; VAL] THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP VAL_THRESHOLD) THEN
  DISCH_THEN(MP_TAC o MATCH_MP
   (REAL_ARITH `abs(x:real) < y ==> ~(x <= --y) /\ ~(x >= y)`)) THEN
  REWRITE_TAC[VAL; intround] THEN STRIP_TAC THEN
  ASM_REWRITE_TAC[] THEN
  FIRST_ASSUM(fun th -> REWRITE_TAC[MATCH_MP FLOAT_ROUNDFLOAT_LEMMA2 th]) THEN
  CONJ_TAC THENL
   [REWRITE_TAC[zerosign] THEN REPEAT COND_CASES_TAC THEN
    ASM_REWRITE_TAC[] THENL
     [REWRITE_TAC[is_integral; plus_zero; float_format] THEN
      REWRITE_TAC[is_finite; is_valid; is_zero] THEN
      REWRITE_TAC[exponent; fraction; expwidth; fracwidth] THEN
      REWRITE_TAC[ARITH] THEN
      EXISTS_TAC `0` THEN REWRITE_TAC[valof] THEN
      REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_MUL_RZERO] THEN
      REWRITE_TAC[REAL_ABS_0];
      REWRITE_TAC[is_integral; minus_zero; float_format] THEN
      REWRITE_TAC[is_finite; is_valid; is_zero] THEN
      REWRITE_TAC[exponent; fraction; expwidth; fracwidth] THEN
      REWRITE_TAC[ARITH] THEN
      EXISTS_TAC `0` THEN REWRITE_TAC[valof] THEN
      REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_MUL_RZERO] THEN
      REWRITE_TAC[REAL_ABS_0];
      FIRST_ASSUM(MP_TAC o MATCH_MP FLOAT_ROUNDFLOAT_LEMMA1) THEN
      REWRITE_TAC[is_closest] THEN DISCH_THEN(MP_TAC o CONJUNCT1) THEN
      REWRITE_TAC[IN_ELIM_THM]];
    REWRITE_TAC[FLOAT_ROUNDFLOAT_LEMMA3] THEN
    FIRST_ASSUM(X_CHOOSE_TAC `b:num#num#num` o MATCH_MP
      FLOAT_ROUNDFLOAT_LEMMA4) THEN
    FIRST_ASSUM(MP_TAC o MATCH_MP FLOAT_ROUNDFLOAT_LEMMA1) THEN
    REWRITE_TAC[is_closest] THEN
    DISCH_THEN(MP_TAC o SPEC `b:num#num#num` o CONJUNCT2) THEN
    ASM_REWRITE_TAC[IN_ELIM_THM] THEN
    MATCH_MP_TAC(REAL_ARITH `b:real <= c ==> a <= b ==> a <= c`) THEN
    ASM_REWRITE_TAC[]]);;

let ISINTEGRAL = prove
 (`!a. ISINTEGRAL(a) <=> ISFINITE(a) /\ ?n. abs(VAL a) = &n`,
  GEN_TAC THEN REWRITE_TAC[ISINTEGRAL; ISFINITE_THM; is_integral; VAL]);;

let FLOAT_TOINT = prove
 (`!a. ISINTEGRAL(a) /\ --(&2 pow 31) <= VAL(a) /\ VAL(a) < &2 pow 31
       ==> (IVAL(TOINT a) = VAL(a))`,
  REPEAT STRIP_TAC THEN REWRITE_TAC[TOINT] THEN CONV_TAC SELECT_CONV THEN
  UNDISCH_TAC `ISINTEGRAL(a)` THEN REWRITE_TAC[ISINTEGRAL] THEN
  DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `n:num` MP_TAC) THEN
  DISCH_THEN(MP_TAC o MATCH_MP
    (REAL_ARITH `(abs(a) = b) ==> (a = b) \/ (~(b = &0) /\ (a = --b))`)) THEN
  DISCH_THEN(DISJ_CASES_THEN MP_TAC) THENL
   [DISCH_THEN SUBST_ALL_TAC THEN
    EXISTS_TAC `INT n` THEN REWRITE_TAC[IVAL] THEN
    SUBGOAL_THEN `INUM(INT n) = n` SUBST1_TAC THENL
     [REWRITE_TAC[GSYM INT_TYBIJ; GSYM REAL_OF_NUM_LT] THEN
      REWRITE_TAC[GSYM REAL_OF_NUM_POW] THEN
      MATCH_MP_TAC REAL_LT_TRANS THEN EXISTS_TAC `&2 pow 31` THEN
      ASM_REWRITE_TAC[] THEN CONV_TAC REAL_RAT_REDUCE_CONV;
      ASM_REWRITE_TAC[GSYM REAL_OF_NUM_LT; GSYM REAL_OF_NUM_POW]];
    DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC SUBST_ALL_TAC) THEN
    EXISTS_TAC `INT(2 EXP 32 - n)` THEN REWRITE_TAC[IVAL] THEN
    SUBGOAL_THEN `INUM(INT(2 EXP 32 - n)) = 2 EXP 32 - n` SUBST1_TAC THENL
     [REWRITE_TAC[GSYM INT_TYBIJ] THEN
      REWRITE_TAC[ARITH_RULE `(a - n < a) <=> ~(n = 0) /\ ~(a = 0)`] THEN
      REWRITE_TAC[EXP_EQ_0; ARITH] THEN ASM_REWRITE_TAC[GSYM REAL_OF_NUM_EQ];
      ALL_TAC] THEN
    SUBGOAL_THEN `~(2 EXP 32 - n < 2 EXP 31)` (fun th -> REWRITE_TAC[th]) THEN
    UNDISCH_TAC `--(&2 pow 31) <= --(&n)` THEN
    REWRITE_TAC[REAL_LE_NEG2; REAL_OF_NUM_LE] THEN
    REWRITE_TAC[num_CONV `32`; EXP] THEN
    REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_LE] THENL
     [ARITH_TAC;
      DISCH_THEN(CHOOSE_THEN SUBST1_TAC o REWRITE_RULE[LE_EXISTS]) THEN
      REWRITE_TAC[MULT_2; GSYM ADD_ASSOC; ADD_SUB2] THEN
      REWRITE_TAC[GSYM REAL_OF_NUM_ADD] THEN REAL_ARITH_TAC]]);;

let FLOAT_TOFLOAT = prove
 (`!i. abs(IVAL i) <= &2 pow 24
       ==> ISFINITE(TOFLOAT i) /\ (VAL(TOFLOAT i) = IVAL i)`,
  GEN_TAC THEN DISCH_TAC THEN REWRITE_TAC[TOFLOAT] THEN
  CONV_TAC SELECT_CONV THEN MATCH_MP_TAC INT_SMALL_ISFLOAT THEN
  ASM_REWRITE_TAC[] THEN REWRITE_TAC[IVAL] THEN
  COND_CASES_TAC THEN ASM_REWRITE_TAC[] THENL
   [EXISTS_TAC `INUM i` THEN REWRITE_TAC[REAL_ABS_NUM]; ALL_TAC] THEN
  EXISTS_TAC `2 EXP 32 - INUM i` THEN
  SUBGOAL_THEN `INUM i < 2 EXP 32` MP_TAC THENL
   [REWRITE_TAC[INT_TYBIJ]; ALL_TAC] THEN
  FIRST_ASSUM(UNDISCH_TAC o check is_neg o concl) THEN
  REWRITE_TAC[NOT_LT; LE_EXISTS] THEN
  DISCH_THEN(CHOOSE_THEN SUBST1_TAC) THEN
  REWRITE_TAC[LT_EXISTS] THEN
  DISCH_THEN(CHOOSE_THEN SUBST1_TAC) THEN
  REWRITE_TAC[ADD_SUB2] THEN
  REWRITE_TAC[GSYM REAL_OF_NUM_ADD] THEN
  REWRITE_TAC[REAL_ARITH `a:real - (a + b) = --b`] THEN
  REWRITE_TAC[REAL_ABS_NEG; REAL_ABS_NUM]);;

(* ------------------------------------------------------------------------- *)
(* Stuff about SCALB.                                                        *)
(* ------------------------------------------------------------------------- *)

let FLOAT_SCALB = prove
 (`!a N. ISFINITE a /\
         abs(exp(IVAL N * ln(&2)) * VAL(a)) < threshold(float_format)
         ==> ISFINITE(SCALB(a,N)) /\
             (VAL(SCALB(a,N)) = exp(IVAL N * ln(&2)) * VAL(a) +
                                error(exp(IVAL N * ln(&2)) * VAL(a)))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISFINITE_THM; ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC] THEN
  UNDISCH_TAC
    `abs(exp(IVAL N * ln(&2)) * VAL(a)) < threshold(float_format)` THEN
  ASM_REWRITE_TAC[SCALB; fscalb; VAL] THEN
  REWRITE_TAC[VALOF_DEFLOAT_FLOAT_ZEROSIGN_ROUND] THEN
  REWRITE_TAC[error; VAL; DEFLOAT_FLOAT_ROUND] THEN
  REWRITE_TAC[prove_constructors_distinct ccode_RECURSION] THEN
  DISCH_TAC THEN CONJ_TAC THENL
   [MATCH_MP_TAC DEFLOAT_FLOAT_ZEROSIGN_ROUND_FINITE THEN ASM_REWRITE_TAC[];
    REAL_ARITH_TAC]);;

let FLOAT_SCALB_OVERFLOWS_SIGNED = prove
 (`!a N. ISFINITE a
         ==> (exp(IVAL N * ln(&2)) * VAL(a) >= threshold(float_format)
              ==> SCALB(a,N) == PLUS_INFINITY) /\
             (exp(IVAL N * ln(&2)) * VAL(a) <= --(threshold(float_format))
              ==> SCALB(a,N) == MINUS_INFINITY)`,
  let lemma1 = prove
   (`~(is_zero(float_format) (plus_infinity float_format)) /\
     ~(is_zero(float_format) (minus_infinity float_format))`,
    REWRITE_TAC[is_zero; float_format; plus_infinity; minus_infinity] THEN
    REWRITE_TAC[exponent; fraction] THEN
    REWRITE_TAC[emax; ARITH; expwidth])
  and lemma2 = prove
   (`(defloat(float(minus_infinity float_format)) =
      minus_infinity float_format) /\
     (defloat(float(plus_infinity float_format)) =
      plus_infinity float_format)`,
    REWRITE_TAC[GSYM float_tybij] THEN
    REWRITE_TAC[is_valid; float_format; plus_infinity; minus_infinity] THEN
    REWRITE_TAC[emax; expwidth; fracwidth; ARITH]) in
  REPEAT GEN_TAC THEN DISCH_TAC THEN
  SUBGOAL_THEN `~(ISNAN(a)) /\ ~(INFINITY(a))` MP_TAC THENL
   [ASM_MESON_TAC[FLOAT_DISTINCT_FINITE];
    REWRITE_TAC[ISFINITE_THM; ISNAN; INFINITY; ISNORMAL; ISDENORMAL; ISZERO] THEN
    STRIP_TAC] THEN
  ASM_REWRITE_TAC[SCALB; fscalb; VAL; round] THEN CONJ_TAC THENL
   [DISCH_THEN(MP_TAC o MATCH_MP(REAL_ARITH
     `a >= b ==> b > &0 ==> a >= b /\ ~(a <= --b)`)) THEN
    SUBGOAL_THEN `threshold float_format > &0` ASSUME_TAC THENL
     [REWRITE_TAC[FLOAT_THRESHOLD_EXPLICIT; REAL_OF_NUM_GT; ARITH];
      ASM_REWRITE_TAC[] THEN STRIP_TAC THEN ASM_REWRITE_TAC[]] THEN
    REWRITE_TAC[zerosign; lemma1; lemma2; PLUS_INFINITY] THEN
    ASM_REWRITE_TAC[FLOAT_EQ_REFL; ISNAN; lemma2] THEN
    REWRITE_TAC[is_nan; float_format; plus_infinity] THEN
    REWRITE_TAC[exponent; emax; expwidth; fracwidth; fraction];
    DISCH_TAC THEN ASM_REWRITE_TAC[] THEN
    REWRITE_TAC[zerosign; lemma1; lemma2; MINUS_INFINITY] THEN
    ASM_REWRITE_TAC[FLOAT_EQ_REFL; ISNAN; lemma2] THEN
    REWRITE_TAC[is_nan; float_format; minus_infinity] THEN
    REWRITE_TAC[exponent; emax; expwidth; fracwidth; fraction]]);;

let FLOAT_SCALB_OVERFLOWS = prove
 (`!a N. ISFINITE a /\
         abs(exp(IVAL N * ln(&2)) * VAL(a)) >= threshold(float_format)
         ==> INFINITY(SCALB(a,N))`,
  REPEAT GEN_TAC THEN
  DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
  DISCH_THEN(MP_TAC o MATCH_MP(REAL_ARITH
   `abs(x:real) >= a ==> x >= a \/ x <= --a`)) THEN
  REWRITE_TAC[FLOAT_INFINITIES] THEN
  ASM_MESON_TAC[FLOAT_SCALB_OVERFLOWS_SIGNED]);;

let FLOAT_ORDERED_OPPSIGN = prove
 (`!a b. sign(defloat a) < sign(defloat b)
         ==> VAL(b) <= VAL(a)`,
  REPEAT STRIP_TAC THEN
  MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
  MP_TAC(SPEC `b:float` IS_VALID_DEFLOAT) THEN
  REWRITE_TAC[IS_VALID] THEN REPEAT STRIP_TAC THEN
  SUBGOAL_THEN `(sign(defloat a) = 0) /\ (sign(defloat b) = 1)`
  ASSUME_TAC THENL
   [UNDISCH_TAC `sign(defloat a) < sign(defloat b)` THEN
    UNDISCH_TAC `sign (defloat b) < 2` THEN
    UNDISCH_TAC `sign (defloat a) < 2` THEN ARITH_TAC;
    ASM_REWRITE_TAC[VAL; VALOF; real_pow; REAL_MUL_LID; REAL_POW_1] THEN
    REWRITE_TAC[REAL_MUL_LNEG; real_div; REAL_MUL_LID] THEN
    REPEAT COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC(REAL_ARITH `&0 <= a /\ &0 <= b ==> --a <= b`) THEN
    CONJ_TAC THEN
    REPEAT((MATCH_MP_TAC REAL_LE_MUL THEN CONJ_TAC) ORELSE
           (MATCH_MP_TAC REAL_LE_ADD THEN CONJ_TAC) ORELSE
           (MATCH_MP_TAC REAL_POW_LE) ORELSE
           (MATCH_MP_TAC REAL_LE_INV)) THEN
    REWRITE_TAC[REAL_POS]]);;

let FLOAT_ORDERED_POS_LT = prove
 (`!a b. (sign(defloat a) = 0) /\
         (sign(defloat b) = 0) /\
         (exponent(defloat a) < exponent(defloat b) \/
          (exponent(defloat a) = exponent(defloat b)) /\
          fraction(defloat a) < fraction(defloat b))
         ==> VAL(a) < VAL(b)`,
  REPEAT STRIP_TAC THEN
  ASM_REWRITE_TAC[VAL; VALOF; float_format; bias; emax] THEN
  REWRITE_TAC[expwidth; fracwidth; ARITH] THEN
  REWRITE_TAC[real_pow; REAL_MUL_LID] THENL
   [SUBGOAL_THEN `~(exponent(defloat b) = 0)` ASSUME_TAC THENL
     [UNDISCH_TAC `exponent (defloat a) < exponent (defloat b)` THEN
      ARITH_TAC; ALL_TAC] THEN
    ASM_CASES_TAC `exponent(defloat a) = 0` THEN ASM_REWRITE_TAC[] THENL
     [MATCH_MP_TAC REAL_LTE_TRANS THEN
      EXISTS_TAC `&2 / &2 pow 127 * &2 pow 23 / &2 pow 23` THEN CONJ_TAC THENL
       [ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
        REWRITE_TAC[GSYM REAL_MUL_ASSOC; real_div] THEN
        MATCH_MP_TAC REAL_LT_RMUL THEN CONJ_TAC THENL
         [MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
          REWRITE_TAC[IS_VALID; float_format; fracwidth] THEN
          STRIP_TAC THEN ASM_REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_LT];
          CONV_TAC REAL_RAT_REDUCE_CONV];
        MATCH_MP_TAC REAL_LE_TRANS THEN
        EXISTS_TAC `&2 pow exponent (defloat b) / &2 pow 127 *
                    (&1 + &0 / &2 pow 23)` THEN
        CONJ_TAC THENL
         [REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_ADD_RID] THEN
          REWRITE_TAC[REAL_MUL_RID] THEN
          CONV_TAC(LAND_CONV(RAND_CONV REAL_RAT_REDUCE_CONV)) THEN
          REWRITE_TAC[REAL_MUL_RID] THEN
          MATCH_MP_TAC REAL_LE_RMUL THEN
          CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
          GEN_REWRITE_TAC LAND_CONV [GSYM REAL_POW_1] THEN
          MATCH_MP_TAC REAL_POW_MONO THEN
          REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
          UNDISCH_TAC `~(exponent (defloat b) = 0)` THEN ARITH_TAC;
          MATCH_MP_TAC REAL_LE_LMUL THEN CONJ_TAC THENL
           [REWRITE_TAC[real_div] THEN
            MATCH_MP_TAC REAL_LE_MUL THEN
            CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
            MATCH_MP_TAC REAL_POW_LE THEN REAL_ARITH_TAC;
            REWRITE_TAC[REAL_LE_LADD; real_div] THEN
            MATCH_MP_TAC REAL_LE_RMUL THEN
            REWRITE_TAC[REAL_POS] THEN CONV_TAC REAL_RAT_REDUCE_CONV]]];
      MATCH_MP_TAC REAL_LTE_TRANS THEN
      EXISTS_TAC `&2 pow exponent (defloat a) / &2 pow 127 * &2` THEN
      CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_LMUL THEN CONJ_TAC THENL
         [REWRITE_TAC[real_div] THEN
          MATCH_MP_TAC REAL_LT_MUL THEN CONJ_TAC THENL
           [MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
            CONV_TAC REAL_RAT_REDUCE_CONV];
          REWRITE_TAC[REAL_ARITH `&1 + x < &2 <=> x < &1`] THEN
          MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN
          EXISTS_TAC `&2 pow 23` THEN
          REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
          CONV_TAC REAL_RAT_REDUCE_CONV THEN
          MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
          REWRITE_TAC[IS_VALID; float_format; fracwidth] THEN
          DISCH_THEN(MP_TAC o last o CONJUNCTS) THEN
          REWRITE_TAC[REAL_OF_NUM_LT; ARITH; REAL_MUL_RID]];
        MATCH_MP_TAC REAL_LE_TRANS THEN
        EXISTS_TAC `&2 pow exponent (defloat b) / &2 pow 127 * &1` THEN
        CONJ_TAC THENL
         [ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
          REWRITE_TAC[real_div; REAL_MUL_ASSOC] THEN
          MATCH_MP_TAC REAL_LE_RMUL THEN
          CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
          REWRITE_TAC[GSYM(CONJUNCT2 real_pow); REAL_MUL_LID] THEN
          MATCH_MP_TAC REAL_POW_MONO THEN
          ASM_REWRITE_TAC[LE_SUC_LT] THEN REAL_ARITH_TAC;
          MATCH_MP_TAC REAL_LE_LMUL THEN CONJ_TAC THENL
           [REWRITE_TAC[real_div] THEN MATCH_MP_TAC REAL_LE_MUL THEN
            CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
            REWRITE_TAC[REAL_OF_NUM_POW; REAL_POS];
            REWRITE_TAC[REAL_LE_ADDR; real_div] THEN
            MATCH_MP_TAC REAL_LE_MUL THEN
            CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV]  THEN
            REWRITE_TAC[REAL_OF_NUM_POW; REAL_POS]]]]];
    COND_CASES_TAC THEN ASM_REWRITE_TAC[] THENL
     [ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
      REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
      MATCH_MP_TAC REAL_LT_RMUL THEN ASM_REWRITE_TAC[REAL_OF_NUM_LT] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV;
      MATCH_MP_TAC REAL_LT_LMUL THEN CONJ_TAC THENL
       [REWRITE_TAC[real_div] THEN
        MATCH_MP_TAC REAL_LT_MUL THEN
        CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
        MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC;
        REWRITE_TAC[REAL_LT_LADD; real_div] THEN
        MATCH_MP_TAC REAL_LT_RMUL THEN
        CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
        ASM_REWRITE_TAC[REAL_OF_NUM_LT]]]]);;

let FLOAT_ORDERED_POS_LE = prove
 (`!a b. (sign(defloat a) = 0) /\
         (sign(defloat b) = 0) /\
         (exponent(defloat a) < exponent(defloat b) \/
          (exponent(defloat a) = exponent(defloat b)) /\
          fraction(defloat a) <= fraction(defloat b))
         ==> VAL(a) <= VAL(b)`,
  REWRITE_TAC[REAL_LE_LT; LE_LT] THEN
  REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[] THENL
   [DISJ1_TAC THEN MATCH_MP_TAC FLOAT_ORDERED_POS_LT THEN ASM_REWRITE_TAC[];
    DISJ1_TAC THEN MATCH_MP_TAC FLOAT_ORDERED_POS_LT THEN ASM_REWRITE_TAC[];
    DISJ2_TAC THEN ASM_REWRITE_TAC[VAL; VALOF]]);;

(* ------------------------------------------------------------------------- *)
(* Other lemmas.                                                             *)
(* ------------------------------------------------------------------------- *)

let FLOAT_BINARY_INTERVAL = prove
 (`!a. ISNORMAL(a)
       ==> &2 pow (exponent(defloat a)) / &2 pow 127 <= abs(VAL(a)) /\
           abs(VAL(a)) < &2 * &2 pow (exponent(defloat a)) / &2 pow 127`,
  GEN_TAC THEN REWRITE_TAC[ISNORMAL; is_normal] THEN
  DISCH_THEN(CONJUNCTS_THEN2 MP_TAC ASSUME_TAC) THEN
  ASM_CASES_TAC `exponent(defloat a) = 0` THEN
  ASM_REWRITE_TAC[LT_REFL] THEN DISCH_TAC THEN
  ASM_REWRITE_TAC[VAL; VALOF] THEN
  ONCE_REWRITE_TAC[REAL_ABS_MUL] THEN
  REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NEG; REAL_ABS_NUM] THEN
  REWRITE_TAC[REAL_POW_ONE; REAL_MUL_LID] THEN
  REWRITE_TAC[bias; float_format; expwidth; fracwidth] THEN
  REWRITE_TAC[ARITH] THEN
  REWRITE_TAC[REAL_ABS_DIV; REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NUM] THEN
  SUBGOAL_THEN `abs(&1 + &(fraction (defloat a)) / &2 pow 23) =
                &1 + &(fraction (defloat a)) / &2 pow 23`
  SUBST1_TAC THENL
   [REWRITE_TAC[REAL_ABS_REFL] THEN
    MATCH_MP_TAC REAL_LE_ADD THEN REWRITE_TAC[REAL_POS] THEN
    REWRITE_TAC[real_div] THEN MATCH_MP_TAC REAL_LE_MUL THEN
    REWRITE_TAC[REAL_POS] THEN
    MATCH_MP_TAC REAL_LE_INV THEN MATCH_MP_TAC REAL_POW_LE THEN
    REWRITE_TAC[REAL_POS]; ALL_TAC] THEN
  CONJ_TAC THEN GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THENL
   [MATCH_MP_TAC REAL_LE_LMUL THEN CONJ_TAC THENL
     [REWRITE_TAC[real_div] THEN MATCH_MP_TAC REAL_LE_MUL THEN CONJ_TAC THEN
      TRY(MATCH_MP_TAC REAL_LE_INV) THEN MATCH_MP_TAC REAL_POW_LE THEN
      REWRITE_TAC[REAL_POS]; ALL_TAC] THEN
    REWRITE_TAC[REAL_LE_ADDR] THEN
    REWRITE_TAC[real_div] THEN MATCH_MP_TAC REAL_LE_MUL THEN
    REWRITE_TAC[REAL_POS] THEN
    MATCH_MP_TAC REAL_LE_INV THEN MATCH_MP_TAC REAL_POW_LE THEN
    REWRITE_TAC[REAL_OF_NUM_LE; ARITH];
    GEN_REWRITE_TAC RAND_CONV [REAL_MUL_SYM] THEN
    REWRITE_TAC[REAL_MUL_RID] THEN
    MATCH_MP_TAC REAL_LT_LMUL THEN CONJ_TAC THENL
     [REWRITE_TAC[real_div] THEN MATCH_MP_TAC REAL_LT_MUL THEN CONJ_TAC THEN
      TRY(MATCH_MP_TAC REAL_LT_INV) THEN MATCH_MP_TAC REAL_POW_LT THEN
      REWRITE_TAC[REAL_OF_NUM_LT; ARITH]; ALL_TAC] THEN
    SUBST1_TAC(REAL_ARITH `&2 = &1 + &1`) THEN
    REWRITE_TAC[REAL_LT_LADD] THEN
    REWRITE_TAC[REAL_ARITH `&1 + &1 = &2`] THEN
    MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 23` THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC; real_div] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    REWRITE_TAC[REAL_MUL_RID; REAL_OF_NUM_LT] THEN
    MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
    REWRITE_TAC[IS_VALID; float_format; fracwidth] THEN
    DISCH_THEN(MP_TAC o last o CONJUNCTS) THEN
    REWRITE_TAC[ARITH]]);;

let FLOAT_BINARY_INTERVAL_UNIQUE = prove
 (`!a. ISNORMAL(a) /\
       &2 pow n / &2 pow 127 <= abs(VAL(a)) /\
       abs(VAL(a)) < &2 * &2 pow n / &2 pow 127
       ==> (exponent(defloat a) = n)`,
  REPEAT STRIP_TAC THEN
  MATCH_MP_TAC(ARITH_RULE `n < SUC m /\ m < SUC n ==> (m = n)`) THEN
  MP_TAC(SPECL [`SUC (exponent (defloat a))`; `n:num`; `&2`]
         REAL_POW_MONO) THEN
  MP_TAC(SPECL [`SUC n`; `exponent (defloat a)`; `&2`]
         REAL_POW_MONO) THEN
  REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
  REWRITE_TAC[ARITH_RULE `SUC n <= m <=> ~(m < SUC n)`] THEN
  MATCH_MP_TAC(TAUT
    `~b /\ ~d ==> (~a ==> b) ==> (~c ==> d) ==> c /\ a`) THEN
  REWRITE_TAC[REAL_NOT_LE] THEN
  FIRST_ASSUM(STRIP_ASSUME_TAC o MATCH_MP FLOAT_BINARY_INTERVAL) THEN
  REWRITE_TAC[real_pow] THEN CONJ_TAC THEN
  MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN
  EXISTS_TAC `inv(&2 pow 127)` THEN
  (CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC]) THEN
  REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN REWRITE_TAC[GSYM real_div] THEN
  MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `abs(VAL a)` THEN
  ASM_REWRITE_TAC[]);;

let FLOAT_BINARY_INTERVAL_LE = prove
 (`!n a. ISFINITE(a) /\
         abs(VAL(a)) < &2 pow n / &2 pow 126
         ==> exponent(defloat a) <= n`,
  INDUCT_TAC THENL
   [GEN_TAC THEN REWRITE_TAC[real_pow; real_div; REAL_MUL_LID] THEN
    DISCH_TAC THEN MP_TAC(SPEC `a:float` ISDENORMAL_VALUE) THEN
    ASM_REWRITE_TAC[] THEN MP_TAC(SPEC `a:float` ISZERO_VALUE) THEN
    ASM_REWRITE_TAC[ISZERO; ISDENORMAL] THEN
    ASM_REWRITE_TAC[LE] THEN MESON_TAC[is_zero; is_denormal];
    GEN_TAC THEN STRIP_TAC THEN REWRITE_TAC[LE] THEN
    ASM_CASES_TAC `&2 pow (SUC n) / &2 pow 127 <= abs(VAL(a))` THENL
     [DISJ1_TAC THEN MATCH_MP_TAC FLOAT_BINARY_INTERVAL_UNIQUE THEN
      ASM_REWRITE_TAC[] THEN CONJ_TAC THENL
       [ASM_REWRITE_TAC[ISNORMAL_VALUE] THEN
        MATCH_MP_TAC REAL_LE_TRANS THEN
        EXISTS_TAC `&2 pow SUC n / &2 pow 127` THEN
        ASM_REWRITE_TAC[] THEN
        MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN
        EXISTS_TAC `&2 pow 127` THEN
        REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
        CONV_TAC REAL_RAT_REDUCE_CONV THEN
        REWRITE_TAC[real_pow; REAL_MUL_RID] THEN
        GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THEN
        MATCH_MP_TAC REAL_LE_LMUL THEN
        REWRITE_TAC[REAL_OF_NUM_LE; ARITH; REAL_OF_NUM_POW] THEN
        REWRITE_TAC[num_CONV `1`; LE_SUC_LT] THEN
        REWRITE_TAC[EXP_EQ_0; ARITH_RULE `0 < x <=> ~(x = 0)`] THEN
        REWRITE_TAC[ARITH];
        SUBGOAL_THEN `&2 * &2 pow SUC n / &2 pow 127 =
                      &2 pow (SUC n) / &2 pow 126`
        (fun th -> ASM_REWRITE_TAC[th]) THEN
        ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
        REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
        AP_TERM_TAC THEN CONV_TAC REAL_RAT_REDUCE_CONV];
      DISJ2_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
      ASM_REWRITE_TAC[] THEN
      FIRST_ASSUM(UNDISCH_TAC o check is_neg o concl) THEN
      SUBGOAL_THEN `&2 pow SUC n / &2 pow 127 =
                    &2 pow n / &2 pow 126`
      (fun th -> ASM_REWRITE_TAC[REAL_NOT_LE; th]) THEN
      REWRITE_TAC[real_pow] THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
      REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
      AP_TERM_TAC THEN CONV_TAC REAL_RAT_REDUCE_CONV]]);;

let FLOAT_ULP_POS = prove
 (`!a. &0 < ULP(a)`,
  GEN_TAC THEN REWRITE_TAC[ULP; ulp] THEN
  REWRITE_TAC[valof; real_pow; REAL_MUL_RID] THEN
  REWRITE_TAC[float_format; fracwidth; expwidth; bias] THEN
  REWRITE_TAC[ARITH] THEN COND_CASES_TAC THEN ASM_REWRITE_TAC[] THENL
   [REWRITE_TAC[real_div; REAL_MUL_RZERO; REAL_MUL_LZERO] THEN
    REWRITE_TAC[REAL_MUL_LID; REAL_SUB_RZERO] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV;
    REWRITE_TAC[REAL_MUL_LID; GSYM REAL_SUB_LDISTRIB] THEN
    REWRITE_TAC[real_div] THEN
    REPEAT(MATCH_MP_TAC REAL_LT_MUL THEN CONJ_TAC) THENL
     [MATCH_MP_TAC REAL_POW_LT THEN CONV_TAC REAL_RAT_REDUCE_CONV;
      CONV_TAC REAL_RAT_REDUCE_CONV;
      CONV_TAC REAL_RAT_REDUCE_CONV]]);;

let FLOAT_ULP_EXPLICIT = prove
 (`!a. ULP(a) = if exponent(defloat a) = 0 then inv(&2 pow 149)
                else &2 pow (exponent(defloat a)) / &2 pow 150`,
  GEN_TAC THEN REWRITE_TAC[ULP; ulp; valof] THEN
  COND_CASES_TAC THEN ASM_REWRITE_TAC[real_pow; REAL_MUL_LID] THENL
   [REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_MUL_RZERO] THEN
    REWRITE_TAC[bias; fracwidth; float_format; expwidth] THEN
    REWRITE_TAC[ARITH] THEN CONV_TAC REAL_RAT_REDUCE_CONV;
    REWRITE_TAC[GSYM REAL_SUB_LDISTRIB] THEN
    REWRITE_TAC[REAL_ARITH `(a + b) - (a + c) = b - c:real`] THEN
    REWRITE_TAC[real_div; REAL_MUL_LZERO; REAL_SUB_RZERO] THEN
    REWRITE_TAC[bias; fracwidth; float_format; expwidth] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC; ARITH] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV]);;

let FLOAT_ULP_NORMAL = prove
 (`!a. ISNORMAL(a) ==> (ULP(a) = &2 pow (exponent(defloat a)) / &2 pow 150)`,
  GEN_TAC THEN REWRITE_TAC[ISNORMAL; is_normal] THEN
  DISCH_THEN(ASSUME_TAC o CONJUNCT1) THEN
  REWRITE_TAC[FLOAT_ULP_EXPLICIT] THEN
  COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
  POP_ASSUM_LIST(MP_TAC o end_itlist CONJ) THEN ARITH_TAC);;

let FLOAT_VAL_EXPONENT = prove
 (`!a b. ISNORMAL(a) /\ ISNORMAL(b) /\ (VAL(a) = VAL(b))
         ==> (exponent(defloat a) = exponent(defloat b))`,
  MESON_TAC[FLOAT_BINARY_INTERVAL; FLOAT_BINARY_INTERVAL_UNIQUE]);;

(* ------------------------------------------------------------------------- *)
(* Trickier lemmas about scaling.                                            *)
(* ------------------------------------------------------------------------- *)

let FLOAT_SCALE_UP = prove
 (`!n a. ISNORMAL(a) /\
         abs(&2 pow n * VAL(a)) < threshold(float_format)
         ==> ?b. ISNORMAL(b) /\
                 (exponent(defloat b) = exponent(defloat a) + n) /\
                 (VAL(b) = &2 pow n * VAL(a))`,
  REPEAT STRIP_TAC THEN
  SUBGOAL_THEN `~(exponent (defloat a) = 0)` ASSUME_TAC THENL
   [MATCH_MP_TAC(ARITH_RULE `0 < a ==> ~(a = 0)`) THEN
    ASM_MESON_TAC[ISNORMAL; is_normal]; ALL_TAC] THEN
  ASM_REWRITE_TAC[VAL; VALOF] THEN
  EXISTS_TAC
   `float(sign(defloat a),exponent(defloat a) + n,fraction(defloat a))` THEN
  SUBGOAL_THEN `exponent(defloat a) + n < emax float_format` ASSUME_TAC THENL
   [REWRITE_TAC[float_format; expwidth; emax] THEN
    UNDISCH_TAC `abs(&2 pow n * VAL a) < threshold float_format` THEN
    CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[ARITH; NOT_LT] THEN
    DISCH_TAC THEN REWRITE_TAC[REAL_NOT_LT; threshold] THEN
    REWRITE_TAC[fracwidth; emax; float_format; bias; expwidth] THEN
    REWRITE_TAC[ARITH] THEN MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&2 pow 254 / &2 pow 127 * &2` THEN
    CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 128` THEN
    CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&2 pow 256` THEN
    CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    ASM_REWRITE_TAC[VAL; VALOF; real_div] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NEG] THEN
    REWRITE_TAC[REAL_ABS_NUM; REAL_POW_ONE; REAL_MUL_LID] THEN
    REWRITE_TAC[bias; float_format; expwidth; fracwidth] THEN
    REWRITE_TAC[NUM_REDUCE_CONV `2 EXP (8 - 1) - 1`] THEN
    REWRITE_TAC[REAL_ABS_INV; REAL_ABS_POW; REAL_ABS_NUM] THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&2 pow 128 * &2 pow n * &2 pow exponent (defloat a) *
               inv (&2 pow 127) * &1` THEN
    CONJ_TAC THENL
     [REWRITE_TAC[REAL_MUL_RID; REAL_MUL_ASSOC] THEN
      ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
      REWRITE_TAC[REAL_MUL_ASSOC] THEN
      CONV_TAC(RAND_CONV REAL_RAT_REDUCE_CONV) THEN
      REWRITE_TAC[GSYM real_pow; GSYM REAL_POW_ADD] THEN
      MATCH_MP_TAC REAL_POW_MONO THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
      UNDISCH_TAC `255 <= exponent (defloat a) + n` THEN ARITH_TAC;
      REWRITE_TAC[REAL_MUL_ASSOC] THEN
      MATCH_MP_TAC REAL_LE_LMUL THEN
      REPEAT(CONJ_TAC THEN TRY (MATCH_MP_TAC REAL_LE_MUL)) THEN
      TRY(MATCH_MP_TAC REAL_LE_INV) THEN
      TRY(MATCH_MP_TAC REAL_POW_LE) THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
      MATCH_MP_TAC(REAL_ARITH `&0 <= a ==> &1 <= abs(&1 + a)`) THEN
      MATCH_MP_TAC REAL_LE_MUL THEN CONJ_TAC THEN
      TRY(MATCH_MP_TAC REAL_LE_INV) THEN
      REWRITE_TAC[REAL_POS] THEN
      MATCH_MP_TAC REAL_POW_LE THEN
      REWRITE_TAC[REAL_OF_NUM_LE; ARITH]]; ALL_TAC] THEN
  REWRITE_TAC[ISNORMAL; is_normal] THEN
  SUBGOAL_THEN
   `defloat(float(sign(defloat a),exponent(defloat a) + n,fraction(defloat a)))
    = (sign(defloat a),exponent(defloat a) + n,fraction(defloat a))`
  SUBST1_TAC THENL
   [REWRITE_TAC[GSYM float_tybij; is_valid] THEN
    MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN REWRITE_TAC[IS_VALID] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC LT_TRANS THEN EXISTS_TAC `emax float_format` THEN
    ASM_REWRITE_TAC[] THEN
    REWRITE_TAC[float_format; expwidth; emax] THEN
    REWRITE_TAC[ARITH];
    REWRITE_TAC[sign; exponent; fraction] THEN
    ASM_REWRITE_TAC[ADD_EQ_0] THEN
    REWRITE_TAC[REAL_POW_ADD; real_div; REAL_MUL_AC] THEN
    UNDISCH_TAC `~(exponent (defloat a) = 0)` THEN ARITH_TAC]);;

let FLOAT_SCALE_DOWN_NORM = prove
 (`!n a. ISNORMAL(a) /\
         inv(&2 pow 126) <= abs(VAL(a) / &2 pow n)
         ==> ?b. ISNORMAL(b) /\
                 (exponent(defloat a) = exponent(defloat b) + n) /\
                 (VAL(b) = VAL(a) / &2 pow n)`,
  REPEAT STRIP_TAC THEN
  SUBGOAL_THEN `~(exponent (defloat a) = 0)` ASSUME_TAC THENL
   [MATCH_MP_TAC(ARITH_RULE `0 < a ==> ~(a = 0)`) THEN
    ASM_MESON_TAC[ISNORMAL; is_normal]; ALL_TAC] THEN
  ASM_REWRITE_TAC[VAL; VALOF] THEN
  EXISTS_TAC
   `float(sign(defloat a),exponent(defloat a) - n,fraction(defloat a))` THEN
  SUBGOAL_THEN `0 < exponent(defloat a) - n` ASSUME_TAC THENL
   [MATCH_MP_TAC(ARITH_RULE `SUC a <= b ==> 0 < b - a`) THEN
    UNDISCH_TAC `inv (&2 pow 126) <= abs (VAL a / &2 pow n)` THEN
    CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[NOT_LE; REAL_NOT_LE] THEN
    DISCH_TAC THEN REWRITE_TAC[real_div; REAL_ABS_MUL] THEN
    MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `&2 pow n` THEN
    CONJ_TAC THENL [MATCH_MP_TAC REAL_POW_LT THEN
                    REWRITE_TAC[REAL_OF_NUM_LT; ARITH]; ALL_TAC] THEN
    REWRITE_TAC[REAL_ABS_INV; REAL_ABS_POW; REAL_ABS_NUM] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    SUBGOAL_THEN `inv(&2 pow n) * &2 pow n = &1` SUBST1_TAC THENL
     [MATCH_MP_TAC REAL_MUL_LINV THEN MATCH_MP_TAC REAL_LT_IMP_NZ THEN
      MATCH_MP_TAC REAL_POW_LT THEN
      REWRITE_TAC[REAL_OF_NUM_LT; ARITH]; ALL_TAC] THEN
    REWRITE_TAC[REAL_MUL_RID] THEN
    ASM_REWRITE_TAC[VAL; VALOF] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_NEG; REAL_ABS_POW] THEN
    REWRITE_TAC[REAL_POW_ONE; REAL_ABS_NUM; REAL_MUL_LID] THEN
    REWRITE_TAC[bias; float_format; fracwidth; expwidth] THEN
    REWRITE_TAC[NUM_REDUCE_CONV `2 EXP (8 - 1) - 1`] THEN
    MATCH_MP_TAC REAL_LTE_TRANS THEN
    EXISTS_TAC `abs (&2 pow exponent (defloat a) / &2 pow 127) * &2` THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LT_LMUL THEN
      REWRITE_TAC[GSYM REAL_ABS_NZ; REAL_POW_EQ_0; real_div; REAL_ENTIRE] THEN
      ASM_REWRITE_TAC[REAL_INV_EQ_0; REAL_POW_EQ_0] THEN
      REWRITE_TAC[REAL_OF_NUM_EQ; ARITH] THEN
      MATCH_MP_TAC(REAL_ARITH `&0 <= x /\ x < &1 ==> abs(&1 + x) < &2`) THEN
      CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LE_MUL THEN REWRITE_TAC[REAL_POS] THEN
        MATCH_MP_TAC REAL_LE_INV THEN MATCH_MP_TAC REAL_POW_LE THEN
        REWRITE_TAC[REAL_OF_NUM_LE; ARITH]; ALL_TAC] THEN
      MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 23` THEN
      CONJ_TAC THENL [MATCH_MP_TAC REAL_POW_LT THEN
        REWRITE_TAC[REAL_OF_NUM_LT; ARITH]; ALL_TAC] THEN
      REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
      CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN
      REWRITE_TAC[REAL_MUL_LID; REAL_MUL_RID] THEN
      REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_LT] THEN
      MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN REWRITE_TAC[IS_VALID] THEN
      REWRITE_TAC[fracwidth; float_format] THEN
      STRIP_TAC THEN ASM_REWRITE_TAC[];
      GEN_REWRITE_TAC RAND_CONV [REAL_MUL_SYM] THEN
      MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN
      EXISTS_TAC `&2 pow 126` THEN
      REWRITE_TAC[REAL_ABS_MUL; GSYM REAL_MUL_ASSOC; real_div] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      REWRITE_TAC[REAL_MUL_RID; REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NUM] THEN
      MATCH_MP_TAC REAL_POW_MONO THEN
      UNDISCH_TAC `exponent (defloat a) < SUC n` THEN ARITH_TAC]; ALL_TAC] THEN
  REWRITE_TAC[ISNORMAL; is_normal] THEN
  SUBGOAL_THEN
   `defloat(float(sign(defloat a),exponent(defloat a)-n,fraction(defloat a))) =
    (sign(defloat a),exponent(defloat a)-n,fraction(defloat a))`
  SUBST1_TAC THENL
   [REWRITE_TAC[GSYM float_tybij; is_valid] THEN
    MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN REWRITE_TAC[IS_VALID] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC LET_TRANS THEN EXISTS_TAC `exponent (defloat a)` THEN
    ASM_REWRITE_TAC[] THEN ARITH_TAC; ALL_TAC] THEN
  ASM_REWRITE_TAC[sign; exponent; fraction] THEN REPEAT CONJ_TAC THENL
   [MATCH_MP_TAC LET_TRANS THEN EXISTS_TAC `exponent (defloat a)` THEN
    CONJ_TAC THENL [ARITH_TAC; ALL_TAC] THEN
    SUBGOAL_THEN `ISFINITE(a)` MP_TAC THENL
     [ASM_MESON_TAC[ISFINITE]; ALL_TAC] THEN
    REWRITE_TAC[ISFINITE_THM; is_finite] THEN
    ASM_REWRITE_TAC[is_normal; is_zero; is_denormal] THEN MESON_TAC[];
    UNDISCH_TAC `0 < exponent (defloat a) - n` THEN ARITH_TAC; ALL_TAC] THEN
  SUBGOAL_THEN `~(exponent(defloat a) - n = 0)` ASSUME_TAC THENL
   [UNDISCH_TAC `0 < exponent (defloat a) - n` THEN ARITH_TAC;
    ASM_REWRITE_TAC[]] THEN
  SUBGOAL_THEN `&2 pow (exponent (defloat a) - n) =
                &2 pow (exponent (defloat a)) / &2 pow n`
  SUBST1_TAC THENL
   [MATCH_MP_TAC REAL_POW_SUB THEN REWRITE_TAC[REAL_OF_NUM_EQ; ARITH] THEN
    UNDISCH_TAC `0 < exponent (defloat a) - n` THEN ARITH_TAC;
    REWRITE_TAC[real_div; REAL_MUL_AC]]);;

let REAL_EXP_IVAL_LEMMA = prove
 (`!N. (?n. exp (IVAL N * ln (&2)) = &2 pow n) \/
       (?n. exp (IVAL N * ln (&2)) = inv(&2 pow n))`,
  GEN_TAC THEN MP_TAC(SPEC `IVAL N` (CONJUNCT2 INT_real_tybij)) THEN
  REWRITE_TAC[CONJUNCT1 INT_real_tybij] THEN
  DISCH_THEN(MP_TAC o CONJUNCT1) THEN
  DISCH_THEN(X_CHOOSE_THEN `n:num` MP_TAC) THEN
  DISCH_THEN(DISJ_CASES_TAC o MATCH_MP(REAL_ARITH
   `(abs(x:real) = a) ==> (x = a) \/ (x = --a)`)) THENL
   [DISJ1_TAC THEN EXISTS_TAC `n:num` THEN ASM_REWRITE_TAC[] THEN
    REWRITE_TAC[REAL_EXP_N] THEN AP_THM_TAC THEN AP_TERM_TAC THEN
    REWRITE_TAC[REAL_EXP_LN] THEN REAL_ARITH_TAC;
    DISJ2_TAC THEN EXISTS_TAC `n:num` THEN ASM_REWRITE_TAC[] THEN
    REWRITE_TAC[REAL_MUL_LNEG; REAL_EXP_NEG] THEN
    REWRITE_TAC[REAL_EXP_N] THEN AP_TERM_TAC THEN
    AP_THM_TAC THEN AP_TERM_TAC THEN
    REWRITE_TAC[REAL_EXP_LN] THEN REAL_ARITH_TAC]);;

let FLOAT_SCALB_NORM = prove
 (`!a N. ISNORMAL a /\
         inv(&2 pow 126) <= abs(exp(IVAL N * ln(&2)) * VAL(a)) /\
         abs(exp(IVAL N * ln(&2)) * VAL(a)) < threshold(float_format)
         ==> ISFINITE(SCALB(a,N)) /\
             ISNORMAL(SCALB(a,N)) /\
             (VAL(SCALB(a,N)) = exp(IVAL N * ln(&2)) * VAL(a)) /\
             (ULP(SCALB(a,N)) = exp(IVAL N * ln(&2)) * ULP(a))`,
  REPEAT GEN_TAC THEN
  MP_TAC(SPECL [`a:float`; `N:INT`] FLOAT_SCALB) THEN
  DISJ_CASES_THEN(X_CHOOSE_THEN `n:num` SUBST1_TAC)
   (SPEC `N:INT` REAL_EXP_IVAL_LEMMA) THEN
  DISCH_THEN(fun th -> STRIP_TAC THEN MP_TAC th) THENL
   [SUBGOAL_THEN `ISFINITE(a)` ASSUME_TAC THENL
     [ASM_MESON_TAC[ISFINITE]; ALL_TAC] THEN
    ASM_REWRITE_TAC[] THEN
    DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
    SUBGOAL_THEN
     `?b. ISNORMAL(b) /\
          (exponent(defloat b) = exponent(defloat a) + n) /\
          (VAL(b) = &2 pow n * VAL(a))`
    STRIP_ASSUME_TAC THENL
     [MATCH_MP_TAC FLOAT_SCALE_UP THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
    SUBGOAL_THEN `error (&2 pow n * VAL a) = &0` SUBST1_TAC THENL
     [MATCH_MP_TAC ERROR_IS_ZERO THEN EXISTS_TAC `b:float` THEN
      ASM_REWRITE_TAC[] THEN ASM_MESON_TAC[ISFINITE];
      REWRITE_TAC[REAL_ADD_RID] THEN DISCH_TAC] THEN
    ASM_REWRITE_TAC[] THEN
    SUBGOAL_THEN `ISNORMAL(SCALB (a,N))` ASSUME_TAC THENL
     [ASM_REWRITE_TAC[ISNORMAL_VALUE]; ALL_TAC] THEN
    ASM_REWRITE_TAC[] THEN
    MP_TAC(SPEC `a:float` FLOAT_ULP_NORMAL) THEN
    MP_TAC(SPEC `SCALB (a,N)` FLOAT_ULP_NORMAL) THEN
    ASM_REWRITE_TAC[] THEN
    REPEAT(DISCH_THEN SUBST1_TAC) THEN
    SUBGOAL_THEN `exponent (defloat (SCALB (a,N))) = exponent(defloat b)`
    SUBST1_TAC THENL
     [MATCH_MP_TAC FLOAT_VAL_EXPONENT THEN ASM_REWRITE_TAC[] THEN
      ASM_MESON_TAC[ISFINITE]; ALL_TAC] THEN
    ASM_REWRITE_TAC[REAL_POW_ADD; REAL_MUL_AC; real_div];

    REWRITE_TAC[ONCE_REWRITE_RULE[REAL_MUL_SYM] (GSYM real_div)] THEN
    RULE_ASSUM_TAC
     (REWRITE_RULE[ONCE_REWRITE_RULE[REAL_MUL_SYM] (GSYM real_div)]) THEN
    SUBGOAL_THEN `ISFINITE(a)` ASSUME_TAC THENL
     [ASM_MESON_TAC[ISFINITE]; ALL_TAC] THEN
    ASM_REWRITE_TAC[] THEN
    DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
    SUBGOAL_THEN
     `?b. ISNORMAL(b) /\
          (exponent(defloat a) = exponent(defloat b) + n) /\
          (VAL(b) = VAL(a) / &2 pow n)`
    STRIP_ASSUME_TAC THENL
     [MATCH_MP_TAC FLOAT_SCALE_DOWN_NORM THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
    SUBGOAL_THEN `error (VAL a / &2 pow n) = &0` SUBST1_TAC THENL
     [MATCH_MP_TAC ERROR_IS_ZERO THEN EXISTS_TAC `b:float` THEN
      ASM_REWRITE_TAC[] THEN ASM_MESON_TAC[ISFINITE];
      REWRITE_TAC[REAL_ADD_RID] THEN DISCH_TAC] THEN
    ASM_REWRITE_TAC[] THEN
    SUBGOAL_THEN `ISNORMAL(SCALB (a,N))` ASSUME_TAC THENL
     [ASM_REWRITE_TAC[ISNORMAL_VALUE]; ALL_TAC] THEN
    ASM_REWRITE_TAC[] THEN
    MP_TAC(SPEC `a:float` FLOAT_ULP_NORMAL) THEN
    MP_TAC(SPEC `SCALB (a,N)` FLOAT_ULP_NORMAL) THEN
    ASM_REWRITE_TAC[] THEN
    REPEAT(DISCH_THEN SUBST1_TAC) THEN
    SUBGOAL_THEN `exponent (defloat (SCALB (a,N))) = exponent(defloat b)`
    SUBST1_TAC THENL
     [MATCH_MP_TAC FLOAT_VAL_EXPONENT THEN ASM_REWRITE_TAC[] THEN
      ASM_MESON_TAC[ISFINITE]; ALL_TAC] THEN
    REWRITE_TAC[REAL_POW_ADD; real_div; GSYM REAL_MUL_ASSOC] THEN
    AP_TERM_TAC THEN
    GEN_REWRITE_TAC RAND_CONV [REAL_MUL_SYM] THEN
    GEN_REWRITE_TAC LAND_CONV [GSYM REAL_MUL_RID] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    AP_TERM_TAC THEN CONV_TAC SYM_CONV THEN
    MATCH_MP_TAC REAL_MUL_LINV THEN REWRITE_TAC[REAL_POW_EQ_0] THEN
    REWRITE_TAC[REAL_OF_NUM_EQ; ARITH]]);;

let FLOAT_NORM_MINIMUM = prove
 (`!a. ISFINITE(a) /\
       abs(VAL(a)) > inv(&2 pow 126)
       ==> abs(VAL(a)) >= inv(&2 pow 126) + inv(&2 pow 150)`,
  GEN_TAC THEN REWRITE_TAC[real_gt] THEN
  DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
  REWRITE_TAC[VAL; VALOF] THEN
  COND_CASES_TAC THEN ASM_REWRITE_TAC[] THENL
   [CONV_TAC CONTRAPOS_CONV THEN DISCH_THEN(K ALL_TAC) THEN
    REWRITE_TAC[REAL_NOT_LT] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NEG] THEN
    REWRITE_TAC[REAL_ABS_NUM; REAL_POW_ONE; REAL_MUL_LID] THEN
    REWRITE_TAC[bias; float_format; fracwidth; expwidth] THEN
    REWRITE_TAC[ARITH] THEN
    SUBGOAL_THEN `abs (&2 / &2 pow 127) = inv(&2 pow 126)` SUBST1_TAC THENL
     [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_RID] THEN
    MATCH_MP_TAC REAL_LE_LMUL THEN
    CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    REWRITE_TAC[real_div; REAL_ABS_MUL; REAL_ABS_NUM] THEN
    REWRITE_TAC[REAL_ABS_POW; REAL_ABS_NUM; REAL_ABS_INV] THEN
    MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 23` THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    CONV_TAC(LAND_CONV REAL_RAT_REDUCE_CONV) THEN
    REWRITE_TAC
     [REAL_MUL_RID; REAL_MUL_LID; REAL_OF_NUM_LE; REAL_OF_NUM_POW] THEN
    MP_TAC(SPEC `a:float` IS_VALID_DEFLOAT) THEN
    REWRITE_TAC[IS_VALID; float_format; fracwidth] THEN
    MESON_TAC[LT_IMP_LE]; ALL_TAC] THEN
  ASM_CASES_TAC `fraction (defloat a) = 0` THEN ASM_REWRITE_TAC[] THENL
   [REWRITE_TAC[REAL_NOT_LT] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NEG] THEN
    REWRITE_TAC[REAL_ABS_NUM; REAL_POW_ONE; REAL_MUL_LID] THEN
    REWRITE_TAC[bias; float_format; fracwidth; expwidth] THEN
    REWRITE_TAC[ARITH] THEN REWRITE_TAC[real_div; REAL_MUL_LZERO] THEN
    REWRITE_TAC[REAL_ADD_RID; REAL_ABS_1; REAL_MUL_RID] THEN
    REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_INV; REAL_ABS_POW; REAL_ABS_NUM] THEN
    REWRITE_TAC[real_ge] THEN DISCH_TAC THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `inv(&2 pow 125)` THEN
    CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 127` THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN CONV_TAC REAL_RAT_REDUCE_CONV THEN
    REWRITE_TAC[REAL_MUL_RID; GSYM (REAL_RAT_REDUCE_CONV `&2 pow 2`)] THEN
    MATCH_MP_TAC REAL_POW_MONO THEN REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
    MATCH_MP_TAC(ARITH_RULE `~(a = 0) /\ ~(a = 1) ==> 2 <= a`) THEN
    ASM_REWRITE_TAC[] THEN DISCH_THEN SUBST_ALL_TAC THEN
    UNDISCH_TAC `inv (&2 pow 126) < &2 pow 1 * inv (&2 pow 127)` THEN
    CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
  DISCH_THEN(K ALL_TAC) THEN
  REWRITE_TAC[REAL_ABS_MUL; REAL_ABS_POW; REAL_ABS_NEG] THEN
  REWRITE_TAC[REAL_ABS_NUM; REAL_POW_ONE; REAL_MUL_LID] THEN
  REWRITE_TAC[real_ge] THEN
  SUBGOAL_THEN
   `inv(&2 pow 126) + inv(&2 pow 150) =
    (&2 pow 1 / &2 pow 127) * (&1 + inv(&2 pow 24))`
  SUBST1_TAC THENL
   [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
  MATCH_MP_TAC REAL_LE_MUL2 THEN
  REWRITE_TAC[bias; float_format; fracwidth; expwidth] THEN
  REWRITE_TAC[ARITH] THEN
  REPEAT CONJ_TAC THEN TRY(CONV_TAC REAL_RAT_REDUCE_CONV THEN NO_TAC) THENL
   [REWRITE_TAC[real_div; REAL_ABS_MUL; REAL_ABS_INV; REAL_ABS_POW] THEN
    REWRITE_TAC[REAL_ABS_NUM] THEN MATCH_MP_TAC REAL_LE_RMUL THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC REAL_POW_MONO THEN
      UNDISCH_TAC `~(exponent (defloat a) = 0)` THEN ARITH_TAC;
      CONV_TAC REAL_RAT_REDUCE_CONV];
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&1 + &(fraction (defloat a)) / &2 pow 23` THEN
    REWRITE_TAC[REAL_ABS_LE; REAL_LE_LADD] THEN
    MATCH_MP_TAC REAL_LE_RCANCEL_IMP THEN EXISTS_TAC `&2 pow 23` THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&1` THEN
    CONV_TAC REAL_RAT_REDUCE_CONV THEN REWRITE_TAC[REAL_MUL_RID] THEN
    REWRITE_TAC[REAL_OF_NUM_LE] THEN
    UNDISCH_TAC `~(fraction (defloat a) = 0)` THEN ARITH_TAC]);;

let FLOAT_SCALB_DENORM = prove
 (`!a N. ISNORMAL a /\
         abs(exp(IVAL N * ln(&2)) * VAL(a)) < inv(&2 pow 126)
         ==> ISFINITE(SCALB(a,N)) /\
             abs(VAL(SCALB(a,N))) <= inv(&2 pow 126) /\
             abs(VAL(SCALB(a,N)) - exp(IVAL N * ln(&2)) * VAL(a))
                <= inv(&2) * ULP(SCALB(a,N)) /\
             (ULP(SCALB(a,N)) = inv(&2 pow 149)) /\
             ULP(SCALB(a,N)) >= &2 * exp(IVAL N * ln(&2)) * ULP(a)`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  MP_TAC(SPECL [`a:float`; `N:INT`] FLOAT_SCALB) THEN
  SUBGOAL_THEN `ISFINITE(a)` ASSUME_TAC THENL
   [ASM_MESON_TAC[ISFINITE]; ALL_TAC] THEN
  ASM_REWRITE_TAC[] THEN
  SUBGOAL_THEN `abs(exp(IVAL N * ln (&2)) * VAL a) < threshold(float_format)`
  ASSUME_TAC THENL
   [MATCH_MP_TAC REAL_LT_TRANS THEN EXISTS_TAC `inv(&2 pow 126)` THEN
    ASM_REWRITE_TAC[] THEN REWRITE_TAC[FLOAT_THRESHOLD_EXPLICIT] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
  ASM_REWRITE_TAC[] THEN STRIP_TAC THEN
  ASM_REWRITE_TAC[REAL_ARITH `(a + b) - a:real = b`] THEN
  FIRST_ASSUM(ASSUME_TAC o MATCH_MP ERROR_BOUND_TINY) THEN
  SUBGOAL_THEN `ULP (SCALB (a,N)) = inv(&2 pow 149)` ASSUME_TAC THENL
   [SUBGOAL_THEN `exponent(defloat(SCALB (a,N))) <= 1` MP_TAC THENL
     [MATCH_MP_TAC FLOAT_BINARY_INTERVAL_LE THEN ASM_REWRITE_TAC[] THEN
      MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC
       `abs(exp(IVAL N * ln (&2)) * VAL a) +
        abs(error(exp(IVAL N * ln (&2)) * VAL a))` THEN
      REWRITE_TAC[REAL_ABS_TRIANGLE] THEN
      MATCH_MP_TAC REAL_LT_TRANS THEN
      EXISTS_TAC `inv (&2 pow 126) + inv (&2 pow 150)` THEN
      CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
      MATCH_MP_TAC REAL_LTE_ADD2 THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
    REWRITE_TAC[num_CONV `1`; LE] THEN REWRITE_TAC[ARITH_SUC] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[FLOAT_ULP_EXPLICIT; ARITH] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
  ASM_REWRITE_TAC[] THEN REPEAT CONJ_TAC THENL
   [FIRST_ASSUM(fun th ->
      GEN_REWRITE_TAC (LAND_CONV o RAND_CONV) [GSYM th]) THEN
    REWRITE_TAC[GSYM REAL_NOT_LT; GSYM real_gt] THEN
    DISCH_THEN(MP_TAC o CONJ (ASSUME `ISFINITE (SCALB (a,N))`)) THEN
    DISCH_THEN(MP_TAC o MATCH_MP FLOAT_NORM_MINIMUM) THEN
    ASM_REWRITE_TAC[REAL_NOT_LE; real_ge] THEN
    MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC
       `abs(exp(IVAL N * ln (&2)) * VAL a) +
        abs(error(exp(IVAL N * ln (&2)) * VAL a))` THEN
    REWRITE_TAC[REAL_ABS_TRIANGLE] THEN
    MATCH_MP_TAC REAL_LTE_ADD2 THEN ASM_REWRITE_TAC[];
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `inv(&2 pow 150)` THEN
    ASM_REWRITE_TAC[] THEN CONV_TAC REAL_RAT_REDUCE_CONV;
    REWRITE_TAC[real_ge] THEN
    FIRST_ASSUM(SUBST1_TAC o MATCH_MP FLOAT_ULP_NORMAL) THEN
    ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
    SUBGOAL_THEN `inv(&2 pow 150) * &2 = inv(&2 pow 149)` SUBST1_TAC THENL
     [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
    GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_LID] THEN
    REWRITE_TAC[REAL_MUL_ASSOC] THEN MATCH_MP_TAC REAL_LE_RMUL THEN
    CONJ_TAC THENL [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
    SUBGOAL_THEN `exp(IVAL N * ln(&2)) * &2 pow exponent (defloat a) < &2`
    MP_TAC THENL
     [MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN EXISTS_TAC `inv(&2 pow 127)` THEN
      CONJ_TAC THENL [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
      SUBGOAL_THEN `&2 * inv (&2 pow 127) = inv(&2 pow 126)` SUBST1_TAC THENL
       [CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
      MATCH_MP_TAC REAL_LET_TRANS THEN
      EXISTS_TAC `abs(exp(IVAL N * ln(&2)) * VAL a)` THEN
      ASM_REWRITE_TAC[] THEN REWRITE_TAC[REAL_ABS_MUL] THEN
      SUBGOAL_THEN `!x. abs(exp x) = exp x` (fun th -> REWRITE_TAC[th]) THENL
       [REWRITE_TAC[REAL_ABS_REFL; REAL_EXP_POS_LE]; ALL_TAC] THEN
      REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
      MATCH_MP_TAC REAL_LE_LMUL THEN REWRITE_TAC[REAL_EXP_POS_LE] THEN
      REWRITE_TAC[GSYM real_div] THEN ASM_MESON_TAC[FLOAT_BINARY_INTERVAL];
      REPEAT_TCL STRIP_THM_THEN SUBST1_TAC
      (SPEC `N:INT` REAL_EXP_IVAL_LEMMA) THENL
       [REWRITE_TAC[REAL_OF_NUM_POW; REAL_OF_NUM_MUL] THEN
        REWRITE_TAC[REAL_OF_NUM_LT; REAL_OF_NUM_LE] THEN ARITH_TAC;
        DISCH_TAC THEN MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
        EXISTS_TAC `&2 pow n` THEN CONJ_TAC THENL
         [MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC; ALL_TAC] THEN
        REWRITE_TAC[REAL_MUL_ASSOC] THEN
        SUBGOAL_THEN `&2 pow n * inv(&2 pow n) = &1` SUBST1_TAC THENL
         [MATCH_MP_TAC REAL_MUL_RINV THEN REWRITE_TAC[REAL_POW_EQ_0] THEN
          REWRITE_TAC[REAL_OF_NUM_EQ; ARITH]; ALL_TAC] THEN
        REWRITE_TAC[REAL_MUL_LID; REAL_MUL_RID] THEN
        MATCH_MP_TAC REAL_POW_MONO THEN
        REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN
        UNDISCH_TAC `inv(&2 pow n) * &2 pow exponent(defloat a) < &2` THEN
        REWRITE_TAC[GSYM LT_SUC_LE] THEN
        CONV_TAC CONTRAPOS_CONV THEN
        REWRITE_TAC[REAL_NOT_LT; NOT_LT] THEN DISCH_TAC THEN
        MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
        EXISTS_TAC `&2 pow n` THEN
        CONJ_TAC THENL
         [MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC; ALL_TAC] THEN
        REWRITE_TAC[REAL_MUL_ASSOC] THEN
        SUBGOAL_THEN `&2 pow n * inv(&2 pow n) = &1` SUBST1_TAC THENL
         [MATCH_MP_TAC REAL_MUL_RINV THEN REWRITE_TAC[REAL_POW_EQ_0] THEN
          REWRITE_TAC[REAL_OF_NUM_EQ; ARITH]; ALL_TAC] THEN
        REWRITE_TAC[REAL_MUL_LID] THEN
        ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
        REWRITE_TAC[GSYM(CONJUNCT2 real_pow)] THEN
        MATCH_MP_TAC REAL_POW_MONO THEN ASM_REWRITE_TAC[] THEN
        REWRITE_TAC[REAL_OF_NUM_LE; ARITH]]]]);;

let FLOAT_NEG_FIELDS = prove
 (`!a. ISFINITE(a)
       ==> (VAL(float(1-sign(defloat a),exponent(defloat a),
                        fraction(defloat a))) = --(VAL(a)))`,
  let lemma = prove
   (`n < 2 ==> (--(&1) pow (1 - n) = --(--(&1) pow n))`,
    REWRITE_TAC[num_CONV `2`; num_CONV `1`; LT] THEN
    STRIP_TAC THEN ASM_REWRITE_TAC[real_pow; SUB] THEN
    REWRITE_TAC[ARITH; real_pow] THEN REAL_ARITH_TAC) in
  GEN_TAC THEN DISCH_TAC THEN
  SUBGOAL_THEN `ISFINITE (float (1 - sign (defloat a),
                         (exponent (defloat a),fraction (defloat a)))) /\
                is_valid(float_format) (1 - sign (defloat a),
                         (exponent (defloat a),fraction (defloat a)))`
  STRIP_ASSUME_TAC THENL
   [MATCH_MP_TAC ISFINITE_LEMMA THEN UNDISCH_TAC `ISFINITE a` THEN
    REWRITE_TAC[IS_FINITE_EXPLICIT; ISFINITE_THM] THEN DISCH_TAC THEN
    ASM_REWRITE_TAC[ARITH] THEN ARITH_TAC;
    ASM_REWRITE_TAC[VAL] THEN
    FIRST_ASSUM(SUBST1_TAC o GEN_REWRITE_RULE I [float_tybij]) THEN
    REWRITE_TAC[VALOF; exponent] THEN COND_CASES_TAC THEN
    ASM_REWRITE_TAC[] THEN REWRITE_TAC[fraction; sign] THEN
    REWRITE_TAC[REAL_MUL_ASSOC; real_div; GSYM REAL_MUL_LNEG] THEN
    REPEAT(AP_THM_TAC THEN AP_TERM_TAC) THEN
    MATCH_MP_TAC lemma THEN
    REWRITE_TAC[REWRITE_RULE[IS_VALID] (SPEC `a:float` IS_VALID_DEFLOAT)]]);;
