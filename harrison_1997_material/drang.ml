(* ========================================================================= *)
(* Verification of polynomial approximations.                                *)
(* ========================================================================= *)

(*
 taylor_long := sum('x^n / n!','n'=1..6);
 a1 := 8388676 / 2^24;
 a2 := 11184876 / 2^26;
 p_single := x + a1 * x^2 + a2 * x^3;
 error_single := taylor_long - p_single;
 error_single_diff := diff(error_single,x);
 error_single_sfd := convert(error_single_diff,sqrfree,x);
 error_single_isfd := error_single_sfd / icontent(error_single_sfd);
 readlib(realroot);
 realroot(error_single_isfd,1/2^30);
 readlib(sturm);
 sturmseq(error_single_isfd,x);
 *)

(* ------------------------------------------------------------------------- *)
(* General theorem about bounding functions.                                 *)
(* ------------------------------------------------------------------------- *)

let BOUND_THEOREM_POS = prove
 (`!f f' a b K.
        (!x. a <= x /\ x <= b ==> (f diffl (f' x)) x) /\
        f(a) <= K /\
        f(b) <= K /\
        (!x. a <= x /\ x <= b /\ (f'(x) = &0) ==> f(x) <= K)
        ==> (!x. a <= x /\ x <= b ==> f(x) <= K)`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  ASM_CASES_TAC `a <= b` THENL
   [ALL_TAC;
    GEN_TAC THEN CONV_TAC CONTRAPOS_CONV THEN
    DISCH_TAC THEN UNDISCH_TAC `~(a <= b)` THEN
    REAL_ARITH_TAC] THEN
  SUBGOAL_THEN `!x. a <= x /\ x <= b ==> f contl x` ASSUME_TAC THENL
   [GEN_TAC THEN STRIP_TAC THEN MATCH_MP_TAC DIFF_CONT THEN
    EXISTS_TAC `(f':real->real) x` THEN
    FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
  MP_TAC(SPECL [`f:real->real`; `a:real`; `b:real`] CONT_ATTAINS) THEN
  ASM_REWRITE_TAC[] THEN
  DISCH_THEN(X_CHOOSE_THEN `M:real` MP_TAC) THEN
  DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `c:real` STRIP_ASSUME_TAC) THEN
  SUBGOAL_THEN `f(c:real) <= K` MP_TAC THENL
   [ASM_CASES_TAC `a:real = c` THENL
     [UNDISCH_TAC `f(a:real) <= K` THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
    ASM_CASES_TAC `c:real = b` THENL
     [UNDISCH_TAC `(f:real->real) c = M` THEN
      DISCH_THEN(K ALL_TAC) THEN
      UNDISCH_TAC `f(b:real) <= K` THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
    UNDISCH_TAC `a <= c` THEN
    DISCH_THEN(MP_TAC o REWRITE_RULE[REAL_LE_LT]) THEN
    UNDISCH_TAC `c <= b` THEN
    DISCH_THEN(MP_TAC o REWRITE_RULE[REAL_LE_LT]) THEN
    ASM_REWRITE_TAC[] THEN
    DISCH_TAC THEN DISCH_TAC THEN
    MP_TAC(SPECL [`a:real`; `b:real`; `c:real`] INTERVAL_LEMMA_LT) THEN
    ASM_REWRITE_TAC[] THEN
    DISCH_THEN(X_CHOOSE_THEN `d:real` STRIP_ASSUME_TAC) THEN
    SUBGOAL_THEN `f'(c:real) = &0` ASSUME_TAC THENL
     [MATCH_MP_TAC DIFF_LMAX THEN
      EXISTS_TAC `f:real->real` THEN
      EXISTS_TAC `c:real` THEN CONJ_TAC THENL
       [FIRST_ASSUM MATCH_MP_TAC THEN CONJ_TAC THEN
        MATCH_MP_TAC REAL_LT_IMP_LE THEN ASM_REWRITE_TAC[];
        EXISTS_TAC `d:real` THEN ASM_REWRITE_TAC[] THEN
        GEN_TAC THEN DISCH_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
        SUBGOAL_THEN `a < y /\ y < b` MP_TAC THENL
         [FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[];
          REAL_ARITH_TAC]];
      SUBGOAL_THEN `f(c:real) <= K` MP_TAC THENL
       [FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[] THEN
        CONJ_TAC THEN MATCH_MP_TAC REAL_LT_IMP_LE THEN
        ASM_REWRITE_TAC[];
        ASM_REWRITE_TAC[] THEN DISCH_TAC THEN
        REPEAT STRIP_TAC THEN MATCH_MP_TAC REAL_LE_TRANS THEN
        EXISTS_TAC `M:real` THEN ASM_REWRITE_TAC[] THEN
        FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[]]];
    ASM_REWRITE_TAC[] THEN DISCH_TAC THEN
    REPEAT STRIP_TAC THEN MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `M:real` THEN ASM_REWRITE_TAC[] THEN
    FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[]]);;

let BOUND_THEOREM_NEG = prove
 (`!f f' a b K.
        (!x. a <= x /\ x <= b ==> (f diffl (f' x)) x) /\
        K <= f(a) /\
        K <= f(b) /\
        (!x. a <= x /\ x <= b /\ (f'(x) = &0) ==> K <= f(x))
        ==> (!x. a <= x /\ x <= b ==> K <= f(x))`,
  REPEAT STRIP_TAC THEN MP_TAC
   (SPECL [`\x:real. --(f x)`; `\x:real. --(f' x)`] BOUND_THEOREM_POS) THEN
  DISCH_THEN(MP_TAC o SPECL [`a:real`; `b:real`; `--K`]) THEN
  REWRITE_TAC[REAL_LE_NEG2] THEN
  ASM_REWRITE_TAC[REAL_ARITH `(--x = &0) <=> (x = &0)`] THEN
  W(C SUBGOAL_THEN ASSUME_TAC o funpow 2 lhand o snd) THENL
   [REPEAT STRIP_TAC THEN MATCH_MP_TAC DIFF_NEG THEN
    FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[];
    ASM_REWRITE_TAC[] THEN DISCH_THEN MATCH_MP_TAC THEN
    ASM_REWRITE_TAC[]]);;

let BOUND_THEOREM_EXACT = prove
 (`!f f' a b K.
        (!x. a <= x /\ x <= b ==> (f diffl (f' x)) x) /\
        abs(f a) <= K /\
        abs(f b) <= K /\
        (!x. a <= x /\ x <= b /\ (f'(x) = &0) ==> abs(f x) <= K)
        ==> (!x. a <= x /\ x <= b ==> abs(f x) <= K)`,
  REPEAT GEN_TAC THEN
  REWRITE_TAC[REAL_ARITH `abs(a) <= b <=> --b <= a /\ a <= b`] THEN
  REWRITE_TAC[TAUT `a ==> b /\ c <=> (a ==> b) /\ (a ==> c)`] THEN
  REWRITE_TAC[FORALL_AND_THM] THEN STRIP_TAC THEN CONJ_TAC THENL
   [MATCH_MP_TAC BOUND_THEOREM_NEG THEN ASM_REWRITE_TAC[] THEN
    EXISTS_TAC `f':real->real` THEN ASM_REWRITE_TAC[];
    MATCH_MP_TAC BOUND_THEOREM_POS THEN ASM_REWRITE_TAC[] THEN
    EXISTS_TAC `f':real->real` THEN ASM_REWRITE_TAC[]]);;

let BOUND_THEOREM_EXACT_ALT = prove
 (`!f f' a b K.
        (!x. a <= x /\ x <= b ==> (f diffl (f' x)) x) /\
        (!x. (x = a) \/ (x = b) \/
             a < x /\ x < b /\ (f'(x) = &0) ==> abs(f x) <= K)
        ==> (!x. a <= x /\ x <= b ==> abs(f x) <= K)`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  MATCH_MP_TAC BOUND_THEOREM_EXACT THEN
  EXISTS_TAC `f':real->real` THEN
  ASM_REWRITE_TAC[] THEN REPEAT STRIP_TAC THEN
  FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[] THEN
  UNDISCH_TAC `x <= b` THEN UNDISCH_TAC `a <= x` THEN
  REAL_ARITH_TAC);;

let BOUND_THEOREM_INEXACT = prove
 (`!f f' l a b.
    (!x. a <= x /\ x <= b ==> (f diffl f'(x)) x) /\
    (!x. a <= x /\ x <= b ==> abs(f'(x)) <= B) /\
    abs(f a) <= K + B * e /\
    abs(f b) <= K + B * e /\
    (!x. a <= x /\ x <= b /\ (f'(x) = &0)
         ==> EX (\(u,v). u <= x /\ x <= v) l) /\
    ALL (\(u,v). a <= u /\ v <= b /\ abs(u - v) <= e /\ abs(f(u)) <= K) l
    ==> !x. a <= x /\ x <= b ==> abs(f(x)) <= K + B * e`,
  let lemma = prove
   (`!l. ALL P l /\ EX Q l ==> EX (\x. P x /\ Q x) l`,
    LIST_INDUCT_TAC THEN REWRITE_TAC[ALL;EX] THEN
    ASM_MESON_TAC[]) in
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  ASM_CASES_TAC `a <= b` THENL
   [ALL_TAC;
    GEN_TAC THEN CONV_TAC CONTRAPOS_CONV THEN
    DISCH_TAC THEN UNDISCH_TAC `~(a <= b)` THEN
    REAL_ARITH_TAC] THEN
  MATCH_MP_TAC BOUND_THEOREM_EXACT THEN
  EXISTS_TAC `f':real->real` THEN ASM_REWRITE_TAC[] THEN
  X_GEN_TAC `x:real` THEN
  DISCH_THEN(fun th -> ASSUME_TAC th THEN MP_TAC th) THEN
  DISCH_THEN(ANTE_RES_THEN MP_TAC) THEN
  UNDISCH_TAC
    `ALL (\(u,v). a <= u /\ v <= b /\
                     abs (u - v) <= e /\ abs (f u) <= K) l` THEN
  REWRITE_TAC[IMP_IMP] THEN
  DISCH_THEN(MP_TAC o MATCH_MP lemma) THEN
  SPEC_TAC(`l:(real#real)list`,`l:(real#real)list`) THEN
  LIST_INDUCT_TAC THEN REWRITE_TAC[EX] THEN
  SPEC_TAC(`h:real#real`,`h:real#real`) THEN
  REWRITE_TAC[TAUT `a \/ b ==> c <=> (a ==> c) /\ (b ==> c)`] THEN
  ASM_REWRITE_TAC[FORALL_PAIR_THM] THEN
  MAP_EVERY X_GEN_TAC [`u:real`; `v:real`] THEN STRIP_TAC THEN
  UNDISCH_TAC `abs (f(u:real)) <= K` THEN
  SUBGOAL_THEN `abs(f(x:real) - f(u)) <= B * e` MP_TAC THENL
   [ALL_TAC; REAL_ARITH_TAC] THEN
  ASM_CASES_TAC `x:real = u` THENL
   [ASM_REWRITE_TAC[REAL_SUB_REFL; REAL_ABS_0] THEN
    MATCH_MP_TAC REAL_LE_MUL THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_TRANS THEN
      EXISTS_TAC `abs((f':real->real) x)` THEN CONJ_TAC THENL
       [REAL_ARITH_TAC;
        FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[]];
      MATCH_MP_TAC REAL_LE_TRANS THEN
      EXISTS_TAC `abs(u - v)` THEN ASM_REWRITE_TAC[] THEN
      REAL_ARITH_TAC];
    ALL_TAC] THEN
  SUBGOAL_THEN `?l z. u < z /\ z < x /\
                      (f diffl l) z /\
                      (f x - f u = (x - u) * l)`
  MP_TAC THENL
   [MATCH_MP_TAC MVT THEN REPEAT CONJ_TAC THENL
     [ASM_REWRITE_TAC[REAL_LT_LE];
      X_GEN_TAC `y:real` THEN STRIP_TAC THEN
      MATCH_MP_TAC DIFF_CONT THEN
      EXISTS_TAC `(f':real->real) y` THEN
      FIRST_ASSUM MATCH_MP_TAC THEN
      MAP_EVERY UNDISCH_TAC
       [`a <= u`; `u <= y`; `y <= x`; `x <= v`; `v <= b`] THEN
      REAL_ARITH_TAC;
      X_GEN_TAC `y:real` THEN STRIP_TAC THEN REWRITE_TAC[differentiable] THEN
      EXISTS_TAC `(f':real->real) y` THEN
      FIRST_ASSUM MATCH_MP_TAC THEN
      MAP_EVERY UNDISCH_TAC
       [`a <= u`; `u < y`; `y < x`; `x <= v`; `v <= b`] THEN
      REAL_ARITH_TAC];
    ALL_TAC] THEN
  DISCH_THEN(X_CHOOSE_THEN `l:real` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `z:real` MP_TAC) THEN
  STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[REAL_ABS_MUL] THEN
  GEN_REWRITE_TAC RAND_CONV [REAL_MUL_SYM] THEN
  MATCH_MP_TAC REAL_LE_MUL2 THEN REWRITE_TAC[REAL_ABS_POS] THEN
  CONJ_TAC THENL
   [MAP_EVERY UNDISCH_TAC
     [`u <= x`; `x <= v`; `abs (u - v) <= e`] THEN
    REAL_ARITH_TAC;
    SUBGOAL_THEN `l = (f':real->real) z` SUBST_ALL_TAC THENL
     [MATCH_MP_TAC DIFF_UNIQ THEN
      EXISTS_TAC `f:real->real` THEN
      EXISTS_TAC `z:real` THEN ASM_REWRITE_TAC[] THEN
      FIRST_ASSUM MATCH_MP_TAC THEN
      MAP_EVERY UNDISCH_TAC
       [`a <= u`; `u < z`; `z < x`] THEN
      SUBGOAL_THEN `x <= b` MP_TAC THENL
       [ASM_REWRITE_TAC[]; REAL_ARITH_TAC];
      FIRST_ASSUM MATCH_MP_TAC THEN
      MAP_EVERY UNDISCH_TAC
       [`a <= u`; `u < z`; `z < x`] THEN
      SUBGOAL_THEN `x <= b` MP_TAC THENL
       [ASM_REWRITE_TAC[]; REAL_ARITH_TAC]]]);;

let recordered = new_recursive_definition list_RECURSION
  `(recordered a [] b <=> a <= b) /\
   (recordered a (CONS h t) b <=>
        a < FST h /\ FST h <= SND h /\ recordered (SND h) t b)`;;

let RECORDERED_CLAUSES = prove
 (`(recordered a [] b <=> a <= b) /\
   (recordered a (CONS (u,v) t) b <=> a < u /\ u <= v /\ recordered v t b)`,
  REWRITE_TAC[recordered]);;

let RECORDERED_MONO = prove
 (`!l a a' b. a <= a' /\ recordered a' l b ==> recordered a l b`,
  LIST_INDUCT_TAC THEN REWRITE_TAC[recordered] THENL
   [REAL_ARITH_TAC;
    REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[] THENL
     [MATCH_MP_TAC REAL_LET_TRANS THEN EXISTS_TAC `a':real` THEN
      ASM_REWRITE_TAC[]]]);;

let FINITE_AD_HOC_LEMMA = prove
 (`!a b n.
    (a INTER b = EMPTY) /\
    (?x:A. x IN a) /\
    (a UNION b) HAS_SIZE n ==> ?m. m < n /\ b HAS_SIZE m`,
  REPEAT GEN_TAC THEN REWRITE_TAC[HAS_SIZE] THEN STRIP_TAC THEN
  SUBGOAL_THEN `FINITE(a:A->bool) /\ FINITE(b:A->bool)` STRIP_ASSUME_TAC THENL
   [CONJ_TAC THEN MATCH_MP_TAC FINITE_SUBSET THEN
    EXISTS_TAC `a:A->bool UNION b` THEN ASM_REWRITE_TAC[] THEN SET_TAC[];
    EXISTS_TAC `CARD(b:A->bool)` THEN ASM_REWRITE_TAC[] THEN
    UNDISCH_TAC `CARD(a:A->bool UNION b) = n` THEN
    DISCH_THEN(SUBST_ALL_TAC o SYM) THEN
    SUBGOAL_THEN `CARD(a UNION b) = CARD(a:A->bool) + CARD(b)`
    SUBST1_TAC THENL
     [MATCH_MP_TAC CARD_UNION THEN ASM_REWRITE_TAC[];
      MATCH_MP_TAC(ARITH_RULE `~(a = 0) ==> b < a + b`) THEN
      SUBGOAL_THEN `~((a:A->bool) HAS_SIZE 0)` MP_TAC THENL
       [REWRITE_TAC[HAS_SIZE_0] THEN REWRITE_TAC[EXTENSION] THEN
        DISCH_THEN(MP_TAC o SPEC `x:A`) THEN ASM_REWRITE_TAC[NOT_IN_EMPTY];
        ASM_REWRITE_TAC[HAS_SIZE]]]]);;

let RECORDERED_CONTAINED_LEMMA = prove
 (`!l a b n.
        recordered a l b /\
        ALL (\(u,v). poly p(u) * poly p(v) <= &0) l /\
        {x | a <= x /\ x <= b /\ (poly p x = &0) /\
             EX (\(u,v). u <= x /\ x <= v) l} HAS_SIZE n
        ==> LENGTH l <= n`,
  LIST_INDUCT_TAC THEN REWRITE_TAC[LENGTH; LE_0] THEN
  REPEAT GEN_TAC THEN
  SPEC_TAC(`h:real#real`,`h:real#real`) THEN
  REWRITE_TAC[FORALL_PAIR_THM] THEN
  MAP_EVERY X_GEN_TAC [`u:real`; `v:real`] THEN
  REWRITE_TAC[recordered; ALL; EX] THEN
  CONV_TAC(ONCE_DEPTH_CONV GEN_BETA_CONV) THEN
  SUBGOAL_THEN
   `{x | a <= x /\ x <= b /\ (poly p x = &0) /\
         (u <= x /\ x <= v \/ EX (\(u,v). u <= x /\ x <= v) t)} =
    {x | a <= x /\ x <= b /\ (poly p x = &0) /\ u <= x /\ x <= v} UNION
    {x | a <= x /\ x <= b /\ (poly p x = &0) /\
         EX (\(u,v). u <= x /\ x <= v) t}`
  SUBST1_TAC THENL
   [REWRITE_TAC[EXTENSION; IN_ELIM_THM; IN_UNION] THEN
    GEN_TAC THEN CONV_TAC TAUT; ALL_TAC] THEN
  STRIP_TAC THEN
  MP_TAC(ISPECL
    [`{x | a <= x /\ x <= b /\ (poly p x = &0) /\ u <= x /\ x <= v}`;
     `{x | a <= x /\ x <= b /\ (poly p x = &0) /\
           EX (\(u,v). u <= x /\ x <= v) t}`;
     `n:num`] FINITE_AD_HOC_LEMMA) THEN
  ASM_REWRITE_TAC[] THEN
  W(C SUBGOAL_THEN (fun th -> REWRITE_TAC[th]) o funpow 2 lhand o snd) THENL
   [CONJ_TAC THENL
     [REWRITE_TAC[EXTENSION; IN_INTER; NOT_IN_EMPTY; IN_ELIM_THM] THEN
      X_GEN_TAC `x:real` THEN STRIP_TAC THEN
      UNDISCH_TAC `EX (\(u,v). u <= x /\ x <= v) t` THEN
      UNDISCH_TAC `recordered v t b` THEN
      UNDISCH_TAC `x <= v` THEN
      SPEC_TAC(`v:real`,`v:real`) THEN
      SPEC_TAC(`t:(real#real)list`,`l:(real#real)list`) THEN
      POP_ASSUM_LIST(K ALL_TAC) THEN
      LIST_INDUCT_TAC THEN REWRITE_TAC[EX; recordered] THEN
      REPEAT GEN_TAC THEN
      SPEC_TAC(`h:real#real`,`h:real#real`) THEN
      REWRITE_TAC[FORALL_PAIR_THM] THEN
      MAP_EVERY X_GEN_TAC [`w:real`; `z:real`] THEN REPEAT STRIP_TAC THENL
       [UNDISCH_TAC `x <= v` THEN
        UNDISCH_TAC `v < w` THEN
        UNDISCH_TAC `w <= x` THEN
        REAL_ARITH_TAC;
        UNDISCH_TAC `EX (\(u,v). u <= x /\ x <= v) t` THEN
        UNDISCH_TAC `recordered z t b` THEN
        FIRST_ASSUM MATCH_MP_TAC THEN
        MAP_EVERY UNDISCH_TAC [`w <= z`; `v < w`; `x <= v`] THEN
        REAL_ARITH_TAC];
      MP_TAC(SPECL [`u:real`; `v:real`; `p:real list`] STURM_NOROOT) THEN
      ASM_REWRITE_TAC[real_gt; GSYM REAL_NOT_LE] THEN
      REWRITE_TAC[NOT_FORALL_THM; NOT_IMP] THEN
      DISCH_THEN(X_CHOOSE_THEN `x:real` STRIP_ASSUME_TAC) THEN
      EXISTS_TAC `x:real` THEN REWRITE_TAC[IN_ELIM_THM] THEN
      ASM_REWRITE_TAC[] THEN CONJ_TAC THENL
       [UNDISCH_TAC `a < u` THEN UNDISCH_TAC `u <= x` THEN REAL_ARITH_TAC;
        MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `v:real` THEN
        ASM_REWRITE_TAC[] THEN UNDISCH_TAC `recordered v t b` THEN
        SPEC_TAC(`v:real`,`v:real`) THEN
        SPEC_TAC(`t:(real#real)list`,`l:(real#real)list`) THEN
        POP_ASSUM_LIST(K ALL_TAC) THEN
        LIST_INDUCT_TAC THEN REWRITE_TAC[recordered] THEN
        REPEAT STRIP_TAC THEN MATCH_MP_TAC REAL_LE_TRANS THEN
        EXISTS_TAC `SND(h:real#real)` THEN CONJ_TAC THENL
         [UNDISCH_TAC `FST h <= SND h` THEN
          UNDISCH_TAC `v < FST(h:real#real)` THEN REAL_ARITH_TAC;
          FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[]]]];
    DISCH_THEN(X_CHOOSE_THEN `m:num` STRIP_ASSUME_TAC) THEN
    REWRITE_TAC[LE_SUC_LT] THEN
    MATCH_MP_TAC LET_TRANS THEN EXISTS_TAC `m:num` THEN
    ASM_REWRITE_TAC[] THEN FIRST_ASSUM MATCH_MP_TAC THEN
    ASM_REWRITE_TAC[] THEN
    MAP_EVERY EXISTS_TAC [`a:real`; `b:real`] THEN
    ASM_REWRITE_TAC[] THEN MATCH_MP_TAC RECORDERED_MONO THEN
    EXISTS_TAC `v:real` THEN ASM_REWRITE_TAC[] THEN
    UNDISCH_TAC `a < u` THEN UNDISCH_TAC `u <= v` THEN
    ARITH_TAC]);;

let RECORDERED_ROOTCOUNT = prove
 (`!l a b.
    {x | a <= x /\ x <= b /\ (poly p x = &0)} HAS_SIZE (LENGTH l) /\
    recordered a l b /\
    ALL (\(u,v). poly p(u) * poly p(v) <= &0) l
    ==> !x. a <= x /\ x <= b /\ (poly p(x) = &0)
            ==> EX (\(u,v). u <= x /\ x <= v) l`,
  REPEAT GEN_TAC THEN DISCH_TAC THEN
  REWRITE_TAC[TAUT `(a ==> b) <=> (a /\ b <=> a)`] THEN
  SUBGOAL_THEN `{x | a <= x /\ x <= b /\ (poly p x = &0) /\
                     EX (\(u,v). u <= x /\ x <= v) l} =
                {x | a <= x /\ x <= b /\ (poly p x = &0)}`
  MP_TAC THENL
   [POP_ASSUM(STRIP_ASSUME_TAC o REWRITE_RULE[HAS_SIZE]) THEN
    MATCH_MP_TAC CARD_SUBSET_LE THEN ASM_REWRITE_TAC[] THEN CONJ_TAC THENL
     [REWRITE_TAC[SUBSET; IN_ELIM_THM] THEN
      REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[];
      MATCH_MP_TAC RECORDERED_CONTAINED_LEMMA THEN
      MAP_EVERY EXISTS_TAC [`a:real`; `b:real`] THEN
      ASM_REWRITE_TAC[HAS_SIZE] THEN
      MATCH_MP_TAC FINITE_SUBSET THEN
      EXISTS_TAC `{x | a <= x /\ x <= b /\ (poly p x = &0)}` THEN
      ASM_REWRITE_TAC[] THEN REWRITE_TAC[SUBSET; IN_ELIM_THM] THEN
      REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[]];
    REWRITE_TAC[EXTENSION; IN_ELIM_THM; GSYM CONJ_ASSOC]]);;

(* ------------------------------------------------------------------------- *)
(* Single precision, error compared with order 6 Taylor truncation.          *)
(* ------------------------------------------------------------------------- *)

let SINGLE_DERIVBOUND = prove
 (`!x. abs(x) <= &10844 / &1000000
       ==> abs(poly [&0; --(&17) / &2097152; --(&49) / &16777216;
                     &1 / &6; &1 / &24; &1 / &120] x) <= inv(&2 pow 21)`,
  GEN_TAC THEN DISCH_THEN(MP_TAC o MATCH_MP POLY_MONO) THEN
  DISCH_THEN(MP_TAC o SPEC
   `[&0; --(&17) / &2097152; --(&49) / &16777216;
     &1 / &6; &1 / &24; &1 / &120]`) THEN
  MATCH_MP_TAC(REAL_ARITH `a <= b ==> c <= a ==> c <= b`) THEN
  REWRITE_TAC[poly; MAP] THEN
  CONV_TAC(ONCE_DEPTH_CONV REAL_RAT_ABS_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let SINGLE_STURM = prove
 (`STURM [&0; --(&2040); --(&735); &41943040; &10485760; &2097152]
         (poly_diff [&0; --(&2040); --(&735); &41943040; &10485760; &2097152])
         [[--(&408); &1338; &25166265; --(&8388608)];
          [&12549417544; --(&21760245838); --(&774072258592059)];
          [--(&39740632656867464012); --(&4415519623031413035448627)];
          [--(&1)]]`,
  CONV_TAC(ONCE_DEPTH_CONV POLY_DIFF_CONV) THEN
  ONCE_REWRITE_TAC[STURM] THEN REPEAT CONJ_TAC THENL
   [EXISTS_TAC `&1` THEN
    CONV_TAC(LAND_CONV (REAL_RAT_LT_CONV)) THEN
    REWRITE_TAC[divides] THEN
    EXISTS_TAC `[&1 / &5; &1 / &5]` THEN
    CONV_TAC((LAND_CONV o RAND_CONV o RAND_CONV) POLY_CMUL_CONV) THEN
    CONV_TAC(LAND_CONV(RAND_CONV POLY_ADD_CONV)) THEN
    CONV_TAC(RAND_CONV(RAND_CONV POLY_MUL_CONV)) THEN REWRITE_TAC[];
    REWRITE_TAC[degree] THEN
    CONV_TAC(ONCE_DEPTH_CONV POLY_NORMALIZE_CONV) THEN
    REWRITE_TAC[LENGTH; PRE] THEN REWRITE_TAC[ARITH];
    ALL_TAC] THEN
  ONCE_REWRITE_TAC[STURM] THEN REPEAT CONJ_TAC THENL
   [EXISTS_TAC `&15 / &33554432` THEN
    CONV_TAC(LAND_CONV (REAL_RAT_LT_CONV)) THEN
    REWRITE_TAC[divides] THEN
    EXISTS_TAC `[--(&293603485) / &33554432; --(&5) / &4]` THEN
    CONV_TAC ((LAND_CONV o RAND_CONV o RAND_CONV) POLY_CMUL_CONV) THEN
    CONV_TAC(LAND_CONV(RAND_CONV POLY_ADD_CONV)) THEN
    CONV_TAC(RAND_CONV(RAND_CONV POLY_MUL_CONV)) THEN REWRITE_TAC[];
    REWRITE_TAC[degree] THEN
    CONV_TAC(ONCE_DEPTH_CONV POLY_NORMALIZE_CONV) THEN
    REWRITE_TAC[LENGTH; PRE] THEN REWRITE_TAC[ARITH];
    ALL_TAC] THEN
  ONCE_REWRITE_TAC[STURM] THEN REPEAT CONJ_TAC THENL
   [EXISTS_TAC `&67108864 / &599187861521811458348977859481` THEN
    CONV_TAC(LAND_CONV (REAL_RAT_LT_CONV)) THEN
    REWRITE_TAC[divides] THEN
    EXISTS_TAC `[--(&19480690127048602303139) /
                 &599187861521811458348977859481;
                 &8388608 / &774072258592059]` THEN
    CONV_TAC ((LAND_CONV o RAND_CONV o RAND_CONV) POLY_CMUL_CONV) THEN
    CONV_TAC(LAND_CONV(RAND_CONV POLY_ADD_CONV)) THEN
    CONV_TAC(RAND_CONV(RAND_CONV POLY_MUL_CONV)) THEN REWRITE_TAC[];
    REWRITE_TAC[degree] THEN
    CONV_TAC(ONCE_DEPTH_CONV POLY_NORMALIZE_CONV) THEN
    REWRITE_TAC[LENGTH; PRE] THEN REWRITE_TAC[ARITH];
    ALL_TAC] THEN
  ONCE_REWRITE_TAC[STURM] THEN REPEAT CONJ_TAC THENL
   [EXISTS_TAC `&244676249793034014341452244280506044465931374449498352659792 /
                &19496813541375471877883801032611338696507156185129` THEN
    CONV_TAC(LAND_CONV (REAL_RAT_LT_CONV)) THEN
    REWRITE_TAC[divides] THEN
    EXISTS_TAC `[&65320671221097898143185059168083718 /
                 &19496813541375471877883801032611338696507156185129;
                 &774072258592059 /
                 &4415519623031413035448627]` THEN
    CONV_TAC ((LAND_CONV o RAND_CONV o RAND_CONV) POLY_CMUL_CONV) THEN
    CONV_TAC(LAND_CONV(RAND_CONV POLY_ADD_CONV)) THEN
    CONV_TAC(RAND_CONV(RAND_CONV POLY_MUL_CONV)) THEN REWRITE_TAC[];
    REWRITE_TAC[degree] THEN
    CONV_TAC(ONCE_DEPTH_CONV POLY_NORMALIZE_CONV) THEN
    REWRITE_TAC[LENGTH; PRE] THEN REWRITE_TAC[ARITH];
    ALL_TAC] THEN
  ONCE_REWRITE_TAC[STURM] THEN
  REWRITE_TAC[divides] THEN
  EXISTS_TAC `[&39740632656867464012; &4415519623031413035448627]` THEN
  CONV_TAC(RAND_CONV(RAND_CONV POLY_MUL_CONV)) THEN
  REWRITE_TAC[]);;

let SINGLE_ROOTCOUNT_LEMMA = prove
 (`!a b.
   a <= b /\
   ~(poly [&0; --(&2040); --(&735); &41943040; &10485760; &2097152] a = &0) /\
   ~(poly [&0; --(&2040); --(&735); &41943040; &10485760; &2097152] b = &0)
   ==> {x | a <= x /\ x <= b /\
            (poly [&0; --(&2040); --(&735); &41943040;
                   &10485760; &2097152] x = &0)} HAS_SIZE
       (variation (MAP (\p. poly p a)
         [[&0; --(&2040); --(&735); &41943040; &10485760; &2097152];
          [--(&2040); --(&1470); &125829120; &41943040; &10485760];
          [--(&408); &1338; &25166265; --(&8388608)];
          [&12549417544; --(&21760245838); --(&774072258592059)];
          [--(&39740632656867464012); --(&4415519623031413035448627)];
          [--(&1)]]) -
        variation (MAP (\p. poly p b)
         [[&0; --(&2040); --(&735); &41943040; &10485760; &2097152];
          [--(&2040); --(&1470); &125829120; &41943040; &10485760];
          [--(&408); &1338; &25166265; --(&8388608)];
          [&12549417544; --(&21760245838); --(&774072258592059)];
          [--(&39740632656867464012); --(&4415519623031413035448627)];
          [--(&1)]]))`,
  REPEAT STRIP_TAC THEN
  SUBST1_TAC(SYM(POLY_DIFF_CONV
   `poly_diff [&0; --(&2040); --(&735); &41943040; &10485760; &2097152]`)) THEN
  MATCH_MP_TAC STURM_THEOREM THEN
  EXISTS_TAC `--(&1)` THEN ASM_REWRITE_TAC[SINGLE_STURM] THEN
  REPEAT CONJ_TAC THENL
   [REWRITE_TAC[POLY_ZERO] THEN
    CONV_TAC(ONCE_DEPTH_CONV POLY_DIFF_CONV) THEN
    REWRITE_TAC[ALL] THEN
    CONV_TAC(ONCE_DEPTH_CONV REAL_INT_EQ_CONV) THEN
    REWRITE_TAC[];
    REWRITE_TAC[NOT_CONS_NIL];
    REWRITE_TAC[LAST; NOT_CONS_NIL];
    CONV_TAC(ONCE_DEPTH_CONV REAL_INT_EQ_CONV) THEN
    REWRITE_TAC[]]);;

let SINGLE_ROOTCOUNT_1_UNSCALED = prove
 (`{x | --(&1) <= x /\ x <= &1 /\
        (poly [&0; --(&2040); --(&735); &41943040;
               &10485760; &2097152] x = &0)} HAS_SIZE 3`,
  MP_TAC(SPECL [`--(&1)`; `&1`] SINGLE_ROOTCOUNT_LEMMA) THEN
  W(C SUBGOAL_THEN (fun th -> REWRITE_TAC[th]) o funpow 2 lhand o snd) THENL
   [REWRITE_TAC[poly] THEN CONV_TAC REAL_RAT_REDUCE_CONV;
    MATCH_MP_TAC EQ_IMP THEN AP_TERM_TAC] THEN
  REWRITE_TAC[MAP] THEN
  REWRITE_TAC[poly] THEN
  CONV_TAC REAL_RAT_REDUCE_CONV THEN
  CONV_TAC(ONCE_DEPTH_CONV VARIATION_CONV) THEN
  CONV_TAC NUM_REDUCE_CONV);;

let SINGLE_ROOTCOUNT_1 = prove
 (`{x | --(&1) <= x /\ x <= &1 /\
        (poly [&0; --(&17) / &2097152; --(&49) / &16777216;
               &1 / &6; &1 / &24; &1 / &120] x = &0)} HAS_SIZE 3`,
  MP_TAC SINGLE_ROOTCOUNT_1_UNSCALED THEN
  MATCH_MP_TAC EQ_IMP THEN
  AP_THM_TAC THEN AP_TERM_TAC THEN
  REWRITE_TAC[EXTENSION; IN_ELIM_THM] THEN
  GEN_TAC THEN AP_TERM_TAC THEN AP_TERM_TAC THEN
  SUBGOAL_THEN
   `poly [&0; -- (&2040); -- (&735); &41943040; &10485760; &2097152] =
    poly (&251658240 ##  [&0; --(&17) / &2097152; --(&49) / &16777216;
                          &1 / &6; &1 / &24; &1 / &120])`
  SUBST1_TAC THENL
   [AP_TERM_TAC THEN CONV_TAC(RAND_CONV POLY_CMUL_CONV) THEN REFL_TAC;
    REWRITE_TAC[POLY_CMUL; REAL_ENTIRE] THEN
    REWRITE_TAC[REAL_OF_NUM_EQ; ARITH]]);;

(* ------------------------------------------------------------------------- *)
(* Hence the final accuracy results.                                         *)
(* ------------------------------------------------------------------------- *)

let SINGLE_ROOTLIST_1 = prove
 (`!x. --(&1) <= x /\ x <= &1 /\
       (poly [&0; --(&17) / &2097152; --(&49) / &16777216;
              &1 / &6; &1 / &24; &1 / &120] x = &0)
       ==> EX (\(u,v). u <= x /\ x <= v)
              [--(&935680) / &2 pow 27, --(&935679) / &2 pow 27;
               &0, &0;
               &936399 / &2 pow 27, &936400 / &2 pow 27]`,
  MATCH_MP_TAC RECORDERED_ROOTCOUNT THEN
  REWRITE_TAC[LENGTH; SINGLE_ROOTCOUNT_1; ARITH_SUC] THEN CONJ_TAC THENL
   [REWRITE_TAC[recordered] THEN CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
  REWRITE_TAC[ALL] THEN
  CONV_TAC(ONCE_DEPTH_CONV GEN_BETA_CONV) THEN
  CONV_TAC(ONCE_DEPTH_CONV
   (GEN_REWRITE_CONV I [poly] THENC
    REWRITE_CONV[poly] THENC
    REAL_RAT_REDUCE_CONV)) THEN
  REWRITE_TAC[SIGN_LEMMA6] THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let SINGLE_ROOTLIST = prove
 (`!x. --(&10844) / &1000000 <= x /\ x <= &10844 / &1000000 /\
       (poly [&0; --(&17) / &2097152; --(&49) / &16777216;
              &1 / &6; &1 / &24; &1 / &120] x = &0)
       ==> EX (\(u,v). u <= x /\ x <= v)
              [--(&935680) / &2 pow 27, --(&935679) / &2 pow 27;
               &0, &0;
               &936399 / &2 pow 27, &936400 / &2 pow 27]`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC SINGLE_ROOTLIST_1 THEN
  ASM_REWRITE_TAC[] THEN CONJ_TAC THENL
   [MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `--(&10844) / &1000000`;
    MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `&10844 / &1000000`] THEN
  ASM_REWRITE_TAC[] THEN CONV_TAC REAL_RAT_REDUCE_CONV);;

let SINGLE_ACCURACY_LEMMA = prove
 (`!x. --(&10844) / &1000000 <= x /\ x <= &10844 / &1000000
       ==> abs(poly [&0; &0; --(&17) / &4194304; --(&49) / &50331648;
                     &1 / &24; &1 / &120; &1 / &720] x)
           <= (&18350234473992790967940567810211 /
               &184320000000000000000000000000000000000000) +
              inv(&2 pow 21) * inv(&2 pow 27)`,
  MATCH_MP_TAC BOUND_THEOREM_INEXACT THEN
  EXISTS_TAC
   `poly [&0; --(&17) / &2097152; --(&49) / &16777216;
          &1 / &6; &1 / &24; &1 / &120]` THEN
  EXISTS_TAC `[--(&935680) / &2 pow 27, --(&935679) / &2 pow 27;
               &0, &0;
               &936399 / &2 pow 27, &936400 / &2 pow 27]` THEN
  REPEAT CONJ_TAC THENL
   [REPEAT STRIP_TAC THEN
    (SUBST1_TAC o SYM o POLY_DIFF_CONV)
       `diff [&0; &0; --(&17) / &4194304; --(&49) / &50331648;
              &1 / &24; &1 / &120; &1 / &720]` THEN
    MATCH_ACCEPT_TAC POLY_DIFF;
    GEN_TAC THEN
    GEN_REWRITE_TAC (LAND_CONV o DEPTH_CONV) [real_div; REAL_MUL_LNEG] THEN
    REWRITE_TAC[GSYM real_div] THEN
    REWRITE_TAC[REAL_ARITH `--a <= x /\ x <= a <=> abs(x) <= a`] THEN
    MATCH_ACCEPT_TAC SINGLE_DERIVBOUND;
    REWRITE_TAC[poly] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV;
    REWRITE_TAC[poly] THEN
    CONV_TAC REAL_RAT_REDUCE_CONV;
    MATCH_ACCEPT_TAC SINGLE_ROOTLIST;
    REWRITE_TAC[ALL]] THEN
  CONV_TAC(ONCE_DEPTH_CONV GEN_BETA_CONV) THEN
  REPEAT CONJ_TAC THENL
   [CONV_TAC REAL_RAT_REDUCE_CONV;
    CONV_TAC REAL_RAT_REDUCE_CONV;
    CONV_TAC REAL_RAT_REDUCE_CONV;
    REWRITE_TAC[poly] THEN CONV_TAC REAL_RAT_REDUCE_CONV;
    CONV_TAC REAL_RAT_REDUCE_CONV;
    CONV_TAC REAL_RAT_REDUCE_CONV;
    CONV_TAC REAL_RAT_REDUCE_CONV;
    REWRITE_TAC[poly] THEN CONV_TAC REAL_RAT_REDUCE_CONV;
    CONV_TAC REAL_RAT_REDUCE_CONV;
    CONV_TAC REAL_RAT_REDUCE_CONV;
    CONV_TAC REAL_RAT_REDUCE_CONV;
    REWRITE_TAC[poly] THEN CONV_TAC REAL_RAT_REDUCE_CONV]);;

remove_interface "exp";;
unparse_as_infix "exp";;

let SINGLE_MCLAURIN_ACCURACY_LEMMA = prove
 (`!t. t <= &10844 / &1000000
       ==> abs(exp t) <= &1 + &2 * (&10844 / &1000000)`,
  REPEAT STRIP_TAC THEN
  SUBGOAL_THEN `abs(exp t) = exp t` SUBST1_TAC THENL
   [REWRITE_TAC[real_abs; REAL_EXP_POS_LE];
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `exp(&10844 / &1000000)` THEN
    ASM_REWRITE_TAC[REAL_EXP_MONO_LE] THEN
    MATCH_MP_TAC REAL_EXP_BOUND_LEMMA THEN
    CONV_TAC REAL_RAT_REDUCE_CONV]);;

let SINGLE_MCLAURIN_ACCURACY = prove
 (`!x t. abs(t) <= abs(x) /\ abs(x) <= &10844 / &1000000
       ==> abs (exp t * inv (&5040) * x pow 7) <= inv (&2 pow 57)`,
  REPEAT STRIP_TAC THEN ONCE_REWRITE_TAC[REAL_ABS_MUL] THEN
  MATCH_MP_TAC REAL_LE_TRANS THEN
  EXISTS_TAC `abs(&1 + &2 * &10844 / &1000000) *
              abs(inv (&5040) * (&10844 / &1000000) pow 7)` THEN
  CONJ_TAC THENL
   [MATCH_MP_TAC REAL_LE_MUL2 THEN REWRITE_TAC[REAL_ABS_POS] THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_TRANS THEN
      EXISTS_TAC `&1 + &2 * (&10844 / &1000000)` THEN
      REWRITE_TAC[REAL_ABS_LE] THEN
      MATCH_MP_TAC SINGLE_MCLAURIN_ACCURACY_LEMMA THEN
      MATCH_MP_TAC REAL_LE_TRANS THEN
      EXISTS_TAC `abs(t)` THEN REWRITE_TAC[REAL_ABS_LE] THEN
      MATCH_MP_TAC REAL_LE_TRANS THEN
      EXISTS_TAC `abs(x)` THEN ASM_REWRITE_TAC[];
      REWRITE_TAC[REAL_ABS_MUL] THEN
      MATCH_MP_TAC REAL_LE_MUL2 THEN REWRITE_TAC[REAL_ABS_POS] THEN
      REWRITE_TAC[REAL_LE_REFL; REAL_ABS_POW] THEN
      MATCH_MP_TAC REAL_POW_LE2 THEN
      REWRITE_TAC[REAL_ABS_POS] THEN
      ASM_REWRITE_TAC[REAL_ABS_DIV; REAL_ABS_NUM]];
    CONV_TAC REAL_RAT_REDUCE_CONV]);;

let SINGLE_ACCURACY = prove
 (`!x. --(&10844) / &1000000 <= x /\ x <= &10844 / &1000000
       ==> abs((exp(x) - &1) -
               (x + (&8388676 / &2 pow 24) * x pow 2 +
                &11184876 / &2 pow 26 * x pow 3))
           <= (&24 / &27) * inv(&2 pow 33)`,
  GEN_TAC THEN DISCH_TAC THEN
  SUBGOAL_THEN
   `(exp(x) - &1) -
    (x + (&8388676 / &2 pow 24) * x pow 2 + &11184876 / &2 pow 26 * x pow 3) =
    exp(x) - poly [&1; &1; &8388676 / &2 pow 24; &11184876 / &2 pow 26] x`
  SUBST1_TAC THENL
   [REWRITE_TAC[poly] THEN REWRITE_TAC[real_sub; REAL_NEG_ADD] THEN
    REWRITE_TAC[GSYM REAL_ADD_ASSOC] THEN AP_TERM_TAC THEN
    AP_TERM_TAC THEN REWRITE_TAC[REAL_ADD_LDISTRIB] THEN
    REWRITE_TAC[REAL_NEG_ADD; REAL_MUL_RZERO; REAL_NEG_0; REAL_ADD_RID] THEN
    REWRITE_TAC[REAL_MUL_RID; num_CONV `3`; num_CONV `2`; num_CONV `1`] THEN
    REWRITE_TAC[REAL_MUL_RID; pow] THEN REWRITE_TAC[ARITH] THEN
    REWRITE_TAC[REAL_ADD_AC; REAL_MUL_AC]; ALL_TAC] THEN
  MATCH_MP_TAC REAL_ABS_TRIANGLE_LE THEN
  EXISTS_TAC `exp(x) - sum(0,7) (\m. x pow m / &(FACT m))` THEN
  MATCH_MP_TAC REAL_LE_TRANS THEN
  EXISTS_TAC `inv(&2 pow 57) +
              (&18350234473992790967940567810211 /
               &184320000000000000000000000000000000000000) +
              inv(&2 pow 21) * inv(&2 pow 27)` THEN
  CONJ_TAC THENL
   [ALL_TAC; CONV_TAC REAL_RAT_REDUCE_CONV] THEN
  MATCH_MP_TAC REAL_LE_ADD2 THEN CONJ_TAC THENL
   [MP_TAC(SPECL [`x:real`; `7`] MCLAURIN_EXP_LE) THEN
    DISCH_THEN(X_CHOOSE_THEN `t:real` STRIP_ASSUME_TAC) THEN
    ASM_REWRITE_TAC[REAL_ARITH `(a + b) - a = b`] THEN
    CONV_TAC(ONCE_DEPTH_CONV NUM_FACT_CONV) THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
    MATCH_MP_TAC SINGLE_MCLAURIN_ACCURACY THEN
    ASM_REWRITE_TAC[] THEN
    ASM_REWRITE_TAC[REAL_ARITH `abs(x) <= b <=> --b <= x /\ x <= b`] THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_LNEG] THEN
    ASM_REWRITE_TAC[GSYM real_div];
    REWRITE_TAC[REAL_ARITH `a - b - (a - c) = c - b`] THEN
    SUBGOAL_THEN
     `sum (0,7) (\m. x pow m / &(FACT m)) =
      poly [&1; &1; &1 / &2; &1 / &6; &1 / &24; &1 / &120; &1 / &720] x`
    SUBST1_TAC THENL
     [CONV_TAC(ONCE_DEPTH_CONV REAL_SUM_CONV) THEN
      REWRITE_TAC[poly; ARITH] THEN
      CONV_TAC(ONCE_DEPTH_CONV NUM_FACT_CONV) THEN
      REWRITE_TAC[REAL_ADD_LDISTRIB; REAL_MUL_RZERO] THEN
      REWRITE_TAC[REAL_ADD_RID] THEN
      REWRITE_TAC(map num_CONV [`1`; `2`; `3`; `4`; `5`; `6`]) THEN
      REWRITE_TAC[real_pow] THEN REWRITE_TAC[ARITH] THEN
      REWRITE_TAC[real_div; REAL_MUL_AC; REAL_INV_1; REAL_MUL_RID] THEN
      REWRITE_TAC[GSYM REAL_ADD_ASSOC];
      MATCH_MP_TAC REAL_LE_TRANS THEN
      EXISTS_TAC
       `abs(poly [&0; &0; --(&17) / &4194304; --(&49) / &50331648;
                  &1 / &24; &1 / &120; &1 / &720] x)` THEN
      CONJ_TAC THENL
       [ALL_TAC;
        MATCH_MP_TAC SINGLE_ACCURACY_LEMMA THEN ASM_REWRITE_TAC[]] THEN
      MATCH_MP_TAC REAL_EQ_IMP_LE THEN AP_TERM_TAC THEN
      REWRITE_TAC[real_sub; GSYM POLY_NEG; GSYM POLY_ADD] THEN
      AP_THM_TAC THEN AP_TERM_TAC THEN REWRITE_TAC[poly_neg] THEN
      CONV_TAC REAL_RAT_REDUCE_CONV THEN
      CONV_TAC(LAND_CONV (RAND_CONV POLY_CMUL_CONV)) THEN
      CONV_TAC(LAND_CONV POLY_ADD_CONV) THEN REFL_TAC]]);;

(* ------------------------------------------------------------------------- *)
(* ******** New version                                                      *)
(* ------------------------------------------------------------------------- *)

