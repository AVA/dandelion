loadt "Library/analysis.ml";;(* Real analysis                               *)
loadt "Library/transc.ml";;  (* Transcendental functions                    *)
loadt "Library/poly.ml";;    (* Theory of polynomials                       *)

loadt "Tang/sturm.ml";;       (* Squarefree decomposition; Sturm's theorem   *)
loadt "Tang/drang.ml";;       (* Accuracy of polynomial approximations       *)

loadt "Tang/prog.ml";;        (* Imperative programming language             *)

loadt "Tang/ieee.ml";;        (* Formalization of IEEE-754                   *)
loadt "Tang/float.ml";;       (* Properties of floats and machine integers   *)

loadt "Tang/tang_lemmas.ml";; (* Lemmas for Tang's exponential algorithm     *)
loadt "Tang/tang.ml";;        (* Verification of Tang exponential algorithm  *)
