(* ------------------------------------------------------------------------- *)
(* Somewhat faster way of doing the 32nd roots so I can demo on laptop.      *)
(* ------------------------------------------------------------------------- *)

prioritize_real();;

let POW32_LEMMA1 = prove
 (`~(p = 0) ==> (abs((&n / &p) pow 2 - &((n EXP 2) DIV p) / &p) < inv(&p))`,
  DISCH_TAC THEN MATCH_MP_TAC REAL_LT_RCANCEL_IMP THEN
  EXISTS_TAC `abs(&p * &p)` THEN
  ASM_REWRITE_TAC[GSYM REAL_ABS_NZ; REAL_ENTIRE; REAL_OF_NUM_EQ] THEN
  REWRITE_TAC[GSYM REAL_ABS_MUL] THEN
  REWRITE_TAC[REAL_SUB_RDISTRIB; real_div] THEN
  SUBGOAL_THEN `(&(n EXP 2 DIV p) * inv (&p)) * &p * &p =
                &(n EXP 2 DIV p * p)`
  SUBST1_TAC THENL
   [REWRITE_TAC[GSYM REAL_OF_NUM_MUL; GSYM REAL_MUL_ASSOC] THEN
    AP_TERM_TAC THEN REWRITE_TAC[REAL_MUL_ASSOC] THEN
    GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_LID] THEN
    AP_THM_TAC THEN AP_TERM_TAC THEN
    MATCH_MP_TAC REAL_MUL_LINV THEN ASM_REWRITE_TAC[REAL_OF_NUM_EQ];
    ALL_TAC] THEN
  SUBGOAL_THEN `(&n * inv (&p)) pow 2 * &p * &p = &(n EXP 2)` SUBST1_TAC THENL
   [REWRITE_TAC[REAL_POW_2] THEN
    ONCE_REWRITE_TAC[AC REAL_MUL_AC `((u:real * v) * w * x) * y * z =
                                     (u * w) * (v * y) * (x * z)`] THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_POW; REAL_POW_2] THEN
    GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_RID] THEN
    AP_TERM_TAC THEN
    SUBGOAL_THEN `inv (&p) * &p = &1`
     (fun th -> REWRITE_TAC[th; REAL_MUL_LID]) THEN
    MATCH_MP_TAC REAL_MUL_LINV THEN ASM_REWRITE_TAC[REAL_OF_NUM_EQ];
    ALL_TAC] THEN
  FIRST_ASSUM(MP_TAC o MATCH_MP DIVISION) THEN
  DISCH_THEN(MP_TAC o SPEC `n EXP 2`) THEN
  DISCH_THEN(CONJUNCTS_THEN2 MP_TAC ASSUME_TAC) THEN
  DISCH_THEN(fun th -> GEN_REWRITE_TAC
        (LAND_CONV o RAND_CONV o LAND_CONV o RAND_CONV) [th]) THEN
  REWRITE_TAC[GSYM REAL_OF_NUM_ADD] THEN
  REWRITE_TAC[REAL_ARITH `abs((a + b) - a) = abs(b)`] THEN
  SUBGOAL_THEN `inv (&p) * abs (&p * &p) = &p` SUBST1_TAC THEN
  ASM_REWRITE_TAC[REAL_ABS_NUM; REAL_OF_NUM_LT; REAL_ABS_MUL] THEN
  GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_LID] THEN
  REWRITE_TAC[REAL_MUL_ASSOC] THEN AP_THM_TAC THEN AP_TERM_TAC THEN
  MATCH_MP_TAC REAL_MUL_LINV THEN ASM_REWRITE_TAC[REAL_OF_NUM_EQ]);;

let POW32_LEMMA2 = prove
 (`~(p = 0) /\
   abs(&x / &p - y) <= e
   ==> ~(p = 0) /\
       (abs(&(x EXP 2 DIV p) / &p - y pow 2)
        <= e pow 2 + &2 * abs(&x / &p) * e + inv(&p))`,
  let lemma = prove
   (`abs(x pow 2 - y pow 2) = abs(x + y) * abs(x - y)`,
    REWRITE_TAC[REAL_POW_2; GSYM REAL_ABS_MUL] THEN
    AP_TERM_TAC THEN REAL_ARITH_TAC) in
  STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
  MATCH_MP_TAC REAL_LE_TRANS THEN
  EXISTS_TAC `abs(&(x EXP 2 DIV p) / &p - (&x / &p) pow 2) +
              abs((&x / &p) pow 2 - y pow 2)` THEN
  CONJ_TAC THENL [REAL_ARITH_TAC; ALL_TAC] THEN
  ONCE_REWRITE_TAC[AC REAL_ADD_AC `a + b + c = c + (a + b)`] THEN
  MATCH_MP_TAC REAL_LE_ADD2 THEN CONJ_TAC THENL
   [ONCE_REWRITE_TAC[REAL_ABS_SUB] THEN
    MATCH_MP_TAC REAL_LT_IMP_LE THEN
    MATCH_MP_TAC POW32_LEMMA1 THEN ASM_REWRITE_TAC[];
    REWRITE_TAC[lemma] THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `(&2 * abs(&x / &p) + e) * e` THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_MUL2 THEN ASM_REWRITE_TAC[REAL_ABS_POS] THEN
      UNDISCH_TAC `abs (&x / &p - y) <= e` THEN REAL_ARITH_TAC;
      REWRITE_TAC[REAL_POW_2] THEN REAL_ARITH_TAC]]);;

let POW32_CONV =
  let destdiv = dest_binop `$/` in
  let amp_tm = `&`
  and sub_tm = `$-`
  and abs_tm = `abs`
  and le_tm = `$<=`
  and div_tm = `$/`
  and one_tm = `&1` in
  let lemma = prove
   (`x pow 2 pow 2 pow 2 pow 2 pow 2 = x pow 32`,
    CONV_TAC(TOP_DEPTH_CONV num_CONV) THEN
    REWRITE_TAC[real_pow; REAL_MUL_LID; REAL_MUL_RID; REAL_MUL_ASSOC]) in
  fun tm n ->
    let ax,ap = try destdiv tm with Failure _ -> tm,one_tm in
    let x = rand ax and p = rand ap in
    let nx = dest_numeral x and np = dest_numeral p in
    let nn = power_num (Int 2) (Int n) in
    let yden = quo_num (nx */ nn) np in
    let ytm = list_mk_comb(div_tm,[mk_comb(amp_tm,mk_numeral yden);
                                   mk_comb(amp_tm,mk_numeral nn)]) in
    let etm = mk_comb(abs_tm,list_mk_comb(sub_tm,[ytm;tm])) in
    let eth = MATCH_MP REAL_EQ_IMP_LE (REAL_RAT_REDUCE_CONV etm) in
    let nzth = EQF_ELIM(NUM_REDUCE_CONV
                 (mk_eq(mk_numeral nn,mk_numeral (Int 0)))) in
    let sth = CONJ nzth eth in
    let STEP th =
      let th1 = MATCH_MP POW32_LEMMA2 th in
      let th2 = CONV_RULE (RAND_CONV (RAND_CONV REAL_RAT_REDUCE_CONV)) th1 in
      let th3 = CONV_RULE (RAND_CONV (LAND_CONV (RAND_CONV (LAND_CONV
                  (LAND_CONV (RAND_CONV NUM_REDUCE_CONV)))))) th2 in
      let etm = rand(rand(concl th3)) in
      let eax,eap = destdiv etm in
      let ex = rand eax and ep = rand eap in
      let enx = dest_numeral ex and enp = dest_numeral ep in
      let eyden = quo_num (enx */ nn) enp +/ Int 1 in
      let eytm = list_mk_comb(div_tm,[mk_comb(amp_tm,mk_numeral eyden);
                                      mk_comb(amp_tm,mk_numeral nn)]) in
      let eetm = list_mk_comb(le_tm,[etm;eytm]) in
      let eeth = EQT_ELIM(REAL_RAT_REDUCE_CONV eetm) in
      CONJ (CONJUNCT1 th)
           (MATCH_MP REAL_LE_TRANS (CONJ (CONJUNCT2 th3) eeth)) in
   CONJUNCT2(PURE_REWRITE_RULE[lemma] (funpow 5 STEP sth));;

let ROOT_2_LEMMA0a = prove
 (`!x. ((\x. x pow 32) diffl (&32 * x pow 31))(x)`,
  W(MP_TAC o DIFF_CONV o lhand o rator o body o rand o snd) THEN
  REWRITE_TAC[REAL_MUL_RID; ARITH]);;

let ROOT_2_LEMMA0b = prove
 (`!x. (\x. x pow 32) differentiable x /\
       (\x. x pow 32) contl x`,
  MESON_TAC[ROOT_2_LEMMA0a; differentiable; DIFF_CONT]);;

let ROOT_2_LEMMA1 = prove
 (`!a b. a <= b ==> ?x. a <= x /\ x <= b /\
                        (b pow 32 = a pow 32 + &32 * x pow 31 * (b - a))`,
  REPEAT GEN_TAC THEN
  DISCH_THEN(DISJ_CASES_TAC o REWRITE_RULE[REAL_LE_LT]) THENL
   [MP_TAC(SPECL [`\x. x pow 32`; `a:real`; `b:real`] MVT) THEN
    ASM_REWRITE_TAC[ROOT_2_LEMMA0b] THEN
    DISCH_THEN(X_CHOOSE_THEN `l:real` MP_TAC) THEN
    DISCH_THEN(X_CHOOSE_THEN `x:real` STRIP_ASSUME_TAC) THEN
    EXISTS_TAC `x:real` THEN REPEAT CONJ_TAC THENL
     [ASM_MESON_TAC[REAL_LT_IMP_LE];
      ASM_MESON_TAC[REAL_LT_IMP_LE];
      ONCE_REWRITE_TAC[REAL_ADD_SYM] THEN
      ASM_REWRITE_TAC[GSYM REAL_EQ_SUB_RADD] THEN
      REWRITE_TAC[REAL_MUL_ASSOC] THEN
      GEN_REWRITE_TAC RAND_CONV [REAL_MUL_SYM] THEN
      AP_TERM_TAC THEN MATCH_MP_TAC DIFF_UNIQ THEN
      MAP_EVERY EXISTS_TAC [`\x. x pow 32`; `x:real`] THEN
      ASM_REWRITE_TAC[ROOT_2_LEMMA0a]];
    EXISTS_TAC `b:real` THEN ASM_REWRITE_TAC[] THEN
    REWRITE_TAC[REAL_SUB_REFL; REAL_LE_REFL; REAL_MUL_RZERO; REAL_ADD_RID]]);;

let ROOT_2_LEMMA2 = prove
 (`&0 <= a /\ a <= b
   ==> &32 * a pow 31 * (b - a) <= b pow 32 - a pow 32`,
  STRIP_TAC THEN
  FIRST_ASSUM(X_CHOOSE_THEN `x:real` STRIP_ASSUME_TAC o
        MATCH_MP ROOT_2_LEMMA1) THEN
  MATCH_MP_TAC REAL_LE_TRANS THEN
  EXISTS_TAC `&32 * x pow 31 * (b - a)` THEN CONJ_TAC THENL
   [MATCH_MP_TAC REAL_LE_MUL2 THEN
    REWRITE_TAC[REAL_OF_NUM_LE; ARITH] THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_MUL THEN ASM_REWRITE_TAC[REAL_SUB_LE] THEN
      MATCH_MP_TAC REAL_POW_LE THEN ASM_REWRITE_TAC[];
      MATCH_MP_TAC REAL_LE_MUL2 THEN
      REWRITE_TAC[REAL_SUB_LE; REAL_LE_REFL] THEN
      CONJ_TAC THENL
       [MATCH_MP_TAC REAL_POW_LE THEN ASM_REWRITE_TAC[];
        ASM_REWRITE_TAC[] THEN MATCH_MP_TAC REAL_POW_LE2 THEN
        ASM_REWRITE_TAC[]]];
    ASM_REWRITE_TAC[REAL_ADD_SUB; REAL_LE_REFL]]);;

let ROOT_2_LEMMA3 = prove
 (`&0 <= a /\ a <= b /\
   abs(b pow 32 - a pow 32) <= e
   ==> &32 * a pow 31 * abs(b - a) <= e`,
  STRIP_TAC THEN MATCH_MP_TAC REAL_LE_TRANS THEN
  EXISTS_TAC `abs(b:real pow 32 - a pow 32)` THEN
  ASM_REWRITE_TAC[] THEN
  SUBGOAL_THEN `a:real pow 32 <= b pow 32` ASSUME_TAC THENL
   [MATCH_MP_TAC REAL_POW_LE2 THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
  ASM_REWRITE_TAC[real_abs; REAL_SUB_LE] THEN
  MATCH_MP_TAC ROOT_2_LEMMA2 THEN ASM_REWRITE_TAC[]);;

let ROOT_2_LEMMA4 = prove
 (`&0 < a /\ a <= b /\ a <= &2 /\
   abs(b pow 32 - a pow 32) <= e
   ==> &32 * a pow 32 * abs(b - a) <= &2 * e`,
  STRIP_TAC THEN MATCH_MP_TAC REAL_LE_TRANS THEN
  EXISTS_TAC `a:real * e` THEN CONJ_TAC THENL
   [REWRITE_TAC[num_CONV `32`; real_pow] THEN
    REWRITE_TAC[SYM(num_CONV `32`)] THEN
    GEN_REWRITE_TAC LAND_CONV [REAL_MUL_SYM] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    MATCH_MP_TAC REAL_LE_LMUL THEN
    CONJ_TAC THENL [ASM_MESON_TAC[REAL_LT_IMP_LE]; ALL_TAC] THEN
    REWRITE_TAC[REAL_MUL_ASSOC] THEN
    GEN_REWRITE_TAC LAND_CONV [REAL_MUL_SYM] THEN
    MATCH_MP_TAC ROOT_2_LEMMA3 THEN
    ASM_REWRITE_TAC[] THEN ASM_MESON_TAC[REAL_LT_IMP_LE];
    MATCH_MP_TAC REAL_LE_RMUL THEN ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `abs(b:real pow 32 - a pow 32)` THEN
    ASM_REWRITE_TAC[REAL_ABS_POS]]);;

let ROOT_2_LEMMA5 = prove
 (`&0 < a /\ a <= &2 /\
   &0 < b /\ b <= &2 /\
   abs(b pow 32 - a pow 32) <= e
   ==> &32 * (a pow 32 - e) * abs(b - a) <= &2 * e`,
  STRIP_TAC THEN
  DISJ_CASES_TAC(SPECL [`a:real`; `b:real`] REAL_LE_TOTAL) THENL
   [MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&32 * a pow 32 * abs(b - a)` THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_LMUL THEN REWRITE_TAC[REAL_OF_NUM_LE; LE_0] THEN
      MATCH_MP_TAC REAL_LE_RMUL THEN REWRITE_TAC[REAL_ABS_POS] THEN
      UNDISCH_TAC `abs(b:real pow 32 - a pow 32) <= e` THEN REAL_ARITH_TAC;
      MATCH_MP_TAC ROOT_2_LEMMA4 THEN ASM_REWRITE_TAC[]];
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `&32 * b pow 32 * abs(b - a)` THEN CONJ_TAC THENL
     [MATCH_MP_TAC REAL_LE_LMUL THEN REWRITE_TAC[REAL_OF_NUM_LE; LE_0] THEN
      MATCH_MP_TAC REAL_LE_RMUL THEN REWRITE_TAC[REAL_ABS_POS] THEN
      UNDISCH_TAC `abs(b:real pow 32 - a pow 32) <= e` THEN REAL_ARITH_TAC;
      ONCE_REWRITE_TAC[REAL_ABS_SUB] THEN
      MATCH_MP_TAC ROOT_2_LEMMA4 THEN
      ONCE_REWRITE_TAC[REAL_ABS_SUB] THEN ASM_REWRITE_TAC[]]]);;

let ROOT_32_POW = prove
 (`!x. &0 <= x ==> ((root 32 x) pow 32 = x)`,
  REWRITE_TAC[num_CONV `32`; ROOT_POW_POS]);;

let ROOT_32_POWJ_POW = prove
 (`!j. (root(32) (&2 pow j)) pow 32 = &2 pow j`,
  GEN_TAC THEN MATCH_MP_TAC ROOT_32_POW THEN
  MATCH_MP_TAC REAL_POW_LE THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[REAL_OF_NUM_LE; ARITH]);;

let POW_32_MONO_LT = prove
 (`&0 <= x /\ &0 <= y ==> (x pow 32 < y pow 32 = x < y)`,
  STRIP_TAC THEN EQ_TAC THENL
   [CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[REAL_NOT_LT] THEN
    DISCH_TAC THEN MATCH_MP_TAC REAL_POW_LE2 THEN ASM_REWRITE_TAC[];
    DISCH_TAC THEN MATCH_MP_TAC REAL_POW_LT2 THEN ASM_REWRITE_TAC[ARITH]]);;

let ROOT_32_MONO_LT = prove
 (`&0 <= x /\ &0 <= y ==> (root 32 x < root 32 y = x < y)`,
  STRIP_TAC THEN
  SUBGOAL_THEN `&0 <= root 32 x /\ &0 <= root 32 y` MP_TAC THENL
   [REWRITE_TAC[num_CONV `32`] THEN
    CONJ_TAC THEN MATCH_MP_TAC ROOT_POS_POSITIVE THEN
    ASM_REWRITE_TAC[]; ALL_TAC] THEN
  DISCH_THEN(SUBST1_TAC o SYM o MATCH_MP POW_32_MONO_LT) THEN
  REWRITE_TAC[num_CONV `32`] THEN
  BINOP_TAC THEN MATCH_MP_TAC ROOT_POW_POS THEN
  ASM_REWRITE_TAC[]);;

let ROOT_32_MONO_LE = prove
 (`&0 <= x /\ &0 <= y ==> (root 32 x <= root 32 y = x <= y)`,
  STRIP_TAC THEN REWRITE_TAC[GSYM REAL_NOT_LT] THEN
  AP_TERM_TAC THEN MATCH_MP_TAC ROOT_32_MONO_LT THEN
  ASM_REWRITE_TAC[]);;

let ROOT_2_APPROX = prove
 (`j <= 32 /\
   &0 < x /\ x <= &2 /\
   e < &2 pow j /\
   abs(x pow 32 - &2 pow j) <= e
   ==> abs(x - root 32 (&2 pow j)) <= e / (&16 * (&2 pow j - e))`,
  STRIP_TAC THEN MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN
  EXISTS_TAC `&16 * (&2 pow j - e)` THEN
  SUBGOAL_THEN `&0 < &16 * (&2 pow j - e)` ASSUME_TAC THENL
   [MATCH_MP_TAC REAL_LT_MUL THEN
    ASM_REWRITE_TAC[REAL_OF_NUM_LT; ARITH; REAL_SUB_LT];
    ASM_REWRITE_TAC[]] THEN
  MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `e:real` THEN CONJ_TAC THENL
   [MATCH_MP_TAC REAL_LE_LCANCEL_IMP THEN EXISTS_TAC `&2` THEN
    REWRITE_TAC[REAL_OF_NUM_LT; ARITH; REAL_MUL_ASSOC] THEN
    REWRITE_TAC[REAL_OF_NUM_MUL; ARITH] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    GEN_REWRITE_TAC (LAND_CONV o RAND_CONV o LAND_CONV o LAND_CONV)
     [GSYM ROOT_32_POWJ_POW] THEN
    MATCH_MP_TAC ROOT_2_LEMMA5 THEN
    ASM_REWRITE_TAC[ROOT_32_POWJ_POW] THEN CONJ_TAC THENL
     [MP_TAC(SPEC `31` ROOT_0) THEN REWRITE_TAC[ARITH] THEN
      DISCH_THEN(SUBST1_TAC o SYM) THEN
      SUBGOAL_THEN `&0 <= &0 /\ &0 <= &2 pow j` MP_TAC THENL
       [REWRITE_TAC[REAL_LE_REFL] THEN
        MATCH_MP_TAC REAL_POW_LE THEN REAL_ARITH_TAC;
        DISCH_THEN(SUBST1_TAC o MATCH_MP ROOT_32_MONO_LT) THEN
        MATCH_MP_TAC REAL_POW_LT THEN REAL_ARITH_TAC];
      MP_TAC(SPECL [`31`; `&2`] POW_ROOT_POS) THEN
      REWRITE_TAC[ARITH; REAL_OF_NUM_LE] THEN
      DISCH_THEN(fun th -> GEN_REWRITE_TAC RAND_CONV [GSYM th]) THEN
      SUBGOAL_THEN `&0 <= &2 pow j /\ &0 <= &2 pow 32` MP_TAC THENL
       [CONJ_TAC THEN MATCH_MP_TAC REAL_POW_LE THEN REAL_ARITH_TAC;
        DISCH_THEN(SUBST1_TAC o MATCH_MP ROOT_32_MONO_LE) THEN
        MATCH_MP_TAC REAL_POW_MONO THEN
        ASM_REWRITE_TAC[REAL_OF_NUM_LE; ARITH]]];
    MATCH_MP_TAC REAL_EQ_IMP_LE THEN
    ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN CONV_TAC SYM_CONV THEN
    REWRITE_TAC[real_div; GSYM REAL_MUL_ASSOC] THEN
    GEN_REWRITE_TAC RAND_CONV [GSYM REAL_MUL_RID] THEN
    AP_TERM_TAC THEN MATCH_MP_TAC REAL_MUL_LINV THEN
    MATCH_MP_TAC REAL_LT_IMP_NZ THEN ASM_REWRITE_TAC[]]);;

let TABLES_OK_APPROX = prove
 (`!S_Lead S_Trail.
        TABLES_OK S_Lead S_Trail
        ==> !j. 0 <= j /\ j < 32
                ==> abs((Val(S_Lead(Int j)) + Val(S_Trail(Int j))) -
                        root(32) (&2 pow j)) <= inv(&2 pow 41)`,
  let lemma = prove
   (`!e1 y e2.
       j <= 32 /\
       &0 < x /\ x <= &2 /\
       abs(y - x pow 32) <= e1 /\
       (e2 = e1 + abs(y - &2 pow j)) /\
       e2 < &2 pow j /\
       e2 / (&16 * (&2 pow j - e2)) <= e
       ==> abs(x - root 32 (&2 pow j)) <= e`,
    REPEAT GEN_TAC THEN STRIP_TAC THEN
    MATCH_MP_TAC REAL_LE_TRANS THEN
    EXISTS_TAC `e2 / (&16 * (&2 pow j - e2))` THEN
    CONJ_TAC THENL [ALL_TAC; FIRST_ASSUM ACCEPT_TAC] THEN
    MATCH_MP_TAC ROOT_2_APPROX THEN
    ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_ABS_TRIANGLE_LE THEN
    EXISTS_TAC `--(y - x:real pow 32)` THEN
    REWRITE_TAC[REAL_ABS_NEG] THEN
    REWRITE_TAC[REAL_ARITH `(a:real - b) - --(c - a) = c - b`] THEN
    ASM_REWRITE_TAC[REAL_LE_RADD]) in
  REPEAT GEN_TAC THEN REWRITE_TAC[TABLES_OK] THEN
  STRIP_TAC THEN GEN_TAC THEN
  CONV_TAC(LAND_CONV(RAND_CONV(RAND_CONV(TOP_SWEEP_CONV num_CONV)))) THEN
  REWRITE_TAC[LT] THEN REWRITE_TAC[ARITH_SUC] THEN
  REWRITE_TAC[LE_0] THEN
  DISCH_THEN(REPEAT_TCL DISJ_CASES_THEN SUBST1_TAC) THEN
  ASM_REWRITE_TAC[] THEN POP_ASSUM_LIST(K ALL_TAC) THEN

  (CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
   CONV_TAC(ONCE_DEPTH_CONV REAL_RAT_ADD_CONV) THEN
   MATCH_MP_TAC lemma THEN
   REWRITE_TAC[RIGHT_EXISTS_AND_THM] THEN
   CONJ_TAC THENL [REWRITE_TAC[ARITH]; ALL_TAC] THEN
   CONJ_TAC THENL[CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
   CONJ_TAC THENL[CONV_TAC REAL_RAT_REDUCE_CONV; ALL_TAC] THEN
   W(fun (_,w) ->
       let tm = lhand(rand(rand(lhand(lhand(snd
                  (dest_exists(snd(dest_exists w)))))))) in
       let th = POW32_CONV tm 80 in
       let ytm = lhand(rand(lhand(concl th))) in
       let etm = rand(concl th) in
       EXISTS_TAC etm THEN EXISTS_TAC ytm THEN REWRITE_TAC[th]) THEN
   CONV_TAC REAL_RAT_REDUCE_CONV THEN
   REWRITE_TAC[UNWIND_THM2] THEN
   CONV_TAC REAL_RAT_REDUCE_CONV));;

prioritize_float();;
