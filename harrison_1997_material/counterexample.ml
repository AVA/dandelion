(* ========================================================================= *)
(* Checking that the smallest counterexample really is one.                  *)
(* ========================================================================= *)

let lemma1 = prove
 (`(THRESHOLD_1     = float(0,134,6056890)) /\
   (THRESHOLD_2     = float(0,102,0)) /\
   (X = float(0,132,3606720))
   ==> ~(abs(X) > THRESHOLD_1) /\
       ~(abs(X) < THRESHOLD_2)`,
  STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
  SUBGOAL_THEN `Finite(float(0,132,3606720))` ASSUME_TAC THENL
   [CONV_TAC ISFINITE_FLOAT_CONV; ALL_TAC] THEN
  FIRST_ASSUM(STRIP_ASSUME_TAC o MATCH_MP FLOAT_ABS) THEN
  SUBGOAL_THEN `Finite(abs(float(0,132,3606720))) /\
                Finite(float(0,134,6056890))`
  MP_TAC THENL [ASM_REWRITE_TAC[] THEN CONV_TAC ISFINITE_FLOAT_CONV;
                ALL_TAC] THEN
  DISCH_THEN(SUBST1_TAC o MATCH_MP FLOAT_GT) THEN
  SUBGOAL_THEN `Finite(abs(float(0,132,3606720))) /\
                Finite(float(0,102,0))`
  MP_TAC THENL [ASM_REWRITE_TAC[] THEN CONV_TAC ISFINITE_FLOAT_CONV;
                ALL_TAC] THEN
  DISCH_THEN(SUBST1_TAC o MATCH_MP FLOAT_LT) THEN
  ASM_REWRITE_TAC[] THEN
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma2 = prove
 (`abs(Val(float(0,132,3606720)) * Val(float(0,132,3713595)) -
       Val(float(0,138,264192))) < inv(&2) * Val(float(0,138,1))`,
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma3 = prove
 (`abs(Val(float(0,138,264192)) - Val(float(0,138,262144))) <= inv(&2)`,
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma4 = prove
 (`Val(float(0,138,262144)) = &32 * &66`,
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma5 = prove
 (`Val(float(0,138,262144)) >= Ival(Int(2 EXP 9))`,
  SUBGOAL_THEN `2 EXP 9 < 2 EXP 31` MP_TAC THENL
   [REWRITE_TAC[ARITH]; ALL_TAC] THEN
  DISCH_THEN(SUBST1_TAC o MATCH_MP IVAL_INT_LEMMA) THEN
  REWRITE_TAC[GSYM REAL_OF_NUM_POW] THEN
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma6 = prove
 (`abs(Val(float(0,138,262144)) * Val(float(0,121,3240448)) -
       Val(float(0,132,3603856))) < inv(&2) * Val(float(0,132,1))`,
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma7 = prove
 (`Val(float(0,0,0)) * Val(float(0,121,3240448)) = Val(float(0,0,0))`,
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma8 = prove
 (`abs((Val(float(0,132,3606720)) - Val(float(0,132,3603856))) -
       Val(float(0,120,3342336))) < inv(&2) * Val(float(0,120,1))`,
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma9 = prove
 (`abs((Val(float(0,120,3342336)) - Val(float(0,0,0))) -
       Val(float(0,120,3342336))) < inv(&2) * Val(float(0,120,1))`,
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma10 = prove
 (`abs(Val(float(1,138,262144)) * Val(float(0,102,4177550)) -
       Val(float(1,113,4570242))) <= inv(&2) * Val(float(0,113,1))`,
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;

let lemma11 = prove
 (`Val(float(0,120,3342336)) + Val(float(1,113,4570242)) >
   &10831 / &1000000`,
  CONV_TAC(ONCE_DEPTH_CONV VAL_FLOAT_CONV) THEN
  CONV_TAC REAL_RAT_REDUCE_CONV);;
