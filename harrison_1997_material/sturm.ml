(* ========================================================================= *)
(* Formalization of Sturm sequences and Sturm's theorem.                     *)
(* ========================================================================= *)

do_list override_interface
 ["divides",`poly_divides:real list->real list->bool`;
  "exp",`poly_exp:real list -> num -> real list`;
   "diff",`poly_diff:real list->real list`];;

(* ------------------------------------------------------------------------- *)
(* Dreary lemmas about sign alternations.                                    *)
(* ------------------------------------------------------------------------- *)

let SIGN_LEMMA0 = prove
 (`!a b c. &0 < a /\ &0 < b /\ &0 < c
           ==> &0 < a * b /\ &0 < a * c /\ &0 < b * c`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC REAL_LT_MUL THEN
  ASM_REWRITE_TAC[]);;

let SIGN_LEMMA1 = prove
 (`!a b c. a * b > &0 ==> (c * a < &0 <=> c * b < &0)`,
  REPEAT GEN_TAC THEN REWRITE_TAC[real_gt] THEN
  REPEAT_TCL DISJ_CASES_THEN ASSUME_TAC
   (SPEC `a:real` REAL_LT_NEGTOTAL) THEN
  REPEAT_TCL DISJ_CASES_THEN ASSUME_TAC
   (SPEC `b:real` REAL_LT_NEGTOTAL) THEN
  REPEAT_TCL DISJ_CASES_THEN ASSUME_TAC
   (SPEC `c:real` REAL_LT_NEGTOTAL) THEN
  ASM_REWRITE_TAC[REAL_MUL_RZERO; REAL_MUL_LZERO; REAL_LT_REFL] THEN
  POP_ASSUM_LIST(MP_TAC o MATCH_MP SIGN_LEMMA0 o end_itlist CONJ) THEN
  REWRITE_TAC[REAL_MUL_LNEG; REAL_MUL_RNEG; REAL_NEG_NEG] THEN
  REWRITE_TAC[REAL_MUL_AC] THEN REAL_ARITH_TAC);;

let SIGN_LEMMA2 = prove
 (`!a b c. a * b > &0 ==> (a * c < &0 <=> b * c < &0)`,
  REPEAT GEN_TAC THEN DISCH_THEN(MP_TAC o SPEC_ALL o MATCH_MP SIGN_LEMMA1) THEN
  REWRITE_TAC[REAL_MUL_AC]);;

let SIGN_LEMMA3 = prove
 (`!a b. a < &0 ==> (a * b > &0 <=> b < &0)`,
  REPEAT GEN_TAC THEN
  REWRITE_TAC[REAL_ARITH `a < &0 <=> --(&1) * a > &0`] THEN
  DISCH_THEN(MP_TAC o MATCH_MP SIGN_LEMMA1) THEN
  DISCH_THEN(MP_TAC o SPEC `--b`) THEN
  REWRITE_TAC[REAL_MUL_AC; real_gt; REAL_MUL_LNEG; REAL_MUL_RNEG;
    REAL_NEG_NEG; REAL_MUL_LID; REAL_MUL_RID] THEN REAL_ARITH_TAC);;

let SIGN_LEMMA4 = prove
 (`!a b c. a * b < &0 /\ ~(c = &0) ==> (c * a < &0 <=> ~(c * b < &0))`,
  REPEAT STRIP_TAC THEN
  MP_TAC(SPECL [`a:real`; `--b`; `c:real`] SIGN_LEMMA2) THEN
  ASM_REWRITE_TAC[REAL_MUL_RNEG; real_gt] THEN
  ASM_REWRITE_TAC[REAL_ARITH `&0 < --a <=> a < &0`] THEN
  REWRITE_TAC[REAL_MUL_AC] THEN DISCH_THEN SUBST1_TAC THEN
  REWRITE_TAC[REAL_MUL_RNEG; REAL_MUL_AC] THEN
  SUBGOAL_THEN `~(b * c = &0)` MP_TAC THENL
   [ASM_REWRITE_TAC[REAL_ENTIRE; DE_MORGAN_THM] THEN
    DISCH_TAC THEN UNDISCH_TAC `a * b < &0` THEN
    ASM_REWRITE_TAC[REAL_MUL_LZERO; REAL_MUL_RZERO; REAL_LT_REFL];
    REAL_ARITH_TAC]);;

let SIGN_LEMMA5 = prove
 (`a * b < &0 <=> a > &0 /\ b < &0 \/ a < &0 /\ b > &0`,
  REPEAT_TCL DISJ_CASES_THEN ASSUME_TAC
   (SPEC `a:real` REAL_LT_NEGTOTAL) THEN
  REPEAT_TCL DISJ_CASES_THEN ASSUME_TAC
   (SPEC `b:real` REAL_LT_NEGTOTAL) THEN
  ASM_REWRITE_TAC[REAL_MUL_LZERO; REAL_MUL_RZERO; REAL_LT_REFL; real_gt] THEN
  POP_ASSUM_LIST(MP_TAC o end_itlist CONJ) THEN
  DISCH_THEN(fun th -> MP_TAC th THEN ASSUME_TAC th) THEN
  POP_ASSUM(MP_TAC o MATCH_MP REAL_LT_MUL) THEN
  REWRITE_TAC[REAL_MUL_LNEG; REAL_MUL_RNEG; REAL_NEG_NEG] THEN
  REAL_ARITH_TAC);;

let SIGN_LEMMA6 = prove
 (`a * b <= &0 <=> a >= &0 /\ b <= &0 \/ a <= &0 /\ b >= &0`,
  REWRITE_TAC[real_ge; REAL_LE_LT; REAL_ENTIRE; SIGN_LEMMA5] THEN
  ASM_CASES_TAC `a = &0` THEN ASM_CASES_TAC `b = &0` THEN
  ASM_REWRITE_TAC[REAL_MUL_LZERO; REAL_MUL_RZERO; REAL_LT_REFL] THEN
  REWRITE_TAC[real_gt] THEN
  FIRST_ASSUM(UNDISCH_TAC o check is_neg o concl) THEN
  REAL_ARITH_TAC);;

(* ------------------------------------------------------------------------- *)
(* The number of variations in sign of a list of reals.                      *)
(* ------------------------------------------------------------------------- *)

let varrec = new_recursive_definition list_RECURSION
 `(varrec prev [] = 0) /\
  (varrec prev (CONS h t) =
      if prev * h < &0 then SUC(varrec h t)
      else if h = &0 then varrec prev t
                     else varrec h t)`;;

let variation = new_definition
 `variation l = varrec (&0) l`;;

(* ------------------------------------------------------------------------- *)
(* Show that it depends only on the sign of the "previous element".          *)
(* ------------------------------------------------------------------------- *)

let VARREC_SIGN = prove
 (`!l r s. r * s > &0 ==> (varrec r l = varrec s l)`,
  LIST_INDUCT_TAC THEN REPEAT GEN_TAC THEN REWRITE_TAC[varrec] THEN
  DISCH_TAC THEN
  FIRST_ASSUM(fun th -> REWRITE_TAC[MATCH_MP SIGN_LEMMA2 th]) THEN
  ASM_CASES_TAC `s * h < &0` THEN ASM_REWRITE_TAC[] THEN
  COND_CASES_TAC THEN REWRITE_TAC[] THEN
  FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[]);;

(* ------------------------------------------------------------------------- *)
(* Middle is irrelevant if surrounding elements have opposite sign.          *)
(* ------------------------------------------------------------------------- *)

let VARREC_STRADDLE = prove
 (`!f g h x. poly f x * poly h x < &0
             ==> (varrec (poly f x) (MAP (\p. poly p x) (CONS g (CONS h t))) =
                  SUC (varrec (poly h x) (MAP (\p. poly p x) t)))`,
  REPEAT GEN_TAC THEN REWRITE_TAC[MAP; varrec] THEN
  ASM_CASES_TAC `poly h x = &0` THEN
  ASM_CASES_TAC `poly g x = &0` THEN
  ASM_REWRITE_TAC[REAL_MUL_LZERO; REAL_MUL_RZERO; real_gt; REAL_LT_REFL] THEN
  DISCH_TAC THEN ASM_REWRITE_TAC[] THEN
  GEN_REWRITE_TAC (LAND_CONV o RATOR_CONV o RATOR_CONV o RAND_CONV o LAND_CONV)
   [REAL_MUL_SYM] THEN
  MP_TAC(SPECL [`poly f x`; `poly h x`; `poly g x`] SIGN_LEMMA4) THEN
  ASM_REWRITE_TAC[] THEN DISCH_THEN SUBST1_TAC THEN
  ASM_CASES_TAC `poly g x * poly h x < &0` THEN ASM_REWRITE_TAC[]);;

(* ------------------------------------------------------------------------- *)
(* Property of being a (standard) Sturm sequence.                            *)
(* ------------------------------------------------------------------------- *)

let STURM = new_recursive_definition list_RECURSION
  `(STURM f f' [] <=> f' divides f) /\
   (STURM f f' (CONS g gs) <=> (?k. &0 < k /\ f' divides (f ++ k ## g)) /\
                               degree g < degree f' /\ STURM f' g gs)`;;

(* ------------------------------------------------------------------------- *)
(* If a polynomial doesn't have a root in an interval, sign doesn't change.  *)
(* ------------------------------------------------------------------------- *)

let STURM_NOROOT = prove
 (`!a b p. a <= b /\
           (!x. a <= x /\ x <= b ==> ~(poly p x = &0))
           ==> poly p a * poly p b > &0`,
  REPEAT GEN_TAC THEN REWRITE_TAC[real_gt] THEN
  GEN_REWRITE_TAC (LAND_CONV o LAND_CONV) [REAL_LE_LT] THEN
  DISCH_THEN(CONJUNCTS_THEN2 DISJ_CASES_TAC ASSUME_TAC) THENL
   [SUBGOAL_THEN `~(poly p a = &0) /\ ~(poly p b = &0)` STRIP_ASSUME_TAC THENL
     [CONJ_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
      UNDISCH_TAC `a < b` THEN REAL_ARITH_TAC;
      REPEAT_TCL DISJ_CASES_THEN MP_TAC (SPEC `poly p a` REAL_LT_NEGTOTAL) THEN
      REPEAT_TCL DISJ_CASES_THEN MP_TAC (SPEC `poly p b` REAL_LT_NEGTOTAL) THEN
      ASM_REWRITE_TAC[] THEN
      REWRITE_TAC[TAUT `a ==> b ==> c <=> b /\ a ==> c`] THEN
      TRY(DISCH_THEN(MP_TAC o MATCH_MP REAL_LT_MUL) THEN
          REWRITE_TAC[REAL_MUL_LNEG; REAL_MUL_RNEG; REAL_NEG_NEG] THEN
          NO_TAC) THEN
      REWRITE_TAC[REAL_ARITH `&0 < --a <=> a < &0`] THEN
      REWRITE_TAC[REAL_ARITH `&0 < x <=> x > &0`] THEN
      UNDISCH_TAC `a < b` THEN
      REWRITE_TAC[IMP_IMP] THENL
       [DISCH_THEN(X_CHOOSE_THEN `x:real` MP_TAC o MATCH_MP POLY_IVT_NEG);
        DISCH_THEN(X_CHOOSE_THEN `x:real` MP_TAC o MATCH_MP POLY_IVT_POS)] THEN
      REPEAT(DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC)) THEN
      CONV_TAC CONTRAPOS_CONV THEN DISCH_TAC THEN
      FIRST_ASSUM MATCH_MP_TAC THEN CONJ_TAC THEN
      MATCH_MP_TAC REAL_LT_IMP_LE THEN ASM_REWRITE_TAC[]];
    ASM_REWRITE_TAC[REAL_LT_SQUARE] THEN
    FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[REAL_LE_REFL]]);;

(* ------------------------------------------------------------------------- *)
(* Now we get the changes in the variation.                                  *)
(* ------------------------------------------------------------------------- *)

let STURM_NOROOT_NOVAR_LEMMA = prove
 (`!f a b l.
    a <= b /\
    (!x. a <= x /\ x <= b ==> ~(poly f x = &0))
    ==> (varrec r (MAP (\p. poly p a) (CONS f l)) +
         varrec (poly f a) (MAP (\p. poly p b) l) =
         varrec r (MAP (\p. poly p b) (CONS f l)) +
         varrec (poly f a) (MAP (\p. poly p a) l))`,
  REPEAT GEN_TAC THEN DISCH_TAC THEN
  GEN_REWRITE_TAC (LAND_CONV o LAND_CONV o RAND_CONV) [MAP] THEN
  GEN_REWRITE_TAC (RAND_CONV o LAND_CONV o RAND_CONV) [MAP] THEN
  REWRITE_TAC[varrec] THEN
  FIRST_ASSUM(ASSUME_TAC o MATCH_MP STURM_NOROOT) THEN
  FIRST_ASSUM(ASSUME_TAC o MATCH_MP SIGN_LEMMA1) THEN
  FIRST_ASSUM(ASSUME_TAC o MATCH_MP VARREC_SIGN) THEN
  ASM_REWRITE_TAC[] THEN
  SUBGOAL_THEN `~(poly f a = &0) /\ ~(poly f b = &0)` STRIP_ASSUME_TAC THENL
   [CONJ_TAC THEN FIRST_ASSUM(MATCH_MP_TAC o CONJUNCT2) THEN
    ASM_REWRITE_TAC[REAL_LE_REFL];
    ASM_REWRITE_TAC[] THEN
    COND_CASES_TAC THEN ASM_REWRITE_TAC[ADD_CLAUSES; ADD_AC]]);;

let STURM_NOROOT_NOVAR = prove
 (`!f a b l.
    a <= b /\
    (!x. a <= x /\ x <= b ==> ~(poly f x = &0)) /\
    (varrec (poly f a) (MAP (\p. poly p a) l) =
     varrec (poly f a) (MAP (\p. poly p b) l))
    ==> (varrec r (MAP (\p. poly p a) (CONS f l)) =
         varrec r (MAP (\p. poly p b) (CONS f l)))`,
  REPEAT GEN_TAC THEN REWRITE_TAC[CONJ_ASSOC] THEN
  DISCH_THEN(CONJUNCTS_THEN2 MP_TAC ASSUME_TAC) THEN
  DISCH_THEN(MP_TAC o SPEC_ALL o MATCH_MP STURM_NOROOT_NOVAR_LEMMA) THEN
  ASM_REWRITE_TAC[] THEN ARITH_TAC);;

let STURM_NOVAR_LEMMA = prove
 (`!n l f f' c.
        (LENGTH l = n) /\
        STURM f f' l /\
        a <= c /\ c <= b /\
        (!x. a <= x /\ x <= b /\ EX (\p. poly p x = &0) (CONS f (CONS f' l))
             ==> (x = c)) /\
        ~(poly f c = &0)
        ==> (varrec (poly f a) (MAP (\p. poly p a) (CONS f' l)) =
             varrec (poly f b) (MAP (\p. poly p b) (CONS f' l)))`,
  MATCH_MP_TAC num_WF THEN GEN_TAC THEN DISCH_TAC THEN
  LIST_INDUCT_TAC THEN REWRITE_TAC[STURM] THENL
   [REPEAT STRIP_TAC THEN SUBGOAL_THEN `~(poly f' c = &0)` ASSUME_TAC THENL
     [UNDISCH_TAC `~(poly f c = &0)` THEN CONV_TAC CONTRAPOS_CONV THEN
      REWRITE_TAC[] THEN DISCH_TAC THEN
      UNDISCH_TAC `f' divides f` THEN REWRITE_TAC[divides] THEN
      DISCH_THEN(CHOOSE_THEN SUBST1_TAC) THEN
      ASM_REWRITE_TAC[POLY_MUL; REAL_MUL_LZERO]; ALL_TAC] THEN
    FIRST_ASSUM(UNDISCH_TAC o check is_forall o concl) THEN
    REWRITE_TAC[EX] THEN DISCH_TAC THEN
    SUBGOAL_THEN `(!x. a <= x /\ x <= b ==> ~(poly f x = &0)) /\
                  (!x. a <= x /\ x <= b ==> ~(poly f' x = &0))`
    STRIP_ASSUME_TAC THENL
     [ASM_MESON_TAC[]; ALL_TAC] THEN
    SUBGOAL_THEN `a <= b` ASSUME_TAC THENL
     [UNDISCH_TAC `c <= b` THEN UNDISCH_TAC `a <= c` THEN ARITH_TAC;
      ALL_TAC] THEN
    MATCH_MP_TAC EQ_TRANS THEN
    EXISTS_TAC `varrec (poly f b) (MAP (\p. poly p a) [f'])` THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC VARREC_SIGN THEN
      MATCH_MP_TAC STURM_NOROOT THEN ASM_REWRITE_TAC[];
      MATCH_MP_TAC STURM_NOROOT_NOVAR THEN ASM_REWRITE_TAC[] THEN
      REWRITE_TAC[MAP]];

    REPEAT STRIP_TAC THEN
    SUBGOAL_THEN `!x. a <= x /\ x <= b ==> ~(poly f x = &0)` ASSUME_TAC THENL

     [UNDISCH_TAC `~(poly f c = &0)` THEN
      UNDISCH_TAC `!x. a <= x /\ x <= b /\
                       EX (\p. poly p x = &0) (CONS f (CONS f' (CONS h t)))
                       ==> (x = c)` THEN
      REWRITE_TAC[EX] THEN MESON_TAC[];
      ASM_REWRITE_TAC[]] THEN
    SUBGOAL_THEN `a <= b` ASSUME_TAC THENL
     [UNDISCH_TAC `c <= b` THEN UNDISCH_TAC `a <= c` THEN ARITH_TAC;
      ALL_TAC] THEN
    MATCH_MP_TAC EQ_TRANS THEN
    EXISTS_TAC
      `varrec (poly f b) (MAP (\p. poly p a) (CONS f' (CONS h t)))` THEN
    CONJ_TAC THENL
     [MATCH_MP_TAC VARREC_SIGN THEN
      MATCH_MP_TAC STURM_NOROOT THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
    ASM_CASES_TAC `poly f' c = &0` THEN ASM_REWRITE_TAC[] THENL
     [UNDISCH_TAC `f' divides f ++ k ## h` THEN REWRITE_TAC[divides] THEN
      DISCH_THEN(X_CHOOSE_THEN `q:real list` MP_TAC) THEN
      DISCH_THEN(MP_TAC o C AP_THM `c:real`) THEN
      REWRITE_TAC[POLY_MUL; POLY_ADD] THEN
      ASM_REWRITE_TAC[REAL_MUL_LZERO] THEN
      REWRITE_TAC[REAL_ARITH `(x + y = &0) <=> (x = --y)`] THEN
      DISCH_TAC THEN
      SUBGOAL_THEN `(!x. a <= x /\ x <= b ==> ~(poly f x = &0)) /\
                    (!x. a <= x /\ x <= b ==> ~(poly h x = &0))`
      STRIP_ASSUME_TAC THENL
       [REWRITE_TAC[AND_FORALL_THM] THEN X_GEN_TAC `x:real` THEN
        UNDISCH_TAC `!x. a <= x /\ x <= b /\
                       EX (\p. poly p x = &0) (CONS f (CONS f' (CONS h t)))
                       ==> (x = c)` THEN
        DISCH_THEN(MP_TAC o SPEC `x:real`) THEN
        REWRITE_TAC[EX] THEN
        SUBGOAL_THEN `(poly h c = &0) <=> (poly f c = &0)` MP_TAC THENL
         [UNDISCH_TAC `poly f c = -- (poly (k ## h) c)` THEN
          DISCH_THEN SUBST1_TAC THEN REWRITE_TAC[POLY_CMUL] THEN
          REWRITE_TAC[GSYM REAL_MUL_LNEG] THEN
          REWRITE_TAC[REAL_ENTIRE] THEN
          ASM_CASES_TAC `poly h c = &0` THEN ASM_REWRITE_TAC[] THEN
          UNDISCH_TAC `&0 < k` THEN REAL_ARITH_TAC;
          ASM_REWRITE_TAC[ASSUME `~(poly f c = &0)`] THEN
          UNDISCH_TAC `~(poly f c = &0)` THEN MESON_TAC[]]; ALL_TAC] THEN
      SUBGOAL_THEN `poly f a * poly f b > &0 /\
                    poly h a * poly h b > &0`
      STRIP_ASSUME_TAC THENL
       [CONJ_TAC THEN MATCH_MP_TAC STURM_NOROOT THEN
        ASM_REWRITE_TAC[]; ALL_TAC] THEN
      SUBGOAL_THEN `(poly f a * poly h a < &0) /\
                    (poly f b * poly h b < &0)`
      MP_TAC THENL
       [SUBGOAL_THEN `a <= c /\
                      (!x. a <= x /\ x <= c ==> ~(poly (f ** h) x = &0))`
        MP_TAC THENL
         [CONJ_TAC THENL
           [ASM_REWRITE_TAC[];
            REWRITE_TAC[REAL_ENTIRE; POLY_MUL; DE_MORGAN_THM] THEN
            GEN_TAC THEN STRIP_TAC THEN CONJ_TAC THEN
            FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[] THEN
            MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `c:real` THEN
            ASM_REWRITE_TAC[] THEN MATCH_MP_TAC REAL_LT_IMP_LE THEN
            ASM_REWRITE_TAC[]];
          DISCH_THEN(MP_TAC o MATCH_MP STURM_NOROOT)] THEN
        SUBGOAL_THEN `c <= b /\
                      (!x. c <= x /\ x <= b ==> ~(poly (f ** h) x = &0))`
        MP_TAC THENL
         [CONJ_TAC THENL
           [ASM_REWRITE_TAC[];
            REWRITE_TAC[REAL_ENTIRE; POLY_MUL; DE_MORGAN_THM] THEN
            GEN_TAC THEN STRIP_TAC THEN CONJ_TAC THEN
            FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[] THEN
            MATCH_MP_TAC REAL_LE_TRANS THEN EXISTS_TAC `c:real` THEN
            ASM_REWRITE_TAC[] THEN MATCH_MP_TAC REAL_LT_IMP_LE THEN
            ASM_REWRITE_TAC[]];
          DISCH_THEN(MP_TAC o MATCH_MP STURM_NOROOT)] THEN
        SUBGOAL_THEN `poly (f ** h) c < &0` MP_TAC THENL
         [ASM_REWRITE_TAC[POLY_MUL; REAL_MUL_LNEG] THEN
          REWRITE_TAC[REAL_ARITH `--x < &0 <=> &0 < x`; POLY_CMUL] THEN
          REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
          MATCH_MP_TAC REAL_LT_MUL THEN ASM_REWRITE_TAC[] THEN
          REWRITE_TAC[REAL_LT_SQUARE] THEN
          FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[];
          GEN_REWRITE_TAC (RAND_CONV o RAND_CONV o LAND_CONV o LAND_CONV)
            [REAL_MUL_SYM] THEN
          DISCH_THEN(fun th -> REWRITE_TAC[MATCH_MP SIGN_LEMMA3 th]) THEN
          REWRITE_TAC[POLY_MUL] THEN CONV_TAC TAUT]; ALL_TAC] THEN
      DISCH_THEN(CONJUNCTS_THEN (ASSUME_TAC o MATCH_MP VARREC_STRADDLE)) THEN
      MATCH_MP_TAC EQ_TRANS THEN
      EXISTS_TAC
        `varrec (poly f a) (MAP (\p. poly p a) (CONS f' (CONS h t)))` THEN
      CONJ_TAC THENL
       [MATCH_MP_TAC VARREC_SIGN THEN ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
        ASM_REWRITE_TAC[];
        ASM_REWRITE_TAC[SUC_INJ]] THEN
      MP_TAC(ISPEC `t:(real list)list` list_CASES) THEN
      DISCH_THEN(DISJ_CASES_THEN2 SUBST_ALL_TAC MP_TAC) THENL
       [REWRITE_TAC[varrec; MAP]; ALL_TAC] THEN
      DISCH_THEN(X_CHOOSE_THEN `g:real list` MP_TAC) THEN
      DISCH_THEN(X_CHOOSE_THEN `m:(real list)list` SUBST_ALL_TAC) THEN
      FIRST_ASSUM(MP_TAC o SPEC `PRE(PRE n)`) THEN
      UNDISCH_TAC `LENGTH (CONS (h:real list) (CONS g m)) = n` THEN
      REWRITE_TAC[LENGTH] THEN DISCH_THEN(SUBST_ALL_TAC o SYM) THEN
      REWRITE_TAC[PRE; ARITH_RULE `n < SUC (SUC n)`] THEN
      DISCH_THEN(MP_TAC o SPECL [`m:(real list)list`; `h:real list`]) THEN
      DISCH_THEN(MP_TAC o SPECL [`g:real list`; `c:real`]) THEN
      ASM_REWRITE_TAC[] THEN DISCH_THEN MATCH_MP_TAC THEN
      RULE_ASSUM_TAC(REWRITE_RULE[STURM]) THEN ASM_REWRITE_TAC[] THEN
      CONJ_TAC THENL
       [REWRITE_TAC[EX] THEN
        REPEAT STRIP_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
        ASM_REWRITE_TAC[EX];
        DISCH_TAC THEN
        UNDISCH_TAC `poly f c = -- (poly (k ## h) c)` THEN
        ASM_REWRITE_TAC[POLY_CMUL; REAL_MUL_RZERO; REAL_NEG_0]];

      SUBGOAL_THEN `~(poly f' a = &0) /\ ~(poly f' b = &0)`
      STRIP_ASSUME_TAC THENL
       [RULE_ASSUM_TAC(REWRITE_RULE[EX]) THEN
        ASM_MESON_TAC[REAL_LE_REFL; REAL_LT_REFL]; ALL_TAC] THEN
      FIRST_ASSUM(MP_TAC o SPEC `PRE n`) THEN
      UNDISCH_TAC `LENGTH (CONS (h:real list) t) = n` THEN
      REWRITE_TAC[LENGTH] THEN DISCH_THEN(SUBST_ALL_TAC o SYM) THEN
      REWRITE_TAC[PRE; ARITH_RULE `n < SUC n`] THEN
      DISCH_THEN(MP_TAC o SPECL [`t:(real list)list`; `f':real list`]) THEN
      DISCH_THEN(MP_TAC o SPECL [`h:real list`; `c:real`]) THEN
      RULE_ASSUM_TAC(REWRITE_RULE[STURM]) THEN ASM_REWRITE_TAC[] THEN
      W(C SUBGOAL_THEN (fun th -> REWRITE_TAC[th]) o lhand o lhand o snd) THENL
       [REWRITE_TAC[EX] THEN
        REPEAT STRIP_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
        ASM_REWRITE_TAC[EX];
        DISCH_TAC] THEN
      ONCE_REWRITE_TAC[MAP] THEN REWRITE_TAC[varrec] THEN
      ASM_REWRITE_TAC[] THEN
      SUBGOAL_THEN `!x. a <= x /\ x <= b ==> ~(poly f' x = &0)`
      ASSUME_TAC THENL
       [UNDISCH_TAC `~(poly f' c = &0)` THEN
        UNDISCH_TAC `!x. a <= x /\ x <= b /\
                         EX (\p. poly p x = &0) (CONS f (CONS f' (CONS h t)))
                         ==> (x = c)` THEN
        REWRITE_TAC[EX] THEN MESON_TAC[];
        MP_TAC(SPECL [`a:real`; `b:real`; `f':real list`] STURM_NOROOT) THEN
        ASM_REWRITE_TAC[] THEN
        DISCH_THEN(fun th -> REWRITE_TAC[MATCH_MP SIGN_LEMMA1 th])]]]);;

let STURM_NOVAR = prove
 (`!l f f' c.
        STURM f f' l /\
        a <= c /\ c <= b /\
        (!x. a <= x /\ x <= b /\ EX (\p. poly p x = &0) (CONS f (CONS f' l))
             ==> (x = c)) /\
        ~(poly f c = &0)
        ==> (varrec (poly f a) (MAP (\p. poly p a) (CONS f' l)) =
             varrec (poly f b) (MAP (\p. poly p b) (CONS f' l)))`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC STURM_NOVAR_LEMMA THEN
  MAP_EVERY EXISTS_TAC [`LENGTH(l:(real list) list)`; `c:real`] THEN
  ASM_REWRITE_TAC[]);;

let STURM_VAR_DLEMMA = prove
 (`!p a b.
        a <= b /\
        (!x. a <= x /\ x <= b ==> ~(poly p x = &0))
        ==> (!x. a <= x /\ x <= b ==> poly p x > &0) \/
            (!x. a <= x /\ x <= b ==> poly p x < &0)`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN REPEAT_TCL DISJ_CASES_THEN ASSUME_TAC
  (SPEC `poly p a` REAL_LT_NEGTOTAL) THENL
   [FIRST_ASSUM(UNDISCH_TAC o check is_forall o concl) THEN
    DISCH_THEN(MP_TAC o SPEC `a:real`) THEN ASM_REWRITE_TAC[REAL_LE_REFL];
    DISJ1_TAC THEN REPEAT STRIP_TAC THEN REWRITE_TAC[real_gt] THEN
    SUBGOAL_THEN `~(poly p x = &0) /\ ~(poly p x < &0)` MP_TAC THENL
     [CONJ_TAC THENL
       [FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[REAL_LE_REFL];
        DISCH_TAC THEN MP_TAC(SPEC `p:real list` POLY_IVT_NEG) THEN
        DISCH_THEN(MP_TAC o SPECL [`a:real`; `x:real`]) THEN
        ASM_REWRITE_TAC[real_gt; NOT_IMP; NOT_EXISTS_THM; REAL_LT_REFL] THEN
        GEN_REWRITE_TAC LAND_CONV [REAL_LT_LE] THEN
        ASM_REWRITE_TAC[] THEN
        ASM_CASES_TAC `a:real = x` THENL
         [ASM_MESON_TAC[REAL_LT_ANTISYM];
          ASM_REWRITE_TAC[] THEN X_GEN_TAC `y:real` THEN
          REWRITE_TAC[TAUT `~(a /\ b /\ c) <=> a /\ b ==> ~c`] THEN
          DISCH_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
          UNDISCH_TAC `a < y /\ y < x` THEN UNDISCH_TAC `x <= b` THEN
          REAL_ARITH_TAC]];
      REAL_ARITH_TAC];
    DISJ2_TAC THEN REPEAT STRIP_TAC THEN
    SUBGOAL_THEN `~(poly p x = &0) /\ ~(poly p x > &0)` MP_TAC THENL
     [CONJ_TAC THENL
       [FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[REAL_LE_REFL];
        DISCH_TAC THEN MP_TAC(SPEC `p:real list` POLY_IVT_POS) THEN
        DISCH_THEN(MP_TAC o SPECL [`a:real`; `x:real`]) THEN
        RULE_ASSUM_TAC(REWRITE_RULE[REAL_ARITH `&0 < -- x <=> x < &0`]) THEN
        ASM_REWRITE_TAC[real_gt; NOT_IMP; NOT_EXISTS_THM; REAL_LT_REFL] THEN
        GEN_REWRITE_TAC LAND_CONV [REAL_LT_LE] THEN
        ASM_REWRITE_TAC[] THEN
        ASM_CASES_TAC `a:real = x` THENL
         [ASM_MESON_TAC[REAL_LT_ANTISYM; real_gt];
          ASM_REWRITE_TAC[] THEN X_GEN_TAC `y:real` THEN
          REWRITE_TAC[TAUT `~(a /\ b /\ c) <=> a /\ b ==> ~c`] THEN
          DISCH_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
          UNDISCH_TAC `a < y /\ y < x` THEN UNDISCH_TAC `x <= b` THEN
          REAL_ARITH_TAC]];
      REAL_ARITH_TAC]]);;

let STURM_VAR_LEMMA = prove
 (`!l f f' c.
        rsquarefree f /\
        (f' = diff f) /\
        STURM f f' l /\
        a < c /\ c < b /\
        (!x. a <= x /\ x <= b /\ EX (\p. poly p x = &0) (CONS f (CONS f' l))
             ==> (x = c)) /\
        (poly f c = &0)
        ==> poly f a * poly f' a < &0 /\
            poly f b * poly f' b > &0`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `a <= b` ASSUME_TAC THENL
   [UNDISCH_TAC `a < c` THEN UNDISCH_TAC `c < b` THEN
    REAL_ARITH_TAC; ALL_TAC] THEN
  SUBGOAL_THEN `!x. a <= x /\ x <= b ==> ~(poly (diff f) x = &0)`
  ASSUME_TAC THENL
   [X_GEN_TAC `x:real` THEN STRIP_TAC THEN
    FIRST_ASSUM(MP_TAC o GEN_REWRITE_RULE I [RSQUAREFREE_ROOTS]) THEN
    DISCH_THEN(MP_TAC o SPEC `c:real`) THEN ASM_REWRITE_TAC[] THEN
    RULE_ASSUM_TAC(REWRITE_RULE[EX]) THEN ASM_MESON_TAC[]; ALL_TAC] THEN
  MP_TAC(SPECL [`diff f`; `a:real`; `b:real`] STURM_VAR_DLEMMA) THEN
  ASM_REWRITE_TAC[] THEN DISCH_THEN DISJ_CASES_TAC THENL
   [CONJ_TAC THENL
     [REWRITE_TAC[REAL_ARITH `p * q < &0 <=> &0 < --p * q`] THEN
      MP_TAC(SPECL [`f:real list`; `a:real`; `c:real`] POLY_MVT) THEN
      ASM_REWRITE_TAC[REAL_SUB_LZERO] THEN
      DISCH_THEN(X_CHOOSE_THEN `x:real` STRIP_ASSUME_TAC) THEN
      ASM_REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
      MATCH_MP_TAC REAL_LT_MUL THEN
      ASM_REWRITE_TAC[REAL_ARITH `&0 < c - a <=> a < c`] THEN
      MATCH_MP_TAC REAL_LT_MUL THEN
      REWRITE_TAC[REAL_ARITH `&0 < x <=> x > &0`] THEN
      CONJ_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
      ASM_REWRITE_TAC[REAL_LE_REFL] THEN
      UNDISCH_TAC `a < x` THEN UNDISCH_TAC `x < c` THEN
      UNDISCH_TAC `c < b` THEN REAL_ARITH_TAC;

      REWRITE_TAC[REAL_ARITH `p * q < &0 <=> &0 < --p * q`] THEN
      MP_TAC(SPECL [`f:real list`; `c:real`; `b:real`] POLY_MVT) THEN
      ASM_REWRITE_TAC[REAL_SUB_RZERO] THEN
      DISCH_THEN(X_CHOOSE_THEN `x:real` STRIP_ASSUME_TAC) THEN
      ASM_REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
      REWRITE_TAC[real_gt] THEN
      MATCH_MP_TAC REAL_LT_MUL THEN
      ASM_REWRITE_TAC[REAL_ARITH `&0 < c - a <=> a < c`] THEN
      MATCH_MP_TAC REAL_LT_MUL THEN
      REWRITE_TAC[REAL_ARITH `&0 < x <=> x > &0`] THEN
      CONJ_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
      ASM_REWRITE_TAC[REAL_LE_REFL] THEN
      UNDISCH_TAC `a < c` THEN UNDISCH_TAC `c < x` THEN
      UNDISCH_TAC `x < b` THEN REAL_ARITH_TAC];

    CONJ_TAC THENL
     [REWRITE_TAC[REAL_ARITH `p * q < &0 <=> &0 < --p * q`] THEN
      MP_TAC(SPECL [`f:real list`; `a:real`; `c:real`] POLY_MVT) THEN
      ASM_REWRITE_TAC[REAL_SUB_LZERO] THEN
      DISCH_THEN(X_CHOOSE_THEN `x:real` STRIP_ASSUME_TAC) THEN
      ASM_REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
      MATCH_MP_TAC REAL_LT_MUL THEN
      ASM_REWRITE_TAC[REAL_ARITH `&0 < c - a <=> a < c`] THEN
      ONCE_REWRITE_TAC[REAL_ARITH `&0 < x * y <=> &0 < --x * -- y`] THEN
      MATCH_MP_TAC REAL_LT_MUL THEN
      REWRITE_TAC[REAL_ARITH `&0 < --x <=> x < &0`] THEN
      CONJ_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
      ASM_REWRITE_TAC[REAL_LE_REFL] THEN
      UNDISCH_TAC `a < x` THEN UNDISCH_TAC `x < c` THEN
      UNDISCH_TAC `c < b` THEN REAL_ARITH_TAC;

      REWRITE_TAC[REAL_ARITH `p * q < &0 <=> &0 < --p * q`] THEN
      MP_TAC(SPECL [`f:real list`; `c:real`; `b:real`] POLY_MVT) THEN
      ASM_REWRITE_TAC[REAL_SUB_RZERO] THEN
      DISCH_THEN(X_CHOOSE_THEN `x:real` STRIP_ASSUME_TAC) THEN
      ASM_REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
      REWRITE_TAC[real_gt] THEN
      MATCH_MP_TAC REAL_LT_MUL THEN
      ASM_REWRITE_TAC[REAL_ARITH `&0 < c - a <=> a < c`] THEN
      ONCE_REWRITE_TAC[REAL_ARITH `&0 < x * y <=> &0 < --x * -- y`] THEN
      MATCH_MP_TAC REAL_LT_MUL THEN
      REWRITE_TAC[REAL_ARITH `&0 < --x <=> x < &0`] THEN
      CONJ_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
      ASM_REWRITE_TAC[REAL_LE_REFL] THEN
      UNDISCH_TAC `a < c` THEN UNDISCH_TAC `c < x` THEN
      UNDISCH_TAC `x < b` THEN REAL_ARITH_TAC]]);;

let STURM_VAR = prove
 (`!l f f' c.
        rsquarefree f /\
        (f' = diff f) /\
        STURM f f' l /\
        a < c /\ c < b /\
        (!x. a <= x /\ x <= b /\ EX (\p. poly p x = &0) (CONS f (CONS f' l))
             ==> (x = c)) /\
        (poly f c = &0)
        ==> (varrec (poly f a) (MAP (\p. poly p a) (CONS f' l)) =
             SUC(varrec (poly f b) (MAP (\p. poly p b) (CONS f' l))))`,
  LIST_INDUCT_TAC THENL
   [REPEAT GEN_TAC THEN
    DISCH_THEN(STRIP_ASSUME_TAC o MATCH_MP STURM_VAR_LEMMA) THEN
    ASM_REWRITE_TAC[MAP; varrec] THEN
    SUBGOAL_THEN `~(poly f b * poly f' b < &0) /\ ~(poly f' b = &0)`
    (fun th -> ASM_REWRITE_TAC[th]) THEN
    UNDISCH_TAC `poly f b * poly f' b > &0` THEN
    ASM_CASES_TAC `poly f' b = &0` THENL
     [ASM_REWRITE_TAC[real_gt; REAL_MUL_LZERO; REAL_MUL_RZERO; REAL_LT_REFL];
      ASM_REWRITE_TAC[] THEN REAL_ARITH_TAC];

    REPEAT GEN_TAC THEN DISCH_TAC THEN
    FIRST_ASSUM(STRIP_ASSUME_TAC o MATCH_MP STURM_VAR_LEMMA) THEN
    ONCE_REWRITE_TAC[MAP] THEN REWRITE_TAC[varrec] THEN
    SUBGOAL_THEN `poly f a * poly f' a < &0 /\
                  ~(poly f b * poly f' b < &0) /\
                  ~(poly f' a = &0) /\ ~(poly f' b = &0)`
    (fun th -> REWRITE_TAC[th]) THENL
     [UNDISCH_TAC `poly f b * poly f' b > &0` THEN
      UNDISCH_TAC `poly f a * poly f' a < &0` THEN
      POP_ASSUM_LIST(K ALL_TAC) THEN
      ASM_CASES_TAC `poly f' a = &0` THEN
      ASM_REWRITE_TAC
       [real_gt; REAL_MUL_LZERO; REAL_MUL_RZERO; REAL_LT_REFL] THEN
      ASM_CASES_TAC `poly f' b = &0` THEN
      ASM_REWRITE_TAC
       [real_gt; REAL_MUL_LZERO; REAL_MUL_RZERO; REAL_LT_REFL] THEN
      REAL_ARITH_TAC;
      AP_TERM_TAC THEN MATCH_MP_TAC STURM_NOVAR THEN
      EXISTS_TAC `c:real` THEN
      RULE_ASSUM_TAC(REWRITE_RULE[STURM]) THEN
      ASM_REWRITE_TAC[] THEN REPEAT CONJ_TAC THENL
       [MATCH_MP_TAC REAL_LT_IMP_LE THEN ASM_REWRITE_TAC[];
        MATCH_MP_TAC REAL_LT_IMP_LE THEN ASM_REWRITE_TAC[];
        REWRITE_TAC[EX] THEN REPEAT STRIP_TAC THEN
        FIRST_ASSUM(MATCH_MP_TAC o last o butlast o CONJUNCTS) THEN
        ASM_REWRITE_TAC[EX];
        MP_TAC(SPEC `f:real list` RSQUAREFREE_ROOTS) THEN
        ASM_REWRITE_TAC[] THEN
        DISCH_THEN(MP_TAC o SPEC `c:real`) THEN ASM_REWRITE_TAC[]]]]);;

(* ------------------------------------------------------------------------- *)
(* The main lemma.                                                           *)
(* ------------------------------------------------------------------------- *)

let STURM_COMPONENT = prove
 (`!l f a b c.
        rsquarefree f /\
        STURM f (diff f) l /\
        a <= c /\ c <= b /\
        ~(poly f a = &0) /\
        ~(poly f b = &0) /\
        (!x. a <= x /\ x <= b /\
             EX (\p. poly p x = &0) (CONS f (CONS (diff f) l))
             ==> (x = c))
        ==> (variation (MAP (\p. poly p a) (CONS f (CONS (diff f) l))) =
                if poly f c = &0 then
                    SUC(variation (MAP (\p. poly p b)
                                       (CONS f (CONS (diff f) l))))
                else variation (MAP (\p. poly p b)
                                    (CONS f (CONS (diff f) l))))`,
  REPEAT STRIP_TAC THEN REWRITE_TAC[variation] THEN
  ONCE_REWRITE_TAC[MAP] THEN
  REWRITE_TAC[varrec; REAL_MUL_LZERO; REAL_MUL_RZERO; REAL_LT_REFL] THEN
  ASM_CASES_TAC `poly f c = &0` THEN ASM_REWRITE_TAC[] THENL
   [MATCH_MP_TAC STURM_VAR; MATCH_MP_TAC STURM_NOVAR] THEN
  EXISTS_TAC `c:real` THEN ASM_REWRITE_TAC[] THEN
  ASM_REWRITE_TAC[REAL_LT_LE] THEN
  CONJ_TAC THEN DISCH_THEN SUBST_ALL_TAC THEN
  ASM_MESON_TAC[]);;

(* ------------------------------------------------------------------------- *)
(* Roots of a list of polynomials (maybe in interval) is finite.             *)
(* ------------------------------------------------------------------------- *)

let POLYS_ROOTS_FINITE_SET = prove
 (`!l. ALL (\p. ~(poly p = poly [])) l ==>
       FINITE { x | EX (\p. poly p x = &0) l }`,
  LIST_INDUCT_TAC THENL
   [SUBGOAL_THEN `{ x | EX (\p. poly p x = &0) [] } = {}`
    (fun th -> REWRITE_TAC[FINITE_RULES; th]) THEN
    REWRITE_TAC[EXTENSION; NOT_IN_EMPTY; IN_ELIM_THM; EX];
    SUBGOAL_THEN `{ x | EX (\p. poly p x = &0) (CONS h t) } =
                  { x | poly h x = &0 } UNION { x | EX (\p. poly p x = &0) t }`
    SUBST1_TAC THENL
     [REWRITE_TAC[EXTENSION; IN_UNION; IN_ELIM_THM; EX];
      REWRITE_TAC[ALL] THEN STRIP_TAC THEN
      MATCH_MP_TAC FINITE_UNION_IMP THEN CONJ_TAC THENL
       [MATCH_MP_TAC POLY_ROOTS_FINITE_SET THEN ASM_REWRITE_TAC[];
        FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[]]]]);;

let POLYS_INTERVAL_ROOTS_FINITE_SET = prove
 (`!l a b. ALL (\p. ~(poly p = poly [])) l ==>
           FINITE { x | a <= x /\ x <= b /\ EX (\p. poly p x = &0) l }`,
  REPEAT GEN_TAC THEN DISCH_TAC THEN
  MATCH_MP_TAC FINITE_SUBSET THEN
  EXISTS_TAC `{ x | EX (\p. poly p x = &0) l }` THEN CONJ_TAC THENL
   [MATCH_MP_TAC POLYS_ROOTS_FINITE_SET THEN ASM_REWRITE_TAC[];
    REWRITE_TAC[SUBSET; IN_ELIM_THM] THEN MESON_TAC[]]);;

(* ------------------------------------------------------------------------- *)
(* Proof that we can lay out a finite set in a linear sequence.              *)
(* ------------------------------------------------------------------------- *)

let FINITE_LEAST = prove
 (`!s. FINITE s ==> (s = {}) \/ ?a. a IN s /\ !x. x IN s ==> a <= x`,
  MATCH_MP_TAC FINITE_INDUCT THEN REWRITE_TAC[NOT_INSERT_EMPTY] THEN
  REPEAT GEN_TAC THEN DISCH_THEN DISJ_CASES_TAC THENL
   [EXISTS_TAC `x:real` THEN
    ASM_REWRITE_TAC[IN_INSERT; NOT_IN_EMPTY] THEN REAL_ARITH_TAC;
    POP_ASSUM(X_CHOOSE_THEN `a:real` STRIP_ASSUME_TAC) THEN
    DISJ_CASES_TAC(SPECL [`a:real`; `x:real`] REAL_LE_TOTAL) THENL
     [EXISTS_TAC `a:real` THEN ASM_REWRITE_TAC[IN_INSERT] THEN ASM_MESON_TAC[];
      EXISTS_TAC `x:real` THEN ASM_REWRITE_TAC[IN_INSERT] THEN
      ASM_MESON_TAC[REAL_LE_TRANS; REAL_LE_REFL]]]);;

let FINITE_LEAST_DELETE = prove
 (`!s. s HAS_SIZE (SUC n)
       ==> ?a. a IN s /\ (s DELETE a) HAS_SIZE n /\
               !x. x IN (s DELETE a) ==> a < x`,
  GEN_TAC THEN DISCH_THEN(fun th -> MP_TAC th THEN MP_TAC th) THEN
  DISCH_THEN(STRIP_ASSUME_TAC o REWRITE_RULE[HAS_SIZE_SUC]) THEN
  REWRITE_TAC[HAS_SIZE] THEN
  DISCH_THEN(MP_TAC o MATCH_MP FINITE_LEAST o CONJUNCT1) THEN
  ASM_REWRITE_TAC[] THEN
  DISCH_THEN(X_CHOOSE_THEN `a:real` STRIP_ASSUME_TAC) THEN
  EXISTS_TAC `a:real` THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[GSYM HAS_SIZE; IN_DELETE] THEN
  REWRITE_TAC[REAL_LT_LE] THEN ASM_MESON_TAC[]);;

let HAS_SIZE_ORDER = prove
 (`!n s. s HAS_SIZE n ==>
         ?i. (!x. x IN s <=> ?k. k < n /\ (x = i k)) /\
                                 (!k. i k < i (SUC k))`,
  INDUCT_TAC THENL
   [GEN_TAC THEN REWRITE_TAC[HAS_SIZE_0] THEN DISCH_THEN SUBST1_TAC THEN
    REWRITE_TAC[LT; NOT_IN_EMPTY] THEN EXISTS_TAC `&` THEN
    REWRITE_TAC[GSYM REAL_OF_NUM_SUC] THEN REAL_ARITH_TAC;
    GEN_TAC THEN DISCH_THEN(MP_TAC o MATCH_MP FINITE_LEAST_DELETE) THEN
    DISCH_THEN(X_CHOOSE_THEN `a:real` (CONJUNCTS_THEN2 ASSUME_TAC MP_TAC)) THEN
    DISCH_THEN(CONJUNCTS_THEN2 MP_TAC ASSUME_TAC) THEN
    DISCH_THEN(fun th -> ASSUME_TAC th THEN MP_TAC th) THEN
    DISCH_THEN(ANTE_RES_THEN MP_TAC) THEN
    DISCH_THEN(X_CHOOSE_THEN `i:num->real` STRIP_ASSUME_TAC) THEN
    ASM_CASES_TAC `n = 0` THENL
     [EXISTS_TAC `\k. a + &k` THEN ASM_REWRITE_TAC[] THEN
      REWRITE_TAC[LT_SUC_LE; LE] THEN CONJ_TAC THENL
       [SUBGOAL_THEN `s = {a:real}` SUBST1_TAC THENL
         [UNDISCH_TAC `s DELETE a:real HAS_SIZE n` THEN
          ASM_REWRITE_TAC[HAS_SIZE_0] THEN ASM SET_TAC[];
          REWRITE_TAC[IN_INSERT; NOT_IN_EMPTY] THEN
          MESON_TAC[REAL_ADD_RID]];
          REWRITE_TAC[GSYM REAL_OF_NUM_SUC] THEN REAL_ARITH_TAC];
      EXISTS_TAC `\k. if k = 0 then a:real else i(PRE k)` THEN
      REWRITE_TAC[] THEN CONJ_TAC THENL
       [X_GEN_TAC `x:real` THEN ASM_CASES_TAC `x:real = a` THENL
         [UNDISCH_TAC `x:real = a` THEN DISCH_THEN SUBST_ALL_TAC THEN
          ASM_REWRITE_TAC[] THEN EXISTS_TAC `0` THEN
          ASM_REWRITE_TAC[LT_SUC_LE; LE_0];
          SUBGOAL_THEN `x IN s <=> x IN (s DELETE a:real)` SUBST1_TAC THENL
           [REWRITE_TAC[IN_DELETE] THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
          ASM_REWRITE_TAC[] THEN EQ_TAC THEN
          DISCH_THEN(X_CHOOSE_THEN `k:num` STRIP_ASSUME_TAC) THENL
           [EXISTS_TAC `SUC k` THEN ASM_REWRITE_TAC[LT_SUC; NOT_SUC; PRE];
            EXISTS_TAC `PRE k` THEN
            UNDISCH_TAC `x = (if k = 0 then a:real else i (PRE k))` THEN
            UNDISCH_TAC `k < SUC n` THEN
            SPEC_TAC(`k:num`,`k:num`) THEN
            INDUCT_TAC THEN ASM_REWRITE_TAC[LT_SUC; PRE; NOT_SUC] THEN
            REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[]]];
        INDUCT_TAC THEN ASM_REWRITE_TAC[NOT_SUC; PRE] THEN
        FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[] THEN
        EXISTS_TAC `0` THEN ASM_REWRITE_TAC[] THEN
        UNDISCH_TAC `~(n = 0)` THEN ARITH_TAC]]]);;

let FINITE_ORDER = prove
 (`!s. FINITE s ==>
         ?i N. (!x. x IN s <=> ?k. k < N /\ (x = i k)) /\
                                   (!k. i k < i (SUC k))`,
  REPEAT STRIP_TAC THEN ONCE_REWRITE_TAC[SWAP_EXISTS_THM] THEN
  EXISTS_TAC `CARD(s:real->bool)` THEN MATCH_MP_TAC HAS_SIZE_ORDER THEN
  ASM_REWRITE_TAC[HAS_SIZE]);;

(* ------------------------------------------------------------------------- *)
(* We can enumerate the roots in order.                                      *)
(* ------------------------------------------------------------------------- *)

let POLYS_ENUM_ROOTS = prove
 (`!l. ALL (\p. ~(poly p = poly [])) l
       ==> ?i N. (!k. i k < i (SUC k)) /\
                 (!x. a <= x /\ x <= b /\ EX (\p. poly p x = &0) l <=>
                      ?n:num. n < N /\ (x = i n))`,
  GEN_TAC THEN
  DISCH_THEN (MP_TAC o MATCH_MP POLYS_INTERVAL_ROOTS_FINITE_SET) THEN
  DISCH_THEN(MP_TAC o SPECL [`a:real`; `b:real`]) THEN
  DISCH_THEN(MP_TAC o MATCH_MP FINITE_ORDER) THEN
  REWRITE_TAC[IN_ELIM_THM; CONJ_SYM]);;

(* ------------------------------------------------------------------------- *)
(* Hence we can get separating intervals for the various roots.              *)
(* ------------------------------------------------------------------------- *)

let POLYS_INTERVAL_SEPARATION = prove
 (`!f l a b.
       a <= b /\
       ALL (\p. ~(poly p = poly [])) l /\
       ~(poly f a = &0) /\
       ~(poly f b = &0)
       ==> ?i N. (i 0 = a) /\ (i N = b) /\
                 (!k. i(k) <= i(SUC k)) /\
                 (!k. k <= N ==> ~(poly f (i k) = &0)) /\
                 (!k. ?c. i(k) <= c /\ c <= i(SUC k) /\
                          !x. i(k) <= x /\ x <= i(SUC k) /\
                              EX (\p. poly p x = &0) (CONS f l)
                              ==> (x = c))`,
  let lemma0 = prove
   (`(!x y. x * inv(&2) <= y <=> x <= &2 * y) /\
     (!x y. x <= y * inv(&2) <=> &2 * x <= y)`,
    REPEAT STRIP_TAC THEN
    GEN_REWRITE_TAC LAND_CONV [ARITH_RULE `a <= b <=> a + a <= b + b`] THEN
    REWRITE_TAC[GSYM REAL_ADD_LDISTRIB] THEN
    CONV_TAC(ONCE_DEPTH_CONV REAL_RAT_REDUCE_CONV) THEN
    REAL_ARITH_TAC) in
  let lemma1 = prove
   (`a <= x1 /\ x1 < x2 ==> a <= (x1 + x2) / &2`,
    REWRITE_TAC[real_div; lemma0] THEN REAL_ARITH_TAC) in
  let lemma2 = prove
   (`x0 < x1 /\ x1 < x2 ==> (x0 + x1) / &2 <= (x1 + x2) / &2`,
    REWRITE_TAC[real_div; lemma0; REAL_MUL_ASSOC] THEN
    REAL_ARITH_TAC) in
  let lemma3 = prove
   (`x1 < x2 /\ x2 <= b ==> (x1 + x2) / &2 <= b`,
    REWRITE_TAC[real_div; lemma0] THEN REAL_ARITH_TAC) in
  let lemma8a = prove
   (`(!k. i k < i(SUC k)) ==> !m n. m < n ==> i m < i n`,
    STRIP_TAC THEN GEN_TAC THEN REWRITE_TAC[LT_EXISTS] THEN
    REWRITE_TAC[LEFT_IMP_EXISTS_THM] THEN
    ONCE_REWRITE_TAC[SWAP_FORALL_THM] THEN
    INDUCT_TAC THEN REWRITE_TAC[ADD_CLAUSES] THEN
    GEN_TAC THEN DISCH_THEN SUBST1_TAC THEN ASM_REWRITE_TAC[] THEN
    MATCH_MP_TAC REAL_LT_TRANS THEN
    EXISTS_TAC `i(SUC(m + d)):real` THEN
    ASM_REWRITE_TAC[] THEN
    FIRST_ASSUM MATCH_MP_TAC THEN
    REWRITE_TAC[ADD_CLAUSES]) in
  let lemma8b = prove
   (`(!k. i k < i(SUC k)) ==> !m n. i m < i n <=> m < n`,
    DISCH_THEN(MP_TAC o MATCH_MP lemma8a) THEN
    MESON_TAC[REAL_LT_REFL; REAL_LT_ANTISYM; LT_CASES]) in
  let lemma8 = prove
   (`(!k. i k < i(SUC k)) ==> !m n. (i m <= i n) <=> m <= n`,
    DISCH_THEN(MP_TAC o MATCH_MP lemma8b) THEN
    REWRITE_TAC[GSYM NOT_LE; GSYM REAL_NOT_LE] THEN MESON_TAC[]) in
  let lemma5 = prove
   (`(!k. i k < i(SUC k)) ==> !k n. i k <= (i n + i(SUC n)) / &2
                                    ==> k <= n`,
    DISCH_TAC THEN
    REWRITE_TAC[GSYM LT_SUC_LE] THEN
    FIRST_ASSUM(fun th -> REWRITE_TAC[GSYM(MATCH_MP lemma8b th)]) THEN
    REPEAT GEN_TAC THEN REWRITE_TAC[real_div; lemma0] THEN
    POP_ASSUM(MP_TAC o SPEC `n:num`) THEN REAL_ARITH_TAC) in
  let lemma6 = prove
   (`(!k. i k < i(SUC k)) ==> !k n. (i k + i (SUC k)) / &2 <= i n
                                    ==> SUC k <= n`,
    DISCH_TAC THEN REWRITE_TAC[LE_SUC_LT] THEN
    FIRST_ASSUM(fun th -> REWRITE_TAC[GSYM(MATCH_MP lemma8b th)]) THEN
    REPEAT GEN_TAC THEN REWRITE_TAC[real_div; lemma0] THEN
    POP_ASSUM(MP_TAC o SPEC `k:num`) THEN REAL_ARITH_TAC) in
  let lemma7 = prove
   (`(!k. i k < i(SUC k)) ==> !k n. (i n + i(SUC n)) / &2 <= i k /\
                                    i k <= (i(SUC n) + i(SUC(SUC n))) / &2
                                    ==> (k = SUC n)`,
    REPEAT STRIP_TAC THEN ONCE_REWRITE_TAC[GSYM LE_ANTISYM] THEN
    CONJ_TAC THENL
     [FIRST_ASSUM(MATCH_MP_TAC o MATCH_MP lemma5);
      FIRST_ASSUM(MATCH_MP_TAC o MATCH_MP lemma6)] THEN
    ASM_REWRITE_TAC[]) in
  let lemma4 = prove
   (`(!k. i k < i(SUC k)) ==> !k n. ~((i k + i (SUC k)) / &2 = i n)`,
    DISCH_TAC THEN REPEAT GEN_TAC THEN
    ONCE_REWRITE_TAC[GSYM REAL_LE_ANTISYM] THEN STRIP_TAC THEN
    SUBGOAL_THEN `~(SUC k <= k)` MP_TAC THENL
     [ARITH_TAC; REWRITE_TAC[]] THEN
    MATCH_MP_TAC LE_TRANS THEN EXISTS_TAC `n:num` THEN
    CONJ_TAC THENL
     [FIRST_ASSUM(MATCH_MP_TAC o MATCH_MP lemma6);
      FIRST_ASSUM(MATCH_MP_TAC o MATCH_MP lemma5)] THEN
    ASM_REWRITE_TAC[]) in
  REPEAT STRIP_TAC THEN
  SUBGOAL_THEN `ALL (\p. ~(poly p = poly [])) (CONS f l)` MP_TAC THENL
   [ASM_REWRITE_TAC[ALL] THEN UNDISCH_TAC `~(poly f a = &0)` THEN
    CONV_TAC CONTRAPOS_CONV THEN REWRITE_TAC[] THEN
    DISCH_THEN SUBST1_TAC THEN REWRITE_TAC[poly]; ALL_TAC] THEN
  DISCH_THEN(MP_TAC o MATCH_MP POLYS_ENUM_ROOTS) THEN
  DISCH_THEN(X_CHOOSE_THEN `i:num->real` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `N:num` STRIP_ASSUME_TAC) THEN
  EXISTS_TAC `\n. if n = 0 then a:real else if n < N then
                                      (i(PRE n) + i(n)) / &2 else b` THEN
  EXISTS_TAC `SUC N` THEN ASM_REWRITE_TAC[NOT_SUC] THEN
  REPEAT CONJ_TAC THENL
   [REWRITE_TAC[ARITH_RULE `~(SUC N < N)`];
    GEN_TAC THEN REWRITE_TAC[PRE; LE_SUC_LT] THEN
    ASM_CASES_TAC `k = 0` THEN ASM_CASES_TAC `k < N:num` THEN
    ASM_CASES_TAC `SUC k < N` THEN ASM_REWRITE_TAC[REAL_LE_REFL] THENL
     [MATCH_MP_TAC lemma1 THEN ASM_REWRITE_TAC[] THEN
      FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) 0`) THEN
      CONV_TAC CONTRAPOS_CONV THEN DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
      EXISTS_TAC `k:num` THEN ASM_REWRITE_TAC[];
      UNDISCH_TAC `SUC k < N` THEN UNDISCH_TAC `~(k < N:num)` THEN
      ARITH_TAC;
      MATCH_MP_TAC lemma2 THEN ASM_REWRITE_TAC[] THEN
      UNDISCH_TAC `~(k = 0)` THEN SPEC_TAC(`k:num`,`k:num`) THEN
      INDUCT_TAC THEN ASM_REWRITE_TAC[PRE];
      MATCH_MP_TAC lemma3 THEN CONJ_TAC THENL
       [UNDISCH_TAC `~(k = 0)` THEN SPEC_TAC(`k:num`,`k:num`) THEN
        INDUCT_TAC THEN ASM_REWRITE_TAC[PRE];
        FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) k`) THEN
        CONV_TAC CONTRAPOS_CONV THEN DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
        EXISTS_TAC `k:num` THEN ASM_REWRITE_TAC[]];
      UNDISCH_TAC `SUC k < N` THEN UNDISCH_TAC `~(k < N:num)` THEN
      ARITH_TAC];
    GEN_TAC THEN ASM_CASES_TAC `k = 0` THEN ASM_REWRITE_TAC[] THEN
    DISCH_TAC THEN COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
    DISCH_TAC THEN
    FIRST_ASSUM(MP_TAC o SPEC `(i (PRE k) + i k) / &2`) THEN
    REWRITE_TAC[EX] THEN ASM_REWRITE_TAC[] THEN
    SUBGOAL_THEN `a <= (i (PRE k) + i k) / &2 /\
                  (i (PRE k) + i k) / &2 <= b`
    (fun th -> REWRITE_TAC[th]) THENL
     [CONJ_TAC THENL
       [MATCH_MP_TAC lemma1 THEN UNDISCH_TAC `~(k = 0)` THEN
        UNDISCH_TAC `k < N:num` THEN UNDISCH_TAC `k <= SUC N` THEN
        UNDISCH_TAC `poly f ((i (PRE k) + i k) / &2) = &0` THEN
        DISCH_THEN(K ALL_TAC) THEN DISCH_THEN(K ALL_TAC) THEN
        SPEC_TAC(`k:num`,`k:num`) THEN
        INDUCT_TAC THEN ASM_REWRITE_TAC[PRE; NOT_SUC] THEN
        DISCH_TAC THEN FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) k`) THEN
        CONV_TAC CONTRAPOS_CONV THEN
        DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
        EXISTS_TAC `k:num` THEN REWRITE_TAC[] THEN
        UNDISCH_TAC `SUC k < N` THEN ARITH_TAC;
        MATCH_MP_TAC lemma3 THEN UNDISCH_TAC `~(k = 0)` THEN
        UNDISCH_TAC `k < N:num` THEN UNDISCH_TAC `k <= SUC N` THEN
        UNDISCH_TAC `poly f ((i (PRE k) + i k) / &2) = &0` THEN
        DISCH_THEN(K ALL_TAC) THEN DISCH_THEN(K ALL_TAC) THEN
        SPEC_TAC(`k:num`,`k:num`) THEN
        INDUCT_TAC THEN ASM_REWRITE_TAC[PRE; NOT_SUC] THEN
        DISCH_TAC THEN FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) (SUC k)`) THEN
        CONV_TAC CONTRAPOS_CONV THEN
        DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
        EXISTS_TAC `SUC k` THEN REWRITE_TAC[] THEN ASM_REWRITE_TAC[]];
      DISCH_THEN(CHOOSE_THEN(MP_TAC o CONJUNCT2)) THEN
      REWRITE_TAC[] THEN
      UNDISCH_TAC `~(k = 0)` THEN SPEC_TAC(`k:num`,`k:num`) THEN
      INDUCT_TAC THEN ASM_REWRITE_TAC[PRE; NOT_SUC] THEN
      ASM_SIMP_TAC[lemma4]];
    GEN_TAC THEN REWRITE_TAC[PRE] THEN
    ASM_CASES_TAC `k = 0` THEN ASM_REWRITE_TAC[] THENL
     [ASM_CASES_TAC `N = 0` THEN ASM_REWRITE_TAC[LT] THENL
       [EXISTS_TAC `a:real` THEN ASM_REWRITE_TAC[REAL_LE_REFL]; ALL_TAC] THEN
      ASM_CASES_TAC `N = SUC 0` THEN ASM_REWRITE_TAC[LT_REFL] THENL
       [EXISTS_TAC `(i:num->real) 0` THEN REWRITE_TAC[LT] THEN
        REPEAT CONJ_TAC THENL
         [FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) 0`) THEN
          CONV_TAC CONTRAPOS_CONV THEN
          DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
          EXISTS_TAC `k:num` THEN ASM_REWRITE_TAC[LT];
          FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) 0`) THEN
          CONV_TAC CONTRAPOS_CONV THEN
          DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
          EXISTS_TAC `k:num` THEN ASM_REWRITE_TAC[LT];
          MESON_TAC[]];
        ALL_TAC] THEN
      SUBGOAL_THEN `SUC 0 < N` ASSUME_TAC THENL
       [REWRITE_TAC[GSYM NOT_LE] THEN REWRITE_TAC[LE] THEN
        ASM_REWRITE_TAC[]; ALL_TAC] THEN
      ASM_REWRITE_TAC[] THEN EXISTS_TAC `(i:num->real) 0` THEN
      SUBGOAL_THEN `a <= i 0 /\
                    i 0 <= (i 0 + i (SUC 0)) / &2 /\
                    (i 0 + i (SUC 0)) / &2 <= b`
      STRIP_ASSUME_TAC THENL
       [REPEAT CONJ_TAC THENL
         [FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) 0`) THEN
          CONV_TAC CONTRAPOS_CONV THEN
          DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
          EXISTS_TAC `k:num` THEN ASM_REWRITE_TAC[LT] THEN
          UNDISCH_TAC `~(N = 0)` THEN ARITH_TAC;
          MATCH_MP_TAC lemma1 THEN ASM_REWRITE_TAC[REAL_LE_REFL];
          MATCH_MP_TAC lemma3 THEN ASM_REWRITE_TAC[] THEN
          FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) (SUC 0)`) THEN
          CONV_TAC CONTRAPOS_CONV THEN
          DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
          EXISTS_TAC `SUC 0` THEN ASM_REWRITE_TAC[]];
        ASM_REWRITE_TAC[]] THEN
      X_GEN_TAC `x:real` THEN STRIP_TAC THEN
      SUBGOAL_THEN `a <= x /\ x <= b /\ EX (\p. poly p x = &0) (CONS f l)`
      MP_TAC THENL
       [REPEAT CONJ_TAC THEN ASM_REWRITE_TAC[] THEN
        MATCH_MP_TAC REAL_LE_TRANS THEN
        EXISTS_TAC `(i 0 + i (SUC 0)) / &2` THEN
        ASM_REWRITE_TAC[];
        ALL_TAC] THEN
      FIRST_ASSUM(fun th -> GEN_REWRITE_TAC LAND_CONV [th]) THEN
      DISCH_THEN(X_CHOOSE_THEN `n:num` STRIP_ASSUME_TAC) THEN
      ASM_REWRITE_TAC[] THEN AP_TERM_TAC THEN
      UNDISCH_TAC `x <= (i 0 + i (SUC 0)) / &2` THEN
      ASM_REWRITE_TAC[] THEN
      FIRST_ASSUM(ASSUME_TAC o MATCH_MP lemma5) THEN
      DISCH_THEN(ANTE_RES_THEN MP_TAC) THEN REWRITE_TAC[LE]; ALL_TAC] THEN
    ASM_CASES_TAC `SUC k < N` THENL
     [SUBGOAL_THEN `k < N:num` ASSUME_TAC THENL
       [UNDISCH_TAC `SUC k < N` THEN ARITH_TAC; ALL_TAC] THEN
      ASM_REWRITE_TAC[] THEN
      EXISTS_TAC `(i:num->real) k` THEN REPEAT CONJ_TAC THENL
       [MATCH_MP_TAC lemma3 THEN UNDISCH_TAC `~(k = 0)` THEN
        SPEC_TAC(`k:num`,`k:num`) THEN INDUCT_TAC THEN
        ASM_REWRITE_TAC[REAL_LE_REFL; PRE];
        MATCH_MP_TAC lemma1 THEN ASM_REWRITE_TAC[REAL_LE_REFL];
        ALL_TAC] THEN
      X_GEN_TAC `x:real` THEN STRIP_TAC THEN
      SUBGOAL_THEN `a <= x /\ x <= b /\ EX (\p. poly p x = &0) (CONS f l)`
      MP_TAC THENL
       [REPEAT CONJ_TAC THEN ASM_REWRITE_TAC[] THENL
         [MATCH_MP_TAC REAL_LE_TRANS THEN
          EXISTS_TAC `(i (PRE k) + i k) / &2` THEN
          ASM_REWRITE_TAC[] THEN
          MATCH_MP_TAC lemma1 THEN CONJ_TAC THENL
           [FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) (PRE k)`) THEN
            CONV_TAC CONTRAPOS_CONV THEN
            DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
            EXISTS_TAC `PRE k` THEN ASM_REWRITE_TAC[] THEN
            UNDISCH_TAC `k < N:num` THEN ARITH_TAC;
            UNDISCH_TAC `~(k = 0)` THEN
            SPEC_TAC(`k:num`,`k:num`) THEN INDUCT_TAC THEN
            ASM_REWRITE_TAC[REAL_LE_REFL; PRE]];
         MATCH_MP_TAC REAL_LE_TRANS THEN
         EXISTS_TAC `(i k + i(SUC k)) / &2` THEN
         ASM_REWRITE_TAC[] THEN
         MATCH_MP_TAC lemma3 THEN ASM_REWRITE_TAC[] THEN
         FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) (SUC k)`) THEN
         CONV_TAC CONTRAPOS_CONV THEN
         DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
         EXISTS_TAC `SUC k` THEN ASM_REWRITE_TAC[]]; ALL_TAC] THEN
      FIRST_ASSUM(fun th -> GEN_REWRITE_TAC LAND_CONV [th]) THEN
      DISCH_THEN(X_CHOOSE_THEN `n:num` STRIP_ASSUME_TAC) THEN
      ASM_REWRITE_TAC[] THEN AP_TERM_TAC THEN
      SUBGOAL_THEN `(i (PRE k) + i k) / &2 <= i n /\
                    i n <= (i k + i (SUC k)) / &2`
      MP_TAC THENL
       [FIRST_ASSUM(UNDISCH_TAC o check is_eq o concl) THEN
        DISCH_THEN SUBST_ALL_TAC THEN ASM_REWRITE_TAC[]; ALL_TAC] THEN
      UNDISCH_TAC `~(k = 0)` THEN  SPEC_TAC(`k:num`,`k:num`) THEN
      INDUCT_TAC THEN REWRITE_TAC[NOT_SUC; PRE] THEN
      FIRST_ASSUM(MATCH_ACCEPT_TAC o MATCH_MP lemma7); ALL_TAC] THEN
    ASM_CASES_TAC `k < N:num` THEN ASM_REWRITE_TAC[] THENL
     [ALL_TAC;
      EXISTS_TAC `b:real` THEN REWRITE_TAC[REAL_LE_REFL] THEN
      REWRITE_TAC[CONJ_ASSOC; REAL_LE_ANTISYM] THEN
      REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[]] THEN
    EXISTS_TAC `(i:num->real) k` THEN REPEAT CONJ_TAC THENL
     [MATCH_MP_TAC lemma3 THEN REWRITE_TAC[REAL_LE_REFL] THEN
      UNDISCH_TAC `~(k = 0)` THEN  SPEC_TAC(`k:num`,`k:num`) THEN
      INDUCT_TAC THEN REWRITE_TAC[NOT_SUC; PRE] THEN ASM_REWRITE_TAC[];
      FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) k`) THEN
      CONV_TAC CONTRAPOS_CONV THEN
      DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
      EXISTS_TAC `k:num` THEN ASM_REWRITE_TAC[];
      ALL_TAC] THEN
    X_GEN_TAC `x:real` THEN STRIP_TAC THEN
    SUBGOAL_THEN `a <= x /\ x <= b /\ EX (\p. poly p x = &0) (CONS f l)`
    MP_TAC THENL
     [REPEAT CONJ_TAC THEN ASM_REWRITE_TAC[] THEN
      MATCH_MP_TAC REAL_LE_TRANS THEN
      EXISTS_TAC `(i (PRE k) + i k) / &2` THEN
      ASM_REWRITE_TAC[] THEN
      MATCH_MP_TAC lemma1 THEN CONJ_TAC THENL
       [FIRST_ASSUM(MP_TAC o SPEC `(i:num->real) (PRE k)`) THEN
        CONV_TAC CONTRAPOS_CONV THEN
        DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
        EXISTS_TAC `PRE k` THEN ASM_REWRITE_TAC[] THEN
        UNDISCH_TAC `k < N:num` THEN ARITH_TAC;
        UNDISCH_TAC `~(k = 0)` THEN
        SPEC_TAC(`k:num`,`k:num`) THEN INDUCT_TAC THEN
        ASM_REWRITE_TAC[REAL_LE_REFL; PRE]]; ALL_TAC] THEN
    FIRST_ASSUM(fun th -> GEN_REWRITE_TAC LAND_CONV [th]) THEN
    DISCH_THEN(X_CHOOSE_THEN `n:num` STRIP_ASSUME_TAC) THEN
    ASM_REWRITE_TAC[] THEN AP_TERM_TAC THEN
    FIRST_ASSUM(MP_TAC o SPECL [`n:num`; `PRE k`] o MATCH_MP lemma7) THEN
    FIRST_ASSUM(UNDISCH_TAC o check is_eq o concl) THEN
    DISCH_THEN SUBST_ALL_TAC THEN
    UNDISCH_TAC `(i (PRE k) + i k) / &2 <= i n` THEN
    SUBGOAL_THEN `i n <= (i k + i(SUC k)) / &2` MP_TAC THENL
     [MATCH_MP_TAC lemma1 THEN ASM_REWRITE_TAC[] THEN
      FIRST_ASSUM(fun th -> REWRITE_TAC[MATCH_MP lemma8 th]) THEN
      UNDISCH_TAC `n < N:num` THEN
      UNDISCH_TAC `~(SUC k < N)` THEN ARITH_TAC; ALL_TAC] THEN
    UNDISCH_TAC `~(k = 0)` THEN  SPEC_TAC(`k:num`,`k:num`) THEN
    INDUCT_TAC THEN REWRITE_TAC[PRE; NOT_SUC] THEN
    MESON_TAC[]]);;

(* ------------------------------------------------------------------------- *)
(* Basic lemma.                                                              *)
(* ------------------------------------------------------------------------- *)

let STURM_LEMMA = prove
 (`!i n.
        rsquarefree f /\
        STURM f (diff f) l /\
        (!k. i k <= i (SUC k)) /\
        (!k. k <= n ==> ~(poly f (i k) = &0)) /\
        (!k. ?c. i k <= c /\
                 c <= i (SUC k) /\
                 (!x. i k <= x /\
                      x <= i (SUC k) /\
                      EX (\p. poly p x = &0) (CONS f (CONS (diff f) l))
                      ==> (x = c)))
        ==> FINITE { x | i 0 <= x /\ x <= i n /\ (poly f x = &0) } /\
            (variation (MAP (\p. poly p (i n))
                            (CONS f (CONS (diff f) l))) +
             CARD { x | i 0 <= x /\ x <= i n /\ (poly f x = &0) } =
             variation (MAP (\p. poly p (i 0))
                            (CONS f (CONS (diff f) l))))`,
  let lemma = prove
   (`(s INTER t = EMPTY) ==> FINITE s /\ FINITE t ==>
        (CARD (s UNION t) = CARD s + CARD t)`,
    MESON_TAC[CARD_UNION]) in
  GEN_TAC THEN INDUCT_TAC THENL
   [STRIP_TAC THEN
    SUBGOAL_THEN `{x | i 0 <= x /\ x <= i 0 /\ (poly f x = &0)} = {}`
    SUBST1_TAC THENL
     [REWRITE_TAC[EXTENSION; IN_ELIM_THM; NOT_IN_EMPTY] THEN
      REWRITE_TAC[CONJ_ASSOC; REAL_LE_ANTISYM] THEN
      UNDISCH_TAC `!k. k <= 0 ==> ~(poly f (i k) = &0)` THEN
      DISCH_THEN(MP_TAC o SPEC `0`) THEN REWRITE_TAC[LE_REFL] THEN MESON_TAC[];
      REWRITE_TAC[FINITE_RULES; CARD_CLAUSES; ADD_CLAUSES]];
    STRIP_TAC THEN
    SUBGOAL_THEN `({x | i 0 <= x /\ x <= i (SUC n) /\ (poly f x = &0)} =
                   {x | i 0 <= x /\ x <= i n /\ (poly f x = &0)} UNION
                   {x | i n <= x /\ x <= i (SUC n) /\ (poly f x = &0)}) /\
                  ({x | i 0 <= x /\ x <= i n /\ (poly f x = &0)} INTER
                   {x | i n <= x /\ x <= i (SUC n) /\ (poly f x = &0)} =
                   EMPTY)`
    STRIP_ASSUME_TAC THENL
     [REWRITE_TAC[EXTENSION; IN_INTER; IN_UNION; IN_ELIM_THM] THEN
      CONJ_TAC THENL
       [GEN_TAC THEN REWRITE_TAC[CONJ_ASSOC] THEN
        REWRITE_TAC[TAUT `a /\ c \/ b /\ c <=> (a \/ b) /\ c`] THEN
        MATCH_MP_TAC(TAUT `(c ==> (a <=> b)) ==> (a /\ c <=> b /\ c)`) THEN
        DISCH_TAC THEN
        SUBGOAL_THEN `i 0 <= i n /\ i n <= i(SUC n)` MP_TAC THENL
         [ALL_TAC; REAL_ARITH_TAC] THEN ASM_REWRITE_TAC[] THEN
        SPEC_TAC(`n:num`,`n:num`) THEN UNDISCH_TAC `!k. i k <= i (SUC k)` THEN
        POP_ASSUM_LIST(K ALL_TAC) THEN DISCH_TAC THEN INDUCT_TAC THEN
        REWRITE_TAC[REAL_LE_REFL] THEN MATCH_MP_TAC REAL_LE_TRANS THEN
        EXISTS_TAC `(i:num->real) n` THEN ASM_REWRITE_TAC[];
        GEN_TAC THEN REWRITE_TAC[NOT_IN_EMPTY] THEN STRIP_TAC THEN
        UNDISCH_TAC `!k. k <= SUC n ==> ~(poly f (i k) = &0)` THEN
        DISCH_THEN(MP_TAC o C MATCH_MP (ARITH_RULE `n <= SUC n`)) THEN
        REWRITE_TAC[] THEN SUBGOAL_THEN `(i:num->real) n = x`
          (fun th -> ASM_REWRITE_TAC[th]) THEN
        ONCE_REWRITE_TAC[GSYM REAL_LE_ANTISYM] THEN
        ASM_REWRITE_TAC[]]; ALL_TAC] THEN
    FIRST_ASSUM(UNDISCH_TAC o check is_imp o concl) THEN
    ASM_REWRITE_TAC[] THEN
    SUBGOAL_THEN `!k:num. k <= n ==> ~(poly f (i k) = &0)` ASSUME_TAC THENL
     [GEN_TAC THEN DISCH_TAC THEN FIRST_ASSUM MATCH_MP_TAC THEN
      UNDISCH_TAC `k <= n:num` THEN ARITH_TAC;
      ASM_REWRITE_TAC[]] THEN
    DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
    FIRST_ASSUM(X_CHOOSE_THEN `c:real` STRIP_ASSUME_TAC o SPEC `n:num`) THEN
    MP_TAC(SPECL [`l:(real list)list`; `f:real list`] STURM_COMPONENT) THEN
    DISCH_THEN(MP_TAC o SPECL [`(i:num->real) n`; `(i:num->real)(SUC n)`]) THEN
    DISCH_THEN(MP_TAC o SPEC `c:real`) THEN ASM_REWRITE_TAC[] THEN
    SUBGOAL_THEN `~(poly f (i n) = &0) /\ ~(poly f (i (SUC n)) = &0)`
    ASSUME_TAC THENL
     [UNDISCH_TAC `!k:num. k <= n ==> ~(poly f (i k) = &0)` THEN
      DISCH_THEN(K ALL_TAC) THEN CONJ_TAC THEN
      FIRST_ASSUM MATCH_MP_TAC THEN ARITH_TAC;
      ASM_REWRITE_TAC[]] THEN
    ASM_CASES_TAC `poly f c = &0` THEN ASM_REWRITE_TAC[] THENL
     [SUBGOAL_THEN
       `FINITE {x | i n <= x /\ x <= i (SUC n) /\ (poly f x = &0)} /\
        (CARD {x | i n <= x /\ x <= i (SUC n) /\ (poly f x = &0)} = 1)`
      STRIP_ASSUME_TAC THENL
       [SUBGOAL_THEN `{x | i n <= x /\ x <= i (SUC n) /\ (poly f x = &0)} =
                      {c}`
        SUBST_ALL_TAC THENL
         [REWRITE_TAC[EXTENSION; IN_ELIM_THM; IN_INSERT; NOT_IN_EMPTY] THEN
          X_GEN_TAC `x:real` THEN EQ_TAC THEN STRIP_TAC THEN
          ASM_REWRITE_TAC[] THEN FIRST_ASSUM MATCH_MP_TAC THEN
          ASM_REWRITE_TAC[EX];
          REWRITE_TAC[FINITE_INSERT; FINITE_RULES] THEN
          MP_TAC(ISPEC `c:real` (CONJUNCT2 CARD_CLAUSES)) THEN
          DISCH_THEN(MP_TAC o SPEC `EMPTY:real->bool`) THEN
          REWRITE_TAC[FINITE_RULES; NOT_IN_EMPTY] THEN
          DISCH_THEN SUBST1_TAC THEN REWRITE_TAC[CARD_CLAUSES] THEN
          REWRITE_TAC[ARITH]];
        ALL_TAC] THEN
      FIRST_ASSUM(MP_TAC o MATCH_MP lemma) THEN ASM_REWRITE_TAC[] THEN
      DISCH_THEN SUBST_ALL_TAC THEN DISCH_THEN SUBST1_TAC THEN
      DISCH_THEN(SUBST1_TAC o SYM) THEN
      REWRITE_TAC[GSYM ADD1; ADD_CLAUSES; SUC_INJ] THEN
      MATCH_MP_TAC FINITE_UNION_IMP THEN ASM_REWRITE_TAC[];
      SUBGOAL_THEN
       `FINITE {x | i n <= x /\ x <= i (SUC n) /\ (poly f x = &0)} /\
        (CARD {x | i n <= x /\ x <= i (SUC n) /\ (poly f x = &0)} = 0)`
      STRIP_ASSUME_TAC THENL
       [SUBGOAL_THEN `{x | i n <= x /\ x <= i (SUC n) /\ (poly f x = &0)} =
                      EMPTY`
        SUBST_ALL_TAC THENL
         [REWRITE_TAC[EXTENSION; IN_ELIM_THM; IN_INSERT; NOT_IN_EMPTY] THEN
          X_GEN_TAC `x:real` THEN STRIP_TAC THEN
          UNDISCH_TAC `~(poly f c = &0)` THEN
          SUBGOAL_THEN `x:real = c` (fun th -> ASM_REWRITE_TAC[SYM th]) THEN
          FIRST_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[EX];
          REWRITE_TAC[FINITE_RULES; CARD_CLAUSES]]; ALL_TAC] THEN
      FIRST_ASSUM(MP_TAC o MATCH_MP lemma) THEN ASM_REWRITE_TAC[] THEN
      DISCH_THEN SUBST_ALL_TAC THEN DISCH_THEN SUBST1_TAC THEN
      DISCH_THEN(SUBST1_TAC o SYM) THEN
      REWRITE_TAC[GSYM ADD1; ADD_CLAUSES; SUC_INJ] THEN
      MATCH_MP_TAC FINITE_UNION_IMP THEN ASM_REWRITE_TAC[]]]);;

(* ------------------------------------------------------------------------- *)
(* We just need to show that things in Sturm sequence are nontrivial.        *)
(* ------------------------------------------------------------------------- *)

let STURM_NONZERO_LEMMA = prove
 (`!l f f'. ~(poly f = poly []) /\ STURM f f' l
            ==> ~(poly f' = poly [])`,
  LIST_INDUCT_TAC THEN REWRITE_TAC[STURM] THENL
   [REPEAT GEN_TAC THEN CONV_TAC CONTRAPOS_CONV THEN
    REWRITE_TAC[DE_MORGAN_THM] THEN
    DISCH_TAC THEN ASM_CASES_TAC `f' divides f` THEN
    ASM_REWRITE_TAC[] THEN
    UNDISCH_TAC `f' divides f` THEN REWRITE_TAC[divides] THEN
    DISCH_THEN(CHOOSE_THEN SUBST1_TAC) THEN
    ASM_REWRITE_TAC[FUN_EQ_THM; POLY_MUL; REAL_MUL_LZERO; poly];
    REPEAT GEN_TAC THEN CONV_TAC CONTRAPOS_CONV THEN
    REWRITE_TAC[] THEN DISCH_TAC THEN ASM_REWRITE_TAC[] THEN
    DISCH_THEN(MP_TAC o el 2 o CONJUNCTS) THEN
    REWRITE_TAC[NOT_LT] THEN
    FIRST_ASSUM(SUBST1_TAC o MATCH_MP DEGREE_ZERO) THEN
    REWRITE_TAC[LE_0]]);;

let STURM_NONZERO = prove
 (`!l f f'. ~(poly f = poly []) /\ STURM f f' l
            ==> ALL (\p. ~(poly p = poly [])) (CONS f' l)`,
  LIST_INDUCT_TAC THEN REWRITE_TAC[STURM] THENL
   [REWRITE_TAC[ALL] THEN REPEAT GEN_TAC THEN STRIP_TAC THEN
    MATCH_MP_TAC STURM_NONZERO_LEMMA THEN
    EXISTS_TAC `[]:(real list)list` THEN
    EXISTS_TAC `f:real list` THEN ASM_REWRITE_TAC[STURM];
    REPEAT STRIP_TAC THEN ONCE_REWRITE_TAC[ALL] THEN
    ASM_REWRITE_TAC[] THEN
    SUBGOAL_THEN `~(poly f' = poly [])` ASSUME_TAC THENL
     [DISCH_THEN(SUBST_ALL_TAC o MATCH_MP DEGREE_ZERO) THEN
      UNDISCH_TAC `degree h < 0` THEN REWRITE_TAC[LT];
      ASM_REWRITE_TAC[] THEN FIRST_ASSUM MATCH_MP_TAC THEN
      EXISTS_TAC `f':real list` THEN ASM_REWRITE_TAC[]]]);;

(* ------------------------------------------------------------------------- *)
(* And finally...                                                            *)
(* ------------------------------------------------------------------------- *)

let STURM_STRONG = prove
 (`!f a b l.
    a <= b /\
    ~(poly f a = &0) /\
    ~(poly f b = &0) /\
    rsquarefree f /\
    STURM f (diff f) l
    ==> FINITE {x | a <= x /\ x <= b /\ (poly f x = &0)} /\
        (variation
           (MAP (\p. poly p b) (CONS f (CONS (diff f) l))) +
         CARD {x | a <= x /\ x <= b /\ (poly f x = &0)} =
         variation
           (MAP (\p. poly p a) (CONS f (CONS (diff f) l))))`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN
  SUBGOAL_THEN `ALL (\p. ~(poly p = poly [])) (CONS (diff f) l)`
  ASSUME_TAC THENL
   [MATCH_MP_TAC STURM_NONZERO THEN
    EXISTS_TAC `f:real list` THEN ASM_REWRITE_TAC[] THEN
    DISCH_THEN SUBST_ALL_TAC THEN
    UNDISCH_TAC `~(poly [] a = &0)` THEN REWRITE_TAC[poly];
    MP_TAC(SPECL [`f:real list`; `CONS (diff f) l`; `a:real`; `b:real`]
             POLYS_INTERVAL_SEPARATION) THEN
    ASM_REWRITE_TAC[]] THEN
  DISCH_THEN(X_CHOOSE_THEN `i:num->real` MP_TAC) THEN
  DISCH_THEN(X_CHOOSE_THEN `N:num` MP_TAC) THEN
  DISCH_THEN(CONJUNCTS_THEN2 (SUBST_ALL_TAC o SYM) MP_TAC) THEN
  DISCH_THEN(CONJUNCTS_THEN2 (SUBST_ALL_TAC o SYM) MP_TAC) THEN
  STRIP_TAC THEN MATCH_MP_TAC STURM_LEMMA THEN
  ASM_REWRITE_TAC[]);;

let STURM_THM = prove
 (`!f a b l.
    a <= b /\
    ~(poly f a = &0) /\
    ~(poly f b = &0) /\
    rsquarefree f /\
    STURM f (diff f) l
    ==> {x | a <= x /\ x <= b /\ (poly f x = &0)}
        HAS_SIZE
        (variation
           (MAP (\p. poly p a) (CONS f (CONS (diff f) l))) -
         variation
           (MAP (\p. poly p b) (CONS f (CONS (diff f) l))))`,
  REPEAT GEN_TAC THEN
  DISCH_THEN(STRIP_ASSUME_TAC o MATCH_MP STURM_STRONG) THEN
  ASM_REWRITE_TAC[HAS_SIZE] THEN
  UNDISCH_TAC
    `variation (MAP (\p. poly p b) (CONS f (CONS (diff f) l))) +
       CARD {x | a <= x /\ x <= b /\ (poly f x = &0)} =
       variation (MAP (\p. poly p a) (CONS f (CONS (diff f) l)))` THEN
  ARITH_TAC);;

(* ------------------------------------------------------------------------- *)
(* Show that what we get at the end of the Sturm sequence is a GCD.          *)
(* ------------------------------------------------------------------------- *)

let STURM_GCD = prove
 (`!l k f f'.
        STURM f f' (CONS k l)
        ==> ?q e r s. (poly f = poly (q ** LAST (CONS k l))) /\
                      (poly f' = poly (e ** LAST (CONS k l))) /\
                      (poly (LAST (CONS k l)) =
                                poly (r ** f ++ s ** f'))`,
  LIST_INDUCT_TAC THENL
   [REPEAT GEN_TAC THEN REWRITE_TAC[STURM; LAST] THEN
    DISCH_THEN(CONJUNCTS_THEN2 MP_TAC STRIP_ASSUME_TAC) THEN
    DISCH_THEN(X_CHOOSE_THEN `c:real` STRIP_ASSUME_TAC) THEN
    UNDISCH_TAC `k divides f'` THEN REWRITE_TAC[divides] THEN
    DISCH_THEN(X_CHOOSE_TAC `e:real list`) THEN
    UNDISCH_TAC `f' divides f ++ c ## k` THEN REWRITE_TAC[divides] THEN
    DISCH_THEN(X_CHOOSE_TAC `g:real list`) THEN
    EXISTS_TAC `e ** g ++ [--c]` THEN
    EXISTS_TAC `e:real list` THEN
    EXISTS_TAC `[--(inv(c))]` THEN
    EXISTS_TAC `inv(c) ## g` THEN
    SUBGOAL_THEN `poly f = poly ((e ** g ++ [-- c]) ** k)` ASSUME_TAC THENL
     [REWRITE_TAC[FUN_EQ_THM] THEN X_GEN_TAC `x:real` THEN
      UNDISCH_TAC `poly (f ++ c ## k) = poly (f' ** g)` THEN
      DISCH_THEN(MP_TAC o SPEC `x:real` o ONCE_REWRITE_RULE[FUN_EQ_THM]) THEN
      REWRITE_TAC[POLY_ADD; POLY_MUL; POLY_CMUL] THEN
      ASM_REWRITE_TAC[poly; REAL_MUL_RZERO; REAL_ADD_RID; POLY_MUL] THEN
      REAL_ARITH_TAC;
      ASM_REWRITE_TAC[]] THEN
    ASM_REWRITE_TAC[FUN_EQ_THM; POLY_MUL; REAL_MUL_AC] THEN
    X_GEN_TAC `x:real` THEN
    ASM_REWRITE_TAC[POLY_MUL; POLY_ADD; POLY_CMUL] THEN
    ASM_REWRITE_TAC[poly; REAL_MUL_RZERO; REAL_ADD_RID; POLY_MUL] THEN
    REWRITE_TAC[REAL_MUL_RNEG; REAL_ADD_RDISTRIB; REAL_MUL_AC] THEN
    REWRITE_TAC[REAL_ARITH `(--a + b) + a = b`] THEN
    REWRITE_TAC[REAL_NEG_NEG; REAL_MUL_AC] THEN
    ONCE_REWRITE_TAC[REAL_MUL_SYM] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    SUBGOAL_THEN `inv(c) * c = &1`
    (fun th -> REWRITE_TAC[th; REAL_MUL_RID]) THEN
    MATCH_MP_TAC REAL_MUL_LINV THEN
    UNDISCH_TAC `&0 < c` THEN REAL_ARITH_TAC;

    REPEAT GEN_TAC THEN ONCE_REWRITE_TAC[LAST] THEN
    ONCE_REWRITE_TAC[STURM] THEN REWRITE_TAC[NOT_CONS_NIL] THEN
    DISCH_THEN(CONJUNCTS_THEN MP_TAC) THEN
    DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
    DISCH_THEN(ANTE_RES_THEN MP_TAC) THEN STRIP_TAC THEN
    DISCH_THEN(X_CHOOSE_THEN `c:real` MP_TAC) THEN
    DISCH_THEN(CONJUNCTS_THEN2 ASSUME_TAC MP_TAC) THEN
    REWRITE_TAC[divides] THEN
    DISCH_THEN(X_CHOOSE_THEN `u:real list` ASSUME_TAC) THEN
    EXISTS_TAC `q ** u ++ (--c) ## e` THEN
    EXISTS_TAC `q:real list` THEN
    EXISTS_TAC `--(inv c) ## s` THEN
    EXISTS_TAC `r ++ inv c ## s ** u` THEN
    ASM_REWRITE_TAC[] THEN
    SUBGOAL_THEN `poly f = poly ((q ** u ++ -- c ## e) ** LAST (CONS h t))`
    ASSUME_TAC THENL
     [UNDISCH_TAC `poly (LAST (CONS h t)) = poly (r ** f' ++ s ** k)` THEN
      DISCH_THEN(K ALL_TAC) THEN
      REWRITE_TAC[FUN_EQ_THM] THEN X_GEN_TAC `x:real` THEN
      UNDISCH_TAC `poly (f ++ c ## k) = poly (f' ** u)` THEN
      DISCH_THEN(MP_TAC o SPEC `x:real` o ONCE_REWRITE_RULE[FUN_EQ_THM]) THEN
      REWRITE_TAC[POLY_ADD; POLY_MUL; POLY_CMUL] THEN
      ASM_REWRITE_TAC[poly; REAL_MUL_RZERO; REAL_ADD_RID; POLY_MUL] THEN
      REAL_ARITH_TAC;
    ASM_REWRITE_TAC[]] THEN
    UNDISCH_TAC `poly (LAST (CONS h t)) = poly (r ** f' ++ s ** k)` THEN
    DISCH_THEN(K ALL_TAC) THEN
    REWRITE_TAC[FUN_EQ_THM] THEN X_GEN_TAC `x:real` THEN
    ASM_REWRITE_TAC[POLY_ADD; POLY_MUL; POLY_CMUL; POLY_NEG] THEN
    REWRITE_TAC[REAL_ADD_LDISTRIB; REAL_ADD_RDISTRIB] THEN
    REWRITE_TAC[REAL_MUL_LNEG; REAL_MUL_RNEG; REAL_MUL_AC] THEN
    REWRITE_TAC[REAL_NEG_NEG] THEN
    REWRITE_TAC[REAL_ARITH `(--a + b) + c + a = b + c`] THEN
    REWRITE_TAC[REAL_ARITH `(a + b = c + a) <=> (b = c)`] THEN
    GEN_REWRITE_TAC RAND_CONV [REAL_MUL_SYM] THEN
    REWRITE_TAC[GSYM REAL_MUL_ASSOC] THEN
    SUBGOAL_THEN `inv(c) * c = &1`
    (fun th -> REWRITE_TAC[th; REAL_MUL_RID]) THEN
    MATCH_MP_TAC REAL_MUL_LINV THEN
    UNDISCH_TAC `&0 < c` THEN REAL_ARITH_TAC]);;

(* ------------------------------------------------------------------------- *)
(* Hence avoid a separate check for squarefreeness.                          *)
(* ------------------------------------------------------------------------- *)

let STURM_THEOREM = prove
 (`!f a b l d.
        a <= b /\
        ~(poly f a = &0) /\
        ~(poly f b = &0) /\
        ~(poly (diff f) = poly []) /\
        STURM f (diff f) l /\
        ~(l = []) /\
        (LAST l = [d]) /\
        ~(d = &0)
        ==> {x | a <= x /\ x <= b /\ (poly f x = &0)} HAS_SIZE
            (variation (MAP (\p. poly p a) (CONS f (CONS (diff f) l))) -
             variation (MAP (\p. poly p b) (CONS f (CONS (diff f) l))))`,
  REPEAT STRIP_TAC THEN MATCH_MP_TAC STURM_THM THEN
  ASM_REWRITE_TAC[] THEN
  UNDISCH_TAC `LAST l = [d:real]` THEN
  UNDISCH_TAC `STURM f (diff f) l` THEN
  UNDISCH_TAC `~(l:(real list)list = [])` THEN
  SPEC_TAC(`l:(real list)list`,`l:(real list)list`) THEN
  LIST_INDUCT_TAC THEN REWRITE_TAC[NOT_CONS_NIL] THEN
  DISCH_THEN(STRIP_ASSUME_TAC o MATCH_MP STURM_GCD) THEN
  DISCH_THEN SUBST_ALL_TAC THEN
  MP_TAC(SPECL [`f:real list`; `q:real list`] POLY_SQUAREFREE_DECOMP) THEN
  DISCH_THEN(MP_TAC o SPECL [`[d:real]`; `e:real list`]) THEN
  DISCH_THEN(MP_TAC o SPECL [`r:real list`; `s:real list`]) THEN
  UNDISCH_TAC `~(poly (diff f) = poly [])` THEN
  DISCH_THEN(fun th -> REWRITE_TAC[th]) THEN
  ASM_REWRITE_TAC[] THEN DISCH_THEN(MP_TAC o CONJUNCT1) THEN
  REWRITE_TAC[rsquarefree] THEN
  SUBGOAL_THEN `(poly q = poly []) <=> (poly (q ** [d]) = poly [])`
  ASSUME_TAC THENL
   [ASM_REWRITE_TAC[poly; REAL_ENTIRE; FUN_EQ_THM; POLY_MUL] THEN
    ASM_REWRITE_TAC[REAL_MUL_RZERO; REAL_ADD_RID];
    ASM_REWRITE_TAC[]] THEN
  MATCH_MP_TAC(TAUT `(p ==> (q <=> r)) ==> (p /\ q ==> p /\ r)`) THEN
  STRIP_TAC THEN
  SUBGOAL_THEN `!a. order a f = order a q`
  (fun th -> REWRITE_TAC[th]) THEN
  X_GEN_TAC `c:real` THEN MATCH_MP_TAC EQ_TRANS THEN
  EXISTS_TAC `order c (q ** [d])` THEN CONJ_TAC THENL
   [MATCH_MP_TAC ORDER_POLY THEN ASM_REWRITE_TAC[];
    FIRST_ASSUM(fun th -> REWRITE_TAC[MATCH_MP ORDER_MUL th])] THEN
  SUBGOAL_THEN `order c [d] = 0` (fun th -> REWRITE_TAC[th; ADD_CLAUSES]) THEN
  MP_TAC(SPECL [`[d:real]`; `c:real`] ORDER_ROOT) THEN
  ASM_REWRITE_TAC[poly; REAL_MUL_RZERO; REAL_ADD_RID] THEN
  CONV_TAC TAUT);;

(* ------------------------------------------------------------------------- *)
(* A conversion for calculating variations.                                  *)
(* ------------------------------------------------------------------------- *)

let VARIATION_CONV =
  let variation_conv = GEN_REWRITE_CONV I [variation]
  and cond_conv = GEN_REWRITE_CONV I [COND_CLAUSES]
  and sig_conv = GEN_REWRITE_CONV I [SIGN_LEMMA5]
  and varrec0_conv = GEN_REWRITE_CONV I [CONJUNCT1 varrec]
  and varrec1_conv = GEN_REWRITE_CONV I [CONJUNCT2 varrec] in
  let rec VARREC_CONV tm =
    try varrec0_conv tm with Failure _ ->
    let th1 = (varrec1_conv THENC
               RATOR_CONV(LAND_CONV(sig_conv THENC REAL_RAT_REDUCE_CONV)) THENC
               cond_conv) tm in
    let tm1 = rand(concl th1) in
    if is_cond tm1 then
      let th2 = (RATOR_CONV(LAND_CONV REAL_RAT_REDUCE_CONV) THENC
                 cond_conv THENC
                 VARREC_CONV) tm1 in
      TRANS th1 th2
    else
      TRANS th1 (RAND_CONV VARREC_CONV tm1) in
  variation_conv THENC VARREC_CONV THENC
  DEPTH_CONV NUM_SUC_CONV;;
