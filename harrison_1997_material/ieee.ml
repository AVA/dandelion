(* ========================================================================= *)
(* Formalization of IEEE-754 Standard for binary floating-point arithmetic.  *)
(* ========================================================================= *)

prioritize_num();;

(* ------------------------------------------------------------------------- *)
(* Derived parameters for floating point formats.                            *)
(* ------------------------------------------------------------------------- *)

let expwidth = new_definition
  `(expwidth:num#num->num) (ew,fw) = ew`;;

let fracwidth = new_definition
  `(fracwidth:num#num->num) (ew,fw) = fw`;;

let wordlength = new_definition
  `wordlength(X) = expwidth(X) + fracwidth(X) + 1`;;

let emax = new_definition
  `emax(X) = 2 EXP expwidth(X) - 1`;;

let bias = new_definition
  `bias(X) = 2 EXP (expwidth(X) - 1) - 1`;;

(* ------------------------------------------------------------------------- *)
(* Predicates for the four IEEE formats.                                     *)
(* ------------------------------------------------------------------------- *)

let is_single = new_definition
  `is_single(X) <=> (expwidth(X) = 8) /\ (wordlength(X) = 32)`;;

let is_double = new_definition
  `is_double(X) <=> (expwidth(X) = 11) /\ (wordlength(X) = 64)`;;

let is_single_extended = new_definition
  `is_single_extended(X) <=> expwidth(X) >= 11 /\ wordlength(X) >= 43`;;

let is_double_extended = new_definition
  `is_double_extended(X) <=> expwidth(X) >= 15 /\ wordlength(X) >= 79`;;

(* ------------------------------------------------------------------------- *)
(* Extractors for fields.                                                    *)
(* ------------------------------------------------------------------------- *)

let sign = new_definition
  `(sign:num#num#num->num) (s,e,f) = s`;;

let exponent = new_definition
  `(exponent:num#num#num->num) (s,e,f) = e`;;

let fraction = new_definition
  `(fraction:num#num#num->num) (s,e,f) = f`;;

(* ------------------------------------------------------------------------- *)
(* Partition of numbers into disjoint classes.                               *)
(* ------------------------------------------------------------------------- *)

let is_nan = new_definition
  `is_nan(X) a <=> (exponent a = emax(X)) /\ ~(fraction a = 0)`;;

let is_infinity = new_definition
  `is_infinity(X) a <=> (exponent a = emax(X)) /\ (fraction a = 0)`;;

let is_normal = new_definition
  `is_normal(X) a <=> 0 < exponent a /\ exponent a < emax(X)`;;

let is_denormal = new_definition
  `is_denormal(X:num#num) a <=> (exponent a = 0) /\ ~(fraction a = 0)`;;

let is_zero = new_definition
  `is_zero(X:num#num) a <=> (exponent a = 0) /\ (fraction a = 0)`;;

(* ------------------------------------------------------------------------- *)
(* Other useful predicates.                                                  *)
(* ------------------------------------------------------------------------- *)

let is_valid = new_definition
  `is_valid(X) (s,e,f) <=>
        s < 2 /\ e < 2 EXP expwidth(X) /\ f < 2 EXP fracwidth(X)`;;

let is_finite = new_definition
  `is_finite(X) a <=> is_valid(X) a /\
                      (is_normal(X) a \/ is_denormal(X) a \/ is_zero(X) a)`;;

(* ------------------------------------------------------------------------- *)
(* Some special values.                                                      *)
(* ------------------------------------------------------------------------- *)

let plus_infinity = new_definition
  `plus_infinity(X) = (0,emax(X),0)`;;

let minus_infinity = new_definition
  `minus_infinity(X) = (1,emax(X),0)`;;

let plus_zero = new_definition
  `plus_zero(X:num#num) = (0,0,0)`;;

let minus_zero = new_definition
  `minus_zero(X:num#num) = (1,0,0)`;;

let topfloat = new_definition
  `topfloat(X) = (0,emax(X) - 1,2 EXP fracwidth(X) - 1)`;;

let bottomfloat = new_definition
  `bottomfloat(X) = (1,emax(X) - 1,2 EXP fracwidth(X) - 1)`;;

(* ------------------------------------------------------------------------- *)
(* Negation operation on floating point values.                              *)
(* ------------------------------------------------------------------------- *)

let minus = new_definition
  `minus(X) a = (1 - sign(a),exponent(a),fraction(a))`;;

(* ------------------------------------------------------------------------- *)
(* Concrete encodings (at least valid for single and double).                *)
(* ------------------------------------------------------------------------- *)

let encoding = new_definition
  `encoding(X) (s,e,f) =
        s * 2 EXP (wordlength(X) - 1) + e * 2 EXP fracwidth(X) + f`;;

(* ------------------------------------------------------------------------- *)
(* Real number valuations.                                                   *)
(* ------------------------------------------------------------------------- *)

prioritize_real();;

let valof = new_definition
  `valof(X) (s,e,f) =
         if e = 0
         then --(&1) pow s * (&2 / &2 pow bias(X)) *
              (&f / &2 pow fracwidth(X))
         else --(&1) pow s * (&2 pow e / &2 pow bias(X)) *
              (&1 + &f / &2 pow fracwidth(X))`;;

(* ------------------------------------------------------------------------- *)
(* A few handy values.                                                       *)
(* ------------------------------------------------------------------------- *)

let largest = new_definition
  `largest(X) = (&2 pow (emax(X) - 1) / &2 pow bias(X)) *
                (&2 - inv(&2 pow fracwidth(X)))`;;

let threshold = new_definition
  `threshold(X) = (&2 pow (emax(X) - 1) / &2 pow bias(X)) *
                  (&2 - inv(&2 pow SUC(fracwidth(X))))`;;

let ulp = new_definition
  `ulp(X) a = valof(X) (0,exponent(a),1) - valof(X) (0,exponent(a),0)`;;

(* ------------------------------------------------------------------------- *)
(* Enumerated type for rounding modes.                                       *)
(* ------------------------------------------------------------------------- *)

let roundmode_INDUCT,roundmode_RECURSION = define_type
  "roundmode = To_nearest | To_zero | To_pinfinity | To_ninfinity";;

(* ------------------------------------------------------------------------- *)
(* Characterization of best approximation from a set of abstract values.     *)
(* ------------------------------------------------------------------------- *)

let is_closest = new_definition
  `is_closest v s x a <=>
        a IN s /\
        !b. b IN s ==> abs(v(a) - x) <= abs(v(b) - x)`;;

(* ------------------------------------------------------------------------- *)
(* Best approximation with a deciding preference for multiple possibilities. *)
(* ------------------------------------------------------------------------- *)

let closest = new_definition
  `closest v p s x =
        @a. is_closest v s x a /\
            ((?b. is_closest v s x b /\ p(b)) ==> p(a))`;;

(* ------------------------------------------------------------------------- *)
(* Rounding to floating point formats.                                       *)
(* ------------------------------------------------------------------------- *)

let round = new_recursive_definition roundmode_RECURSION
  `(round(X) To_nearest x =
        if x <= --(threshold(X)) then minus_infinity(X)
        else if x >= threshold(X) then plus_infinity(X)
        else closest (valof(X)) (\a. EVEN(fraction(a)))
                { a | is_finite(X) a} x) /\

   (round(X) To_zero x =
        if x < --(largest(X)) then bottomfloat(X)
        else if x > largest(X) then topfloat(X)
        else closest (valof(X)) (\x. T)
                { a | is_finite(X) a /\ abs(valof(X) a) <= abs(x) } x) /\

   (round(X) To_pinfinity x =
        if x < --(largest(X)) then bottomfloat(X)
        else if x > largest(X) then plus_infinity(X)
        else closest (valof(X)) (\x. T)
                { a | is_finite(X) a /\ valof(X) a >= x } x) /\

   (round(X) To_ninfinity x =
        if x < --(largest(X)) then minus_infinity(X)
        else if x > largest(X) then topfloat(X)
        else closest (valof(X)) (\x. T)
                { a | is_finite(X) a /\ valof(X) a <= x } x)`;;

(* ------------------------------------------------------------------------- *)
(* Rounding to integer values in floating point format.                      *)
(* ------------------------------------------------------------------------- *)

let is_integral = new_definition
  `is_integral(X) a <=> is_finite(X) a /\ ?n. abs(valof(X) a) = &n`;;

let intround = new_recursive_definition roundmode_RECURSION
  `(intround(X) To_nearest x =
        if x <= --(threshold(X)) then minus_infinity(X)
        else if x >= threshold(X) then plus_infinity(X)
        else closest (valof(X)) (\a. ?n. EVEN n /\ (abs(valof(X) a) = &n))
                { a | is_integral(X) a} x) /\

   (intround(X) To_zero x =
        if x < --(largest(X)) then bottomfloat(X)
        else if x > largest(X) then topfloat(X)
        else closest (valof(X)) (\x. T)
                { a | is_integral(X) a /\ abs(valof(X) a) <= abs(x) } x) /\

   (intround(X) To_pinfinity x =
        if x < --(largest(X)) then bottomfloat(X)
        else if x > largest(X) then plus_infinity(X)
        else closest (valof(X)) (\x. T)
                { a | is_integral(X) a /\ valof(X) a >= x } x) /\

   (intround(X) To_ninfinity x =
        if x < --(largest(X)) then minus_infinity(X)
        else if x > largest(X) then topfloat(X)
        else closest (valof(X)) (\x. T)
                { a | is_integral(X) a /\ valof(X) a <= x } x)`;;

(* ------------------------------------------------------------------------- *)
(* A hack for our (non-standard) treatment of NaNs.                          *)
(* ------------------------------------------------------------------------- *)

let some_nan = new_definition
  `some_nan(X) = @a. is_nan(X) a`;;

(* ------------------------------------------------------------------------- *)
(* Coercion for signs of zero results.                                       *)
(* ------------------------------------------------------------------------- *)

let zerosign = new_definition
  `zerosign(X) s a = if is_zero(X) a then
                        if s = 0 then plus_zero(X) else minus_zero(X)
                     else a`;;

(* ------------------------------------------------------------------------- *)
(* A remainder operation different from the core one.                        *)
(* ------------------------------------------------------------------------- *)

parse_as_infix("irem",(22,"left"));;

let irem = new_definition
 `x irem y =
    let n = closest I (\x. ?n. EVEN(n) /\ (abs(x) = &n))
              { x | ?n. abs(x) = &n } (x / y) in
    x - n * y`;;

(* ------------------------------------------------------------------------- *)
(* Definitions of the arithmetic operations.                                 *)
(* ------------------------------------------------------------------------- *)

let fintrnd = new_definition
  `fintrnd(X) m a =
        if is_nan(X) a then some_nan(X)
        else if is_infinity(X) a then a
        else zerosign(X) (sign(a)) (intround(X) m (valof(X) a))`;;

let fadd = new_definition
  `fadd(X) m a b =
        if is_nan(X) a \/ is_nan(X) b \/
           (is_infinity(X) a /\ is_infinity(X) b /\ ~(sign(a) = sign(b)))
        then some_nan(X)
        else if is_infinity(X) a then a
        else if is_infinity(X) b then b
        else zerosign(X) (if is_zero(X) a /\ is_zero(X) b /\
                             (sign(a) = sign(b)) then sign(a)
                          else if m = To_ninfinity then 1 else 0)
               (round(X) m (valof(X) a + valof(X) b))`;;

let fsub = new_definition
  `fsub(X) m a b =
        if is_nan(X) a \/ is_nan(X) b \/
           (is_infinity(X) a /\ is_infinity(X) b /\ (sign(a) = sign(b)))
        then some_nan(X)
        else if is_infinity(X) a then a
        else if is_infinity(X) b then minus(X) b
        else zerosign(X) (if is_zero(X) a /\ is_zero(X) b /\
                             ~(sign(a) = sign(b)) then sign(a)
                          else if m = To_ninfinity then 1 else 0)
               (round(X) m (valof(X) a - valof(X) b))`;;

let fmul = new_definition
  `fmul(X) m a b =
        if is_nan(X) a \/
           is_nan(X) b \/
           is_zero(X) a /\ is_infinity(X) b \/
           is_infinity(X) a /\ is_zero(X) b
        then some_nan(X)
        else if is_infinity(X) a \/ is_infinity(X) b then
           if sign(a) = sign(b) then plus_infinity(X) else minus_infinity(X)
        else zerosign(X) (if sign(a) = sign(b) then 0 else 1)
               (round(X) m (valof(X) a * valof(X) b))`;;

let fdiv = new_definition
  `fdiv(X) m a b =
        if is_nan(X) a \/
           is_nan(X) b \/
           is_zero(X) a /\ is_zero(X) b \/
           is_infinity(X) a /\ is_infinity(X) b
        then some_nan(X)
        else if is_infinity(X) a \/ is_zero(X) b then
           if sign(a) = sign(b)
           then plus_infinity(X) else minus_infinity(X)
        else if is_infinity(X) b then
           if sign(a) = sign(b)
           then plus_zero(X) else minus_zero(X)
        else zerosign(X) (if sign(a) = sign(b) then 0 else 1)
               (round(X) m (valof(X) a / valof(X) b))`;;

let fsqrt = new_definition
  `fsqrt(X) m a =
        if is_nan(X) a then some_nan(X)
        else if is_zero(X) a \/
                is_infinity(X) a /\ (sign(a) = 0) then a
        else if sign(a) = 1 then some_nan(X)
        else zerosign(X) (sign(a))
               (round(X) m (sqrt(valof(X) a)))`;;

let frem = new_definition
  `frem(X) m a b =
        if is_nan(X) a \/ is_nan(X) b \/
           is_infinity(X) a \/ is_zero(X) b
        then some_nan(X)
        else if is_infinity(X) b then a
        else zerosign(X) (sign(a))
               (round(X) m (valof(X) a irem valof(X) b))`;;

(* ------------------------------------------------------------------------- *)
(* Negation is specially simple.                                             *)
(* ------------------------------------------------------------------------- *)

let fneg = new_definition
  `fneg(X) m a = (1-sign(a),exponent(a),fraction(a))`;;

(* ------------------------------------------------------------------------- *)
(* Comparison codes.                                                         *)
(* ------------------------------------------------------------------------- *)

let ccode_INDUCTION,ccode_RECURSION = define_type
  "ccode = Gt | Lt | Eq | Un";;

(* ------------------------------------------------------------------------- *)
(* Comparison operations.                                                    *)
(* ------------------------------------------------------------------------- *)

let fcompare = new_definition
  `fcompare(X) a b =
        if is_nan(X) a \/ is_nan(X) b then Un
        else if is_infinity(X) a /\ (sign(a) = 1) then
          if is_infinity(X) b /\ (sign(b) = 1) then Eq else Lt
        else if is_infinity(X) a /\ (sign(a) = 0) then
          if is_infinity(X) b /\ (sign(b) = 0) then Eq else Gt
        else if is_infinity(X) b /\ (sign(b) = 1) then Gt
        else if is_infinity(X) b /\ (sign(b) = 0) then Lt
        else if valof(X) a < valof(X) b then Lt
        else if valof(X) a = valof(X) b then Eq
        else Gt`;;

let flt = new_definition
  `flt(X) a b <=> (fcompare(X) a b = Lt)`;;

let fle = new_definition
  `fle(X) a b <=> (fcompare(X) a b = Lt) \/ (fcompare(X) a b = Eq)`;;

let fgt = new_definition
  `fgt(X) a b <=> (fcompare(X) a b = Gt)`;;

let fge = new_definition
  `fge(X) a b <=> (fcompare(X) a b = Gt) \/ (fcompare(X) a b = Eq)`;;

let feq = new_definition
  `feq(X) a b <=> (fcompare(X) a b = Eq)`;;

(* ------------------------------------------------------------------------- *)
(* Actual float type with round-to-even.                                     *)
(* ------------------------------------------------------------------------- *)

let float_format = new_definition
  `float_format = (8,23)`;;

let float_tybij =
  let th = prove
   (`?a. is_valid(float_format) a`,
    EXISTS_TAC `0,0,0` THEN
    REWRITE_TAC[float_format; is_valid; EXP_LT_0; ARITH]) in
  new_type_definition "float" ("float","defloat") th;;

let VAL = new_definition
  `VAL(a) = valof(float_format) (defloat a)`;;

let FLOAT = new_definition
  `FLOAT(x) = float (round(float_format) To_nearest x)`;;

let SIGN = new_definition
  `SIGN(a) = sign(defloat a)`;;

let EXPONENT = new_definition
  `EXPONENT(a) = exponent(defloat a)`;;

let FRACTION = new_definition
  `FRACTION(a) = fraction(defloat a)`;;

let ULP = new_definition
  `ULP(a) = ulp(float_format) (defloat a)`;;

(* ------------------------------------------------------------------------- *)
(* Lifting of the discriminator functions.                                   *)
(* ------------------------------------------------------------------------- *)

let ISNAN = new_definition
  `ISNAN(a) <=> is_nan(float_format) (defloat a)`;;

let INFINITY = new_definition
  `INFINITY(a) <=> is_infinity(float_format) (defloat a)`;;

let ISNORMAL = new_definition
  `ISNORMAL(a) <=> is_normal(float_format) (defloat a)`;;

let ISDENORMAL = new_definition
  `ISDENORMAL(a) <=> is_denormal(float_format) (defloat a)`;;

let ISZERO = new_definition
  `ISZERO(a) <=> is_zero(float_format) (defloat a)`;;

let ISFINITE = new_definition
  `ISFINITE(a) <=> ISNORMAL(a) \/ ISDENORMAL(a) \/ ISZERO(a)`;;

let ISINTEGRAL = new_definition
  `ISINTEGRAL(a) <=> is_integral(float_format) (defloat a)`;;

(* ------------------------------------------------------------------------- *)
(* Basic operations on floats.                                               *)
(* ------------------------------------------------------------------------- *)

make_overloadable "%" `:A->A->A`;;
make_overloadable "==" `:A->A->bool`;;
make_overloadable "sqrt" `:A->A`;;

overload_interface ("sqrt",`sqrt:real->real`);;

parse_as_infix ("==",(10,"right"));;
parse_as_infix ("%",(22,"left"));;

let prioritize_float() =
  overload_interface ("+",`float_add:float->float->float`);
  overload_interface ("*",`float_mul:float->float->float`);
  overload_interface ("-",`float_sub:float->float->float`);
  overload_interface ("/",`float_div:float->float->float`);
  overload_interface ("%",`float_rem:float->float->float`);
  overload_interface ("<",`float_lt:float->float->bool`);
  overload_interface ("<=",`float_le:float->float->bool`);
  overload_interface (">",`float_gt:float->float->bool`);
  overload_interface (">=",`float_ge:float->float->bool`);
  overload_interface ("==",`float_eq:float->float->bool`);
  overload_interface ("abs",`float_abs:float->float`);
  overload_interface ("--",`float_neg:float->float`);
  overload_interface ("sqrt",`float_sqrt:float->float`);;

prioritize_float();;

let PLUS_ZERO = new_definition
  `PLUS_ZERO = float (plus_zero(float_format))`;;

let MINUS_ZERO = new_definition
  `MINUS_ZERO = float (minus_zero(float_format))`;;

let MINUS_INFINITY = new_definition
  `MINUS_INFINITY = float (minus_infinity(float_format))`;;

let PLUS_INFINITY = new_definition
  `PLUS_INFINITY = float (plus_infinity(float_format))`;;

let add_f = new_definition
  `a + b = float (fadd(float_format) To_nearest (defloat a) (defloat b))`;;

let sub_f = new_definition
  `a - b = float (fsub(float_format) To_nearest (defloat a) (defloat b))`;;

let mul_f = new_definition
  `a * b = float (fmul(float_format) To_nearest (defloat a) (defloat b))`;;

let div_f = new_definition
  `a / b = float (fdiv(float_format) To_nearest (defloat a) (defloat b))`;;

let rem_f = new_definition
  `a % b = float (frem(float_format) To_nearest (defloat a) (defloat b))`;;

let sqrt_f = new_definition
  `sqrt(a) = float (fsqrt(float_format) To_nearest (defloat a))`;;

let ROUNDFLOAT = new_definition
  `ROUNDFLOAT(a) = float (fintrnd(float_format) To_nearest (defloat a))`;;

let lt_f = new_definition
  `a < b <=> flt(float_format) (defloat a) (defloat b)`;;

let le_f = new_definition
  `a <= b <=> fle(float_format) (defloat a) (defloat b)`;;

let gt_f = new_definition
  `a > b <=> fgt(float_format) (defloat a) (defloat b)`;;

let ge_f = new_definition
  `a >= b <=> fge(float_format) (defloat a) (defloat b)`;;

let eq_f = new_definition
  `a == b <=> feq(float_format) (defloat a) (defloat b)`;;

let neg_f = new_definition
  `--a = float(fneg(float_format) To_nearest (defloat a))`;;

let abs_f = new_definition
  `abs(a) = if a >= PLUS_ZERO then a else --a`;;
